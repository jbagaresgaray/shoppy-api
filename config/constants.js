'use strict';

const EVAL_CATEGORY = {
    applicants: "APPLICANTS",
    inReview: "IN-REVIEW",
    inProgress: "IN-PROGRESS",
    completed: "COMPLETED"
};

const EVAL_STATUS = {
    success: "success",
    warning: "warning",
    danger: "danger",
    info: "info"
};

const NOTIFICATION_TYPE = {
    PROMOTIONS: 1,
    SELLER_UPDATES: 2,
    WALLET_UPDATES: 3,
    APP_UPDATES: 4,
    ORDER_UPDATES: 5,
    ACTIVITY: 6,
    RATING_UPDATES: 7
};

const MAX_DAYS_AS_NEW_PRODUCT = 7;

module.exports = {
    EVAL_CATEGORY: EVAL_CATEGORY,
    EVAL_STATUS: EVAL_STATUS,
    NOTIFICATION_TYPE: NOTIFICATION_TYPE,
    MAX_DAYS_AS_NEW_PRODUCT: MAX_DAYS_AS_NEW_PRODUCT
};