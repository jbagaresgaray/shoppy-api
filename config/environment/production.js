/*jshint camelcase: false */

'use strict';

module.exports = {
    env: 'production',
    db_host: process.env.DB_HOST || '',
    db_user: process.env.DB_USER || '',
    db_password: process.env.DB_PASSWORD || '',
    db_name: 'shoppy',
    db_port: 3306,
    port: 3000, // PLEASE DONT REMOVE 'process.env.PORT'
    ip: process.env.IP,
    app_name: 'ShoppyApp',
    api_host_url: process.env.API_HOST_URL || '',
    frontend_host_url: process.env.FRONTEND_HOST_URL || '',
    frontend_portal_host_url: process.env.FRONTEND_HOST_URL || '',
    api_version: process.env.API_VERSION || '/api/1',
    app_version: process.env.APP_VERSION || '/app/1',
    DROPBOX_KEY: process.env.DROPBOX_KEY || '',
    DROPBOX_SECRET: process.env.DROPBOX_KEY || '',
    token_secret: 'shoppyecommerce',
    token_secret_mobile: 'shoppyecommerce-api1',
    sendgrid_api_key: '6ut4fcBxQ721tdoAKE7bOA',
    sendgrid_key: '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',
    sendbird_app_id: '523BB462-ECE0-4C29-8235-92871DA7E480',
    sendbird_app_token: '602de0f4167a18fc97a28b7a9d8adafadd2d56f3',
    sendbird_api_url: 'https://api.sendbird.com',
    ONESIGNAL_APP_ID: '9d13ed09-2c62-4863-af09-a2fcbd4939f6',
    ONESIGNAL_API_KEY: 'MWFmMDM2MjUtMzUxMi00NWU1LWJkNGMtNWFjMmM4ZWViMWQ3',
    ONESIGNAL_USER_KEY: 'YTFlZGY2NTMtODgyOS00Mjk5LWE1MDItZWQ5MjMzY2IzN2E0'
};