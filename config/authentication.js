// jshint unused:false
'use strict';

module.exports = function (app, config, ejwt) {

    // DASHBOARD
    app.use(config.app_version, ejwt({
        secret: config.token_secret
    }).unless({
        path: [
            '/',
            '/docx',
            '/docx-v1',
            '/helpcentre',
            config.app_version + '/sendMail',
            config.app_version + '/shareMail',
            config.app_version + '/forgotpassword',
            config.app_version + '/resetpassword',
            config.app_version + '/authenticate',
            config.app_version + '/country',
            config.app_version + '/testupload',
            config.app_version + '/getallupload',
            config.app_version + '/previewupload/:file',
            // config.app_version + '/push',
            /^\/app\/1\/previewupload\/.*/,
            config.app_version + '/users/:hashkey/verify',
            /^\/app\/1\/users\/.*verify/,
            config.app_version + '/helpcentre',
            config.app_version + '/helpcentre/category',
            /^\/app\/1\/helpcentre\/category/,
            config.app_version + '/helpcentre/:postId',
            /^\/app\/1\/helpcentre\/.*/,
            config.app_version + '/helpcentre/category/:categoryId',
            /^\/api\/1\/helpcentre\/category\/.*/,
            {
                url: config.app_version + '/users',
                methods: ['POST']
            },
            config.app_version + '/officialshop/validate',
            config.app_version + '/officialshop/info',
            config.app_version + '/officialshop/upload/cor',
            config.app_version + '/officialshop/upload/license',
            config.app_version + '/officialshop/upload/logo',
        ]
    }));

    // MOBILE / API
    app.use(config.api_version, ejwt({
        secret: config.token_secret_mobile
    }).unless({
        path: [{
                url: config.api_version + '/register',
                methods: ['POST']
            }, {
                url: config.api_version + '/social/signup',
                methods: ['POST']
            }, {
                url: config.api_version + '/login',
                methods: ['POST']
            }, {
                url: config.api_version + '/social',
                methods: ['POST']
            },
            config.api_version + '/forgot',
            config.api_version + '/reset',
            config.api_version + '/verify/:hashkey/id/:uuid',
            /^\/api\/1\/verify\/.*id\/.*/,
            {
                url: config.api_version + '/category',
                methods: ['GET']
            },
            /^\/api\/1\/category\/.*products/,
            config.api_version + '/category/:categoryId/products',
            {
                url: config.api_version + '/brand',
                methods: ['GET']
            },
            /^\/api\/1\/brand\/.*products/,
            config.api_version + '/brand/:brandId/products',
            {
                url: config.api_version + '/banks',
                methods: ['GET']
            },
            {
                url: config.api_version + '/country',
                methods: ['GET']
            },
            {
                url: config.api_version + '/currency',
                methods: ['GET']
            },
            {
                url: config.api_version + '/timezone',
                methods: ['GET']
            },
            {
                url: config.api_version + '/officialshop',
                methods: ['GET']
            },
            /^\/api\/1\/category\/.*subcategory/,
            config.api_version + '/category/:categoryId/subcategory',
            /^\/api\/1\/category\/.*subcategory\/.*products/,
            config.api_version + '/category/:categoryId/subcategory/:subId/products',
            {
                url: config.api_version + '/mobile/banner',
                methods: ['GET']
            },
            {
                url: config.api_version + '/shipping',
                methods: ['GET']
            },
            {
                url: config.api_version + '/products',
                methods: ['GET']
            },
            {
                url: config.api_version + '/shop',
                methods: ['GET']
            },
            /^\/api\/1\/products\/.*/,
            {
                url: config.api_version + '/products/:uuid',
                methods: ['GET']
            },
            /^\/api\/1\/products\/.*more/,
            {
                url: config.api_version + '/products/:uuid/more',
                methods: ['GET']
            },
            /^\/api\/1\/shop\/.*/,
            {
                url: config.api_version + '/shop/:uuid',
                methods: ['GET']
            },
            config.api_version + '/shop/:uuid/toppicks/active',
            /^\/api\/1\/shop\/.*toppicks\/active/,
            /^\/api\/1\/shop\/.*vouchers/,
            {
                url: config.api_version + '/shop/:uuid/vouchers',
                methods: ['GET']
            },
            /^\/api\/1\/products\/.*rating/,
            {
                url: config.api_version + 'products/:uuid/rating',
                methods: ['GET']
            },
        ]
    }));

    app.use(function (err, req, res, next) {
        console.info('PRODUCTION error: ', err);
        if (err.name === 'UnauthorizedError') {
            res.status(401).json({
                result: 'UnauthorizedError',
                success: false,
                msg: err.inner.message
            });
        }
    });

    if (app.get('env') === 'development' || app.get('env') === 'staging') {
        app.use(function (err, req, res, next) {
            console.info('DEVELOPMENT / STAGING error: ', err);
            if (err.name === 'UnauthorizedError') {
                res.status(401).json({
                    result: 'UnauthorizedError',
                    success: false,
                    msg: err.inner.message
                });
            }
        });
    }

};