/*jshint camelcase: false */

'use strict';

let express = require('express');
let morgan = require('morgan');
let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
let methodOverride = require('method-override');
let middleware = require('../app/utils/middleware');;
let flash = require('connect-flash');
let expressValidator = require('express-validator');
let path = require('path');

module.exports = function(app, config, passport, ejwt) {
    var port = process.env.PORT || config.port || 5000;

    app.use('/public', express.static(__dirname + './../public'));
    app.use('/docs', express.static(__dirname + './../docs'));
    app.use('/views', express.static(__dirname + './../app/views'));
    app.use('/build', express.static(path.resolve('app', 'views', 'help-centre', 'build')));
    app.use('/assets', express.static(path.resolve('app', 'views', 'help-centre', 'assets')));
    app.use('/uploads', express.static(__dirname + './../public/uploads'));
    app.use('/bower_components', express.static(__dirname + './../public/bower_components'));
    app.use('/plugins', express.static(__dirname + './../public/plugins'));
    app.use('/modules', express.static(path.resolve('app', 'views', 'officialshop', 'modules')));
    app.use('/_assets', express.static(path.resolve('app', 'views', 'officialshop', '_assets')));
    app.use('/_build', express.static(path.resolve('app', 'views', 'officialshop', '_build')));
    app.set('port', port);
    app.set('ip', config.ip);
    app.set('env', config.env);
    app.set('config', config);
    app.set('api_version', process.env.APP_VER || config.api_version);
    app.set('view engine', 'ejs');
    app.set('views', 'app/views/');
    app.use(morgan('dev'));
    app.use(methodOverride());
    app.use(cookieParser());
    app.use(expressValidator());
    app.use(bodyParser.json({
        type: 'application/json',
        limit: '50mb'
    }));
    app.use(bodyParser.urlencoded({
        extended: true,
        limit: '50mb'
    }));

    app.use(flash());
    app.use(middleware.allowCrossDomain);
    app.enable('trust proxy');
    app.use(passport.initialize());
    app.use(passport.session());

    /*process.on('uncaughtException', function(err) {
        console.log('Caught exception: ', err);
    });*/
};