/*jshint camelcase: false */

'use strict';


module.exports = (io) => {
    io.on('connection', function(client) {
        console.log('connection client : ', client.id);
        client.on('create-room', function(data) {
            console.log('create-room: ', data);
            client.join(data);
        });

        client.on('event', function(data) {
            console.log('event: ', data);
        });
        client.on('disconnect', function() {
            console.log('socket disconnect;');
        });
    });
};