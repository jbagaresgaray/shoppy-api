'use strict';

let env = process.env.NODE_ENV;
let config = require('../config/environment/' + env);

let mysql = require('mysql');
let Database = require('../app/utils/database').Database;
let db = new Database();

let BasicStrategy = require('passport-http').BasicStrategy;
let _ = require('lodash');
let async = require('async');
const bcrypt = require('bcryptjs');
const uuidv1 = require('uuid/v1');

module.exports = function (passport, jwt) {

    passport.use('user', new BasicStrategy(
        (username, password, done) => {
            let str = mysql.format('SELECT username,password,isVerify,email,firstname,lastname,phone,fax,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
                'FROM dashboard_users WHERE (username=? OR email =?) LIMIT 1;', [username, username]);
            console.log('str: ', str);
            db.query(str, verifyDashboardAuth(password, done));
        }
    ));

    passport.use('mobile', new BasicStrategy(
        (username, password, done) => {
            let str = mysql.format('SELECT username,password,isVerify,email,firstname,lastname,isSeller,sellerApprovalCode,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
                'FROM users WHERE (username=? OR email =?) LIMIT 1;', [username, username]);
            console.log('str: ', str);
            db.query(str, verifyMobileAuth(password, done));
        }
    ));

    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        done(null, user);
    });

    let verifyDashboardAuth = (password, done) => {
        return (err, user) => {
            if (err) {
                return done(err);
            }

            if (_.isEmpty(user)) {
                return done(null, {
                    msg: 'User does not exist with this user account.',
                    success: false,
                    result: ''
                });
            }

            user = user[0];
            if (user.isVerify === 0) {
                done(null, {
                    msg: 'Your account has not been VERIFIED.',
                    success: false,
                    result: ''
                });
            } else {
                bcrypt.compare(password, user.password, function (err, res) {
                    if (res) {
                        delete user.password;
                        delete user.isVerify;

                        const payload = {
                            uuid: user.uuid,
                            email: user.email,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            phone: user.phone,
                            fax: user.fax,
                            seller_code: user.sellerApprovalCode,
                            img_name: user.img_name,
                            img_path: user.img_path,
                            img_type: user.img_type,
                            img_size: user.img_size
                        };

                        var token = jwt.sign(payload, config.token_secret, {
                            expiresIn: '1d',
                            issuer: config.token_secret,
                            jwtid: uuidv1()
                        });

                        done(null, {
                            msg: 'Login successfully',
                            success: true,
                            result: {
                                user: user,
                                token: token
                            }
                        });
                    } else {
                        done(null, {
                            msg: 'Invalid Username or Password',
                            success: false,
                            result: ''
                        });
                    }
                });
            }
        };
    }

    let verifyMobileAuth = (password, done) => {
        return (err, user) => {
            if (err) {
                return done(err);
            }

            if (_.isEmpty(user)) {
                return done(null, {
                    msg: 'User does not exist with this user account.',
                    success: false,
                    result: ''
                });
            }

            user = user[0];
            if (user.isVerify === 0) {
                done(null, {
                    msg: 'Your account has not been VERIFIED.',
                    success: false,
                    result: ''
                });
            } else {
                bcrypt.compare(password, user.password, function (err, res) {
                    if (res) {
                        delete user.password;
                        delete user.isVerify;
                        user.isSeller = _.isNull(user.isSeller) ? 0 : user.isSeller;
                        user.isSeller = (user.isSeller == 0) ? false : true;

                        const payload = {
                            uuid: user.uuid,
                            email: user.email,
                            firstname: user.firstname,
                            isSeller: user.isSeller,
                            lastname: user.lastname,
                            img_name: user.img_name,
                            img_path: user.img_path,
                            img_type: user.img_type,
                            img_size: user.img_size
                        };

                        var token = jwt.sign(payload, config.token_secret_mobile, {
                            expiresIn: '7 days',
                            issuer: config.token_secret_mobile,
                            jwtid: uuidv1()
                        });

                        done(null, {
                            msg: 'Login successfully',
                            success: true,
                            result: {
                                user: user,
                                token: token
                            }
                        });
                    } else {
                        done(null, {
                            msg: 'Invalid Username or Password',
                            success: false,
                            result: ''
                        });
                    }
                });
            }
        };
    }
};