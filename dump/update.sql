CREATE TABLE `shoppy`.`mobile_banners` (
  `bannerId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `img_path` TEXT NULL,
  `img_size` INT(11) NULL,
  `img_type` VARCHAR(45) NULL,
  `img_name` TEXT NULL,
  `link` TEXT NULL,
  `sortorder` INT NULL,
  `isPublished` TINYINT(4) NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`bannerId`));
DROP TRIGGER IF EXISTS `shoppy`.`tg_mobile_banners`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `shoppy`.`tg_mobile_banners` BEFORE INSERT ON `mobile_banners` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;


CREATE TABLE `shoppy`.`mobile_cms` (
  `cmsId` INT NOT NULL AUTO_INCREMENT,
  `title` TEXT NULL,
  `summary` TEXT NULL,
  `content` LONGTEXT NULL,
  `date_created` TIMESTAMP NULL,
  `date_published` TIMESTAMP NULL,
  `isPublished` TINYINT(4) NULL,
  `uuid` VARCHAR(45) NULL,
  `tags` TEXT NULL,
  `created_by` INT NULL,
  PRIMARY KEY (`cmsId`));
DROP TRIGGER IF EXISTS `shoppy`.`tg_mobile_cms`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `shoppy`.`tg_mobile_cms` BEFORE INSERT ON `mobile_cms` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;


CREATE TABLE `shoppy`.`tags` (
  `tagId` INT NOT NULL AUTO_INCREMENT,
  `tags` VARCHAR(100) NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`tagId`));
DROP TRIGGER IF EXISTS `shoppy`.`tg_tags`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `tg_tags` BEFORE INSERT ON `tags` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;



CREATE TABLE `shoppy`.`notifications` (
  `notificationId` INT NOT NULL AUTO_INCREMENT,
  `title` TEXT NULL,
  `content` TEXT NULL,
  `datetime` TIMESTAMP NULL,
  `link` VARCHAR(45) NULL,
  `notifTypeId` INT NULL,
  `isRead` TINYINT(4) NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`notificationId`));
DROP TRIGGER IF EXISTS `shoppy`.`tg_notifications`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `shoppy`.`tg_notifications` BEFORE INSERT ON `notifications` FOR EACH ROW
BEGIN
	SET NEW.uuid= UUID();
END$$
DELIMITER ;


CREATE TABLE `shoppy`.`notificationtype` (
  `notifTypeId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`notifTypeId`));
DROP TRIGGER IF EXISTS `shoppy`.`tg_notificationtype`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `shoppy`.`tg_notificationtype` BEFORE INSERT ON `notificationtype` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;

INSERT INTO `shoppy`.`notificationtype` (`name`) VALUES ('Promotions');
INSERT INTO `shoppy`.`notificationtype` (`name`) VALUES ('Seller Updates');
INSERT INTO `shoppy`.`notificationtype` (`name`) VALUES ('Wallet Updates');
INSERT INTO `shoppy`.`notificationtype` (`name`) VALUES ('App Updates');


ALTER TABLE `shoppy`.`products` 
ADD COLUMN `productCondition` VARCHAR(45) NULL AFTER `productWeight`,
ADD COLUMN `productLength` DECIMAL NULL AFTER `productSubCategoryId`,
ADD COLUMN `productHeight` DECIMAL NULL AFTER `productLength`,
ADD COLUMN `productWidth` DECIMAL NULL AFTER `productHeight`,
ADD COLUMN `isPreOrder` TINYINT(4) NULL DEFAULT 0 AFTER `uuid`;

CREATE TABLE `shoppy`.`product_shipping_fee` (
  `product_shipping_feeId` INT NOT NULL AUTO_INCREMENT,
  `shippingCourierId` INT NULL,
  `fee` DECIMAL(11,2) NULL,
  `isCover` TINYINT(4) NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`product_shipping_feeId`),
  INDEX `fk_product_shipping_fee_id_idx` (`shippingCourierId` ASC),
  CONSTRAINT `fk_product_shipping_fee_id`
    FOREIGN KEY (`shippingCourierId`)
    REFERENCES `shoppy`.`shipping_courier` (`shippingCourierId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

ALTER TABLE `shoppy`.`product_shipping_fee` 
ADD COLUMN `productId` INT NULL AFTER `shippingCourierId`,
ADD INDEX `fk_product_shipping_fee_productId_idx` (`productId` ASC);
ALTER TABLE `shoppy`.`product_shipping_fee` 
ADD CONSTRAINT `fk_product_shipping_fee_productId`
  FOREIGN KEY (`productId`)
  REFERENCES `shoppy`.`products` (`productId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


DROP TRIGGER IF EXISTS `shoppy`.`tg_product_shipping_fee`;
DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `shoppy`.`tg_product_shipping_fee` BEFORE INSERT ON `product_shipping_fee` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;

ALTER TABLE `shoppy`.`users` 
ADD COLUMN `sellerDescription` TEXT NULL AFTER `sellerApplicationDate`,
ADD COLUMN `sellerName` VARCHAR(60) NULL AFTER `sellerDescription`,
ADD COLUMN `currencyId` VARCHAR(45) NULL AFTER `isVerify`,
ADD COLUMN `countryID` VARCHAR(45) NULL AFTER `currencyId`;


CREATE TABLE `shoppy`.`user_shipping` (
  `userShippingId` INT NOT NULL AUTO_INCREMENT,
  `shippingId` INT NULL,
  `userId` INT NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`userShippingId`),
  INDEX `fk_shippingCourrierId_idx` (`shippingId` ASC),
  INDEX `fk_user_shipping_userId_idx` (`userId` ASC),
  CONSTRAINT `fk_shippingCourrierId`
    FOREIGN KEY (`shippingId`)
    REFERENCES `shoppy`.`shipping_courier` (`shippingCourierId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_shipping_userId`
    FOREIGN KEY (`userId`)
    REFERENCES `shoppy`.`users` (`userId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE);
DROP TRIGGER IF EXISTS `shoppy`.`tg_user_shipping`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `tg_user_shipping` BEFORE INSERT ON `user_shipping` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;


ALTER TABLE `shoppy`.`shipping_courier` 
ADD COLUMN `description` TEXT NULL AFTER `name`,
ADD COLUMN `isSupported` TINYINT(4) NULL DEFAULT 0 AFTER `description`;


DROP TRIGGER IF EXISTS `shoppy`.`tg_payment_providers`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `tg_payment_providers` BEFORE INSERT ON `payment_providers` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;

ALTER TABLE `shoppy`.`orders` 
ADD COLUMN `orderNum` VARCHAR(45) NULL AFTER `userId`,
ADD COLUMN `batchID` VARCHAR(45) NULL AFTER `orderNum`,
ADD COLUMN `isReceived` TINYINT(4) NULL AFTER `completedDateTime`,
ADD COLUMN `receivedDateTime` TIMESTAMP NULL AFTER `isReceived`,
ADD COLUMN `isCancelled` TINYINT(4) NULL AFTER `receivedDateTime`,
ADD COLUMN `cancelledDateTime` TIMESTAMP NULL AFTER `isCancelled`,
ADD COLUMN `isReturned` TINYINT(4) NULL AFTER `cancelledDateTime`,
ADD COLUMN `returnedDateTime` TIMESTAMP NULL AFTER `isReturned`,
ADD COLUMN `returnedReason` TEXT NULL AFTER `returnedDateTime`,
ADD COLUMN `orderNote` TEXT NULL AFTER `returnedReason`;

ALTER TABLE `shoppy`.`orders` 
ADD COLUMN `shippingDeadline` TIMESTAMP NULL AFTER `shippedDateTime`,
ADD COLUMN `paymentDeadline` VARCHAR(45) NULL AFTER `paymentDateTime`,
ADD COLUMN `deliveryDeadline` TIMESTAMP NULL AFTER `receivedDateTime`,
ADD COLUMN `modeofpayment` VARCHAR(45) NULL AFTER `orderTrackingNumber`;

ALTER TABLE `shoppy`.`orders` 
ADD COLUMN `cancelledReason` TEXT NULL AFTER `cancelledDateTime`;


ALTER TABLE `shoppy`.`order_details`
ADD COLUMN `shippingId` INT(11) NULL AFTER `productId`,
ADD COLUMN `shippingFee` FLOAT NULL AFTER `shippingId`;

ALTER TABLE `shoppy`.`orders` 
CHANGE COLUMN `isShipped` `isShipped` TINYINT(4) NULL DEFAULT 0 ,
CHANGE COLUMN `isPaid` `isPaid` TINYINT(4) NULL DEFAULT 0 ,
CHANGE COLUMN `isCompleted` `isCompleted` TINYINT(4) NULL DEFAULT 0 ,
CHANGE COLUMN `isReceived` `isReceived` TINYINT(4) NULL DEFAULT 0 ,
CHANGE COLUMN `isCancelled` `isCancelled` TINYINT(4) NULL DEFAULT 0 ,
CHANGE COLUMN `isReturned` `isReturned` TINYINT(4) NULL DEFAULT 0 ,
ADD COLUMN `isAutoCancelled` TINYINT(4) NULL DEFAULT 0 AFTER `isCancelled`;

ALTER TABLE `shoppy`.`orders` 
ADD COLUMN `sellerId` INT NULL AFTER `userId`,
ADD INDEX `fk_orders_seller_idx` (`sellerId` ASC);
ALTER TABLE `shoppy`.`orders` 
ADD CONSTRAINT `fk_orders_seller`
  FOREIGN KEY (`sellerId`)
  REFERENCES `shoppy`.`users` (`userId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `shoppy`.`orders` 
DROP COLUMN `completedDateTime`,
DROP COLUMN `isCompleted`,
ADD COLUMN `isCOD` TINYINT(4) NULL DEFAULT 0 AFTER `orderEmail`,
ADD COLUMN `isPrepared` TINYINT(4) NULL DEFAULT 0 AFTER `isCOD`,
ADD COLUMN `preparedDateTime` TIMESTAMP NULL AFTER `isPrepared`;




CREATE TABLE `shoppy`.`user_transaction` (
  `transId` INT NOT NULL AUTO_INCREMENT,
  `transaction_id` VARCHAR(45) NULL,
  `type` VARCHAR(45) NULL,
  `currencyId` VARCHAR(45) NULL,
  `value` FLOAT NULL,
  `userId` INT NULL,
  `transDateTime` TIMESTAMP NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`transId`),
  INDEX `fk_trans_userID_idx` (`userId` ASC),
  CONSTRAINT `fk_trans_userID`
    FOREIGN KEY (`userId`)
    REFERENCES `shoppy`.`users` (`userId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
DROP TRIGGER IF EXISTS `shoppy`.`tg_user_transaction`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `tg_user_transaction` BEFORE INSERT ON `user_transaction` FOR EACH ROW
BEGIN
  SET NEW.uuid = UUID();
END$$
DELIMITER ;


ALTER TABLE `shoppy`.`users` 
ADD COLUMN `sendBirdToken` VARCHAR(45) NULL AFTER `twitterId`,
ADD COLUMN `sellerIsVacationMode` TINYINT(4) NULL AFTER `sellerName`;



CREATE TABLE `shoppy`.`files` (
  `fileId` INT NOT NULL AUTO_INCREMENT,
  `file_name` TEXT NULL,
  `file_type` VARCHAR(45) NULL,
  `file_size` FLOAT NULL,
  `file_orig_path` TEXT NULL,
  `file_100_path` TEXT NULL,
  `file_200_path` TEXT NULL,
  `file_400_path` TEXT NULL,
  `file_800_path` TEXT NULL,
  `file_thumb_path` TEXT NULL,
  `file_preview_path` TEXT NULL,
  `file_large_path` TEXT NULL,
  `file_extra_large_path` TEXT NULL,
  `dp_id` VARCHAR(45) NULL,
  `dp_rev` VARCHAR(45) NULL,
  `dp_file_hash` VARCHAR(60),
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`fileId`));
DROP TRIGGER IF EXISTS `shoppy`.`tg_files`;

DELIMITER $$
USE `shoppy`$$
CREATE DEFINER = CURRENT_USER TRIGGER `shoppy`.`tg_files` BEFORE INSERT ON `files` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END$$
DELIMITER ;



ALTER TABLE `shoppy`.`products` 
DROP COLUMN `productThumb_size`,
DROP COLUMN `productThumb_type`,
DROP COLUMN `productThumb_name`,
DROP COLUMN `productThumb_path`;

ALTER TABLE `shoppy`.`users` 
ADD COLUMN `onesignal_player_id` VARCHAR(45) NULL AFTER `sendBirdToken`;

ALTER TABLE `shoppy`.`dashboard_users`
ADD COLUMN `sendBirdToken` VARCHAR
(45) NULL AFTER `verificationCode`;


ALTER TABLE `shoppy`.`users` 
DROP COLUMN `onesignal_player_id`;


CREATE TABLE `shoppy`.`user_devices` (
  `device_id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NULL,
  `onesignal_player_id` VARCHAR(45) NULL,
  `date_created` TIMESTAMP NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`device_id`));


CREATE TABLE `newsletter` (
  `cmsId` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `summary` text,
  `content` longtext,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_published` timestamp NULL DEFAULT NULL,
  `isPublished` tinyint(4) DEFAULT NULL,
  `tags` text,
  `type` varchar(45),
  `created_by` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cmsId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


ALTER TABLE `shoppy`.`notifications` 
ADD COLUMN `userId` INT NULL AFTER `isRead`,
ADD COLUMN `user_player_ids` INT NULL AFTER `userId`;

ALTER TABLE `shoppy`.`notifications` 
CHANGE COLUMN `user_player_ids` `user_player_ids` VARCHAR(45) NULL DEFAULT NULL ;


ALTER TABLE `shoppy`.`notifications` 
DROP COLUMN `user_player_ids`,
DROP COLUMN `userId`,
DROP COLUMN `isRead`;


CREATE TABLE `shoppy`.`user_notifications` (
  `iduser_notifications` INT NOT NULL AUTO_INCREMENT,
  `notificationId` INT NULL,
  `isRead` TINYINT(4) NULL DEFAULT 0,
  `userId` INT NULL,
  `user_player_ids` VARCHAR(45) NULL,
  `date_created` TIMESTAMP NULL,
  `uuid` VARCHAR(45) NULL,
  PRIMARY KEY (`iduser_notifications`),
  INDEX `fk_notif_userId_idx` (`userId` ASC),
  INDEX `fk_notif_id_idx` (`notificationId` ASC),
  CONSTRAINT `fk_notif_userId`
    FOREIGN KEY (`userId`)
    REFERENCES `shoppy`.`users` (`userId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_notif_id`
    FOREIGN KEY (`notificationId`)
    REFERENCES `shoppy`.`notifications` (`notificationId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


ALTER TABLE `shoppy`.`users` 
DROP COLUMN `twitterId`,
CHANGE COLUMN `instagramId` `googleId` VARCHAR(60) NULL DEFAULT NULL ;


ALTER TABLE `shoppy`.`product_images` 
DROP FOREIGN KEY `fk_product_images_products1`;
ALTER TABLE `shoppy`.`product_images` 
ADD INDEX `fk_product_images_products1_idx` (`productId` ASC),
DROP INDEX `fk_product_images_products1_idx` ;
ALTER TABLE `shoppy`.`product_images` 
ADD CONSTRAINT `fk_product_images_products1`
  FOREIGN KEY (`productId`)
  REFERENCES `shoppy`.`products` (`productId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;












ALTER TABLE `shopyapp_ecommerce`.`voucher_products` 
ADD COLUMN `voucherId` INT NULL AFTER `productId`,
ADD INDEX `fk_voucher_products_voucherIdx_idx` (`voucherId` ASC);
ALTER TABLE `shopyapp_ecommerce`.`voucher_products` 
ADD CONSTRAINT `fk_voucher_products_voucherIdx`
  FOREIGN KEY (`voucherId`)
  REFERENCES `shopyapp_ecommerce`.`vouchers` (`voucherId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
