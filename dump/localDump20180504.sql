CREATE DATABASE  IF NOT EXISTS `shoppy` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `shoppy`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: shoppy
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `bankId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address` text,
  `city` text,
  `suburb` text,
  `state` text,
  `zipcode` varchar(45) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`bankId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,'sadsads --hahahahah','asdsad','asdsad','asdsad','asdasd','9000','BE','c653305d-212e-11e8-aed1-206a8a764328'),(2,'fdgdgd-test','gfhfgh','fghgfh','fghfgh','fghfgh','34324','PH','6efc0970-212f-11e8-aed1-206a8a764328');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand` (
  `brandId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `img_path` text,
  `img_name` text,
  `img_type` varchar(45) DEFAULT NULL,
  `img_size` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`brandId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Apple',NULL,NULL,NULL,NULL,'a9fc54c5-1c55-11e8-9455-206a8a764328'),(2,'Adidas','public\\uploads\\brands\\development\\1521032256_adidas.png','adidas.png','image/png',12151,'44f6e625-2787-11e8-a7e8-206a8a764328'),(3,'Chanel','public\\uploads\\brands\\development\\1521032266_chanel.png','chanel.png','image/png',12627,'4ab5929f-2787-11e8-a7e8-206a8a764328'),(4,'KTM','public\\uploads\\brands\\development\\1521032273_ktm.png','ktm.png','image/png',87923,'4f2ae738-2787-11e8-a7e8-206a8a764328'),(5,'panasonic','public\\uploads\\brands\\development\\1521032281_panasonic.png','panasonic.png','image/png',12134,'53e00739-2787-11e8-a7e8-206a8a764328'),(6,'Puma','public\\uploads\\brands\\development\\1521032291_puma.png','puma.png','image/png',17982,'59c15a35-2787-11e8-a7e8-206a8a764328'),(7,'zara','public\\uploads\\brands\\development\\1521032298_zara.png','zara.png','image/png',3486,'5df31f25-2787-11e8-a7e8-206a8a764328');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buyer_rating`
--

DROP TABLE IF EXISTS `buyer_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buyer_rating` (
  `buyerRatingId` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT NULL,
  `comment` text,
  `orderId` int(11) NOT NULL,
  `buyer_userId` int(11) NOT NULL,
  `seller_userId` int(11) NOT NULL,
  `ratedOn` timestamp NULL DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`buyerRatingId`),
  KEY `fk_buyer_rating_orders1_idx` (`orderId`),
  KEY `fk_buyer_rating_users1_idx` (`buyer_userId`),
  KEY `fk_buyer_rating_users2_idx` (`seller_userId`),
  CONSTRAINT `fk_buyer_rating_orders1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`orderId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_buyer_rating_users1` FOREIGN KEY (`buyer_userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_buyer_rating_users2` FOREIGN KEY (`seller_userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buyer_rating`
--

LOCK TABLES `buyer_rating` WRITE;
/*!40000 ALTER TABLE `buyer_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `buyer_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `img_path` text,
  `img_name` text,
  `img_type` varchar(45) DEFAULT NULL,
  `img_size` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Men\'s Apparel','public\\uploads\\category\\development\\1521029259_polo.png','polo.png','image/png',2763,'ce11e349-1c55-11e8-9455-206a8a764328'),(2,'Women\'s Apparel','public\\uploads\\category\\development\\1521029286_dress.png','dress.png','image/png',3781,'d6614fcd-1c55-11e8-9455-206a8a764328'),(3,'Shoes','public\\uploads\\category\\development\\1521018833_sneakers.png','sneakers.png','image/png',3592,'046f606c-2768-11e8-a7e8-206a8a764328'),(4,'Beauty & Health',NULL,NULL,NULL,NULL,'7ab5508a-2780-11e8-a7e8-206a8a764328'),(5,'Consumer Electronics',NULL,NULL,NULL,NULL,'8f6ab111-2780-11e8-a7e8-206a8a764328'),(6,'Computer & Office',NULL,NULL,NULL,NULL,'a7addc1d-2780-11e8-a7e8-206a8a764328'),(7,'Mobiles & Accessories',NULL,NULL,NULL,NULL,'b772a96c-2780-11e8-a7e8-206a8a764328'),(8,'Electronic & Appliances',NULL,NULL,NULL,NULL,'0041f8a2-2781-11e8-a7e8-206a8a764328'),(9,'Furnitures',NULL,NULL,NULL,NULL,'0fb7cb65-2781-11e8-a7e8-206a8a764328'),(10,'Men\'s Shoes',NULL,NULL,NULL,NULL,'2c70af0f-2781-11e8-a7e8-206a8a764328'),(11,'Women\'s Shoes',NULL,NULL,NULL,NULL,'32b0926b-2781-11e8-a7e8-206a8a764328'),(12,'Men\'s Accessories',NULL,NULL,NULL,NULL,'4480f61f-2781-11e8-a7e8-206a8a764328'),(13,'Women\'s Accessories',NULL,NULL,NULL,NULL,'4baf79b4-2781-11e8-a7e8-206a8a764328'),(14,'Automobiles & Motorcycles',NULL,NULL,NULL,NULL,'5ef17075-2781-11e8-a7e8-206a8a764328'),(15,'Sports and Entertainment',NULL,NULL,NULL,NULL,'80fb5331-2781-11e8-a7e8-206a8a764328'),(16,'Groceries & Pet Care',NULL,NULL,NULL,NULL,'e52fc8da-2781-11e8-a7e8-206a8a764328');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_users`
--

DROP TABLE IF EXISTS `dashboard_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_users` (
  `duserId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `isVerify` tinyint(4) DEFAULT NULL,
  `verificationCode` varchar(60) DEFAULT NULL,
  `dateVerify` timestamp NULL DEFAULT NULL,
  `user_img_path` text,
  `user_img_name` text,
  `user_img_type` varchar(45) DEFAULT NULL,
  `user_img_size` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`duserId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_users`
--

LOCK TABLES `dashboard_users` WRITE;
/*!40000 ALTER TABLE `dashboard_users` DISABLE KEYS */;
INSERT INTO `dashboard_users` VALUES (1,'user123','user123@yopmail.com','$2a$08$DNOMywn4OIfPridOb0.PH.kqXzfqdsuYxrTPt6pGjpEKx6KkHhjg6','John','Doe',NULL,NULL,1,'c1409ef01ac411e8a8dc7d0c2fe95486','2018-02-26 07:18:39',NULL,NULL,NULL,NULL,'c1426067-1ac4-11e8-94f9-206a8a764328');
/*!40000 ALTER TABLE `dashboard_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_banners`
--

DROP TABLE IF EXISTS `mobile_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_banners` (
  `bannerId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `img_path` text,
  `img_size` int(11) DEFAULT NULL,
  `img_type` varchar(45) DEFAULT NULL,
  `img_name` text,
  `link` text,
  `sortorder` int(11) DEFAULT NULL,
  `isPublished` tinyint(4) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`bannerId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_banners`
--

LOCK TABLES `mobile_banners` WRITE;
/*!40000 ALTER TABLE `mobile_banners` DISABLE KEYS */;
INSERT INTO `mobile_banners` VALUES (1,'Banner 1','public/uploads/mobile/banners/development/1522137536_banner_mobile_app.jpg',94136,'image/jpeg','banner_mobile_app.jpg','null',NULL,1,'d0b3af06-318c-11e8-820f-38c98626bd93'),(2,'Banner 2','public/uploads/mobile/banners/development/1522136300_app-banner.jpg',114869,'image/jpeg','app-banner.jpg','undefined',NULL,1,'d263f6bc-3191-11e8-820f-38c98626bd93'),(3,'Banner 3','public/uploads/mobile/banners/development/1522137870_b2346113cdc5873d9a55c227d263bd7f.jpg',18525,'image/jpeg','b2346113cdc5873d9a55c227d263bd7f.jpg','undefined',NULL,1,'7a261d32-3195-11e8-820f-38c98626bd93'),(4,'Banner 4','public/uploads/mobile/banners/development/1522140122_sZBoqUeXwaW8CPTmNRhoJuCfKRCc1tTjO59ANGNQXkLLUTj6wCUNcub7XfdZSCgC.jpg',21249,'image/jpeg','sZBoqUeXwaW8CPTmNRhoJuCfKRCc1tTjO59ANGNQXkLLUTj6wCUNcub7XfdZSCgC.jpg','undefined',NULL,0,'b86d5790-319a-11e8-820f-38c98626bd93');
/*!40000 ALTER TABLE `mobile_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_cms`
--

DROP TABLE IF EXISTS `mobile_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_cms` (
  `cmsId` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `summary` text,
  `content` longtext,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_published` timestamp NULL DEFAULT NULL,
  `isPublished` tinyint(4) DEFAULT NULL,
  `tags` text,
  `created_by` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cmsId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_cms`
--

LOCK TABLES `mobile_cms` WRITE;
/*!40000 ALTER TABLE `mobile_cms` DISABLE KEYS */;
INSERT INTO `mobile_cms` VALUES (1,'sadasdasd','dsfdgdfgdfgdf','dfgdfgdfgdfgdfgdf','2018-03-21 06:54:56',NULL,0,NULL,NULL,'c3e1ba7e-2cd4-11e8-a638-206a8a764328'),(2,'Sample Title','<h3>Hello Jonathan! </h3> dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry\'s</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with\n                                    <br>\n                                    <br>','<h3>Hello Jonathan! </h3> dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry\'s</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with\n                                    <br>\n                                    <br>','2018-03-21 07:09:00',NULL,0,NULL,NULL,'bb20ccae-2cd6-11e8-a638-206a8a764328');
/*!40000 ALTER TABLE `mobile_cms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `notificationId` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` text,
  `datetime` timestamp NULL DEFAULT NULL,
  `link` varchar(45) DEFAULT NULL,
  `notifTypeId` int(11) DEFAULT NULL,
  `isRead` tinyint(4) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`notificationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `shoppy`.`tg_notifications` BEFORE INSERT ON `notifications` FOR EACH ROW
BEGIN
	SET NEW.uuid= UUID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `notificationtype`
--

DROP TABLE IF EXISTS `notificationtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationtype` (
  `notifTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`notifTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificationtype`
--

LOCK TABLES `notificationtype` WRITE;
/*!40000 ALTER TABLE `notificationtype` DISABLE KEYS */;
INSERT INTO `notificationtype` VALUES (1,'Promotions','c4b3bbf0-316f-11e8-820f-38c98626bd93'),(2,'Seller Updates','c4cf91fe-316f-11e8-820f-38c98626bd93'),(3,'Wallet Updates','c4cf9e1a-316f-11e8-820f-38c98626bd93'),(4,'App Updates','c4cfa6d0-316f-11e8-820f-38c98626bd93');
/*!40000 ALTER TABLE `notificationtype` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `shoppy`.`tg_notificationtype` BEFORE INSERT ON `notificationtype` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `detailsId` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `detailName` text,
  `detailPrice` float DEFAULT NULL,
  `detailSKU` varchar(60) DEFAULT NULL,
  `detailQty` int(11) DEFAULT NULL,
  PRIMARY KEY (`detailsId`),
  KEY `fk_order_details_orders1_idx` (`orderId`),
  KEY `fk_order_details_products1_idx` (`productId`),
  CONSTRAINT `fk_order_details_orders1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`orderId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_details_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `orderNum` varchar(45) DEFAULT NULL,
  `orderDateTime` timestamp NULL DEFAULT NULL,
  `orderAmt` float DEFAULT NULL,
  `orderShipName` text,
  `orderShipAddress` text,
  `orderShipCity` text,
  `orderShipState` text,
  `orderShipZipCode` varchar(12) DEFAULT NULL,
  `orderShipCountry` varchar(45) DEFAULT NULL,
  `orderShipSuburb` text,
  `orderPhone` varchar(45) DEFAULT NULL,
  `orderFax` varchar(45) DEFAULT NULL,
  `orderShippingFee` float DEFAULT NULL,
  `orderTax` float DEFAULT NULL,
  `orderEmail` varchar(100) DEFAULT NULL,
  `isShipped` tinyint(4) DEFAULT NULL,
  `shippedDateTime` timestamp NULL DEFAULT NULL,
  `orderTrackingNumber` varchar(80) DEFAULT NULL,
  `isPaid` tinyint(4) DEFAULT NULL,
  `paymentDateTime` timestamp NULL DEFAULT NULL,
  `isCompleted` tinyint(4) DEFAULT NULL,
  `completedDateTime` timestamp NULL DEFAULT NULL,
  `isReceived` tinyint(4) DEFAULT NULL,
  `receivedDateTime` timestamp NULL DEFAULT NULL,
  `isCancelled` tinyint(4) DEFAULT NULL,
  `cancelledDateTime` timestamp NULL DEFAULT NULL,
  `isReturned` tinyint(4) DEFAULT NULL,
  `returnedDateTime` timestamp NULL DEFAULT NULL,
  `returnedReason` text,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  KEY `fk_orders_users1_idx` (`userId`),
  CONSTRAINT `fk_orders_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_providers`
--

DROP TABLE IF EXISTS `payment_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_providers` (
  `paymentProvidersId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`paymentProvidersId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_providers`
--

LOCK TABLES `payment_providers` WRITE;
/*!40000 ALTER TABLE `payment_providers` DISABLE KEYS */;
INSERT INTO `payment_providers` VALUES (1,'Paypal','059ad510-3187-11e8-820f-38c98626bd93'),(2,'Cash On Delivery','bd2a0c82-4f8c-11e8-9c88-38c98626bd93');
/*!40000 ALTER TABLE `payment_providers` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tg_payment_providers` BEFORE INSERT ON `payment_providers` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `product_comment`
--

DROP TABLE IF EXISTS `product_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_comment` (
  `productCommentId` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text,
  `commentOn` timestamp NULL DEFAULT NULL,
  `productId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productCommentId`),
  KEY `fk_product_comment_products1_idx` (`productId`),
  KEY `fk_product_comment_users1_idx` (`userId`),
  CONSTRAINT `fk_product_comment_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_comment_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_comment`
--

LOCK TABLES `product_comment` WRITE;
/*!40000 ALTER TABLE `product_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `productImagesId` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` text,
  `img_path` text,
  `img_type` varchar(45) DEFAULT NULL,
  `img_size` int(11) DEFAULT NULL,
  `productId` int(11) NOT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productImagesId`),
  KEY `fk_product_images_products1_idx` (`productId`),
  CONSTRAINT `fk_product_images_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_like`
--

DROP TABLE IF EXISTS `product_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_like` (
  `productLikeId` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `liked_userId` int(11) NOT NULL,
  `likedDateTime` timestamp NULL DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productLikeId`),
  KEY `fk_product_like_products1_idx` (`productId`),
  KEY `fk_product_like_users1_idx` (`liked_userId`),
  CONSTRAINT `fk_product_like_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_like_users1` FOREIGN KEY (`liked_userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_like`
--

LOCK TABLES `product_like` WRITE;
/*!40000 ALTER TABLE `product_like` DISABLE KEYS */;
INSERT INTO `product_like` VALUES (5,10,1,'2018-04-03 08:29:41',NULL),(7,9,1,'2018-04-03 08:33:10',NULL);
/*!40000 ALTER TABLE `product_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_rating`
--

DROP TABLE IF EXISTS `product_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_rating` (
  `productRatingId` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT NULL,
  `comment` text,
  `ratedOn` timestamp NULL DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productRatingId`),
  KEY `fk_product_rating_users1_idx` (`userId`),
  KEY `fk_product_rating_products1_idx` (`productId`),
  CONSTRAINT `fk_product_rating_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_rating_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_rating`
--

LOCK TABLES `product_rating` WRITE;
/*!40000 ALTER TABLE `product_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_shipping_fee`
--

DROP TABLE IF EXISTS `product_shipping_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_shipping_fee` (
  `product_shipping_feeId` int(11) NOT NULL AUTO_INCREMENT,
  `shippingCourierId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `fee` decimal(11,2) DEFAULT NULL,
  `isCover` tinyint(4) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`product_shipping_feeId`),
  KEY `fk_product_shipping_fee_id_idx` (`shippingCourierId`),
  KEY `fk_product_shipping_fee_productId_idx` (`productId`),
  CONSTRAINT `fk_product_shipping_fee_id` FOREIGN KEY (`shippingCourierId`) REFERENCES `shipping_courier` (`shippingCourierId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_shipping_fee_productId` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_shipping_fee`
--

LOCK TABLES `product_shipping_fee` WRITE;
/*!40000 ALTER TABLE `product_shipping_fee` DISABLE KEYS */;
INSERT INTO `product_shipping_fee` VALUES (3,1,9,150.00,0,'e86c9142-3675-11e8-a2ee-38c98626bd93'),(4,2,9,120.00,0,'e872f4f6-3675-11e8-a2ee-38c98626bd93');
/*!40000 ALTER TABLE `product_shipping_fee` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `shoppy`.`tg_product_shipping_fee` BEFORE INSERT ON `product_shipping_fee` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `product_variation`
--

DROP TABLE IF EXISTS `product_variation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_variation` (
  `productVariationId` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `stock` float DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productVariationId`),
  KEY `fk_product_variation_products1_idx` (`productId`),
  CONSTRAINT `fk_product_variation_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_variation`
--

LOCK TABLES `product_variation` WRITE;
/*!40000 ALTER TABLE `product_variation` DISABLE KEYS */;
INSERT INTO `product_variation` VALUES (1,9,'Black 16GB',12000,5,'92502d96-1c77-11e8-9455-206a8a764328');
/*!40000 ALTER TABLE `product_variation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_wholesale`
--

DROP TABLE IF EXISTS `product_wholesale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_wholesale` (
  `productWholesaleId` int(11) NOT NULL AUTO_INCREMENT,
  `minAmt` int(11) DEFAULT NULL,
  `maxAmt` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `productId` int(11) NOT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`productWholesaleId`),
  KEY `fk_product_wholesale_products1_idx` (`productId`),
  CONSTRAINT `fk_product_wholesale_products1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_wholesale`
--

LOCK TABLES `product_wholesale` WRITE;
/*!40000 ALTER TABLE `product_wholesale` DISABLE KEYS */;
INSERT INTO `product_wholesale` VALUES (3,0,10,1000,9,'ed8e03f7-1d34-11e8-8979-206a8a764328'),(4,0,10,1000,9,'499602d7-1d35-11e8-8979-206a8a764328');
/*!40000 ALTER TABLE `product_wholesale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `productSKU` varchar(50) DEFAULT NULL,
  `productName` varchar(200) DEFAULT NULL,
  `productDesc` text,
  `productPrice` float DEFAULT NULL,
  `productWeight` float DEFAULT NULL,
  `productCondition` varchar(45) DEFAULT NULL,
  `productBrandId` int(11) DEFAULT NULL,
  `productThumb_path` text,
  `productThumb_name` text,
  `productThumb_type` varchar(45) DEFAULT NULL,
  `productThumb_size` int(11) DEFAULT NULL,
  `productStock` float DEFAULT NULL,
  `productCategoryID` int(11) NOT NULL,
  `productSubCategoryId` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '0',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`productId`),
  KEY `fk_products_brand1_idx` (`productBrandId`),
  KEY `fk_products_categories1_idx` (`productCategoryID`),
  KEY `fk_products_subcategory1_idx` (`productSubCategoryId`),
  KEY `fk_products_userId1_idx` (`userId`),
  CONSTRAINT `fk_products_brand1` FOREIGN KEY (`productBrandId`) REFERENCES `brand` (`brandId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_categories1` FOREIGN KEY (`productCategoryID`) REFERENCES `categories` (`categoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_subcategory1` FOREIGN KEY (`productSubCategoryId`) REFERENCES `subcategory` (`subcategoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_userId1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (9,'123456789','iPhone 6','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.',32000,NULL,'new',1,'public\\uploads\\products\\development\\1519805608_design02.png','design02.png','image/png',80852,10,1,1,'41fdeea2-1c5f-11e8-9455-206a8a764328',1,'2018-04-02 10:07:17',1),(10,'123456789','iPhone 5c','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.',32000,NULL,'new',1,'public\\uploads\\products\\development\\1519805657_design02.png','design02.png','image/png',80852,10,2,1,'5f4ec342-1c5f-11e8-9455-206a8a764328',1,'2018-04-02 10:07:17',1),(11,'123456789','Macbook Pro','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.',32000,NULL,'new',1,'public\\uploads\\products\\development\\1519805657_design02.png','design02.png','image/png',80852,10,5,NULL,'5f4ec342-1c5f-11e8-9455-206a8a764321',1,'2018-04-02 10:07:17',1),(12,'123456789','Macbook Air','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.',32000,NULL,'new',1,'public\\uploads\\products\\development\\1519805657_design02.png','design02.png','image/png',80852,10,5,NULL,'5f4ec342-1c5f-11e8-9455-206a8a764323',1,'2018-04-02 10:07:17',3);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_courier`
--

DROP TABLE IF EXISTS `shipping_courier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_courier` (
  `shippingCourierId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `isSupported` tinyint(4) DEFAULT '0',
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`shippingCourierId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_courier`
--

LOCK TABLES `shipping_courier` WRITE;
/*!40000 ALTER TABLE `shipping_courier` DISABLE KEYS */;
INSERT INTO `shipping_courier` VALUES (1,'LBC Express',NULL,0,'fb9aa504-3186-11e8-820f-38c98626bd93'),(2,'JRS Express',NULL,0,'016c8182-3187-11e8-820f-38c98626bd93');
/*!40000 ALTER TABLE `shipping_courier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `subcategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `categoryId` int(11) NOT NULL,
  `img_path` text,
  `img_name` text,
  `img_type` varchar(45) DEFAULT NULL,
  `img_size` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`subcategoryId`),
  KEY `fk_subcategory_categories_idx` (`categoryId`),
  CONSTRAINT `fk_subcategory_categories` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`categoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (1,'Casual Tops',1,NULL,NULL,NULL,NULL,'2f421a63-1c56-11e8-9455-206a8a764328'),(2,'Hoodies & Sweatshirts',1,NULL,NULL,NULL,NULL,'882fab29-2844-11e8-9d6f-206a8a764328'),(3,'Jackets & Coats',1,NULL,NULL,NULL,NULL,'8f46e512-2844-11e8-9d6f-206a8a764328');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `tagId` int(11) NOT NULL AUTO_INCREMENT,
  `tags` varchar(100) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_address` (
  `userAddressId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `city` text,
  `state` text,
  `zipcode` varchar(12) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `suburb` varchar(100) DEFAULT NULL,
  `detailAddress` text,
  `mobilenum` varchar(45) DEFAULT NULL,
  `isDefaultAddress` tinyint(4) DEFAULT NULL,
  `isPickUpAddress` varchar(45) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userAddressId`),
  KEY `fk_user_address_users1_idx` (`userId`),
  CONSTRAINT `fk_user_address_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_address`
--

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;
INSERT INTO `user_address` VALUES (1,'sadfafadadad','fdgdfgd def oro','adadad','9000','ALB','sdfsdfsdf','fdfgdfgdfgdfgdgdg','23123123',1,'1',1,NULL),(2,'Janine Jasmin','Cagayan De Oro City','Misamis Oriental','9000','PHL','Brgy. Poblacion 1','SMSI Bldg. 47 Hayes St. Near Grand Central Bldg.','09093130225',0,'0',1,NULL);
/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_bank`
--

DROP TABLE IF EXISTS `user_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bank` (
  `userBankId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `accountNo` varchar(45) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `bankId` int(11) NOT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userBankId`),
  KEY `fk_user_bank_users1_idx` (`userId`),
  KEY `fk_user_bank_bank1_idx` (`bankId`),
  CONSTRAINT `fk_user_bank_bank1` FOREIGN KEY (`bankId`) REFERENCES `bank` (`bankId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_bank_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_bank`
--

LOCK TABLES `user_bank` WRITE;
/*!40000 ALTER TABLE `user_bank` DISABLE KEYS */;
INSERT INTO `user_bank` VALUES (1,'Philip Cesar Garay','123435433453',1,1,'54e15da8-42fb-11e8-8218-38c98626bd93');
/*!40000 ALTER TABLE `user_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_followers`
--

DROP TABLE IF EXISTS `user_followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_followers` (
  `userFollowersId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `followerUserId` int(11) NOT NULL,
  `followedDate` timestamp NULL DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userFollowersId`),
  KEY `fk_user_followers_users1_idx` (`userId`),
  KEY `fk_user_followers_users2_idx` (`followerUserId`),
  CONSTRAINT `fk_user_followers_users1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_followers_users2` FOREIGN KEY (`followerUserId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_followers`
--

LOCK TABLES `user_followers` WRITE;
/*!40000 ALTER TABLE `user_followers` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_followers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_shipping`
--

DROP TABLE IF EXISTS `user_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shipping` (
  `userShippingId` int(11) NOT NULL AUTO_INCREMENT,
  `shippingId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userShippingId`),
  KEY `fk_shippingCourrierId_idx` (`shippingId`),
  KEY `fk_user_shipping_userId_idx` (`userId`),
  CONSTRAINT `fk_shippingCourrierId` FOREIGN KEY (`shippingId`) REFERENCES `shipping_courier` (`shippingCourierId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_shipping_userId` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_shipping`
--

LOCK TABLES `user_shipping` WRITE;
/*!40000 ALTER TABLE `user_shipping` DISABLE KEYS */;
INSERT INTO `user_shipping` VALUES (5,2,1,'f60f6332-439b-11e8-bdb5-38c98626bd93'),(6,1,1,'94060a32-4f9a-11e8-8822-38c98626bd93');
/*!40000 ALTER TABLE `user_shipping` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tg_user_shipping` BEFORE INSERT ON `user_shipping` FOR EACH ROW
BEGIN
	SET NEW.uuid = UUID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `facebookId` varchar(60) DEFAULT NULL,
  `instagramId` varchar(60) DEFAULT NULL,
  `twitterId` varchar(60) DEFAULT NULL,
  `isVerify` tinyint(4) DEFAULT NULL,
  `currencyId` varchar(45) DEFAULT NULL,
  `countryID` varchar(45) DEFAULT NULL,
  `registrationDate` timestamp NULL DEFAULT NULL,
  `verificationCode` varchar(60) DEFAULT NULL,
  `dateVerify` timestamp NULL DEFAULT NULL,
  `user_img_path` text,
  `user_img_name` text,
  `user_img_type` varchar(45) DEFAULT NULL,
  `user_img_size` int(11) DEFAULT NULL,
  `isSeller` tinyint(4) DEFAULT NULL,
  `sellerApprovalDate` timestamp NULL DEFAULT NULL,
  `sellerApprovalCode` varchar(50) DEFAULT NULL,
  `sellerApplicationDate` timestamp NULL DEFAULT NULL,
  `sellerDescription` text,
  `sellerName` varchar(60) DEFAULT NULL,
  `uuid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'jbagaresgaray','philipgaray2@gmail.com','$2a$08$tWNQ6ayZ9l982uteA12AruvAdi3tGGrbKUHylnJxMprSVcH1uIXpm','Philip','Garay','male','2018-04-06','09394049310','13434535345345',NULL,NULL,NULL,1,'AFN','ASM','2018-02-26 07:10:19','1a2ed8701ac411e8a8dc7d0c2fe95486','2018-02-26 07:10:44',NULL,NULL,NULL,NULL,1,'2018-02-28 05:36:58','73281495174947','2018-02-28 04:30:16',NULL,'jbagaresgaray','1a30a72f-1ac4-11e8-94f9-206a8a764328'),(3,'janinejasmin','janine@yopmail.com','$2a$08$c686yw/i7prgrGLRksU.se217imtvQNvPIRbdZmKVqSKifH5F40/C','Janine','Jasmin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'2018-03-23 05:26:30','bdcb45f02e5a11e8b3ab25cc9af7b9b9','2018-03-23 05:26:55',NULL,NULL,NULL,NULL,1,'2018-03-27 06:11:26','44744455293997','2018-03-23 08:29:22',NULL,'janinejasmin','bdcf0a5a-2e5a-11e8-99af-38c98626bd93');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'shoppy'
--

--
-- Dumping routines for database 'shoppy'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-04 21:28:58
