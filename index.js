'use strict';

process.env.TZ = 'UTC';

require('./config/env')();
var env = process.env.NODE_ENV || 'development';
process.env.NODE_ENV = env;


var application = require('./config/application'),
    express = require('express'),
    bunyan = require('bunyan'),
    mysql = require('mysql'),
    ejwt = require('express-jwt'),
    jwt = require('jsonwebtoken'),
    passport = require('passport'),
    middleware = require('./app/utils/middleware'),
    config = require('./config/environment/' + env),
    Database = require('./app/utils/database').Database,
    db = new Database(mysql, config),
    log = bunyan.createLogger({
        name: 'app_name_here'
    }),
    http = require('http'),
    app = express(),
    server = http.createServer(app);

/* global.io = require('socket.io')(server);
global.io.configure(function () {  
  global.io.set("transports", ["xhr-polling"]); 
  global.io.set("polling duration", 10); 
}); */

var router = express.Router({
    strict: true,
    caseSensitive: true
});


require(application.utils + 'helper')(db, server, config, log);
require(application.config + 'express')(app, config, passport, ejwt);
// require(application.config + 'socket')(io);
require(application.config + 'authentication')(app, config, ejwt);
require(application.config + 'passport')(passport, jwt);
// Routes
require(application.routes + 'documents')(app, middleware);
require(application.routes + '/')(app, passport, config, middleware);
require(application.routes + '/mobile')(app, passport, config, middleware, ejwt);

module.exports = app;