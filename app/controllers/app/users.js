'use strict';

let usersDao = require('../../daos/app/users');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');
let fs = require('fs');
let path = require('path');
let ejs = require('ejs');
let moment = require('moment');

function Users() {
    this.usersDao = usersDao;
}

Users.prototype.IsUserExisted = (sUserName, next) => {
    usersDao.checkUserUsername(sUserName, (err, response) => {
        if (err) {
            next(err, null);
        } else {
            if (!_.isEmpty(response)) {
                next(null, {
                    msg: '',
                    result: response,
                    success: true,
                    IsUserExisted: true
                });
            } else {
                next(null, {
                    msg: '',
                    result: response,
                    success: false,
                    IsUserExisted: false
                });
            }
        }
    });
};

Users.prototype.createUser = (data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUsername(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Username already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            usersDao.checkUserEmail(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Email Address already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            usersDao.createUser(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                        let template = path.join(__dirname, '../..', 'views/emails/confirm.ejs');
                        let ejstemp = {
                            Fullname: (response.firstname + ' ' + response.lastname),
                            Email: response.email,
                            Link: config.api_host_url + '/verify?code=' + response.verificationCode + '&uuid=' + response.uuid + '&action=dash_user'
                        };

                        ejs.renderFile(template, ejstemp, (err, html) => {
                            if (err) {
                                console.log(err); // Handle error
                            }
                            let mailData = {
                                from: 'admin@shoppyapp.net',
                                email: response.email,
                                subject: 'Verify your account for Shoppy Dashboard',
                                message: html,
                                attachment: null
                            };

                            func.sendMail(mailData, (err, mailresponse) => {
                                if (err) {
                                    console.log('err: ', err.body);
                                }
                                console.log('mailresponse: ', mailresponse);
                            });
                        });

                        next(null, {
                            msg: 'Record successfully saved!',
                            result: response,
                            success: true
                        });
                    } else {
                        next(null, {
                            msg: 'Record successfully saved!',
                            result: response,
                            success: true
                        });
                    }
                }
            });
        }
    ]);
};

Users.prototype.getAllUsers = (params, next) => {
    usersDao.getAllUsers(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.getUser = (user_id, next) => {
    usersDao.getUser(user_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            };

            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.deleteUser = (user_id, next) => {
    usersDao.deleteUser(user_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.updateUserAccount = (user_id, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserUsernameForUpdate(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Username already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            usersDao.checkUserEmailForUpdate(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Email Address already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            usersDao.updateUserAccount(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    return next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

Users.prototype.updateUser = (user_id, data, next) => {
    usersDao.updateUser(user_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};


Users.prototype.updateUserEmail = (user_id, data, next) => {
    async.waterfall([
        (callback) => {
            usersDao.checkUserEmailForUpdate(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Email Address already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            usersDao.updateUserEmail(user_id, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    // TODO: Send user email for changes
                    return next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

Users.prototype.checkIfVerified = (hash_key, next) => {
    usersDao.checkIfVerified(hash_key, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                next(null, {
                    msg: 'Your account has already been verified',
                    success: true,
                    result: ''
                });
            } else {
                next(null, {
                    msg: 'Your account has not been verified. Please check your mail for activation link.',
                    success: false,
                    result: ''
                });
            }
        }
    });
};

Users.prototype.verifyUser = (hash_key, next) => {
    usersDao.verifyUser(hash_key, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: 'Your account has successfully activated. Thank you!',
                success: true,
                result: response
            });
        }
    });
};

Users.prototype.forgotPassword = (data, next) => {
    usersDao.getUserByUsernameAndEmail(data.email, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];
                var template = path.join(__dirname, '../..', 'views/emails/forgot.ejs');
                var ejstemp = {
                    Fullname: response.firstname + ' ' + response.lastname,
                    Email: response.email,
                    Link: config.api_host_url + '/resetpassword?code=' + response.verificationCode + '&uuid=' + response.uuid + '&action=dash_user'
                };
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) {
                        console.log(err); // Handle error
                    }
                    var mailData = {
                        from: 'admin@shoppyapp.net',
                        email: response.email,
                        subject: 'Reset your Shoppy Dashboard password',
                        message: html,
                        attachment: null
                    };

                    func.sendMail(mailData, (err, mailresponse) => {
                        if (err) {
                            console.log('err: ', err.body);
                        }
                        console.log('mailresponse: ', mailresponse);
                    });
                });
                next(null, {
                    success: true,
                    msg: 'Forgot Password confirmation has been sent. Kindly check your email for the next step.',
                    result: {
                        Email: response.Email,
                        Fullname: response.Fullname,
                        UserName: response.UserName,
                        EmployeeID: response.EmployeeID,
                        UUID: response.UUID
                    }
                });
            } else {
                next(null, {
                    result: null,
                    msg: 'User does not exist with the given info.',
                    success: false
                });
            }
        }
    });
};

Users.prototype.resetPassword = (data, next) => {
    usersDao.getUserByUUID(data.uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];

                usersDao.updateUserPassword(data, (err1, result) => {
                    if (err1) {
                        return next({
                            result: err1,
                            msg: err1.message,
                            success: true
                        }, null);
                    } else {
                        var template = path.join(__dirname, '../..', 'views/emails/reset.ejs');
                        var ejstemp = {
                            Fullname: response.firstname + ' ' + response.lastname,
                            Email: response.email,
                            Date: moment().format('MM-DD-YYYY hh:mm:ss A Z')
                        };
                        ejs.renderFile(template, ejstemp, (err, html) => {
                            if (err) {
                                console.log(err); // Handle error
                            }
                            var mailData = {
                                from: 'admin@shoppyapp.net',
                                email: response.email,
                                subject: 'Your Shoppy Dashboard password has been changed',
                                message: html,
                                attachment: null
                            };

                            func.sendMail(mailData, (err, mailresponse) => {
                                if (err) {
                                    console.log('err: ', err.body);
                                }
                                console.log('mailresponse: ', mailresponse);
                            });
                        });

                        return next(null, {
                            success: true,
                            msg: 'Password successfully changed.',
                            result: {
                                Email: response.email,
                                Fullname: response.firstname + ' ' + response.lastname,
                            }
                        });
                    }
                });
            } else {
                return next(null, {
                    result: null,
                    msg: 'User does not exist with the given info.',
                    success: false
                });
            }
        }
    });
};

exports.Users = Users;