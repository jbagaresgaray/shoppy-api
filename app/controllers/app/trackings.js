'use strict';

let trackingsDao = require('../../daos/app/trackings');
let async = require('async');

function Trackings() {
    this.trackingsDao = trackingsDao;
}


Trackings.prototype.getTrackingCheckingFlags = (next) => {
    trackingsDao.getTrackingCheckingFlags((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Trackings.prototype.getTrackingShippingTags = (next) => {
    trackingsDao.getTrackingShippingTags((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Trackings.prototype.getAllOrderTrackings = (params, next) => {
    trackingsDao.getAllOrderTrackings(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Trackings.prototype.getOrderTrackingByOrderNum = (orderNum, next) => {
    trackingsDao.getOrderTrackingByOrderNum(orderNum, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }

    });
};

Trackings.prototype.getOrderTrackingByTrackingNum = (slug, tracking_number, next) => {
    trackingsDao.getOrderTrackingByTrackingNum(slug, tracking_number, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Trackings.prototype.updateOrderTracking = (order_trackingId, data, next) => {
    trackingsDao.updateOrderTracking(order_trackingId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

exports.Trackings = Trackings;