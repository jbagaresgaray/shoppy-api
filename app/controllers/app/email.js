'use strict';

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let functions = require('../../../app/utils/functions');
let fs = require('fs');
let mkdirp = require('mkdirp');
let path = require('path');
let ejs = require('ejs');


function Sharer() {}


Sharer.prototype.shareExperience = (data, next) => {
    let objectData = {
        from: data.from,
        to: data.email,
        subject: data.subject,
        html: data.message,
        pdf: data.pdf
    };

    /*let options = {
        format: data.format || 'Letter',
        orientation: data.orientation || 'portrait',
        border: {
            top: '0.5in', // default is 0, units: mm, cm, in, px
            right: '0.5in',
            bottom: '0.5in',
            left: '0.5in'
        },
        zoom: 0.1
    };*/

    let sendMail = function (mailData) {

        let helper = require('sendgrid').mail;
        let from_email = new helper.Email(mailData.from);
        let to_email = new helper.Email(mailData.to);
        let subject = mailData.subject;
        let content = new helper.Content('text/html', mailData.html);


        let mail = new helper.Mail(from_email, subject, to_email, content);

        if (data.pdf) {
            let attachment = new helper.Attachment();
            let file = fs.readFileSync(mailData.attachment);
            let base64File = new Buffer(file).toString('base64');
            attachment.setContent(base64File);
            attachment.setType('application/text');
            attachment.setFilename(mailData.filename);
            attachment.setDisposition('attachment');
            mail.addAttachment(attachment);
        }


        let sg = require('sendgrid')(config.sendgrid_key);
        let request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON(),
        });

        sg.API(request, function (error, response) {
            if (error) {
                next(error, null);
            } else {
                if (response.statusCode === 200 || response.statusCode === 202) {
                    next(null, {
                        result: response.body,
                        msg: 'Email Successfully Sent',
                        success: true
                    });
                } else {
                    next({
                        result: error,
                        msg: error.message,
                        success: false
                    }, null);
                }
            }
        });
    };

    if (data._id) {
        objectData.filename = data._id;
        let filename = objectData.filename + '.pdf';
        objectData.filename = filename;

        let dir = 'public/tmp';

        if (!functions.existsSync(dir)) {
            mkdirp(dir, function (err) {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: false
                    }, null);
                }
            });
        }

        /*pdf.create(objectData.pdf, options).toFile(dir + '/' + filename, function(err, res) {
            if (err) {
                console.log('err: ', err);
                next({
                    result: err,
                    msg: err.message,
                    success: false
                }, null);
            }

            objectData.attachment = res.filename;
            sendMail(objectData);
        });*/
        sendMail(objectData);
    } else {
        sendMail(objectData);
    }
};

Sharer.prototype.sendMail = (data, next) => {
    let request = require('request');
    let objectData = {};

    if (data.attachment) {
        let file = request(data.attachment);
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: file
        };

    } else {
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: null
        };
    }

    let helper = require('sendgrid').mail;
    let from_email = new helper.Email(objectData.from);
    let to_email = new helper.Email(objectData.to);
    let subject = objectData.subject;
    let content = new helper.Content('text/html', objectData.html);

    let mail = new helper.Mail(from_email, subject, to_email, content);

    if (objectData.attachment) {
        let attachment = new helper.Attachment();
        let file1 = fs.readFileSync(objectData.attachment);
        let base64File = new Buffer(file1).toString('base64');
        attachment.setContent(base64File);
        attachment.setType('application/text');
        attachment.setFilename(objectData.filename);
        attachment.setDisposition('attachment');
        mail.addAttachment(attachment);
    }

    let sg = require('sendgrid')(config.sendgrid_key);
    let request1 = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(request1, function (error, response) {
        if (error) {
            next(error, null);
        } else {
            if (response.statusCode === 200 || response.statusCode === 202) {
                next(null, {
                    result: response.body,
                    msg: 'Email Successfully Sent',
                    success: true
                });
            } else {
                next({
                    result: error,
                    msg: error.message,
                    success: false
                }, null);
            }
        }
    });
};

Sharer.prototype.sendTestMail = (data, next) => {
    let request = require('request');
    let template = null;
    let mailData = {};
    console.log('data: ', data);

    switch (data.action) {
        case 'confirm':
            template = path.join(__dirname, '../..', 'views/emails/confirm.ejs');
            break;
        case 'forgot':
            template = path.join(__dirname, '../..', 'views/emails/forgot.ejs');
            break;
        default:
            break;
    }
    mailData.from = 'sampleuser@shoppy.net';
    mailData.email = data.email;
    mailData.subject = 'Test mail';

    console.log('template: ', template);
    ejs.renderFile(template, {}, (err, html) => {
        if (err) {
            console.log(err); // Handle error
        }
        mailData.message = html;

        let helper = require('sendgrid').mail;
        let from_email = new helper.Email(mailData.from);
        let to_email = new helper.Email(mailData.email);
        let subject = mailData.subject;
        let content = new helper.Content('text/html', mailData.message);
        let mail = new helper.Mail(from_email, subject, to_email, content);

        let sg = require('sendgrid')(config.sendgrid_key);
        let request1 = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON(),
        });

        sg.API(request1, function (error, response) {
            if (error) {
                next(error, null);
            } else {
                if (response.statusCode === 200 || response.statusCode === 202) {
                    next(null, {
                        result: response.body,
                        msg: 'Email Successfully Sent',
                        success: true
                    });
                } else {
                    next({
                        result: error,
                        msg: error.message,
                        success: false
                    }, null);
                }
            }
        });
    });
};

exports.Sharer = Sharer;