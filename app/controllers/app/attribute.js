'use strict';

let attributeDao = require('../../daos/app/attribute');
let async = require('async');

function Attribute() {
    this.attributeDao = attributeDao;
}


Attribute.prototype.createAttribute = (data, next) => {
    async.waterfall([
        (callback) => {
            attributeDao.checkAttribute(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Attribute already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            attributeDao.createAttribute(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

Attribute.prototype.getAllAttribute = (params, next) => {
    attributeDao.getAllAttribute(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Attribute.prototype.getAttribute = (_id, next) => {
    attributeDao.getAttribute(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Attribute.prototype.deleteAttribute = (_id, next) => {
    attributeDao.deleteAttribute(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Attribute.prototype.updateAttribute = (_id, data, next) => {
    async.waterfall([
        (callback) => {
            attributeDao.checkAttributeforUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Attribute already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            attributeDao.updateAttribute(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

exports.Attribute = Attribute;