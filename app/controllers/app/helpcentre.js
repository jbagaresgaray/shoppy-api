'use strict';

let helpDao = require('../../daos/app/helpcentre');
let async = require('async');

function Help() {
    this.helpDao = helpDao;
}

Help.prototype.getAllHelpCategory = (next) => {
    helpDao.getAllHelpCategory((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Help.prototype.createHelpPost = (user, data, next) => {
    // body... 
    helpDao.createHelpPost(user, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Post successfully created!',
                result: response,
                success: true
            });
        }
    });
};

Help.prototype.getAllHelpPosts = (next) => {
    helpDao.getAllHelpPosts((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Help.prototype.getPostByPostID = (postId, next) => {
    helpDao.getPostByPostID(postId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Help.prototype.getAllHelpByCategory = (categoryId, next) => {
    helpDao.getAllHelpByCategory(categoryId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

exports.Help = Help;