'use strict';

let courierDao = require('../../daos/app/courier');
let async = require('async');

function Courier() {
    this.courierDao = courierDao;
}


Courier.prototype.createCourier = (data, next) => {
    async.waterfall([
        (callback) => {
            courierDao.checkCourier(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Shipping Courier already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            courierDao.createCourier(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

Courier.prototype.getAllCourier = (params, next) => {
    courierDao.getAllCourier(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Courier.prototype.getCourier = (_id, next) => {
    courierDao.getCourier(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Courier.prototype.deleteCourier = (_id, next) => {
    courierDao.deleteCourier(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Courier.prototype.updateCourier = (_id, data, next) => {
    async.waterfall([
        (callback) => {
            courierDao.checkCourierforUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Shipping Courier already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            courierDao.updateCourier(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

exports.Courier = Courier;