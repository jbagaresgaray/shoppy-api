'use strict';

let shopDao = require('../../daos/app/officialshop');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const sharp = require('sharp');
const fs = require("fs");
let path = require('path');

function OfficialShop() {
    this.shopDao = shopDao;
}

OfficialShop.prototype.validateSellerCode = (data, next) => {
    shopDao.validateSellerCode(data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (!_.isEmpty(response)) {
                response.action = 'user_seller';
                return next(null, {
                    msg: 'Validate successfully',
                    result: response,
                    success: true
                });
            } else {
                return next(null, {
                    msg: 'Invalid Application details!',
                    result: null,
                    success: false
                });
            }
        }
    });
};

OfficialShop.prototype.validateSellerCodePromise = (data, next) => {
    shopDao.validateSellerCodePromise(data).then((result) => {
        return next(null, result);
    }).catch((error) => {
        return next(error, null);
    });
};

OfficialShop.prototype.getApplicationInfo = (params, next) => {
    let data = {
        code: params.code,
        token: params.token,
    };
    shopDao.getApplicationInfo(data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            delete response.shop_ref_code;
            delete response.shop_url_code;
            delete response.shop_url_token;
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

OfficialShop.prototype.saveApplicationInfo = (data, next) => {
    shopDao.saveApplicationInfo(data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            console.log('controller saveApplicationInfo: ', response);
            if (response && response != 0) {
                next(null, {
                    msg: 'Record successfully saved',
                    result: response,
                    success: true
                });
            } else {
                next(null, {
                    msg: 'Error while saving the record!',
                    result: response,
                    success: false
                });
            }
        }
    });
};

OfficialShop.prototype.deleteCORFile = (data, next) => {
    async.waterfall([
        (callback) => {
            shopDao.deleteCORFile({
                urlcode: data.code,
                refcode: data.ref_code,
                userId: data.userId
            }, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            shopDao.emptyCORFile({
                urlcode: data.code,
                refcode: data.ref_code,
                userId: data.userId
            }, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response != 0) {
                        callback(null, {
                            msg: 'Record successfully deleted!',
                            result: response,
                            success: true
                        });
                    } else {
                        callback(null, {
                            msg: 'Error while deleting the record!',
                            result: response,
                            success: false
                        });
                    }
                }
            });
        }
    ], next);
};

OfficialShop.prototype.uploadCORFile = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let errResponse = (error) => {
        console.error('Dropbox Error: ', error);
        return next({
            msg: err.message,
            result: err,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            shopDao.deleteCORFile(data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('deleteCORFile: ', response);
                    callback();
                }
            });
        },
        (callback) => {
            dbx.filesUpload({
                    path: '/' + env + '/officialshops/cor/' + data.img_name,
                    contents: fs.readFileSync(data.img_path)
                }).then((response) => {
                    console.log('filesUpload: ', response);
                    dbx.sharingCreateSharedLink({
                        path: response.path_display,
                    }).then((resp) => {
                        data.img_name = response.path_display;
                        data.img_path = resp.url.replace('dl=0', 'raw=1');
                        shopDao.uploadCORFile(data, (err, response) => {
                            console.log('err: ', err);
                            console.log('response: ', response);
                            if (err) {
                                return next({
                                    msg: err.msg,
                                    result: err,
                                    success: false
                                }, null)
                            } else {
                                callback(null, {
                                    msg: 'Record successfully saved!',
                                    result: response,
                                    success: true
                                });
                            }
                        });
                    }).catch(errResponse);
                })
                .catch(errResponse);
        }
    ], next);
};

OfficialShop.prototype.deleteBusinessLicenseFile = (data, next) => {
    async.waterfall([
        (callback) => {
            shopDao.deleteBusinessLicenseFile({
                urlcode: data.code,
                refcode: data.ref_code,
                userId: data.userId
            }, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            shopDao.emptyBusinessLicenseFile({
                urlcode: data.code,
                refcode: data.ref_code,
                userId: data.userId
            }, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response != 0) {
                        callback(null, {
                            msg: 'Record successfully deleted!',
                            result: response,
                            success: true
                        });
                    } else {
                        callback(null, {
                            msg: 'Error while deleting the record!',
                            result: response,
                            success: false
                        });
                    }
                }
            });
        }
    ], next);
};

OfficialShop.prototype.uploadBusinessLicenseFile = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let errResponse = (error) => {
        console.error('Dropbox Error: ', error);
        return next({
            msg: err.message,
            result: err,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            shopDao.deleteBusinessLicenseFile(data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('deleteFile: ', response);
                    if (response) {
                        callback();
                    } else {}
                }
            });
        },
        (callback) => {
            dbx.filesUpload({
                    path: '/' + env + '/officialshops/license/' + data.img_name,
                    contents: fs.readFileSync(data.img_path)
                }).then((response) => {
                    console.log('filesUpload: ', response);
                    dbx.sharingCreateSharedLink({
                        path: response.path_display,
                    }).then((resp) => {
                        data.img_name = response.path_display;
                        data.img_path = resp.url.replace('dl=0', 'raw=1');

                        shopDao.uploadBusinessLicenseFile(data, (err, response) => {
                            console.log('err: ', err);
                            console.log('response: ', response);
                            if (err) {
                                return next({
                                    msg: err.msg,
                                    result: err,
                                    success: false
                                }, null)
                            } else {
                                callback(null, {
                                    msg: 'Record successfully saved!',
                                    result: response,
                                    success: true
                                });
                            }
                        });
                    }).catch(errResponse);
                })
                .catch(errResponse);
        }
    ], next);
};

OfficialShop.prototype.deleteBusinessLogoFile = (data, next) => {
    async.waterfall([
        (callback) => {
            shopDao.deleteBusinessLogoFile({
                urlcode: data.code,
                refcode: data.ref_code,
                userId: data.userId
            }, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            shopDao.emptyBusinessLogoFile({
                urlcode: data.code,
                refcode: data.ref_code,
                userId: data.userId
            }, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response != 0) {
                        callback(null, {
                            msg: 'Record successfully deleted!',
                            result: response,
                            success: true
                        });
                    } else {
                        callback(null, {
                            msg: 'Error while deleting the record!',
                            result: response,
                            success: false
                        });
                    }
                }

            })
        }
    ], next);
};

OfficialShop.prototype.uploadBusinessLogoFile = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let errResponse = (error) => {
        console.error('Dropbox Error: ', error);
        return next({
            msg: err.message,
            result: err,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            shopDao.deleteBusinessLogoFile(data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('deleteFile: ', response);
                    if (response) {
                        callback();
                    } else {}
                }
            });
        },
        (callback) => {
            sharp(data.img_path)
                .resize(800, 800)
                .max()
                .toBuffer()
                .then(resp => {
                    console.log('uploadCORFile resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/officialshops/logo/' + data.img_name,
                            contents: resp
                        }).then((response) => {
                            console.log('filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                data.img_name = response.path_display;
                                data.img_path = resp.url.replace('dl=0', 'raw=1');

                                shopDao.uploadBusinessLogoFile(data, (err, response) => {
                                    console.log('err: ', err);
                                    console.log('response: ', response);
                                    if (err) {
                                        return next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null)
                                    } else {
                                        return callback(null, {
                                            msg: 'Record successfully saved!',
                                            result: response,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ], next)
};

OfficialShop.prototype.getApplicationList = (params, next) => {
    shopDao.getApplicationList(params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            return next(null, response);
        }
    });
};

OfficialShop.prototype.getApplicationDetail = (shopId, next) => {
    shopDao.getApplicationDetail(shopId, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

OfficialShop.prototype.getApplicationEvaluationList = (params, next) => {
    shopDao.getApplicationEvaluationList(params, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            return next(null, response);
        }
    });
};

OfficialShop.prototype.updateApplicationEvaluation = (data, next) => {
    shopDao.updateApplicationEvaluation(data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            var summary = htmlToText.fromString(response.notification.content);
            func.sendNotifications(config.app_name, summary, response.notification.notification_data, response.notification.devices, (err, resp) => {
                console.log('sendNotifications Err: ', err);
                if (!err && resp) {
                    console.log('sendNotifications Response: ', resp);
                }
            });

            return next(null, {
                msg: 'Evaluation logged successfully!',
                result: response,
                success: true
            });
        }
    });
};

OfficialShop.prototype.approveOfficialShopApplication = (user, data, next) => {
    let htmlToText = require('html-to-text');
    shopDao.approveOfficialShopApplication(user, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            console.log("approveOfficialShopApplication: ", response);
            // ================================================================================
            // Push Notification Order Updates COD Order Approval - Buyer
            // ================================================================================
            var summary = htmlToText.fromString(response.notification.content);
            func.sendNotifications(config.app_name, summary, response.notification.notification_data, response.notification.devices, (err, resp) => {
                console.log('sendNotifications Err: ', err);
                if (!err && resp) {
                    console.log('sendNotifications Response: ', resp);
                }
            });

            // ================================================================================
            // SEND EMAIL TO NOTIFY SELLER ABOUT THE PRODUCT ENTRY
            // ================================================================================
            let template = path.join(__dirname, '../..', 'views/emails/mobile/officialshop_approval.ejs');
            func.sendMailTemplate(template, {
                shopName: response.officialshop.shop_name
            }, 'info@shoppyapp.net', response.officialshop.shop_email, 'Your Official Shop Application for ' + response.officialshop.shop_name + ' has been approved. WELCOME TO SHOPPYAPP OFFICIAL SHOP');

            return next(null, {
                msg: 'Evaluation logged successfully!',
                result: response,
                success: true
            });
        }
    });
};

exports.OfficialShop = OfficialShop;