'use strict';

let bankDao = require('../../daos/app/banks');
let async = require('async');

function Banks() {
    this.bankDao = bankDao;
}


Banks.prototype.createBank = (data, next) => {
    async.waterfall([
        (callback) => {
            bankDao.checkBank(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Bank already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            bankDao.createBank(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

Banks.prototype.getAllBank = (params, next) => {
    bankDao.getAllBank(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Banks.prototype.getBank = (_id, next) => {
    bankDao.getBank(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Banks.prototype.deleteBank = (_id, next) => {
    bankDao.deleteBank(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Banks.prototype.updateBank = (_id, data, next) => {
    async.waterfall([
        (callback) => {
            bankDao.checkBankforUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Bank already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            bankDao.updateBank(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

exports.Banks = Banks;