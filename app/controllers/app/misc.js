'use strict';

let miscDao = require('../../daos/app/misc');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let func = require('../../../app/utils/functions');
let async = require('async');
let _ = require('lodash');
let OneSignal = require('onesignal-node');


function Misc() {
    this.miscDao = miscDao;
}

Misc.prototype.getAllTimezones = (next) => {
    miscDao.getAllTimezones((err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            next(null, {
                result: response,
                msg: '',
                success: true
            });
        }
    });
};

Misc.prototype.getAllCountry = (next) => {
    miscDao.getAllCountry((err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            next(null, {
                result: response,
                msg: '',
                success: true
            });
        }
    });
};

Misc.prototype.getAllCurrencies = (next) => {
    miscDao.getAllCurrencies((err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            let currencies = [];
            var arr = _.mapKeys(response, (value, key) => {
                value.id = key;
                currencies.push(value);
            });

            next(null, {
                result: currencies,
                msg: '',
                success: true
            });
        }
    });
};


Misc.prototype.testUpload = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });
    dbx.filesUpload({
            path: '/testfolder/' + data.img_name,
            contents: data.content
        }).then(function(response) {
            console.log('filesUpload: ', response);
            next(null, {
                msg: 'File successfully uploaded',
                result: response,
                success: true
            });
        })
        .catch(function(error) {
            console.error('filesUpload error: ', error);
            next({
                msg: 'File upload error',
                result: error,
                success: false
            }, null);
        });
};

Misc.prototype.getAllUploads = (next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });
    dbx.filesListFolder({
            path: '/testfolder'
        })
        .then(function(response) {
            console.log('filesListFolder: ', response);

            async.each(response.entries, (item, callback) => {
                console.log('item: ', item);
                dbx.filesGetTemporaryLink({
                        path: item.path_display
                    })
                    .then(function(resp) {
                        item.link = resp.link;
                        callback();
                    });
            }, () => {
                next(null, {
                    msg: '',
                    result: response,
                    success: true
                });
            });
        })
        .catch(function(error) {
            console.log('filesListFolder error: ', error);
            next({
                msg: 'Dropbox error',
                result: error,
                success: false
            }, null);
        });
};

Misc.prototype.previewFileUpload = (file, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });
    dbx.filesGetTemporaryLink({
            path: '/testfolder/1519639256_Capture2.PNG'
        })
        .then(function(response) {
            console.log('filesGetPreview: ', response);
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        })
        .catch(function(error) {
            console.log('filesGetPreview error: ', error);
            next({
                msg: 'Dropbox error',
                result: error,
                success: false
            }, null);
        });
};

Misc.prototype.testPushNotif = (data, next) => {
    var myClient = new OneSignal.Client({
        userAuthKey: config.ONESIGNAL_USER_KEY,
        app: { appAuthKey: config.ONESIGNAL_API_KEY, appId: config.ONESIGNAL_APP_ID }
    });

    var firstNotification = new OneSignal.Notification({
        contents: {
            en: data.content
        }
    });

    // set target users
    firstNotification.setIncludedSegments(['All']);
    firstNotification.setExcludedSegments(['Inactive Users']);
    // set notification parameters
    firstNotification.setParameter('headings', { "en": data.title });
    firstNotification.setParameter('data', { "tags": data.tag });

    // send this notification to All Users except Inactive ones
    console.log('firstNotification: ', firstNotification);
    myClient.sendNotification(firstNotification, (err, httpResponse, data) => {
        if (err) {
            console.log('Something went wrong...');
            next({
                msg: 'OneSignal error',
                result: err,
                success: false
            }, null);
        } else {
            console.log('sendNotification: ', data, httpResponse.statusCode);
            next(null, {
                msg: 'Notification successfully sent',
                result: data,
                success: true
            });
        }
    });
};

Misc.prototype.getAllNotifications = (next) => {
    var myClient = new OneSignal.Client({
        userAuthKey: config.ONESIGNAL_USER_KEY,
        app: { appAuthKey: config.ONESIGNAL_API_KEY, appId: config.ONESIGNAL_APP_ID }
    });

    myClient.viewNotifications(null, function(err, httpResponse, data) {
        if (err) {
            console.log('Something went wrong...');
            next({
                msg: 'OneSignal error',
                result: err,
                success: false
            }, null);
        } else {
            if (httpResponse.statusCode === 200 && !err) {
                next(null, {
                    msg: '',
                    result: JSON.parse(data),
                    success: true
                });
            }
        }
    });
};

Misc.prototype.getNotification = (notificationId, next) => {
    var myClient = new OneSignal.Client({
        userAuthKey: config.ONESIGNAL_USER_KEY,
        app: { appAuthKey: config.ONESIGNAL_API_KEY, appId: config.ONESIGNAL_APP_ID }
    });

    myClient.viewNotification(notificationId, function(err, httpResponse, data) {
        if (err) {
            console.log('Something went wrong...');
            next({
                msg: 'OneSignal error',
                result: err,
                success: false
            }, null);
        } else {
            if (httpResponse.statusCode === 200 && !err) {
                next(null, {
                  msg: "",
                  result: JSON.parse(data),
                  success: true
                });
            }
        }
    });
};

Misc.prototype.deleteNotification = (notificationId,next)=>{
    console.log("deleteNotification 2");
    var myClient = new OneSignal.Client({
      userAuthKey: config.ONESIGNAL_USER_KEY,
      app: {
        appAuthKey: config.ONESIGNAL_API_KEY,
        appId: config.ONESIGNAL_APP_ID
      }
    });

    myClient.cancelNotification(notificationId, function (err, httpResponse, data) {
        if (err) {
            next({ msg: "OneSignal error", result: err, success: false }, null);
        } else {
            console.log("httpResponse: ", httpResponse.statusCode);
            console.log("cancelNotification: ",data);
            if (httpResponse.statusCode === 200 && !err) {
                let resp = JSON.parse(data);
                if (resp && resp.errors) {
                    next(null, {
                        msg: resp.errors[0],
                        result: resp,
                        success: false
                    });
                } else {
                    next(null, {
                        msg: '',
                        result: resp,
                        success: true
                    });
                }
            } else if (httpResponse.statusCode === 400 && !err) {
                let resp = JSON.parse(data);
                next(null, {
                    msg: resp.errors[0],
                    result: resp,
                    success: false
                });
            }
        }
        
    })
};

Misc.prototype.getAllDevices = (next) => {
    var myClient = new OneSignal.Client({
        userAuthKey: config.ONESIGNAL_USER_KEY,
        app: { appAuthKey: config.ONESIGNAL_API_KEY, appId: config.ONESIGNAL_APP_ID }
    });

    myClient.viewDevices('limit=100&offset=0', function(err, httpResponse, data) {
        if (err) {
            console.log('Something went wrong...');
            next({
                msg: 'OneSignal error',
                result: err,
                success: false
            }, null);
        } else {
            if (httpResponse.statusCode === 200 && !err) {
                let resp = JSON.parse(data);
                if (resp && resp.errors){
                    next(null, {
                      msg: resp.errors[0],
                      result: resp,
                      success: false
                    });
                }else{
                    next(null, {
                        msg: '',
                        result: resp,
                        success: true
                    });
                }
            }
        }
    });
};

exports.Misc = Misc;