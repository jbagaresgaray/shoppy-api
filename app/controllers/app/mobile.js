'use strict';

let dao = require('../../daos/app/mobile');
let async = require('async');
let _ = require('lodash');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

function Mobile() {
    this.dao = dao;
}


Mobile.prototype.createMobileBanner = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            dao.checkMobileBanner(data.name, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Mobile Banner name already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.toDeleteFile) {
                let errorResponse = (error) => {
                    console.error('Dropbox Error: ', error);
                    next({
                        msg: error.message,
                        result: error,
                        success: false
                    }, null)
                }

                dbx.filesUpload({
                        path: '/' + env + '/banners/' + data.img_name,
                        contents: data.content
                    }).then((response) => {
                        console.log('filesUpload: ', response);
                        dbx.sharingCreateSharedLink({
                            path: response.path_display,
                        }).then((resp) => {
                            data.img_name = response.path_display;
                            data.img_path = resp.url.replace('dl=0', 'raw=1');;
                            dao.createMobileBanner(data, (err, response) => {
                                if (err) {
                                    next({
                                        msg: err.msg,
                                        result: err,
                                        success: false
                                    }, null)
                                } else {
                                    next(null, {
                                        msg: 'Record successfully saved!',
                                        result: response,
                                        success: true
                                    });
                                }
                            });
                        }).catch(errorResponse);
                    })
                    .catch(errorResponse);
            } else {
                dao.createMobileBanner(data, (err, response) => {
                    if (err) {
                        next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully saved!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

Mobile.prototype.updateMobileBanner = (_id, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            dao.checkMobileBannerUpdate(_id, data.name, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Mobile Banner name already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.toDeleteFile) {
                let errorResponse = (error) => {
                    console.error('Dropbox Error: ', error);
                    next({
                        msg: error.message,
                        result: error,
                        success: false
                    }, null)
                }

                dbx.filesUpload({
                        path: '/' + env + '/banners/' + data.img_name,
                        contents: data.content
                    }).then((response) => {
                        console.log('filesUpload: ', response);
                        dbx.sharingCreateSharedLink({
                            path: response.path_display,
                        }).then((resp) => {
                            data.img_name = response.path_display;
                            data.img_path = resp.url.replace('dl=0', 'raw=1');;
                            dao.updateMobileBanner(_id, data, (err, response) => {
                                if (err) {
                                    next({
                                        msg: err.msg,
                                        result: err,
                                        success: false
                                    }, null)
                                } else {
                                    next(null, {
                                        msg: 'Record successfully updated!',
                                        result: response,
                                        success: true
                                    });
                                }
                            });
                        }).catch(errorResponse);
                    })
                    .catch(errorResponse);
            } else {
                dao.updateMobileBanner(_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully updated!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

Mobile.prototype.getBanner = (bannerId, next) => {
    dao.getBanner(bannerId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.getAllMobileBanners = (next) => {
    dao.getAllMobileBanners((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.getAllActiveMobileBanners = (next) => {
    dao.getAllActiveMobileBanners((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.deleteMobileBanner = (_id, next) => {
    dao.deleteMobileBanner(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.publishBanner = (_id, next) => {
    dao.publishBanner(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Banner successfully published!',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.unpublishBanner = (_id, next) => {
    dao.unpublishBanner(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Banner successfully unpublished!',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.createMobileCMS = (user, data, next) => {
    dao.createMobileCMS(user, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.getAllMobileCMS = (next) => {
    dao.getAllMobileCMS((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.getMobileCMS = (cmsId, next) => {
    dao.getMobileCMS(cmsId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.deleteMobileCMS = (cmsId, next) => {
    dao.deleteMobileCMS(cmsId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Mobile.prototype.updateMobileCMS = (cmsId, data, next) => {
    // dao.deleteMobileCMS(cmsId, data, (err, response) => { //this is the previous, 
    dao.updateMobileCMS(cmsId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};



exports.Mobile = Mobile;