'use strict';

let brandsDao = require('../../daos/app/brands');

let async = require('async');
const sharp = require('sharp');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

function Brands() {
    this.brandsDao = brandsDao;
}


Brands.prototype.createBrand = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            brandsDao.checkBrandName(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Brand already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.img_path) {
                let errResponse = (error) => {
                    console.error('Dropbox Error: ', error);
                    callback({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }


                sharp(data.img_path)
                    .resize(800, 800)
                    .max()
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadBrandImage resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/brands/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                dbx.sharingCreateSharedLink({
                                    path: response.path_display,
                                }).then((resp) => {
                                    data.img_name = response.path_display;
                                    data.img_path = resp.url.replace('dl=0', 'raw=1');
                                    brandsDao.createBrand(data, (err, response) => {
                                        if (err) {
                                            next({
                                                msg: err.msg,
                                                result: err,
                                                success: false
                                            }, null)
                                        } else {
                                            next(null, {
                                                msg: 'Record successfully saved!',
                                                result: response,
                                                success: true
                                            });
                                        }
                                    });
                                }).catch(errResponse);
                            })
                            .catch(errResponse);
                    });
            } else {
                brandsDao.createBrand(data, (err, response) => {
                    if (err) {
                        next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully saved!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

Brands.prototype.getAllBrand = (params, next) => {
    brandsDao.getAllBrand(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Brands.prototype.getBrand = (_id, next) => {
    brandsDao.getBrand(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Brands.prototype.deleteBrand = (_id, next) => {
    brandsDao.deleteBrand(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Brands.prototype.updateBrand = (_id, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            brandsDao.checkBrandForUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Brand already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.toDeleteFile) {
                let errResponse = (error) => {
                    console.error('Dropbox Error: ', error);
                    callback({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null);
                }

                sharp(data.img_path)
                    .resize(800, 800)
                    .max()
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadBrandImage resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/brands/' + _id + '/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                dbx.sharingCreateSharedLink({
                                    path: response.path_display,
                                }).then((resp) => {
                                    data.img_name = response.path_display;
                                    data.img_path = resp.url.replace('dl=0', 'raw=1');
                                    brandsDao.updateBrand(_id, data, (err, response) => {
                                        if (err) {
                                            next({
                                                msg: err.msg,
                                                result: err,
                                                success: false
                                            }, null)
                                        } else {
                                            next(null, {
                                                msg: 'Record successfully updated!',
                                                result: response,
                                                success: true
                                            });
                                        }
                                    });
                                }).catch(errResponse);
                            })
                            .catch(errResponse);
                    });
            } else {
                brandsDao.updateBrand(_id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully updated!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

Brands.prototype.getAllBrandProducts = (brandId, params, next) => {
    brandsDao.getAllBrandProducts(brandId, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

exports.Brands = Brands;