'use strict';

let dao = require('../../daos/app/newsletter');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);


let async = require('async');
let _ = require('lodash');
let path = require('path');
let ejs = require('ejs');
let func = require('../../../app/utils/functions');


function CMS() {
    this.dao = dao;
}

CMS.prototype.createNewsletter = (user, data, next) => {
    dao.createNewsletter(user, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

CMS.prototype.getAllNewsletter = (next) => {
    dao.getAllNewsletter((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

CMS.prototype.getNewsLetter = (cmsId, next) => {
    console.log('getNewsletter: 2');
    dao.getNewsLetter(cmsId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

CMS.prototype.deleteNewsletter = (cmsId, next) => {
    dao.deleteNewsletter(cmsId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

CMS.prototype.updateNewsletter = (cmsId, user, data, next) => {
    dao.updateNewsletter(cmsId, user, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

CMS.prototype.publishedNewsletter = (_id, next) => {
    async.waterfall([
        (callback) => {
            dao.isPublished(_id, (err, response) => {
                if (err) {
                    next(err, null);
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                        return next(null, {
                            msg: 'Newsletter already published!',
                            result: response,
                            success: false
                        })
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.publishedNewsletter(_id, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Newsletter successfully published!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ], next);
};

CMS.prototype.unpublishNewsletter = (_id, next) => {
    dao.unpublishNewsletter(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Banner successfully unpublished!',
                result: response,
                success: true
            });
        }
    });
};

CMS.prototype.publishedPushNewsletter = (_id, next) => {
    let htmlToText = require('html-to-text');
    dao.notifyNewsletter(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            console.log('response: ', response.notifications);
            var summary = htmlToText.fromString(response.notifications.content);
            let devices = _.map(response.notifications.user_notifications, (row) => {
                return row.user_player_ids;
            });
            func.sendNotifications(response.notifications.title, summary, response.notifications.notification_data, devices, (err, resp) => {
                if (err) {
                    console.log('Something went wrong...');
                    console.log('sendNotifications Err: ', err);
                    next({
                        msg: 'OneSignal error',
                        result: err,
                        success: false
                    }, null);
                } else {
                    console.log('sendNotifications Response: ', resp);
                    next(null, {
                        msg: 'Newsletter successfully published!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    });
};

CMS.prototype.publishedMailNewsletter = (_id, next) => {
    let helper = require('sendgrid').mail;
    let sg = require('sendgrid')(config.sendgrid_key);

    dao.notifyNewsletter(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            console.log('response: ', response.newsletter);
            console.log('response: ', response.users);

            let template = path.join(__dirname, '../..', 'views/emails/newsletter.ejs');
            let ejstemp = {
                title: response.newsletter.title,
                content: response.newsletter.content,
            };
            ejs.renderFile(template, ejstemp, (err, html) => {
                if (err) {
                    console.log(err); // Handle error
                }

                let from_email = new helper.Email('info@shoppyapp.net');
                let subject = response.newsletter.title;
                let content = new helper.Content('text/html', html);
                let mailBody = {
                    personalizations: _.map(response.users, (row) => {
                        return {
                            to: [new helper.Email(row.email)],
                            subject: subject
                        };
                    }),
                    from: from_email,
                    content: [content]
                };

                let request1 = sg.emptyRequest({
                    method: 'POST',
                    path: '/v3/mail/send',
                    body: mailBody,
                });
                sg.API(request1, function(error, response) {
                    if (error) {
                        next(error, null);
                    } else {
                        if (response.statusCode === 200 || response.statusCode === 202) {
                            next(null, {
                                result: response.body,
                                msg: 'Email Successfully Sent',
                                success: true
                            });
                        } else {
                            next({
                                result: error,
                                msg: error.message,
                                success: false
                            }, null);
                        }
                    }
                });
            });
        }
    });
};

CMS.prototype.getAllNotificationType = (next) => {
    dao.getAllNotificationType((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

exports.CMS = CMS;