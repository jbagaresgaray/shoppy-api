'use strict';

let subCategoryDao = require('../../daos/app/subcategory');

let async = require('async');
const sharp = require('sharp');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);


function SubCategory() {
    this.subCategoryDao = subCategoryDao;
}


SubCategory.prototype.createCategory = (categoryId, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            subCategoryDao.checkCategoryName(categoryId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    console.log('response after checkCategoryName ', response);
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Category name already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            data.categoryId = categoryId;

            if (data.toDeleteFile) {
                sharp(data.img_path)
                    .resize(800, 800)
                    .max()
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadCategoryImage resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/subcategory/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                data.img_path = response.path_display;
                                subCategoryDao.createCategory(data, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null)
                                    } else {
                                        next(null, {
                                            msg: 'Record successfully saved!',
                                            result: response,
                                            success: true
                                        });
                                    }
                                });
                            })
                            .catch((error) => {
                                console.error('Dropbox Error: ', error);
                                // callback();
                                callback({
                                    msg: err.message,
                                    result: err,
                                    success: false
                                }, null)
                            });
                    });
            } else {
                subCategoryDao.createCategory(data, (err, response) => {
                    if (err) {
                        next({
                            msg: response,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully saved!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

SubCategory.prototype.getAllCatetories = (categoryId, params, next) => {
    subCategoryDao.getAllCatetories(categoryId, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

SubCategory.prototype.getCategory = (categoryId, _id, next) => {
    require("isomorphic-fetch");
    var Dropbox = require("dropbox").Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    subCategoryDao.getCategory(categoryId, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];

                if (response.img_path) {
                    dbx.sharingCreateSharedLink({
                        path: response.img_path,
                    }).then((resp) => {
                        console.log('category sharedlink: ', resp.url);
                        response.img_public_path = resp.url.replace("dl=0", "raw=1");
                    }).catch((error) => {
                        console.error('DropBox error: ', error);
                        response.img_public_path = '';
                    }).then(() => {
                        next(null, {
                            msg: '',
                            result: response,
                            success: true
                        });
                    });
                } else {
                    response.img_public_path = "";
                    next(null, {
                        msg: '',
                        result: response,
                        success: true
                    });
                }
            } else {
                next(null, {
                    msg: '',
                    result: response,
                    success: true
                });
            }
        }
    });
};

SubCategory.prototype.deleteCategory = (categoryId, _id, next) => {
    subCategoryDao.deleteCategory(categoryId, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

SubCategory.prototype.updateCategory = (categoryId, _id, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            subCategoryDao.checkCategoryForUpdate(categoryId, _id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Category name already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.toDeleteFile) {
                sharp(data.img_path)
                    .resize(800, 800)
                    .max()
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadCategoryImage resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/subcategory/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                data.img_path = response.path_display;
                                subCategoryDao.updateCategory(categoryId, _id, data, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null)
                                    } else {
                                        next(null, {
                                            msg: 'Record successfully updated!',
                                            result: response,
                                            success: true
                                        });
                                    }
                                });
                            })
                            .catch((error) => {
                                console.error('Dropbox Error: ', error);
                                // callback();
                                callback({
                                    msg: err.message,
                                    result: err,
                                    success: false
                                }, null)
                            });
                    });
            } else {
                subCategoryDao.updateCategory(categoryId, _id, data, (err, response) => {
                    if (err) {
                        next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully updated!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

SubCategory.prototype.getAllSubCategoryProducts = (categoryId, _id, params, next) => {
    subCategoryDao.getAllSubCategoryProducts(categoryId, _id, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

exports.SubCategory = SubCategory;