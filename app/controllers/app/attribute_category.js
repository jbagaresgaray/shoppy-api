'use strict';

let attributeCategoryDao = require('../../daos/app/attribute_category');
let async = require('async');

function AttributeCategory() {
    this.attributeCategoryDao = attributeCategoryDao;
}


AttributeCategory.prototype.createAttributeCategory = (categoryId, data, next) => {
    async.waterfall([
        (callback) => {
            callback();
        },
        (callback) => {
            attributeCategoryDao.createAttributeCategory(categoryId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

AttributeCategory.prototype.getAllAttributeCategory = (categoryId, params, next) => {
    attributeCategoryDao.getAllAttributeCategory(categoryId, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

AttributeCategory.prototype.getAttributeCategory = (category_id, attribute_id, next) => {
    attributeCategoryDao.getAttributeCategory(category_id, attribute_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

AttributeCategory.prototype.deleteAttributeCategory = (category_id, attribute_id, next) => {
    attributeCategoryDao.deleteAttributeCategory(category_id, attribute_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

// AttributeCategory.prototype.updateAttribute = (_id, data, next) => {
//     async.waterfall([
//         (callback) => {
//             attributeCategoryDao.checkAttributeforUpdate(_id, data, (err, response) => {
//                 if (err) {
//                     next({
//                         msg: err.msg,
//                         result: err,
//                         success: false
//                     }, null)
//                 } else {
//                     if (response && response.length > 0) {
//                         next(null, {
//                             msg: 'Attribute already existed',
//                             result: null,
//                             success: false
//                         });
//                     } else {
//                         callback();
//                     }
//                 }
//             });
//         },
//         (callback) => {
//             attributeCategoryDao.updateAttribute(_id, data, (err, response) => {
//                 if (err) {
//                     next({
//                         msg: err.msg,
//                         result: err,
//                         success: false
//                     }, null)
//                 } else {
//                     next(null, {
//                         msg: 'Record successfully updated!',
//                         result: response,
//                         success: true
//                     });
//                 }
//             });
//         }
//     ]);
// };

exports.AttributeCategory = AttributeCategory;