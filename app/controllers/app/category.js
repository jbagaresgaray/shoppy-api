'use strict';

let categoryDao = require('../../daos/app/category');

const async = require('async');
const sharp = require('sharp');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

function Category() {
    this.categoryDao = categoryDao;
}


Category.prototype.createCategory = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            categoryDao.checkCategoryName(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Category name already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.toDeleteFile) {
                let errorResponse = (error) => {
                    console.error('Dropbox Error: ', error);
                    callback({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null)
                }

                sharp(data.img_path)
                    .resize(800, 800)
                    .max()
                    // .toFile(path800 + '/' + '800-' + data.file_name)
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadCategoryImage resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/category/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                dbx.sharingCreateSharedLink({
                                    path: response.path_display,
                                }).then((resp) => {
                                    data.img_name = response.path_display;
                                    data.img_path = resp.url.replace('dl=0', 'raw=1');;
                                    categoryDao.createCategory(data, (err, response) => {
                                        if (err) {
                                            next({
                                                msg: err.msg,
                                                result: err,
                                                success: false
                                            }, null)
                                        } else {
                                            next(null, {
                                                msg: 'Record successfully saved!',
                                                result: response,
                                                success: true
                                            });
                                        }
                                    });
                                }).catch(errorResponse);
                            })
                            .catch(errorResponse);
                    });
            } else {
                categoryDao.createCategory(data, (err, response) => {
                    if (err) {
                        next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        next(null, {
                            msg: 'Record successfully saved!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ]);
};

Category.prototype.getAllCatetories = (params, next) => {
    categoryDao.getAllCatetories(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response) {
                response.msg = '';
                response.success = true;
            }
            next(null, response);
        }
    });
};

Category.prototype.getCategory = (_id, next) => {
    categoryDao.getCategory(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }
    
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Category.prototype.deleteCategory = (_id, next) => {
    categoryDao.deleteCategory(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Category.prototype.updateCategory = (_id, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            categoryDao.checkCategoryForUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Category name already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            if (data.toDeleteFile) {
                let errorResponse = (error) => {
                    console.error('Dropbox Error: ', error);
                    callback({
                        msg: err.message,
                        result: err,
                        success: false
                    }, null)
                }

                sharp(data.img_path)
                    .resize(800, 800)
                    .max()
                    // .toFile(path800 + '/' + '800-' + data.file_name)
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadCategoryImage resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/category/' + _id + '/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                dbx.sharingCreateSharedLink({
                                    path: response.path_display,
                                }).then((resp) => {
                                    data.img_name = response.path_display;
                                    data.img_path = resp.url.replace('dl=0', 'raw=1');;
                                    categoryDao.updateCategory(_id, data, (err, response) => {
                                        if (err) {
                                            callback({
                                                msg: err.msg,
                                                result: err,
                                                success: false
                                            }, null)
                                        } else {
                                            callback(null, {
                                                msg: 'Record successfully updated!',
                                                result: response,
                                                success: true
                                            });
                                        }
                                    });
                                }).catch(errorResponse);
                            })
                            .catch(errorResponse);
                    });
            } else {
                categoryDao.updateCategory(_id, data, (err, response) => {
                    if (err) {
                        callback({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null)
                    } else {
                        callback(null, {
                            msg: 'Record successfully updated!',
                            result: response,
                            success: true
                        });
                    }
                });
            }
        }
    ], next);
};

Category.prototype.getAllCategoryProducts = (_id, params, next) => {
    categoryDao.getAllCategoryProducts(_id, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

exports.Category = Category;