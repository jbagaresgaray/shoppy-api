'use strict';

let formInputTypeDao = require('../../daos/app/form_input_type');
let async = require('async');

function FormInputType() {
    this.formInputTypeDao = formInputTypeDao;
}


FormInputType.prototype.createFormInputType = (data, next) => {
    async.waterfall([
        (callback) => {
            formInputTypeDao.checkFormInputType(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Form Input Type already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            formInputTypeDao.createFormInputType(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

FormInputType.prototype.getAllFormInputType = (params, next) => {
    formInputTypeDao.getAllFormInputType(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

FormInputType.prototype.getFormInputType = (_id, next) => {
    formInputTypeDao.getFormInputType(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

FormInputType.prototype.deleteFormInputType = (_id, next) => {
    formInputTypeDao.deleteFormInputType(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

FormInputType.prototype.updateFormInputType = (_id, data, next) => {
    async.waterfall([
        (callback) => {
            formInputTypeDao.checkFormInputTypeforUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Form Input Type already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            formInputTypeDao.updateFormInputType(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

exports.FormInputType = FormInputType;