'use strict';

let tagsDao = require('../../daos/app/tags');
let async = require('async');

function Tags() {
    this.tagsDao = tagsDao;
}


Tags.prototype.createTags = (data, next) => {
    async.waterfall([
        (callback) => {
            tagsDao.getTag(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Tags already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            tagsDao.createTags(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

Tags.prototype.getAllTags = (next) => {
    tagsDao.getAllTags((err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Tags.prototype.getTag = (_id, next) => {
    tagsDao.getTag(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            };
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Tags.prototype.deleteTag = (_id, next) => {
    tagsDao.deleteTag(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Tags.prototype.updateTag = (_id, data, next) => {
    async.waterfall([
        (callback) => {
            tagsDao.getTag(_id, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Tags already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            tagsDao.updateTag(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }

            });
        }
    ]);
};

exports.Tags = Tags;