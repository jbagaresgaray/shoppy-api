'use strict';

let BSDao = require('../../daos/app/buyer_seller');
let buyerDao = require('../../daos/api/users');

let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');
let _ = require('lodash');
let fs = require('fs');
let path = require('path');
let ejs = require('ejs');
let moment = require('moment');

function BuyerSeller() {
    this.BSDao = BSDao;
    this.buyerDao = buyerDao;
}


BuyerSeller.prototype.approveSellerApplication = (uuid, data, next) => {
    /* console.log('approveSellerApplication emit: ', uuid);*/

    async.waterfall([
        (callback) => {
            buyerDao.checkIfUserIsSeller(uuid, (err, response) => {
                if (err) {
                    return next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        io.emit('approve-seller', {
                            response: response
                        });
                        return next(null, {
                            msg: 'User Account already a seller',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            buyerDao.checkIfUserHasSellerApplication(uuid, (err, response) => {
                if (err) {
                    return next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: 'User Account dont have a seller application record',
                            result: null,
                            success: false
                        });
                    }
                }
            });
        },
        (callback) => {
            BSDao.approveSellerApplication(uuid, data, (err, response) => {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    // SEND NOTIFICATION TO USER FOR THE APPROVAL
                    let template = path.join(__dirname, '../..', 'views/emails/sellerapproval.ejs');
                    let ejstemp = {
                        Fullname: (response.firstname + ' ' + response.lastname),
                        Email: response.email
                    };

                    ejs.renderFile(template, ejstemp, (err, html) => {
                        if (err) {
                            console.log(err); // Handle error
                        }
                        let mailData = {
                            from: 'info@shoppyapp.net',
                            email: response.email,
                            subject: 'Yay, You are now a seller',
                            message: html,
                            attachment: null
                        };

                        func.sendMail(mailData, (err, mailresponse) => {
                            if (err) {
                                console.log('err: ', err.body);
                            }
                            console.log('mailresponse: ', mailresponse);
                        });
                    });

                    // io.in(uuid).emit('approve-seller');

                    next(null, {
                        msg: 'Seller Application Successfully approved',
                        success: true,
                        result: response
                    });
                }
            });
        }
    ], next);
};

BuyerSeller.prototype.getAllApplicants = (next) => {
    BSDao.getAllApplicants((err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: '',
                success: true,
                result: response
            });
        }
    });
};

BuyerSeller.prototype.getAllSellers = (next) => {
    BSDao.getAllSellers((err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: '',
                success: true,
                result: response
            });
        }
    });
};

BuyerSeller.prototype.getAllBuyerUser = (params, next) => {
    BSDao.getAllBuyerUser(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.officialShopSubmision = (uuid, data, next) => {
    BSDao.officialShopSubmision(uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            // Send Email to Shoppy Team
            // Send Email to Shopp User
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.rateBuyer = (buyerId, data, next) => {
    BSDao.rateBuyer(buyerId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.getBuyerRatings = (buyerId, params, next) => {
    BSDao.getBuyerRatings(buyerId, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

/**
 * This is for shop ratings from buyers perspective
 */
BuyerSeller.prototype.rateShop = (userId, data, next) => {
    BSDao.rateShop(userId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.getShopRatings = (userId, next) => {
    BSDao.getShopRatings(userId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response && response.length > 0 ? response[0] : {},
                success: true
            });
        }
    });
};
/**
 * End of shop ratings from buyers perspective
 */

BuyerSeller.prototype.buyerRefund = (userId, orderId, productId, next) => {
    BSDao.buyerRefund(userId, orderId, productId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.getPreferredSeller = (next) => {
    BSDao.getPreferredSeller((err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: '',
                success: true,
                result: response
            });
        }
    });
};

BuyerSeller.prototype.getPreferredSellerById = (id, next) => {
    BSDao.getPreferredSellerById(id, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: '',
                success: true,
                result: response
            });
        }
    });
};

BuyerSeller.prototype.createPreferredSeller = (data, next) => {
    BSDao.createPreferredSeller(data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.updatePreferredSeller = (id, data, next) => {
    BSDao.updatePreferredSeller(id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};

BuyerSeller.prototype.deletePreferredSeller = (id, next) => {
    BSDao.deletePreferredSeller(id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};






exports.BuyerSeller = BuyerSeller;