'use strict';

let dao = require('../../daos/app/orders');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');
let _ = require('lodash');
let fs = require('fs');
let path = require('path');
let ejs = require('ejs');
let moment = require('moment');

function Orders() {
    this.dao = dao;
}


Orders.prototype.getOrderDetails = (orderId, next) => {
    dao.getOrderDetails(orderId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.getOrderDetailsByBatchNum = (batchNum, next) => {
    dao.getOrderDetailsByBatchNum(batchNum, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.getOrdersList = (action, type, next) => {
    if (action == 'pay' || action == 'unpaid') {
        dao.getPayableOrdersList(action, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'ship' || action == 'shipping') {
        dao.getShippedOrdersList(action, type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'receive') {
        dao.getReceivableOrdersList((err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'completed') {
        dao.getCompletedOrdersList(type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }

        });
    } else if (action == 'cancelled') {
        dao.getCancelledOrdersList(type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'return') {
        dao.getReturnedOrdersList(type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else {
        dao.getAllOrdersList((err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    }
};

Orders.prototype.payOrder = (orderId, data, next) => {
    dao.payOrder(orderId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: "Order successfully paid!",
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.shippedOrder = (orderId, data, next) => {
    let htmlToText = require('html-to-text');

    dao.shippedOrder(orderId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            console.log('shippedOrder response: ', response);
            // ================================================================================
            // Push Notification Order Updates COD Order Approval - Buyer
            // ================================================================================
            let devices = _.map(response.notification.devices, (row) => {
                return row.onesignal_player_id;
            });
            var summary = htmlToText.fromString(response.notification.content);
            func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                console.log('sendNotifications Err: ', err);
                if (!err && resp) {
                    console.log('sendNotifications Response: ', resp);
                }
            });

            // ================================================================================
            // SEND EMAIL TO NOTIFY SELLER ABOUT THE PRODUCT ENTRY
            // ================================================================================
            let template = path.join(__dirname, '../..', 'views/emails/mobile/order_shipped.ejs');
            let totalPayment = parseFloat(response.orders.orderAmt) + parseFloat(response.orders.shippingFee);
            _.each(response.details, (row) => {
                row.detailPrice = config.currency.symbol + ' ' + row.detailPrice.toFixed(config.currency.decimal_digits);
            });
            let ejstemp = {
                sellerName: response.orders.sellerName,
                orderNum: response.orders.orderNum,
                orderDateTime: response.orders.orderDateTime,
                buyerName: response.orders.buyerName,
                buyerEmail: response.orders.orderEmail,
                orderNote: response.orders.orderNote,
                orderDetails: response.orders.details,
                subTotal: config.currency.symbol + ' ' + response.orders.orderAmt.toFixed(config.currency.decimal_digits),
                shippingFee: config.currency.symbol + ' ' + response.orders.shippingFee.toFixed(config.currency.decimal_digits),
                totalPayment: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                recipientName: response.orders.orderShipName,
                recipientNumber: response.orders.orderPhone,
                recipientAddress: response.orders.orderShipAddress + ' ' + response.orders.orderShipSuburb + ' ' + response.orders.orderShipCity + ' ' + response.orders.orderShipState + ' ' + ' ' + response.orders.orderShipCountry + ', ' + response.orders.orderShipZipCode,
                paymentMethod: (response.orders.isCOD == 1) ? 'COD' : 'Others',
                amountPayable: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
            };
            func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.orders.orderEmail, 'Your Order #' + response.orders.orderNum + ' is being delivered');

            next(null, {
                msg: "Order successfully shipped out!",
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.orderReceivedReminder = (orderId, next) => {
    let htmlToText = require('html-to-text');

    dao.orderReceivedReminder(orderId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            console.log('orderReceivedReminder response: ', response);

            // ================================================================================
            // Push Notification Order Updates COD Order Approval - Buyer
            // ================================================================================
            let devices = _.map(response.notification.devices, (row) => {
                return row.onesignal_player_id;
            });
            var summary = htmlToText.fromString(response.notification.content);
            func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                console.log('sendNotifications Err: ', err);
                if (!err && resp) {
                    console.log('sendNotifications Response: ', resp);
                }
            });

            // ================================================================================
            // SEND EMAIL TO NOTIFY SELLER ABOUT THE PRODUCT ENTRY
            // ================================================================================
            let template = path.join(__dirname, '../..', 'views/emails/mobile/order_received.ejs');
            let totalPayment = parseFloat(response.orders.orderAmt) + parseFloat(response.orders.shippingFee);
            _.each(response.details, (row) => {
                row.detailPrice = config.currency.symbol + ' ' + row.detailPrice.toFixed(config.currency.decimal_digits);
            });
            let ejstemp = {
                sellerName: response.orders.sellerName,
                orderNum: response.orders.orderNum,
                orderDateTime: response.orders.orderDateTime,
                buyerName: response.orders.buyerName,
                buyerEmail: response.orders.orderEmail,
                orderNote: response.orders.orderNote,
                orderDetails: response.orders.details,
                subTotal: config.currency.symbol + ' ' + response.orders.orderAmt.toFixed(config.currency.decimal_digits),
                shippingFee: config.currency.symbol + ' ' + (response.orders && response.orders.shippingFee ? response.orders.shippingFee.toFixed(config.currency.decimal_digits) : null),
                totalPayment: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                recipientName: response.orders.orderShipName,
                recipientNumber: response.orders.orderPhone,
                recipientAddress: response.orders.orderShipAddress + ' ' + response.orders.orderShipSuburb + ' ' + response.orders.orderShipCity + ' ' + response.orders.orderShipState + ' ' + ' ' + response.orders.orderShipCountry + ', ' + response.orders.orderShipZipCode,
                paymentMethod: (response.orders.isCOD == 1) ? 'COD' : 'Others',
                amountPayable: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
            };
            func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.orders.orderEmail, 'Hey ' + response.orders.buyerName + ', Have you received order #' + response.orders.orderNum + '?');

            next(null, {
                msg: "Order received notification successfully sent.",
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.autoCancelUnshippedOrders = (orderId, next) => {
    dao.autoCancelUnshippedOrders(orderId, (err, resp) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: resp,
                result: resp,
                success: true
            });
        }
    });
};

Orders.prototype.autoReleasePayment = (orderId, next) => {
    dao.autoReleasePayment(orderId, (err, resp) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: resp,
                result: resp,
                success: true
            });
        }
    });
};



exports.Orders = Orders;