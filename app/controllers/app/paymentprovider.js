'use strict';

let paymentDao = require('../../daos/app/paymentprovider');
let async = require('async');

function PaymentProvider() {
    this.paymentDao = paymentDao;
}


PaymentProvider.prototype.createPaymentProvider = (data, next) => {
    async.waterfall([
        (callback) => {
            paymentDao.checkPaymentProvider(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Payment Provider already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            paymentDao.createPaymentProvider(data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully saved!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

PaymentProvider.prototype.getAllPaymentProvider = (params, next) => {
    paymentDao.getAllPaymentProvider(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

PaymentProvider.prototype.getPaymentProvider = (_id, next) => {
    paymentDao.getPaymentProvider(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];
            } else {}
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

PaymentProvider.prototype.deletePaymentProvider = (_id, next) => {
    paymentDao.deletePaymentProvider(_id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

PaymentProvider.prototype.updatePaymentProvider = (_id, data, next) => {
    async.waterfall([
        (callback) => {
            paymentDao.checkPaymentProviderforUpdate(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        next(null, {
                            msg: 'Payment Provider already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            paymentDao.updatePaymentProvider(_id, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    next(null, {
                        msg: 'Record successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ]);
};

exports.PaymentProvider = PaymentProvider;