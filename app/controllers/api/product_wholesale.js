'use strict';

let dao = require('../../daos/api/product_wholesale');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');


function ProductWholeSale() {
    this.dao = dao;
}

ProductWholeSale.prototype.createProductWholesale = (product_id, data, next) => {
    dao.createProductWholesale(product_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Wholesale successfully created',
                result: response,
                success: true
            });
        }
    });
};

ProductWholeSale.prototype.deleteAllProductWholeSale = (productId, next) => {
    dao.deleteAllProductWholeSale(productId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'All Product Wholesales successfully deleted',
                result: response,
                success: true
            });
        }
    });
};

ProductWholeSale.prototype.getAllProductWholeSale = (productId, next) => {
    dao.getAllProductWholeSale(productId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

ProductWholeSale.prototype.deleteProductWholeSale = (productId, _id, next) => {
    dao.deleteProductWholeSale(productId, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Wholesale successfully deleted',
                result: response,
                success: true
            });
        }
    });
};

ProductWholeSale.prototype.getProductWholeSale = (productId, _id, next) => {
    dao.getProductWholeSale(productId, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }

            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

ProductWholeSale.prototype.updateProductWholeSale = (productId, _id, data, next) => {
    dao.updateProductWholeSale(productId, _id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Wholesale successfully updated',
                result: response,
                success: true
            });
        }
    });
};

exports.ProductWholeSale = ProductWholeSale;