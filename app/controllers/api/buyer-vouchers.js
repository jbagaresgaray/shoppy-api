"use strict";

const daoVouchers = require('../../daos/api/buyer-vouchers');

function Buyer() {
    this.daoVouchers = daoVouchers;
}

Buyer.prototype.getBuyerVouchers = (userId, params, next) => {
    daoVouchers.getBuyerVouchers(userId, params, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Data successfully retrieve',
                result: response,
                success: true
            });
        }
    });
};

Buyer.prototype.claimVoucher = (user, data, next) => {
    daoVouchers.claimVoucher(user, data, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Data successfully retrieve',
                result: response,
                success: true
            });
        }
    });
};

exports.Buyer = Buyer;