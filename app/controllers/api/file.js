'use strict';

let dao = require('../../daos/api/file');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let func = require('../../../app/utils/functions');

const async = require('async');
const _ = require('lodash');
const sharp = require('sharp');
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');

function File() {
    this.dao = dao;
}

File.prototype.uploadFile = (data, next) => {
    console.log('data: ', data);

    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let path100 = path.resolve('./public/uploads/resize/100');
    let path200 = path.resolve('./public/uploads/resize/200');
    let path400 = path.resolve('./public/uploads/resize/400');
    let path800 = path.resolve('./public/uploads/resize/800');
    let pathThumb = path.resolve('./public/uploads/resize/thumb');
    let previewPath = path.resolve('./public/uploads/resize/preview');
    let largePath = path.resolve('./public/uploads/resize/large');
    let xlargePath = path.resolve('./public/uploads/resize/xlarge');


    function successResponse(response) {
        console.log('filesUpload: ', response);
        /* next(null, {
            msg: 'File successfully uploaded',
            result: response,
            success: true
        }); */
    }

    function errorResponse(error) {
        console.error('filesUpload error: ', error);
        /* next({
            msg: 'File upload error',
            result: error,
            success: false
        }, null); */
    }


    if (!fs.existsSync(path100)) {
        mkdirp(path100, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(path200)) {
        mkdirp(path200, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(path400)) {
        mkdirp(path400, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(path800)) {
        mkdirp(path800, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(previewPath)) {
        mkdirp(previewPath, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(largePath)) {
        mkdirp(largePath, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    if (!fs.existsSync(xlargePath)) {
        mkdirp(xlargePath, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    let fileObj = {
        file_name: data.file_name,
        file_type: data.file_type,
        file_size: data.file_size,
    };

    async.waterfall([
        (callback) => {
            // Upload Original Size
            dbx.filesUpload({
                    path: '/' + env + '/Files/' + data.file_name,
                    contents: data.content
                }).then((response) => {
                    console.log('filesUpload: ', response);
                    dbx.sharingCreateSharedLink({
                        path: response.path_display,
                    }).then((resp) => {
                        fileObj.file_orig_path = resp.url.replace('dl=0', 'raw=1');;
                        callback();
                    }).catch(errorResponse);
                })
                .catch(errorResponse);
        },
        (callback) => {
            // 100 X 100
            sharp(data.img_path)
                .resize(100, 100, {
                    kernel: sharp.kernel.nearest
                })
                .max()
                // .toFile(path100 + '/' + '100-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('100 resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/100/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('100 resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_100_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // 200 X 200
            sharp(data.img_path)
                .resize(200, 200, {
                    kernel: sharp.kernel.nearest
                })
                .max()
                // .toFile(path200 + '/' + '200-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('200 resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/200/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('200 resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_200_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // 400 X 400
            sharp(data.img_path)
                .resize(400, 400, {
                    kernel: sharp.kernel.nearest
                })
                .max()
                // .toFile(path400 + '/' + '400-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('400 resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/400/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('400 resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_400_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // 800 x 800
            sharp(data.img_path)
                .resize(800, 800)
                .max()
                // .toFile(path800 + '/' + '800-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('800 resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/800/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('800 resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_800_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // Thumbnail
            sharp(data.img_path)
                .resize(75, 75)
                .max()
                // .toFile(pathThumb + '/' + 'thumb-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('Thumb resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/thumb/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('Thumb resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_thumb_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // Preview
            sharp(data.img_path)
                .resize(960, 540)
                .max()
                // .toFile(previewPath + '/' + 'preview-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('Preview resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/preview/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('Preview resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_preview_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // Large
            sharp(data.img_path)
                .resize(1280, 720)
                .max()
                // .toFile(largePath + '/' + 'large-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('Large resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/large/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('Large resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_large_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        },
        (callback) => {
            // XLarge
            sharp(data.img_path)
                .resize(1600, 1200)
                .max()
                // .toFile(xlargePath + '/' + 'xlarge-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('xLarge resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/resize/xlarge/' + data.file_name,
                            contents: resp
                        }).then((response) => {
                            console.log('xLarge resp filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_extra_large_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        })
                        .catch(errorResponse);
                });
        }
    ], () => {
        dao.saveFileUpload(fileObj, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "File successfully saved and uploaded!",
                    result: response,
                    success: true
                });
            }
        });
    });

};

File.prototype.getFile = (fileId, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    function successResponse(response) {
        console.log('DropBox success: ', response);

    }

    function errorResponse(error) {
        console.error('DropBox error: ', error);

    }

    dao.getFile(fileId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];

                async.parallel([
                    (callback) => {
                        if (response.file_orig_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_orig_path,
                            }).then((resp) => {
                                response.file_orig_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_orig_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_100_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_100_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_100_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_100_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_200_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_200_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_200_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_200_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_400_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_400_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_400_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_400_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_800_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_800_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_800_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_800_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_thumb_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_thumb_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_thumb_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_thumb_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_preview_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_preview_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_preview_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_preview_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_large_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_large_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_large_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_large_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_extra_large_path) {
                            dbx.sharingCreateSharedLink({
                                path: response.file_extra_large_path,
                                short_url: false
                            }).then((resp) => {
                                response.file_extra_large_public_path = resp.url.replace('dl=0', 'raw=1');;
                                callback();
                            }).catch(errorResponse);
                        } else {
                            response.file_extra_large_public_path = '';
                            callback();
                        }
                    }
                ], () => {
                    next(null, {
                        msg: "",
                        result: response,
                        success: true
                    });
                });
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        }
    });
};

File.prototype.deleteFile = (fileId, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    function successResponse(response) {
        console.log('DropBox success: ', response);
    }

    function errorResponse(error) {
        console.error('DropBox error: ', error);
    }

    dao.getFile(fileId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];

                async.waterfall([
                    (callback) => {
                        if (response.file_orig_path) {
                            dbx.filesDelete({
                                path: response.file_orig_path
                            }).then((resp) => {
                                console.log('Delete resp 1: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 1: ', error);
                                callback();
                            });
                        } else {
                            response.file_orig_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_100_path) {
                            dbx.filesDelete({
                                path: response.file_100_path
                            }).then((resp) => {
                                console.log('Delete resp 2: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 2: ', error);
                                callback();
                            });
                        } else {
                            response.file_100_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_200_path) {
                            dbx.filesDelete({
                                path: response.file_200_path
                            }).then((resp) => {
                                console.log('Delete resp 3: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 3: ', error);
                                callback();
                            });
                        } else {
                            response.file_200_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_400_path) {
                            dbx.filesDelete({
                                path: response.file_400_path
                            }).then((resp) => {
                                console.log('Delete resp 4: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 4: ', error);
                                callback();
                            });
                        } else {
                            response.file_400_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_800_path) {
                            dbx.filesDelete({
                                path: response.file_800_path
                            }).then((resp) => {
                                console.log('Delete resp 5: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 5: ', error);
                                callback();
                            });
                        } else {
                            response.file_800_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_thumb_path) {
                            dbx.filesDelete({
                                path: response.file_thumb_path
                            }).then((resp) => {
                                console.log('Delete resp 6: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 6: ', error);
                                callback();
                            });
                        } else {
                            response.file_thumb_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_preview_path) {
                            dbx.filesDelete({
                                path: response.file_preview_path
                            }).then((resp) => {
                                console.log('Delete resp 7: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 7: ', error);
                                callback();
                            });
                        } else {
                            response.file_preview_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_large_path) {
                            dbx.filesDelete({
                                path: response.file_large_path
                            }).then((resp) => {
                                console.log('Delete resp 8: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 8: ', error);
                                callback();
                            });
                        } else {
                            response.file_large_public_path = '';
                            callback();
                        }
                    },
                    (callback) => {
                        if (response.file_extra_large_path) {
                            dbx.filesDelete({
                                path: response.file_extra_large_path
                            }).then((resp) => {
                                console.log('Delete resp 9: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 9: ', error);
                                callback();
                            });
                        } else {
                            response.file_extra_large_public_path = '';
                            callback();
                        }
                    }
                ], () => {
                    dao.deleteFile(fileId, (err, response) => {
                        if (err) {
                            next({
                                msg: err.msg,
                                result: err,
                                success: false
                            }, null);
                        } else {
                            next(null, {
                                msg: "File successfully deleted!",
                                result: response,
                                success: true
                            });
                        }
                    });
                });
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        }
    });
};

exports.File = File;