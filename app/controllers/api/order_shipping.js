'use strict';

let orderShippingDao = require('../../daos/api/order_shipping');

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const moment = require('moment');
const mkdirp = require('mkdirp');
const sharp = require('sharp');

function Orders() {
    this.orderShippingDao = orderShippingDao;
}

Orders.prototype.isOrderArrangeShipment = (orderId, slug, next) => {
    orderShippingDao.isOrderArrangeShipment(orderId, {
        slug: slug
    }, (err, response) => {
        if (err) {
            return next(err, null);
        } else {
            if (response && response.length > 0) {
                return next(null, {
                    msg: 'Order already arrange for shipment!',
                    result: null,
                    success: false
                });
            } else {
                return next(null, {
                    msg: '',
                    result: null,
                    success: true
                });
            }
        }
    });
};

Orders.prototype.arrangeShipment = (orderId, data, next) => {
    async.waterfall([
        (callback) => {
            orderShippingDao.isOrderArrangeShipment(orderId, data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Order already arrange for shipment!',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderShippingDao.arrangeShipment(orderId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    orderShippingDao.updateStocks(orderId,(e,r)=>{
                        if(r) {
                            callback(null, {
                                msg: "Order shipment successfully arrange! Kindly wait for the pickup and the tracking number.",
                                result: response,
                                success: true,
                                stockUpdated: true
                            });
                        } else {
                            callback(null, {
                                msg: "Order shipment successfully arrange! Kindly wait for the pickup and the tracking number.",
                                result: response,
                                success: true,
                                stockUpdated: false
                            });
                        }
                    })
                }
            });
        }
    ], next);
};

Orders.prototype.getOrderTrackings = (orderId, slug, next) => {
    orderShippingDao.getOrderTrackings(orderId, slug, (err, response) => {
        if (err) {
            return next(err, null);
        } else {
            next(null, {
                msg: "",
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.getOrderTrackingLastCheckPoint = (orderId, slug, next) => {
    orderShippingDao.getOrderTrackingLastCheckPoint(orderId, slug, (err, response) => {
        if (err) {
            return next(err, null);
        } else {
            next(null, {
                msg: "",
                result: response,
                success: true
            });
        }
    });
};



exports.Orders = Orders;