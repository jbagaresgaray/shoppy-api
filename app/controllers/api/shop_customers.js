'use strict';

const dao = require('../../daos/api/shop_customers');

const func = require('../../../app/utils/functions');
const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

function ShopCustomers() {
    this.dao = dao;
}

ShopCustomers.prototype.geShopCustomers = (user, next) => {
    dao.geShopCustomers(user, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: "Data successfully retrieve",
                result: response,
                success: true
            });
        }
    });
};

exports.ShopCustomers = ShopCustomers;