'use strict';


let orderCancelDao = require('../../daos/api/order_cancel');
let dao = require('../../daos/api/orders');

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

let func = require('../../../app/utils/functions');

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const moment = require('moment');
const mkdirp = require('mkdirp');
const sharp = require('sharp');

function Orders() {
    this.orderCancelDao = orderCancelDao;
    this.dao = dao;
}



Orders.prototype.cancelOrderRequest = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderPrepared(orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback(null, true);
                    } else {
                        callback(null, false);
                    }
                }
            });
        },
        (isOrderPrepared, callback) => {
            if (isOrderPrepared) {
                dao.isOrderShipped(user.uuid, orderId, (err, response) => {
                    if (err) {
                        return next({
                            msg: err.msg,
                            result: err,
                            success: false
                        }, null);
                    } else {
                        if (response && response.length > 0) {
                            return next(null, {
                                msg: "Unable to cancel order. Order already shipped out!",
                                result: response[0],
                                success: false
                            });
                        } else {
                            callback(null, isOrderPrepared);
                        }
                    }
                });
            } else {
                callback(null, isOrderPrepared);
            }
        },
        (isOrderPrepared, callback) => {
            orderCancelDao.isOrderCancelledRequested(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Order already requested for cancellation!",
                            result: response[0],
                            success: true
                        });
                    } else {
                        callback(null, isOrderPrepared);
                    }
                }
            });
        },
        (isOrderPrepared, callback) => {
            let requestResponse = (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    // ====================================================
                    //      Send Push Notification to the seller
                    // ====================================================

                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response: ', resp);
                        }
                    });

                    callback(null, {
                        msg: "Cancellation request successfully!",
                        result: response.orders,
                        success: true
                    });
                }
            };


            if (isOrderPrepared) {
                orderCancelDao.cancelOrderRequest(user.uuid, orderId, data, requestResponse);
            } else {
                orderCancelDao.cancelOrderDirectly(user.uuid, orderId, data, requestResponse);
            }
        }
    ], next);
};

Orders.prototype.cancelOrder = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderShipped(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Unable to cancel order. Order already shipped out!",
                            result: response[0],
                            success: true
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderCancelDao.isOrderCancelledRequested(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Order must be requested for cancellation!",
                            result: null,
                            success: false
                        });
                    }
                }
            });
        },
        (callback) => {
            orderCancelDao.cancelOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    // ====================================================
                    //      Send Push Notification to the seller
                    // ====================================================

                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response: ', resp);
                        }
                    });

                    let notif2 = '<b>' + response.seller.username + '</b> had cancelled your order <b>' + response.orders.orderNum + '</b>. Contact seller for more information if you wish.';
                    var summary2 = htmlToText.fromString(notif2);
                    func.sendNotifications(config.app_name, summary2, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err2: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response2: ', resp);
                        }
                    });

                    callback(null, {
                        msg: "Order cancelled successfully!",
                        result: response.orders,
                        success: true
                    });
                }
            });
        }
    ], next);
};

Orders.prototype.rejectCancelOrder = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderShipped(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Unable to cancel order. Order already shipped out!",
                            result: response[0],
                            success: true
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderCancelDao.isOrderCancelledRequested(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Order must be requested for cancellation!",
                            result: null,
                            success: false
                        });
                    }
                }
            });
        },
        (callback) => {
            orderCancelDao.rejectCancelOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    // ====================================================
                    //      Send Push Notification to the seller
                    // ====================================================

                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response: ', resp);
                        }
                    });

                    callback(null, {
                        msg: "Order cancellation rejected successfully!",
                        result: response.orders,
                        success: true
                    });
                }
            });
        }
    ], next);
};

Orders.prototype.withdrawCancelOrder = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderShipped(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Unable to cancel order. Order already shipped out!",
                            result: response[0],
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderCancelDao.isOrderCancelledRequested(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Order must be requested for cancellation!",
                            result: response[0],
                            success: true
                        });
                    }
                }
            });
        },
        (callback) => {
            orderCancelDao.withdrawCancelOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    // ====================================================
                    //      Send Push Notification to the seller
                    // ====================================================

                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response: ', resp);
                        }
                    });

                    callback(null, {
                        msg: "Order cancellation withdraw successfully!",
                        result: response.orders,
                        success: true
                    });
                }
            });
        }
    ], next);
};

exports.Orders = Orders;