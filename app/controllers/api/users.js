'use strict';

let dao = require('../../daos/api/users');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const moment = require('moment');
const jwt = require('jsonwebtoken');
const uuidv1 = require('uuid/v1');
const OneSignal = require('onesignal-node');
const sharp = require('sharp');


function Users() {
    this.dao = dao;
}

Users.prototype.IsUserExisted = (sUserName, next) => {
    dao.checkUserUsername(sUserName, (err, response) => {
        if (err) {
            next(err, null);
        } else {
            if (!_.isEmpty(response)) {
                next(null, {
                    msg: '',
                    result: response,
                    success: true,
                    IsUserExisted: true
                });
            } else {
                next(null, {
                    msg: '',
                    result: response,
                    success: false,
                    IsUserExisted: false
                });
            }
        }
    });
};

Users.prototype.createUser = (data, next) => {
    async.waterfall([
        (callback) => {
            dao.checkUserUsername(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Username already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.checkUserEmail(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Email Address already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.createUser(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        response = response[0];

                        let template = path.join(__dirname, '../..', 'views/emails/mobile/confirm.ejs');
                        let ejstemp = {
                            Fullname: (response.firstname + ' ' + response.lastname),
                            Email: response.email,
                            Link: config.api_host_url + '/verify?code=' + response.verificationCode + '&uuid=' + response.uuid + '&action=user_buyer'
                        };
                        func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.email, 'Verify your account on Shoppy');

                        next(null, {
                            msg: 'Thank you for registering at Shoppy!',
                            result: response,
                            success: true
                        });
                    } else {
                        next(null, {
                            msg: 'Thank you for registering at Shoppy!',
                            result: response,
                            success: true
                        });
                    }
                }
            });
        }
    ]);
};

Users.prototype.loginSocial = (data, next) => {
    dao.loginSocial(data.provider, data.Id, (err, user) => {
        if (err) {
            return next(err);
        } else {
            if (_.isEmpty(user)) {
                return next(null, {
                    msg: 'User does not exist with this user account.',
                    success: false,
                    result: ''
                });
            }

            user = user[0];
            if (user.isVerify === 0) {
                next(null, {
                    msg: 'Your account has not been VERIFIED.',
                    success: false,
                    result: ''
                });
            } else {
                delete user.password;
                delete user.isVerify;
                user.isSeller = _.isNull(user.isSeller) ? 0 : user.isSeller;
                user.isSeller = (user.isSeller == 0) ? false : true;

                const payload = {
                    uuid: user.uuid,
                    email: user.email,
                    firstname: user.firstname,
                    isSeller: user.isSeller,
                    lastname: user.lastname,
                    img_name: user.img_name,
                    img_path: user.img_path,
                    img_type: user.img_type,
                    img_size: user.img_size
                };
                console.log('payload: ', payload);

                var token = jwt.sign(payload, config.token_secret_mobile, {
                    expiresIn: '7 days',
                    issuer: config.token_secret_mobile,
                    jwtid: uuidv1()
                });

                next(null, {
                    msg: 'Login successfully',
                    success: true,
                    result: {
                        user: user,
                        token: token
                    }
                });
            }
        }
    });
};

Users.prototype.socialSignUp = (data, next) => {
    async.waterfall([
        (callback) => {
            dao.isSocialAccountExisted(data.provider, data.Id, (err, response) => {
                if (err) {
                    return next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: _.upperFirst(data.provider) + ' account already existed',
                            success: false,
                            result: null
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.checkUserEmail(data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Email Address already existed',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.socialSignUp(data, (err, response) => {
                if (err) {
                    return next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                        let template = path.join(__dirname, '../..', 'views/emails/mobile/confirm.ejs');
                        let ejstemp = {
                            Fullname: (response.firstname + ' ' + response.lastname),
                            Email: response.email,
                            Link: config.api_host_url + '/verify?code=' + response.verificationCode + '&uuid=' + response.uuid + '&action=user_buyer'
                        };

                        ejs.renderFile(template, ejstemp, (err, html) => {
                            if (err) {
                                console.log(err); // Handle error
                            }
                            let mailData = {
                                from: 'info@shoppyapp.net',
                                email: response.email,
                                subject: 'Verify your account on Shoppy',
                                message: html,
                                attachment: null
                            };

                            func.sendMail(mailData, (err, mailresponse) => {
                                if (err) {
                                    console.log('err: ', err.body);
                                }
                                console.log('mailresponse: ', mailresponse);
                            });
                        });

                        next(null, {
                            msg: 'Thank you for registering at Shoppy!',
                            result: response,
                            success: true
                        });
                    } else {
                        next(null, {
                            msg: 'Thank you for registering at Shoppy!',
                            result: response,
                            success: true
                        });
                    }
                }
            });
        }
    ], next);
};


Users.prototype.checkIfVerified = (hash_key, uuid, next) => {
    dao.checkIfVerified(hash_key, uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                return next(null, {
                    msg: 'Your account has already been verified',
                    success: true,
                    result: ''
                });
            } else {
                return next(null, {
                    msg: 'Your account has not been verified. Please check your mail for activation link.',
                    success: false,
                    result: ''
                });
            }
        }
    });
};

Users.prototype.verifyUser = (hash_key, uuid, next) => {
    dao.verifyUser(hash_key, uuid, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: 'Your account has successfully activated. Thank you!',
                success: true,
                result: response
            });
        }
    });
};

Users.prototype.getUserProfile = (user, next) => {
    dao.getUserProfile(user.uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
                response.isSeller = (_.isEmpty(response.isSeller) || response.isSeller == 0) ? false : true;
            };

            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.getCurrentUser = (user, next) => {
    dao.getCurrentUser(user.uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.getUserAddresses = (user, params, next) => {
    dao.getUserAddresses(user.uuid, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Users.prototype.getUserAddress = (user, _id, next) => {
    dao.getUserAddress(user.uuid, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }

            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.deleteUserAddress = (user, _id, next) => {
    dao.deleteUserAddress(user.uuid, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record Successfully Deleted',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.updateUserAddress = (user, _id, data, next) => {
    dao.updateUserAddress(user.uuid, _id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record Successfully Updated',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.createUserAddress = (user, data, next) => {
    dao.createUserAddress(user.uuid, data, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            next(null, {
                msg: 'Record Successfully Saved',
                success: true,
                result: response
            });
        }
    });
};

Users.prototype.forgotPassword = (data, next) => {
    dao.getUserByUsernameAndEmail(data.email, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];

                var template = path.join(__dirname, '../..', 'views/emails/mobile/forgot.ejs');
                var ejstemp = {
                    Fullname: response.firstname + ' ' + response.lastname,
                    Email: response.email,
                    Link: config.api_host_url + '/resetpassword?code=' + response.verificationCode + '&uuid=' + response.uuid + '&action=user_buyer'
                };
                ejs.renderFile(template, ejstemp, (err, html) => {
                    if (err) {
                        console.log(err); // Handle error
                    }
                    var mailData = {
                        from: 'info@shoppyapp.net',
                        email: response.email,
                        subject: 'Reset your Shoppy password',
                        message: html,
                        attachment: null
                    };

                    func.sendMail(mailData, (err, mailresponse) => {
                        if (err) {
                            console.log('err: ', err.body);
                        }
                        console.log('mailresponse: ', mailresponse);
                    });
                });
                next(null, {
                    success: true,
                    msg: 'Forgot Password confirmation has been sent. Kindly check your email for the next step.',
                    result: {
                        Email: response.Email,
                        Fullname: response.Fullname,
                        UserName: response.UserName,
                        EmployeeID: response.EmployeeID,
                        UUID: response.UUID
                    }
                });
            } else {
                next(null, {
                    result: null,
                    msg: 'Email address not exist with the given info.',
                    success: false
                });
            }
        }
    });
};

Users.prototype.resetPassword = (data, next) => {
    dao.getUserByUUID(data.uuid, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];

                dao.updateUserPassword(data, (err1, result) => {
                    if (err1) {
                        next({
                            result: err1,
                            msg: err1.message,
                            success: true
                        }, null);
                    } else {
                        var template = path.join(__dirname, '../..', 'views/emails/reset.ejs');
                        var ejstemp = {
                            Fullname: response.firstname + ' ' + response.lastname,
                            Email: response.email,
                            Date: moment().format('MM-DD-YYYY hh:mm:ss A Z')
                        };
                        ejs.renderFile(template, ejstemp, (err, html) => {
                            if (err) {
                                console.log(err); // Handle error
                            }
                            var mailData = {
                                from: 'info@shoppyapp.net',
                                email: response.email,
                                subject: 'Your Shoppy password has been changed',
                                message: html,
                                attachment: null
                            };

                            func.sendMail(mailData, (err, mailresponse) => {
                                if (err) {
                                    console.log('err: ', err.body);
                                }
                                console.log('mailresponse: ', mailresponse);
                            });
                        });

                        next(null, {
                            success: true,
                            msg: 'Password successfully changed.',
                            result: {
                                Email: response.email,
                                Fullname: response.firstname + ' ' + response.lastname,
                            }
                        });
                    }
                });
            } else {
                next(null, {
                    result: null,
                    msg: 'User does not exist with the given info.',
                    success: false
                });
            }
        }
    });
};

Users.prototype.applyUserAsSeller = (user, next) => {
    async.waterfall([
        (callback) => {
            dao.checkIfUserIsSeller(user.uuid, (err, response) => {
                if (err) {
                    return next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'User already a seller',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.checkIfUserHasSellerApplication(user.uuid, (err, response) => {
                if (err) {
                    return next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'User already have a pending application',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.applyUserAsSeller(user.uuid, (err, response) => {
                if (err) {
                    next({
                        result: err,
                        msg: err.message,
                        success: true
                    }, null);
                } else {
                    // SEND NOTIFICATION ON DASHBOARD FOR APPLICATION
                    // SEND EMAIL NOTIFICATION TO USER ACCOUNT FOR THE APPROVAL
                    let template = path.join(__dirname, '../..', 'views/emails/mobile/sellerapplication.ejs');
                    let ejstemp = {
                        Fullname: (response.firstname + ' ' + response.lastname),
                        Email: response.email,
                        Code: response.sellerApprovalCode
                    };

                    ejs.renderFile(template, ejstemp, (err, html) => {
                        if (err) {
                            console.log(err); // Handle error
                        }
                        let mailData = {
                            from: 'info@shoppyapp.net',
                            email: response.email,
                            subject: 'Thank you for applying as seller',
                            message: html,
                            attachment: null
                        };

                        func.sendMail(mailData, (err, mailresponse) => {
                            if (err) {
                                console.log('err: ', err.body);
                            }
                            console.log('mailresponse: ', mailresponse);
                        });
                    });

                    next(null, {
                        msg: 'Application Successfully Sent',
                        success: true,
                        result: response
                    });
                }
            });
        }
    ], next);
};

Users.prototype.updateUserAccount = (user, data, next) => {
    dao.updateUserAccount(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: 'Record succesfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.updateUserEmail = (user, data, next) => {
    dao.updateUserEmail(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: 'User email succesfully updated!',
                result: response,
                success: true
            });
        }
    });
};


Users.prototype.updateUserCurrency = (user, data, next) => {
    dao.updateUserCurrency(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: 'Record succesfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.updateUserCountry = (user, data, next) => {
    dao.updateUserCountry(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: 'Record succesfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.updateUserPassword = (user, data, next) => {
    dao.getUserByUUID(user.uuid, (err, response) => {
        if (err) {
            next({
                result: err,
                msg: err.message,
                success: true
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];
                data.uuid = user.uuid;
                dao.updateUserPassword(data, (err1, result) => {
                    if (err1) {
                        next({
                            result: err1,
                            msg: err1.message,
                            success: true
                        }, null);
                    } else {
                        var template = path.join(__dirname, '../..', 'views/emails/reset.ejs');
                        var ejstemp = {
                            Fullname: response.firstname + ' ' + response.lastname,
                            Email: response.email,
                            Date: moment().format('MM-DD-YYYY hh:mm:ss A Z')
                        };
                        ejs.renderFile(template, ejstemp, (err, html) => {
                            if (err) {
                                console.log(err); // Handle error
                            }
                            var mailData = {
                                from: 'info@shoppyapp.net',
                                email: response.email,
                                subject: 'Your Shoppy password has been changed',
                                message: html,
                                attachment: null
                            };

                            func.sendMail(mailData, (err, mailresponse) => {
                                if (err) {
                                    console.log('err: ', err.body);
                                }
                                console.log('mailresponse: ', mailresponse);
                            });
                        });

                        next(null, {
                            success: true,
                            msg: 'Password successfully changed.',
                            result: {
                                Email: response.email,
                                Fullname: response.firstname + ' ' + response.lastname,
                            }
                        });
                    }
                });
            } else {
                next(null, {
                    result: null,
                    msg: 'User does not exist with the given info.',
                    success: false
                });
            }
        }
    });
};

Users.prototype.validatePassword = (user, data, next) => {
    dao.validatePassword(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            console.log('response: ', response);
            if (response.success) {
                return next(null, {
                    msg: 'success!',
                    result: response,
                    success: true
                });
            } else {
                return next(null, {
                    msg: 'Invalid Password!',
                    result: response,
                    success: false
                });
            }
        }
    });
};

Users.prototype.getAllUserBanks = (user, params, next) => {
    dao.getAllUserBanks(user.uuid, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};

Users.prototype.saveUserBank = (user, data, next) => {
    dao.saveUserBank(user.uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.getUserBank = (user, _id, next) => {
    dao.getUserBank(user.uuid, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response[0]) {
                response = response[0];
                response.accountNo = response.accountNo.substr(0, (response.accountNo.length - 4)).replace(/[\S]/g, "*") + response.accountNo.slice(-4);
            }
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.deleteUserBank = (user, _id, next) => {
    dao.deleteUserBank(user.uuid, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.updateUserBank = (user, _id, data, next) => {
    dao.updateUserBank(user.uuid, _id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.createUserSellerShipping = (user, data, next) => {
    dao.createUserSellerShipping(user.uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully created!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.getAllUserSellerShipping = (user, next) => {
    dao.getAllUserSellerShipping(user.uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.deleteUserSellerShipping = (user, _id, next) => {
    dao.deleteUserSellerShipping(user.uuid, _id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.saveUserDevice = (user, data, next) => {
    dao.saveUserDevice(user.uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Device successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.deleteUserDevice = (user, data, next) => {
    dao.deleteUserDevice(user.uuid, data, (err, response) => {
        console.log('deleteUserDevice err: ', err);
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            console.log('deleteUserDevice response: ', response);
            next(null, {
                msg: 'Device successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.linkGoogleAccount = (user, data, next) => {
    dao.linkGoogleAccount(user.uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Facebook Account successfully link!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.unlinkGoogleAccount = (user, next) => {
    dao.unlinkGoogleAccount(user.uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Facebook Account successfully remove!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.linkFacebookAccount = (user, data, next) => {
    dao.linkFacebookAccount(user.uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Facebook Account successfully link!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.unlinkFacebookAccount = (user, next) => {
    dao.unlinkFacebookAccount(user.uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Facebook Account successfully remove!',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.followUser = (user, data, next) => {
    async.waterfall([
        (callback) => {
            dao.checkIfUserIfFollowed(user.uuid, data.userId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'User already followed!',
                            result: null,
                            success: false
                        })
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.followUser(user.uuid, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response.notifications) {
                        let user_player_ids = _.map(response.notifications.user_notifications, (row) => {
                            return row.user_player_ids;
                        });
                        console.log('user_player_ids: ', user_player_ids);
                        func.sendNotifications(config.app_name, response.notifications.title, response.notifications.notification_data, user_player_ids, (err, resp) => {
                            console.log('sendNotifications Err: ', err);
                            if (resp) {
                                console.log('sendNotifications Response: ', resp);
                            }
                        });
                    }
                    callback(null, response.response);
                }
            });
        }
    ], next);
};

Users.prototype.unfollowUser = (user, data, next) => {
    dao.unfollowUser(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'User successfully unfollow!',
                result: response,
                success: true
            });
        }
    });
};


Users.prototype.getUserFollowers = (uuid, next) => {
    dao.getUserFollowers(uuid, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};


Users.prototype.getUserFollowing = (uuid, next) => {
    dao.getUserFollowing(uuid, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Users.prototype.uploadUserAvatar = (type, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let fileObj = {
        img_name: data.img_name,
        img_type: data.img_type,
        img_size: data.img_size,
        uuid: data.uuid
    };

    let pathThumb;

    if (type == 'avatar') {
        pathThumb = path.resolve('./public/uploads/avatar');
    } else {
        pathThumb = path.resolve('./public/uploads/banner');
    }

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }



    function errResponse(error) {
        console.error('Dropbox Error: ', error);
        next({
            msg: error.message,
            result: error,
            success: false
        }, null);
    }

    if (type == 'avatar') {
        async.waterfall([
            (callback) => {
                dao.deleteUserAvatar(fileObj.uuid, (err, response) => {
                    if (err) {
                        next(err, null);
                    }
                    callback();
                });
            },
            (callback) => {
                sharp(data.img_path)
                    .resize(600, 600)
                    .max()
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadUserAvatar resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/avatar/' + fileObj.uuid + '/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                dbx.sharingCreateSharedLink({
                                    path: response.path_display,
                                }).then((resp) => {
                                    fileObj.img_name = response.path_display;
                                    fileObj.img_path = resp.url.replace('dl=0', 'raw=1');
                                    dao.uploadUserAvatar(fileObj, (err, response) => {
                                        if (err) {
                                            next({
                                                msg: err.msg,
                                                result: err,
                                                success: false
                                            }, null);
                                        }

                                        next(null, {
                                            msg: 'User Avatar successfully uploaded!',
                                            result: response,
                                            success: true
                                        });
                                    });
                                }).catch(errResponse);
                            })
                            .catch(errResponse);
                    });
            }
        ]);
    } else {
        async.waterfall([
            (callback) => {
                dao.deleteUserBanner(fileObj.uuid, (err, response) => {
                    if (err) {
                        next(err, null);
                    }
                    callback();
                });
            },
            (callback) => {
                sharp(data.img_path)
                    .resize(990, 390)
                    .max()
                    .toBuffer()
                    .then(resp => {
                        console.log('uploadUserBanner resp: ', resp);
                        dbx.filesUpload({
                                path: '/' + env + '/banner/' + fileObj.uuid + '/' + data.img_name,
                                contents: resp
                            }).then((response) => {
                                console.log('filesUpload: ', response);
                                dbx.sharingCreateSharedLink({
                                    path: response.path_display,
                                }).then((resp) => {
                                    fileObj.img_name = response.path_display;
                                    fileObj.img_path = resp.url.replace('dl=0', 'raw=1');

                                    dao.uploadUserBanner(fileObj, (err, response) => {
                                        if (err) {
                                            next({
                                                msg: err.msg,
                                                result: err,
                                                success: false
                                            }, null);
                                        }

                                        next(null, {
                                            msg: 'User Banner successfully uploaded!',
                                            result: response,
                                            success: true
                                        });
                                    });
                                }).catch(errResponse);
                            })
                            .catch(errResponse);
                    });
            }
        ]);
    }



};

Users.prototype.applyUserAsSellerUploadSelfie = (param, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let fileObj = {
        img_name: data.img_name,
        img_type: data.img_type,
        img_size: data.img_size,
        uuid: data.uuid
    };

    let pathThumb;
    pathThumb = path.resolve('./public/uploads/selfies');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    function errResponse(error) {
        console.error('Dropbox Error: ', error);
        next({
            msg: error.message,
            result: error,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            // TODO CODES HERE
            // dao.deleteUserAvatar(fileObj.uuid, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     callback();
            // });
            callback();
        },
        (callback) => {
            sharp(data.img_path)
                .resize(600, 600)
                .max()
                .toBuffer()
                .then(resp => {
                    dbx.filesUpload({
                            path: `/${env}/seller/selfies/${fileObj.uuid}/${data.img_name}`,
                            contents: resp
                        }).then((response) => {
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.img_name = response.path_display;
                                fileObj.img_path = resp.url.replace('dl=0', 'raw=1');

                                // This is the fields required in seller_evaluation
                                fileObj.seller_selfie_file = resp.url;
                                fileObj.seller_selfie_filename = data.img_name;

                                // This is the fields required in seller_evaluation_history
                                fileObj.statusClass = data.statusClass ? data.statusClass : '';
                                fileObj.tagName = data.tagName ? data.tagName : '';

                                dao.applyUserAsSellerUploadSelfie(fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'User Photo successfully uploaded!',
                                            result: response,
                                            sharing_resp: resp,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ]);
};

Users.prototype.applyUserAsSellerUploadProof = (param, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let fileObj = {
        img_name: data.img_name,
        img_type: data.img_type,
        img_size: data.img_size,
        uuid: data.uuid
    };

    let pathThumb;
    pathThumb = path.resolve('./public/uploads/proof');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    function errResponse(error) {
        console.error('Dropbox Error: ', error);
        next({
            msg: error.message,
            result: error,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            // TODO CODES HERE
            // dao.deleteUserAvatar(fileObj.uuid, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     callback();
            // });
            callback();
        },
        (callback) => {
            sharp(data.img_path)
                .resize(600, 600)
                .max()
                .toBuffer()
                .then(resp => {
                    dbx.filesUpload({
                            path: `/${env}/seller/proof/${fileObj.uuid}/${data.img_name}`,
                            contents: resp
                        }).then((response) => {
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.img_name = response.path_display;
                                fileObj.img_path = resp.url.replace('dl=0', 'raw=1');

                                // This is the fields required in seller_evaluation
                                fileObj.seller_evaluationId = data.seller_evaluationId;
                                fileObj.seller_proof_address = resp.url;
                                fileObj.seller_proof_address_filename = data.img_name;

                                // This is the fields required in seller_evaluation_history
                                fileObj.statusClass = data.statusClass ? data.statusClass : '';
                                fileObj.tagName = data.tagName ? data.tagName : '';

                                dao.applyUserAsSellerUploadProof(fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'User Photo successfully uploaded!',
                                            result: response,
                                            sharing_resp: resp,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ]);
};

Users.prototype.applyUserAsSellerUploadGovId = (param, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let fileObj = {
        img_name: data.img_name,
        img_type: data.img_type,
        img_size: data.img_size,
        uuid: data.uuid
    };

    let pathThumb;
    pathThumb = path.resolve('./public/uploads/govids');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    function errResponse(error) {
        console.error('Dropbox Error: ', error);
        next({
            msg: error.message,
            result: error,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            // TODO CODES HERE
            // dao.deleteUserAvatar(fileObj.uuid, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     callback();
            // });
            callback();
        },
        (callback) => {
            sharp(data.img_path)
                .resize(600, 600)
                .max()
                .toBuffer()
                .then(resp => {
                    dbx.filesUpload({
                            path: `/${env}/seller/govids/${fileObj.uuid}/${data.img_name}`,
                            contents: resp
                        }).then((response) => {
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.img_name = response.path_display;
                                fileObj.img_path = resp.url.replace('dl=0', 'raw=1');

                                // This is the fields required in seller_evaluation
                                fileObj.seller_evaluationId = data.seller_evaluationId;
                                fileObj.seller_government_id = resp.url;
                                fileObj.seller_government_id_filename = data.img_name;
                                fileObj.seller_government_idnumber = data.seller_government_idnumber ? data.seller_government_idnumber : '';

                                // This is the fields required in seller_evaluation_history
                                fileObj.statusClass = data.statusClass ? data.statusClass : '';
                                fileObj.tagName = data.tagName ? data.tagName : '';

                                dao.applyUserAsSellerUploadGovId(fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'User Photo successfully uploaded!',
                                            result: response,
                                            sharing_resp: resp,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ]);
};

Users.prototype.applyUserAsSellerUploadPersonalId = (param, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let fileObj = {
        img_name: data.img_name,
        img_type: data.img_type,
        img_size: data.img_size,
        uuid: data.uuid
    };

    let pathThumb;
    pathThumb = path.resolve('./public/uploads/personal');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    function errResponse(error) {
        console.error('Dropbox Error: ', error);
        next({
            msg: error.message,
            result: error,
            success: false
        }, null);
    }

    async.waterfall([
        (callback) => {
            // TODO CODES HERE
            // dao.deleteUserAvatar(fileObj.uuid, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     callback();
            // });
            callback();
        },
        (callback) => {
            sharp(data.img_path)
                .resize(600, 600)
                .max()
                .toBuffer()
                .then(resp => {
                    dbx.filesUpload({
                            path: `/${env}/seller/personal/${fileObj.uuid}/${data.img_name}`,
                            contents: resp
                        }).then((response) => {
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.img_name = response.path_display;
                                fileObj.img_path = resp.url.replace('dl=0', 'raw=1');

                                // This is the fields required in seller_evaluation
                                fileObj.seller_evaluationId = data.seller_evaluationId;
                                fileObj.seller_supporting_docs = resp.url;
                                fileObj.seller_supporting_docs_filename = data.img_name

                                // This is the fields required in seller_evaluation_history
                                fileObj.statusClass = data.statusClass ? data.statusClass : '';
                                fileObj.tagName = data.tagName ? data.tagName : '';

                                dao.applyUserAsSellerUploadPersonalId(fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'User Photo successfully uploaded!',
                                            result: response,
                                            sharing_resp: resp,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ]);
};

Users.prototype.userRatings = (user, params, next) => {
    dao.userRatings(user.uuid, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    })
};

exports.Users = Users;