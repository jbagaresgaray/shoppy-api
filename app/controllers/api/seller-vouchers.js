"use strict";

const daoVouchers = require('../../daos/api/seller-vouchers');

function Seller() {
    this.daoVouchers = daoVouchers;
}

Seller.prototype.getSellerVouchers = (sellerId, next) => {
    daoVouchers.getSellerVouchers(sellerId, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: err,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Seller vouchers succeessfully retrieve',
                result: response,
                success: true
            });
        }
    });
};


Seller.prototype.createSellerVoucher = (sellerId, data, next) => {
    daoVouchers.createSellerVoucher(sellerId, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};


Seller.prototype.updateSellerVoucher = (sellerId, voucherId, data, next) => {
    daoVouchers.updateSellerVoucher(sellerId, voucherId, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.deleteSellerVoucher = (sellerId, voucherId, next) => {
    daoVouchers.deleteSellerVoucher(sellerId, voucherId, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getSellerProductVouchers = (sellerId, voucherId, next) => {
    daoVouchers.getSellerProductVouchers(sellerId, voucherId, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.createSellerProductVoucher = (sellerId, voucherId, data, next) => {
    daoVouchers.createSellerProductVoucher(sellerId, voucherId, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};


Seller.prototype.deleteSellerProductVoucher = (sellerId, voucherId, productId, next) => {
    daoVouchers.deleteSellerProductVoucher(sellerId, voucherId, productId, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

exports.Seller = Seller;