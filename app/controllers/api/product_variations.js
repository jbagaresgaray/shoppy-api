'use strict';

let dao = require('../../daos/api/product_variations');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');
let _ = require('lodash');
let fs = require('fs');
let path = require('path');
let ejs = require('ejs');
let moment = require('moment');


function Variations() {
    this.dao = dao;
}

Variations.prototype.createVariations = (uuid, data, next) => {
    dao.createVariations(uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Variants Successfully added',
                result: response,
                success: true
            });
        }
    });
};

Variations.prototype.getProductVariations = (user, uuid, next) => {
    dao.getProductVariations(user, uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Variations.prototype.deleteAllProductVariations = (user, uuid, next) => {
    dao.deleteAllProductVariations(user, uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Variants succcessfully deleted',
                result: response,
                success: true
            });
        }
    });
};

Variations.prototype.deleteProductVariations = (uuid, id, next) => {
    dao.deleteProductVariations(uuid, id, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Variant succcessfully deleted',
                result: response,
                success: true
            });
        }
    });
};

Variations.prototype.updateProductVariations = (uuid, id, data, next) => {
    dao.updateProductVariations(uuid, id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Variant succcessfully updated',
                result: response,
                success: true
            });
        }
    });
};

exports.Variations = Variations;