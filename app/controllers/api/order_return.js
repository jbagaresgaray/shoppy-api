'use strict';

let orderReturnDao = require('../../daos/api/order_return');
let ledgerDao = require('../../daos/api/ledger');

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

let func = require('../../../app/utils/functions');

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const moment = require('moment');
const mkdirp = require('mkdirp');
const sharp = require('sharp');
const htmlToText = require('html-to-text');

function Orders() {
    this.orderReturnDao = orderReturnDao;
}

Orders.prototype.returnOrderRequest = (user, orderId, data, next) => {
    async.waterfall([
        (callback) => {
            orderReturnDao.isOrderReturnRequested(orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Order return already requested!",
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderReturnDao.isOrderReturnAccepted(data.returnId, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Order return request already accepted!",
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderReturnDao.returnOrderRequest(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {});

                    callback(null, {
                        msg: "Order successfully requested for return!",
                        result: response.returns,
                        success: true
                    });
                }
            });
        }
    ], next);
};

Orders.prototype.acceptReturnOrderRequest = (user, orderId, data, next) => {
    async.waterfall([
        (callback) => {
            orderReturnDao.isOrderReturnRequested(orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Return/Refund of order must be requested!",
                            result: null,
                            success: false
                        });
                    }
                }
            });
        },
        (callback) => {
            orderReturnDao.acceptReturnOrderRequest(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    // ================================================================================
                    // Push Notification Order Return to the seller
                    // ================================================================================
                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        if (!err && resp) {
                            console.log('acceptReturnOrderRequest sendNotifications Response: ', resp);
                        }
                    });

                    callback(null, {
                        msg: "Order successfully accepted for return!",
                        result: response,
                        success: true
                    });
                }
            });
        },
        (resp, callback) => {
            if (data.isReturnRequestedAcceptedRefund == 1) {
                async.waterfall([
                    (callback2) => {
                        data.orderNum = data.orderNum;
                        data.returnTransCode = data.returnTransCode;
                        data.Amount = data.returnTotal;
                        orderReturnDao.returnOrder(user.uuid, orderId, data, (err, response) => {
                            if (err) {
                                next({
                                    msg: err.msg,
                                    result: err,
                                    success: false
                                }, null);
                            } else {
                                let devices = _.map(response.notification.devices, (row) => {
                                    return row.onesignal_player_id;
                                });
                                var summary = htmlToText.fromString(response.notification.content);
                                func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                                    if (!err && resp) {
                                        console.log('returnOrder sendNotifications Response: ', resp);
                                    }
                                });
                                callback2(null, {
                                    msg: "Order successfully returned!",
                                    result: response,
                                    success: true
                                });
                            }
                        });
                    },
                    (returnResp, callback2) => {
                        // ================================================================================
                        // Save Transaction to Seller ledger
                        // ================================================================================
                        ledgerDao.saveUserLedger(data.sellerId, {
                            TransCode: func.generateOrderNumber(15),
                            Referenceno: data.orderNum,
                            Amount: data.returnTotal,
                            action: 'return_refund',
                            Remarks: 'Return payment for ORDER ' + data.orderNum + ' and RETURN REQUEST ID: ' + data.returnTransCode,
                            userId: data.sellerId
                        }, (ledgerErr, ledgerResponse) => {
                            if (ledgerErr) {
                                console.log('saveUserLedger ERROR: ', ledgerErr);
                            }
                            console.log('Ledger Response: ', ledgerResponse);
                            console.log('returnResp Response: ', returnResp);
                        });

                        // ================================================================================
                        // Save Transaction to Buyer ledger
                        // ================================================================================
                        ledgerDao.saveUserLedger(data.sellerId, {
                            TransCode: func.generateOrderNumber(15),
                            Referenceno: data.orderNum,
                            Amount: data.returnTotal,
                            action: 'return_refund',
                            Remarks: 'Accept refund payment for ORDER ' + data.orderNum + ' and RETURN REQUEST ID: ' + data.returnTransCode,
                            userId: data.buyerId
                        }, (ledgerErr, ledgerResponse) => {
                            if (ledgerErr) {
                                console.log('saveUserLedger ERROR: ', ledgerErr);
                            }
                            console.log('Ledger Response: ', ledgerResponse);
                            console.log('returnResp Response: ', returnResp);
                        });
                    },
                ], callback);
            } else {
                callback(null, resp);
            }
        }
    ], next);
};

Orders.prototype.returnOrderArrangeShipment = (user, orderId, data, next) => {
    orderReturnDao.returnOrderShipmentInfo(user.uuid, orderId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            let devices = _.map(response.notification.devices, (row) => {
                return row.onesignal_player_id;
            });
            var summary = htmlToText.fromString(response.notification.content);
            func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                if (!err && resp) {
                    console.log('returnOrder sendNotifications Response: ', resp);
                }
            });
            next(null, {
                msg: "Order successfully returned to shipment!",
                result: response.returns,
                success: true
            });
        }
    });
};

Orders.prototype.returnOrderCancelRequest = (user, orderId, data, next) => {
    async.waterfall([
        (callback) => {
            orderReturnDao.isOrderReturnRequested(orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Return/Refund of order must be requested!",
                            result: null,
                            success: false
                        });
                    }
                }
            });
        },
        (callback) => {
            orderReturnDao.returnOrderCancelRequest(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        if (!err && resp) {
                            console.log('returnOrderCancelRequest sendNotifications Response: ', resp);
                        }
                    });

                    callback(null, {
                        msg: "Order return request successfully cancelled!",
                        result: response,
                        success: true
                    });
                }
            });
        }
    ], next);
};

Orders.prototype.returnOrder = (user, orderId, data, next) => {
    async.waterfall([
        (callback) => {
            orderReturnDao.isOrderReturnRequested(orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Order return must be requested!",
                            result: null,
                            success: false
                        });
                    }
                }
            });
        },
        (callback) => {
            orderReturnDao.isOrderReturnAccepted(data.returnId, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        callback();
                    } else {
                        return next(null, {
                            msg: "Order return request must be accepted by the seller for evaluation!",
                            result: null,
                            success: false
                        });
                    }
                }

            });
        },
        (callback) => {
            orderReturnDao.isOrderReturnCancelled(data.returnId, orderId, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "WARNING: Order return request has been cancelled!",
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            orderReturnDao.returnOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        if (!err && resp) {
                            console.log('returnOrder sendNotifications Response: ', resp);
                        }
                    });

                    // ================================================================================
                    // Save Transaction to Seller ledger
                    // ================================================================================
                    ledgerDao.saveUserLedger(data.sellerId, {
                        TransCode: func.generateOrderNumber(15),
                        Referenceno: response.returns.orderNum,
                        Amount: response.returns.returnTotal,
                        action: 'return_refund',
                        Remarks: 'Refund payment for ORDER ' + response.returns.orderNum + ' and RETURN REQUEST ID: ' + response.returns.returnTransCode,
                        userId: response.seller.userId
                    }, (ledgerErr, ledgerResponse) => {
                        if (ledgerErr) {
                            console.log('saveUserLedger ERROR: ', ledgerErr);
                        }
                        console.log('Ledger Response: ', ledgerResponse);
                    });

                    // ================================================================================
                    // Save Transaction to Buyer ledger
                    // ================================================================================
                    ledgerDao.saveUserLedger(data.sellerId, {
                        TransCode: func.generateOrderNumber(15),
                        Referenceno: response.returns.orderNum,
                        Amount: response.returns.returnTotal,
                        action: 'return_refund',
                        Remarks: 'Accept refund payment for ORDER ' + response.returns.orderNum + ' and RETURN REQUEST ID: ' + response.returns.returnTransCode,
                        userId: response.buyer.userId
                    }, (ledgerErr, ledgerResponse) => {
                        if (ledgerErr) {
                            console.log('saveUserLedger ERROR: ', ledgerErr);
                        }
                        console.log('Ledger Response: ', ledgerResponse);
                    });

                    callback(null, {
                        msg: "Order successfully returned!",
                        result: response.returns,
                        success: true
                    });
                }
            });
        }
    ], next);
};


Orders.prototype.uploadOrderReturnFiles = (user, orderId, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let pathThumb = path.resolve('./public/uploads/returns');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    let fileObj = {
        file_name: data.img_name,
        file_type: data.img_type,
        file_size: data.img_size,
    };
    async.waterfall([
        (callback) => {
            orderReturnDao.isOrderReturnRequested(orderId, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                    }
                    fileObj.orderReturnId = response.returnId;
                    callback();
                }
            });
        },
        (callback) => {
            function errResponse(error) {
                console.error('Dropbox Error: ', error);
                callback({
                    msg: error.message,
                    result: error,
                    success: false
                }, null);
            }

            sharp(data.img_path)
                .resize(800, 800)
                .max()
                .toBuffer()
                .then(resp => {
                    console.log('uploadOrderReturnFiles resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/returns/files/' + fileObj.orderReturnId + '/' + data.img_name,
                            contents: resp
                        }).then((response) => {
                            console.log('filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_key = response.path_display;
                                fileObj.file_path = resp.url.replace('dl=0', 'raw=1');
                                orderReturnDao.uploadReturnRequestFiles(user.uuid, orderId, fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'Return attached image successfully saved!',
                                            result: response,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ], next);
};

Orders.prototype.returnOrderRequestShippingFiles = (user, orderId, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let pathThumb = path.resolve('./public/uploads/returns/shipping');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    let fileObj = {
        file_name: data.img_name,
        file_type: data.img_type,
        file_size: data.img_size,
    };
    async.waterfall([
        (callback) => {
            orderReturnDao.isOrderReturnRequested(orderId, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                    }
                    fileObj.orderReturnId = response.returnId;
                    callback();
                }
            });
        },
        (callback) => {
            function errResponse(error) {
                console.error('Dropbox Error: ', error);
                callback({
                    msg: error.message,
                    result: error,
                    success: false
                }, null);
            }

            sharp(data.img_path)
                .resize(800, 800)
                .max()
                .toBuffer()
                .then(resp => {
                    console.log('returnOrderRequestShippingFiles resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/returns/shipping/' + fileObj.orderReturnId + '/' + data.img_name,
                            contents: resp
                        }).then((response) => {
                            console.log('filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.file_key = response.path_display;
                                fileObj.file_path = resp.url.replace('dl=0', 'raw=1');
                                orderReturnDao.uploadReturnShippingFiles(user.uuid, orderId, fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'Return attached image successfully saved!',
                                            result: response,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ], next);
};


exports.Orders = Orders;