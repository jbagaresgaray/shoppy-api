'use strict';

let productsDao = require('../../daos/api/products');
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const moment = require('moment');
const mkdirp = require('mkdirp');
const sharp = require('sharp');


function Products() {
    this.productsDao = productsDao;
}


Products.prototype.createProduct = (user, data, next) => {
    productsDao.createProduct(user, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.mailCreatedProduct = (user, uuid, next) => {
    async.waterfall([
        (callback) => {
            productsDao.getProductImages(uuid, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                    }
                    console.log('response: ', response);
                    callback(null, response);
                }
            });
        },
        (product, callback) => {
            // SEND EMAIL TO NOTIFY SELLER ABOUT THE PRODUCT ENTRY
            let template = path.join(__dirname, '../..', 'views/emails/mobile/product.ejs');
            let ejstemp = {
                Fullname: (user.firstname + ' ' + user.lastname),
                Email: user.email,
                Product: product.productName,
                Price: product.productPrice,
                Description: product.productDesc,
                Image: product.image.img_path
            };

            ejs.renderFile(template, ejstemp, (err, html) => {
                if (err) {
                    console.log(err); // Handle error
                }
                let mailData = {
                    from: 'info@shoppyapp.net',
                    email: user.email,
                    subject: 'Yay, your product is up!',
                    message: html,
                    attachment: null
                };

                func.sendMail(mailData, (err, mailresponse) => {
                    if (err) {
                        console.log('err: ', err.body);
                        next(err, null);
                    }
                    console.log('mailresponse: ', mailresponse);
                    next(null, mailresponse);
                });
            });
        }
    ]);
};

Products.prototype.uploadProductImage = (uuid, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    let pathThumb = path.resolve('./public/uploads/products');

    if (!fs.existsSync(pathThumb)) {
        mkdirp(pathThumb, function (err) {
            if (err) console.error(err)
            else console.log('dir created')
        });
    }

    let fileObj = {
        img_name: data.img_name,
        img_type: data.img_type,
        img_size: data.img_size,
    };

    async.waterfall([
        (callback) => {
            productsDao.getProductById(uuid, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                    }
                    fileObj.productId = response.productId;
                    callback();
                }
            });
        },
        (callback) => {
            function errResponse(error) {
                console.error('Dropbox Error: ', error);
                callback({
                    msg: error.message,
                    result: error,
                    success: false
                }, null);
            }

            sharp(data.img_path)
                .resize(800, 800)
                .max()
                // .toFile(path800 + '/' + '800-' + data.file_name)
                .toBuffer()
                .then(resp => {
                    console.log('uploadProductImage resp: ', resp);
                    dbx.filesUpload({
                            path: '/' + env + '/products/' + fileObj.productId + '/' + data.img_name,
                            contents: resp
                        }).then((response) => {
                            console.log('filesUpload: ', response);
                            dbx.sharingCreateSharedLink({
                                path: response.path_display,
                            }).then((resp) => {
                                fileObj.img_name = response.path_display;
                                fileObj.img_path = resp.url.replace('dl=0', 'raw=1');;
                                productsDao.uploadProductImage(uuid, fileObj, (err, response) => {
                                    if (err) {
                                        next({
                                            msg: err.msg,
                                            result: err,
                                            success: false
                                        }, null);
                                    } else {
                                        next(null, {
                                            msg: 'Product Image successfully saved!',
                                            result: response,
                                            success: true
                                        });
                                    }
                                });
                            }).catch(errResponse);
                        })
                        .catch(errResponse);
                });
        }
    ], next);
};

Products.prototype.deleteProductImage = (uuid, imageId, next) => {
    productsDao.deleteProductImage(uuid, imageId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Image successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};


Products.prototype.getProductInfo = (uuid, params, next) => {
    productsDao.getProductInfo(uuid, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.getProductOnSameShop = (uuid, next) => {
    productsDao.getProductOnSameShop(uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.getProductVouchers = (uuid, next) => {
    productsDao.getProductVouchers(uuid, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'query successful',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.getProductDiscount = (uuid, next) => {
    productsDao.getProductDiscount(uuid, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'query successful',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.deleteProduct = (uuid, next) => {
    productsDao.deleteProduct(uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Successfully deleted',
                result: response,
                success: true
            });
        }
    });
};


Products.prototype.updateProduct = (user, product_id, data, next) => {
    productsDao.updateProduct(user, product_id, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.getAllProducts = (params, next) => {
    productsDao.getAllProducts(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response) {
                response.msg = '';
                response.success = true;
            }
            next(null, response);
        }
    });
};

Products.prototype.addProductShippingFee = (productId, data, next) => {
    productsDao.addProductShippingFee(productId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Shipping successfully added!',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.deleteProductShipping = (productId, next) => {
    productsDao.deleteProductShipping(productId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Product Shipping successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.getProductShipping = (productId, next) => {
    productsDao.getProductShipping(productId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: "",
                result: response,
                success: true
            });
        }
    });
};

Products.prototype.getproductsByCategory = (sellerId, categoryId, params, next) => {
    productsDao.getproductsByCategory(sellerId, categoryId, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: "Data successfully retrieve",
                result: response,
                success: true
            });
        }
    });
};


exports.Products = Products;