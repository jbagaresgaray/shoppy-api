"use strict";

const daoCollection = require('../../daos/api/seller-top-collections');
const _ = require('lodash');

function Seller() {
    this.daoCollection = daoCollection;
}


// ======================== SELLER TOP PICKS ============================ //
// ======================== SELLER TOP PICKS ============================ //
// ======================== SELLER TOP PICKS ============================ //

Seller.prototype.createTopPicks = (sellerId, data, next) => {
    daoCollection.createTopPicks(sellerId, data, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getTopPicks = (sellerId, params, next) => {
    daoCollection.getTopPicks(sellerId, params, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getTopPick = (id, sellerId, next) => {
    daoCollection.getTopPick(id, sellerId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};


Seller.prototype.updateTopPick = (id, sellerId, data, next) => {
    daoCollection.updateTopPick(id, sellerId, data, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.deleteTopPick = (sellerId, id, next) => {
    daoCollection.deleteTopPick(sellerId, id, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.activateTopPickCollection = (sellerId, collectionId, next) => {
    daoCollection.activateTopPickCollection(sellerId, collectionId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Collection successfully activated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.deActivateTopPickCollection = (sellerId, collectionId, next) => {
    daoCollection.deActivateTopPickCollection(sellerId, collectionId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Collection successfully deactivated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getActiveTopPickCollection = (sellerId, next) => {
    console.log("getActiveTopPickCollection 2");
    daoCollection.getActiveTopPickCollection(sellerId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

// =============
Seller.prototype.createTopPickProduct = (sellerId, collectionId, data, next) => {
    daoCollection.createTopPickProduct(sellerId, collectionId, data, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getTopPickProducts = (sellerId, collectionId, next) => {
    daoCollection.getTopPickProducts(sellerId, collectionId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getTopPickProduct = (sellerId, collectionId, productId, next) => {
    daoCollection.getTopPickProduct(sellerId, collectionId, productId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.deleteTopPickProduct = (sellerId, collectionId, productId, next) => {
    daoCollection.deleteTopPickProduct(sellerId, collectionId, productId, (err, response) => {
        if (err) {
            next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};


exports.Seller = Seller;