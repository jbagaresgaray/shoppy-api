"use strict";

const daoDiscounts = require('../../daos/api/seller-discount-promotions');

function Seller() {
    this.daoDiscounts = daoDiscounts;
}

// Get All Discount Promotions
Seller.prototype.getDiscountPromotions = (uuid, next) => {
    daoDiscounts.getDiscountPromotions(uuid, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

// Create Discount Promotions
Seller.prototype.createDiscountPromotion = (uuid, data, next) => {
    daoDiscounts.createDiscountPromotion(uuid, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

// Get Discount Promotion by id
Seller.prototype.getDiscountPromotion = (uuid, id, next) => {
    daoDiscounts.getDiscountPromotion(uuid, id, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

// Update Discount Promotions
Seller.prototype.updateDiscountPromotion = (uuid, id, data, next) => {
    daoDiscounts.updateDiscountPromotion(uuid, id, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

// Update Discount Promotions
Seller.prototype.deleteDiscountPromotion = (uuid, id, next) => {
    daoDiscounts.deleteDiscountPromotion(uuid, id, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

// get all Discount Promotions to a product
Seller.prototype.getProductDiscountPromotions = (uuid, id, next) => {
    daoDiscounts.getProductDiscountPromotions(uuid, id, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.createProductDiscountPromotion = (uuid, id, data, next) => {
    daoDiscounts.createProductDiscountPromotion(uuid, id, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully saved!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.updateProductVariantsDiscountPromotion = (uuid, id, promoId, variantId, data, next) => {
    daoDiscounts.updateProductVariantsDiscountPromotion(uuid, id, promoId, variantId, data, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.deleteProductVariantsDiscountPromotion = (uuid, id, promoId, variantId, next) => {
    daoDiscounts.deleteProductVariantsDiscountPromotion(uuid, id, promoId, variantId, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.deleteProductDiscountPromotion = (uuid, id, productId, next) => {
    daoDiscounts.deleteProductDiscountPromotion(uuid, id, productId, (err, response) => {
        if (err) {
            return next({
                msg: err,
                result: response,
                success: false
            }, null)
        } else {
            return next(null, {
                msg: 'Record successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};

exports.Seller = Seller;