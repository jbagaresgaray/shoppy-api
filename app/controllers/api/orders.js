'use strict';

let dao = require('../../daos/api/orders');
let ledgerDao = require('../../daos/api/ledger');
let notificationsDao = require('../../daos/api/notifications');

let func = require('../../../app/utils/functions');

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const moment = require('moment');
const mkdirp = require('mkdirp');
const sharp = require('sharp');


function Orders() {
    this.dao = dao;
}

Orders.prototype.createOrder = (user, data, next) => {
    dao.createOrder(user, data, (err, response) => {
        if (err) {
            return next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            console.log('createOrder: ', response.notifications);
            async.eachSeries(response.notifications, (item, callback) => {
                let devices = _.map(item.devices, (row) => {
                    return row.onesignal_player_id;
                });
                func.sendNotifications(config.app_name, item.title, item.notification_data, devices, (err, resp) => {
                    console.log('sendNotifications Err: ', err);
                    if (!err && resp) {
                        console.log('sendNotifications Response: ', resp);
                        callback();
                    }
                });
            });

            async.eachSeries(response.orderIds, (orderId, callback) => {
                dao.getOrderDetails(user.uuid, 'buyer', orderId, (err, response) => {
                    if (err) {
                        console.log('Send order email error: ', err);
                        callback();
                    }

                    // Send Email on here.
                    let template = path.join(__dirname, '../..', 'views/emails/mobile/neworder.ejs');
                    let totalPayment = parseFloat(response.orderAmt) + parseFloat(response.shippingFee);
                    _.each(response.details, (row) => {
                        row.detailPrice = config.currency.symbol + ' ' + row.detailPrice.toFixed(config.currency.decimal_digits);
                    });
                    let ejstemp = {
                        sellerName: response.sellerName,
                        orderNum: response.orderNum,
                        orderDateTime: response.orderDateTime,
                        buyerName: response.buyerName,
                        buyerEmail: response.orderEmail,
                        orderNote: response.orderNote,
                        orderDetails: response.details,
                        subTotal: config.currency.symbol + ' ' + response.orderAmt.toFixed(config.currency.decimal_digits),
                        shippingFee: config.currency.symbol + ' ' + response.shippingFee.toFixed(config.currency.decimal_digits),
                        totalPayment: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                        recipientName: response.orderShipName,
                        recipientNumber: response.orderPhone,
                        recipientAddress: response.orderShipAddress + ' ' + response.orderShipSuburb + ' ' + response.orderShipCity + ' ' + response.orderShipState + ' ' + ' ' + response.orderShipCountry + ', ' + response.orderShipZipCode,
                        paymentMethod: (response.isCOD == 1) ? 'COD' : 'Others',
                        amountPayable: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                    };
                    func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.sellerEmail, 'New Order on your ShoppyApp Seller Center');
                    callback();
                });
            });

            next(null, {
                msg: "Order successfully saved!",
                result: response.batchNum,
                success: true
            });
        }
    });
};

Orders.prototype.getOrderDetails = (user, type, orderId, next) => {
    dao.getOrderDetails(user.uuid, type, orderId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.getOrderDetailsByBatchNum = (user, batchNum, next) => {
    dao.getOrderDetailsByBatchNum(user.uuid, batchNum, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.getOrdersList = (user, action, type, next) => {
    if (action == 'pay' || action == 'unpaid') {
        dao.getPayableOrdersList(user.uuid, action, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'ship' || action == 'shipping') {
        dao.getShippedOrdersList(user.uuid, action, type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                _.remove(response, (row) => {
                    if (!_.isEmpty(row.order_return_info)) {
                        let retur = (row.order_return_info.isReturned == 1 && !_.isEmpty(row.order_return_info.returnedDateTime));
                        console.log('retur: ', retur);
                        return retur;
                    }
                });
                console.log('response: ', _.size(response));
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'receive') {
        dao.getReceivableOrdersList(user.uuid, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                _.remove(response, (row) => {
                    if (!_.isEmpty(row.order_return_info)) {
                        return (row.order_return_info.isReturned == 1 && !_.isEmpty(row.order_return_info.returnedDateTime));
                    }
                });
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'completed') {
        dao.getCompletedOrdersList(user.uuid, type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'cancelled') {
        dao.getCancelledOrdersList(user.uuid, type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else if (action == 'return') {
        dao.getReturnedOrdersList(user.uuid, type, (err, response) => {
            if (err) {
                next({
                    msg: err.msg,
                    result: err,
                    success: false
                }, null);
            } else {
                next(null, {
                    msg: "",
                    result: response,
                    success: true
                });
            }
        });
    } else {
        next(null, {
            msg: "No action tag found",
            result: [],
            success: true
        });
    }
};

Orders.prototype.acceptOrder = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderAcceptedSeller(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Order already accepted and preparing for shipping!",
                            result: response,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.acceptOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('acceptOrder: ', response);
                    // ================================================================================
                    // Push Notification Order Updates COD Order Approval - Buyer
                    // ================================================================================
                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response: ', resp);
                        }
                    });

                    // ================================================================================
                    // SEND EMAIL TO NOTIFY SELLER ABOUT THE PRODUCT ENTRY
                    // ================================================================================
                    let template = path.join(__dirname, '../..', 'views/emails/mobile/order_confirm.ejs');
                    let totalPayment = parseFloat(response.orders.orderAmt) + parseFloat(response.orders.shippingFee);
                    _.each(response.orders.details, (row) => {
                        row.detailPrice = config.currency.symbol + ' ' + row.detailPrice.toFixed(config.currency.decimal_digits);
                    });
                    let ejstemp = {
                        sellerName: response.orders.sellerName,
                        orderNum: response.orders.orderNum,
                        orderDateTime: response.orders.orderDateTime,
                        buyerName: response.orders.buyerName,
                        buyerEmail: response.orders.orderEmail,
                        orderNote: response.orders.orderNote,
                        orderDetails: response.orders.details,
                        subTotal: config.currency.symbol + ' ' + response.orders.orderAmt.toFixed(config.currency.decimal_digits),
                        shippingFee: config.currency.symbol + ' ' + response.orders.shippingFee.toFixed(config.currency.decimal_digits),
                        totalPayment: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                        recipientName: response.orders.orderShipName,
                        recipientNumber: response.orders.orderPhone,
                        recipientAddress: response.orders.orderShipAddress + ' ' + response.orders.orderShipSuburb + ' ' + response.orders.orderShipCity + ' ' + response.orders.orderShipState + ' ' + ' ' + response.orders.orderShipCountry + ', ' + response.orders.orderShipZipCode,
                        paymentMethod: (response.orders.isCOD == 1) ? 'COD' : 'Others',
                        amountPayable: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                    };
                    func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.orders.orderEmail, 'Your COD order #' + response.orders.orderNum + ' has been confirmed');

                    callback(null, {
                        msg: "Order Successfully Accepted!",
                        result: response.orders,
                        success: true
                    });
                }
            });
        }
    ], next);
};

Orders.prototype.payOrder = (user, orderId, data, next) => {
    dao.payOrder(user.uuid, orderId, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null);
        } else {
            next(null, {
                msg: "Order successfully paid!",
                result: response,
                success: true
            });
        }
    });
};

Orders.prototype.shippedOrder = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderAcceptedSeller(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Order already accepted and preparing for shipping!",
                            result: response,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.shippedOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    console.log('shippedOrder: ', response);
                    // ================================================================================
                    // Push Notification Order Updates COD Order Approval - Buyer
                    // ================================================================================
                    let devices = _.map(response.notification.devices, (row) => {
                        return row.onesignal_player_id;
                    });
                    var summary = htmlToText.fromString(response.notification.content);
                    func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {
                        console.log('sendNotifications Err: ', err);
                        if (!err && resp) {
                            console.log('sendNotifications Response: ', resp);
                        }
                    });

                    // ================================================================================
                    // SEND EMAIL TO NOTIFY SELLER ABOUT THE PRODUCT ENTRY
                    // ================================================================================
                    let template = path.join(__dirname, '../..', 'views/emails/mobile/order_shipped.ejs');
                    let totalPayment = parseFloat(response.orders.orderAmt) + parseFloat(response.orders.shippingFee);
                    _.each(response.details, (row) => {
                        row.detailPrice = config.currency.symbol + ' ' + row.detailPrice.toFixed(config.currency.decimal_digits);
                    });
                    let ejstemp = {
                        sellerName: response.orders.sellerName,
                        orderNum: response.orders.orderNum,
                        orderDateTime: response.orders.orderDateTime,
                        buyerName: response.orders.buyerName,
                        buyerEmail: response.orders.orderEmail,
                        orderNote: response.orders.orderNote,
                        orderDetails: response.orders.details,
                        subTotal: config.currency.symbol + ' ' + response.orders.orderAmt.toFixed(config.currency.decimal_digits),
                        shippingFee: config.currency.symbol + ' ' + response.orders.shippingFee.toFixed(config.currency.decimal_digits),
                        totalPayment: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                        recipientName: response.orders.orderShipName,
                        recipientNumber: response.orders.orderPhone,
                        recipientAddress: response.orders.orderShipAddress + ' ' + response.orders.orderShipSuburb + ' ' + response.orders.orderShipCity + ' ' + response.orders.orderShipState + ' ' + ' ' + response.orders.orderShipCountry + ', ' + response.orders.orderShipZipCode,
                        paymentMethod: (response.orders.isCOD == 1) ? 'COD' : 'Others',
                        amountPayable: config.currency.symbol + ' ' + totalPayment.toFixed(config.currency.decimal_digits),
                    };
                    func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.orders.orderEmail, 'Your Order #' + response.orders.orderNum + ' is being delivered');
                    callback(null, {
                        msg: "Order successfully shipped!",
                        result: response.orders,
                        success: true
                    });
                }
            });
        }
    ], next);
};

Orders.prototype.receivedOrder = (user, orderId, data, next) => {
    let htmlToText = require('html-to-text');
    async.waterfall([
        (callback) => {
            dao.isOrderReceived(user.uuid, orderId, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: "Order already received!",
                            result: response,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.receivedOrder(user.uuid, orderId, data, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null);
                } else {
                    // console.log('receivedOrder: ', response);

                    async.parallel([
                        (callback1) => {
                            // ================================================================================
                            // Save Transaction to Seller ledger
                            // ================================================================================
                            ledgerDao.saveUserLedger(response.seller.userId, {
                                TransCode: func.generateOrderNumber(15),
                                Referenceno: response.orders.orderNum,
                                Amount: response.orders.orderAmt,
                                Remarks: 'Payment for order ' + response.orders.orderNum,
                                userId: response.orders.sellerId,
                            }, (ledgerErr, ledgerResponse) => {
                                if (ledgerErr) {
                                    console.log('saveUserLedger ERROR: ', ledgerErr);
                                }
                                console.log('saveUserLedger ledgerResponse: ', ledgerResponse);

                                // ================================================================================
                                // Push Notification Order Received to the seller
                                // ================================================================================
                                notificationsDao.generateNotification({
                                    title: 'Payment Transfer',
                                    content: 'We are transferring payment for order <b>' + response.orders.orderNum + '</b> to you soon.',
                                    notifType: 3,
                                    userId: response.buyer.userId,
                                    sellerId: response.seller.userId,
                                    action: 'seller',
                                    params: {
                                        action: 'payment',
                                        ledgerId: ledgerResponse.ledgerId,
                                        refNo: ledgerResponse.Referenceno,
                                        userId: response.seller.userId
                                    },
                                }, (err, notifResp) => {
                                    if (err) {
                                        console.log('generateNotification ERROR: ', err);
                                    }
                                    let devices = _.map(notifResp.notification.devices, (row) => {
                                        return row.onesignal_player_id;
                                    });
                                    var summary = htmlToText.fromString(notifResp.notification.content);
                                    func.sendNotifications(config.app_name, summary, notifResp.notification.notification_data, devices, (err, resp) => {});
                                });

                                callback1();
                            });
                        },
                        (callback1) => {
                            // ================================================================================
                            // Push Notification Order Received to the seller
                            // ================================================================================
                            let devices = _.map(response.notification.devices, (row) => {
                                return row.onesignal_player_id;
                            });
                            var summary = htmlToText.fromString(response.notification.content);
                            func.sendNotifications(config.app_name, summary, response.notification.notification_data, devices, (err, resp) => {});
                            callback1();
                        }
                    ], () => {
                        callback(null, {
                            msg: "Order successfully received!",
                            result: response.orders,
                            success: true
                        });
                    });
                }
            });
        }
    ], next);
};


exports.Orders = Orders;