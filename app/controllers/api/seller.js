'use strict';

const dao = require('../../daos/api/seller');

const func = require('../../../app/utils/functions');
const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const _ = require('lodash');
const path = require('path');

function Seller() {
    this.dao = dao;
}

Seller.prototype.getSellerInfo = (uuid, next) => {
    dao.getSellerInfo(uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }
            return next(null, {
                msg: "",
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getAllSellers = (params, next) => {
    dao.getAllSellers(params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            response.msg = '';
            response.success = true;
            next(null, response);
        }
    });
};


Seller.prototype.getUserShopProfile = (uuid, next) => {
    dao.getUserShopProfile(uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.updateShopInformation = (user, data, next) => {
    dao.updateShopInformation(user.uuid, data, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: 'Shop Profile Successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getSellerCustomers = (user, next) => {
    console.log('getSellerCustomers 2');
    dao.getSellerCustomers(user.uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getSellerIncome = (user, next) => {
    console.log('getSellerIncome 2');
    dao.getSellerIncome(user.uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            const onGoingOrders = _.sumBy(response, (row) => {
                if (row.isPrepared == 1 && row.isPaid == 0 && row.isReceived == 0 && row.isCancelled == 0) {
                    return row.orderAmt;
                }
            });
            const completedOrders = _.sumBy(response, (row) => {
                if (row.isPrepared == 1 && row.isPaid == 1 && row.isReceived == 1 && row.isCancelled == 0 && row.isReturned == 0) {
                    return row.orderAmt;
                }
            });
            console.log('completedOrders: ', completedOrders);
            console.log('onGoingOrders: ', onGoingOrders);


            const myIncome = {
                onGoingOrders: {
                    totalAmt: onGoingOrders || 0,
                    orders: _.filter(response, (row) => {
                        if (row.isPrepared == 1 && row.isPaid == 0 && row.isReceived == 0 && row.isCancelled == 0) {
                            return row;
                        }
                    })
                },
                completedOrders: {
                    totalAmt: completedOrders || 0,
                    orders: _.filter(response, (row) => {
                        if (row.isPrepared == 1 && row.isPaid == 1 && row.isReceived == 1 && row.isCancelled == 0 && row.isReturned == 0) {
                            return row;
                        }
                    })
                }
            }
            console.log('myIncome: ', myIncome);

            return next(null, {
                msg: '',
                result: myIncome,
                success: true
            });
        }
    });
};

Seller.prototype.getSellerRatings = (uuid, next) => {
    console.log('getSellerRatings');
    dao.getSellerRatings(uuid, (err, response) => {
        if (err) {
            return next({
                result: err,
                msg: err.message,
                success: false
            }, null);
        } else {
            return next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getSellerProducts = (user, next) => {
    dao.getSellerProducts(user, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getShopProducts = (uuid, next) => {
    dao.getSellerProducts({
        uuid: uuid
    }, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.applySellerAsOfficialShop = (uuid, user, next) => {
    dao.applySellerAsOfficialShop(uuid, user, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            // Send Email on here.
            let template = path.join(__dirname, '../..', 'views/emails/mobile/officialshop_application.ejs');
            let ejstemp = {
                shopName: response.shopInfo.sellerName,
                Email: response.shopInfo.email,
                ShopCode: response.official_shop.shop_ref_code,
                SellerLink: config.api_host_url + '/home',
                Link: config.api_host_url + '/officialshop?code=' + response.official_shop.shop_url_code + '&token=' + response.official_shop.shop_url_token + '&action=user_seller'
            };
            func.sendMailTemplate(template, ejstemp, 'info@shoppyapp.net', response.shopInfo.email, 'Application for Official Shop');


            next(null, {
                msg: 'Thank you for joining the ShoppyApp Official Shop. Kindly check your email on what to do next.',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.officialShopApplicationStatus = (shopId, next) => {
    dao.officialShopApplicationStatus(shopId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getShopRatings = (uuid, params, next) => {
    dao.getShopRatings(uuid, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getShopSummaryRatings = (uuid, next) => {
    dao.getShopSummaryRatings(uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.rateShop = (uuid, data, next) => {
    dao.rateShop(uuid, data, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Record successfully submitted',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getShopCategories = (user, next) => {
    dao.getShopCategories(user, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Seller.prototype.getShopCategoriesByOtherSeller = (uuid, next) => {
    dao.getShopCategoriesByOtherSeller(uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

exports.Seller = Seller;