'use strict';

let dao = require('../../daos/api/product_like');
let rating = require('../../daos/api/product_rating');

let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');



function ProductLike() {
    this.dao = dao;
}

ProductLike.prototype.likeProduct = (user, uuid, next) => {
    async.waterfall([
        (callback) => {
            dao.checkIfProductIsLiked(user, uuid, (err, response) => {
                if (err) {
                    return next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    if (response && response.length > 0) {
                        return next(null, {
                            msg: 'Product already liked!',
                            result: null,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            dao.likeProduct(user, uuid, (err, response) => {
                if (err) {
                    next({
                        msg: err.msg,
                        result: err,
                        success: false
                    }, null)
                } else {
                    async.eachSeries(response.notification, (notif, cb) => {
                        func.sendNotifications(config.app_name, notif.title, response.notification_data, [notif.user_player_ids], (err, resp) => {
                            console.log('sendNotifications Err: ', err);
                            console.log('sendNotifications Response: ', resp);
                            if (resp) {
                                cb();
                            }
                        });
                    });

                    next(null, {
                        msg: 'Liked successfully',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ], next);
};

ProductLike.prototype.unlikeProduct = (user, uuid, next) => {
    dao.unlikeProduct(user, uuid, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Unliked successfully',
                result: response,
                success: true
            });
        }
    });
};

ProductLike.prototype.getAllUserProductLikes = (user, next) => {
    dao.getAllUserProductLikes(user, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};


function ProductRating() {
    this.rating = rating;
}

// ======================== PRODUCTS RATING ============================ //
// ======================== PRODUCTS RATING ============================ //
// ======================== PRODUCTS RATING ============================ //

ProductRating.prototype.rateProduct = (user, productId, data, next) => {
    async.waterfall([
        (callback) => {
            rating.checkIfProductIsRated(user, productId, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        response = response[0];
                        return next(null, {
                            msg: 'Product already rated!',
                            result: response,
                            success: false
                        });
                    } else {
                        callback();
                    }
                }
            });
        },
        (callback) => {
            rating.rateProduct(user, productId, data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    callback(null, {
                        msg: 'Product successfully rated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ], next);
};

ProductRating.prototype.updateRating = (user, productId, data, next) => {
    async.waterfall([
        (callback) => {
            rating.checkIfProductIsRated(user, productId, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        callback()
                    } else {
                        return next(null, {
                            msg: 'Product has no rating from this user',
                            result: response,
                            success: true
                        });
                    }
                }
            });
        },
        (callback) => {
            rating.updateRating(user, productId, data, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    callback(null, {
                        msg: 'Product rating successfully updated!',
                        result: response,
                        success: true
                    });
                }
            });
        }
    ], next);
};


ProductRating.prototype.getAllProductRatings = (productId, params, next) => {
    rating.getAllProductRatings(productId, params, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

ProductRating.prototype.uploadRatingAttachments = (productRatingId, data, next) => {
    rating.uploadRatingAttachments(productRatingId, data, (err, response) => {
        if (err) {
            return next(err, null);
        } else {
            return next(null, {
                msg: 'Rating attachments successfully uploaded',
                result: response,
                success: true
            });
        }
    })
};



exports.ProductLike = ProductLike;
exports.ProductRating = ProductRating;