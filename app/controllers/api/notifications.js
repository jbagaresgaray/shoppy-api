'use strict';

let dao = require('../../daos/api/notifications');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);


function Notification() {
    this.dao = dao;
}


Notification.prototype.getUpdatesNotifications = (user, next) => {
    dao.getUpdatesNotifications(user, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Notification.prototype.getNotificationDetails = (user, notifId, next) => {
    dao.getNotificationDetails(user, notifId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            if (response && response.length > 0) {
                response = response[0];
            }

            next(null, {
                msg: '',
                result: response,
                success: true
            });
        }
    });
};

Notification.prototype.updateNotification = (user, notifId, next) => {
    dao.updateNotification(user, notifId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Notification successfully updated!',
                result: response,
                success: true
            });
        }
    });
};

Notification.prototype.deleteNotification = (user, notifId, next) => {
    dao.deleteNotification(user, notifId, (err, response) => {
        if (err) {
            next({
                msg: err.msg,
                result: err,
                success: false
            }, null)
        } else {
            next(null, {
                msg: 'Notification successfully deleted!',
                result: response,
                success: true
            });
        }
    });
};



exports.Notification = Notification;