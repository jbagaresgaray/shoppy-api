webpackJsonp([0],{

/***/ 111:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 111;

/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/help-content/help-content.module": [
		154
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 153;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpContentPageModule", function() { return HelpContentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__help_content__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HelpContentPageModule = /** @class */ (function () {
    function HelpContentPageModule() {
    }
    HelpContentPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__help_content__["a" /* HelpContentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__help_content__["a" /* HelpContentPage */]),
            ],
        })
    ], HelpContentPageModule);
    return HelpContentPageModule;
}());

//# sourceMappingURL=help-content.module.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HelpContentPage = /** @class */ (function () {
    function HelpContentPage(navCtrl, navParams, services) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.services = services;
        this.content = {};
        this.action = navParams.get('action');
        this.actionId = navParams.get('actionId');
        this.content = {
            title: '',
            content: ''
        };
    }
    HelpContentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HelpContentPage');
        if (this.action == 'post') {
            this.getHelpPost(this.actionId);
        }
    };
    HelpContentPage.prototype.getHelpPost = function (postId) {
        var _this = this;
        this.services.getHelpPost(postId).then(function (data) {
            console.log('getHelpPost: ', data);
            if (data && data.success) {
                _this.content = data.result;
            }
        }, function (error) {
            console.log('error: ', error);
        });
    };
    HelpContentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-help-content',template:/*ion-inline-start:"C:\Projects\shoppy-api\app\views\help-centre\src\pages\help-content\help-content.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{content.title}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-grid fixed center>\n    <div padding [innerHtml]="content.content"></div>\n    <div padding-top text-center>\n    	<p>Does this answer your question?</p>\n    	<ion-row>\n    		<ion-col col-6>\n    			<button ion-button icon-start outline><ion-icon name="ios-thumbs-up-outline"></ion-icon>&nbsp;Yes</button>\n    		</ion-col>\n    		<ion-col col-6>\n    			<button ion-button icon-start outline><ion-icon name="ios-thumbs-down-outline"></ion-icon>&nbsp;No</button>\n    		</ion-col>\n    	</ion-row>\n    </div>\n    <ion-item-divider color="light">&nbsp;</ion-item-divider>\n    <ion-list no-lines>\n      <ion-list-header>\n        <small>Need further clarfications?</small>\n      </ion-list-header>\n      <ion-item no-lines>\n        <ion-icon name="ios-chatbubbles-outline" item-start color="danger"></ion-icon>\n        <h2>Live Chat</h2>\n        <p>Contact ShoppyApp Now!</p>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-icon name="ios-mail-open-outline" item-start color="danger"></ion-icon>\n        <h2>Email</h2>\n        <p>support@shoppyapp.com</p>\n      </ion-item>\n      <ion-item no-lines>\n        <ion-icon name="ios-call-outline" item-start color="secondary"></ion-icon>\n        <h2>Call</h2>\n        <p>02 123 123131</p>\n      </ion-item>\n      <ion-list-header>\n        <small>Mon-Fri 9am-6pm. except public holidays</small>\n      </ion-list-header>\n    </ion-list>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"C:\Projects\shoppy-api\app\views\help-centre\src\pages\help-content\help-content.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */]])
    ], HelpContentPage);
    return HelpContentPage;
}());

//# sourceMappingURL=help-content.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__help_content_help_content__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_services_services__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, loadingCtrl, services) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.services = services;
        this.params = {};
        this.categories = [];
        this.posts = [];
        this.action = navParams.get('action');
        this.actionId = navParams.get('actionId');
        this.params = navParams.get('params');
        if (this.params) {
            this.myInput = this.params.name;
        }
    }
    HomePage_1 = HomePage;
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('HomePage HelpContentPage');
        if (this.action == 'category') {
            this.getAllHelpPosts(this.actionId);
        }
        else {
            this.getAllHelpCategory();
        }
    };
    HomePage.prototype.viewCategory = function (categorId) {
        var params = __WEBPACK_IMPORTED_MODULE_2_lodash___default.a.find(this.categories, { 'categoryId': categorId });
        this.navCtrl.push(HomePage_1, {
            action: 'category',
            params: params,
            actionId: categorId
        });
    };
    HomePage.prototype.viewContent = function (postId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__help_content_help_content__["a" /* HelpContentPage */], {
            action: 'post',
            actionId: postId
        });
    };
    HomePage.prototype.doRefresh = function (ev) {
        setTimeout(function () {
            ev.complete();
        }, 1000);
    };
    HomePage.prototype.getAllHelpCategory = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            dismissOnPageChange: true
        });
        loading.present();
        this.services.getAllHelpCategory().then(function (data) {
            console.log('getAllHelpCategory: ', data);
            if (data && data.success) {
                _this.categories = data.result;
            }
            loading.dismiss();
        }, function (error) {
            console.log('error: ', error);
            loading.dismiss();
        });
    };
    HomePage.prototype.getAllHelpPosts = function (categoryId) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            dismissOnPageChange: true
        });
        loading.present();
        this.services.getAllHelpPosts(categoryId).then(function (data) {
            console.log('getAllHelpPosts: ', data);
            if (data && data.success) {
                _this.posts = data.result;
            }
            loading.dismiss();
        }, function (error) {
            console.log('error: ', error);
            loading.dismiss();
        });
    };
    HomePage = HomePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Projects\shoppy-api\app\views\help-centre\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-searchbar [(ngModel)]="myInput" placeholder="Search for a topic"></ion-searchbar>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content refreshingSpinner="dots"></ion-refresher-content>\n  </ion-refresher>\n  <ion-grid fixed>\n    <div *ngIf="action != \'category\'">\n      <ion-item-divider color="light">&nbsp;</ion-item-divider>\n      <ion-list-header>CATEGORIES</ion-list-header>\n      <ion-item-divider color="light">&nbsp;</ion-item-divider>\n      <ion-grid>\n        <ion-row text-wrap>\n          <ion-col col-4 (click)="viewCategory(2)">\n            <div text-center>\n              <ion-icon name="ios-list-box-outline"></ion-icon>\n              <p>Orders & Payment</p>\n            </div>\n          </ion-col>\n          <ion-col col-4 (click)="viewCategory(3)">\n            <div text-center>\n              <ion-icon name="ios-bus-outline"></ion-icon>\n              <p>Shipping & Delivery</p>\n            </div>\n          </ion-col>\n          <ion-col col-4 (click)="viewCategory(5)">\n            <div text-center>\n              <ion-icon name="ios-git-compare-outline"></ion-icon>\n              <p>Returns & Refunds</p>\n            </div>\n          </ion-col>\n          <ion-col col-4 (click)="viewCategory(4)">\n            <div text-center>\n              <ion-icon name="ios-paper-outline"></ion-icon>\n              <p>Selling & Billing</p>\n            </div>\n          </ion-col>\n          <ion-col col-4 (click)="viewCategory(1)">\n            <div text-center>\n              <ion-icon name="ios-lock-outline"></ion-icon>\n              <p>Account Safety & Others</p>\n            </div>\n          </ion-col>\n          <ion-col col-4 (click)="viewCategory(6)">\n            <div text-center>\n              <ion-icon name="ios-basket-outline"></ion-icon>\n              <p>Official Shop</p>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <ion-item-divider color="light">&nbsp;</ion-item-divider>\n      <ion-list no-lines>\n        <ion-list-header>\n          <small>Need further clarfications?</small>\n        </ion-list-header>\n        <ion-item no-lines>\n          <ion-icon name="ios-chatbubbles-outline" item-start color="danger"></ion-icon>\n          <h2>Live Chat</h2>\n          <p>Contact ShoppyApp Now!</p>\n        </ion-item>\n        <ion-item no-lines>\n          <ion-icon name="ios-mail-open-outline" item-start color="danger"></ion-icon>\n          <h2>Email</h2>\n          <p>support@shoppyapp.com</p>\n        </ion-item>\n        <ion-item no-lines>\n          <ion-icon name="ios-call-outline" item-start color="secondary"></ion-icon>\n          <h2>Call</h2>\n          <p>02 123 123131</p>\n        </ion-item>\n        <ion-list-header>\n          <small>Mon-Fri 9am-6pm. except public holidays</small>\n        </ion-list-header>\n      </ion-list>\n    </div>\n    <div *ngIf="action == \'category\'">\n      <ion-list>\n      	<button ion-item text-wrap *ngFor="let post of posts" (click)="viewContent(post.postId)">\n      		<h2>{{post.title}}</h2>\n      	</button>\n      </ion-list>\n    </div>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"C:\Projects\shoppy-api\app\views\help-centre\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__providers_services_services__["a" /* ServicesProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_services_services__["a" /* ServicesProvider */]) === "function" && _d || Object])
    ], HomePage);
    return HomePage;
    var HomePage_1, _a, _b, _c, _d;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(222);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_help_content_help_content_module__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_services_services__ = __webpack_require__(78);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {
                    backButtonText: '',
                    iconMode: 'ios',
                    mode: 'ios',
                }, {
                    links: [
                        { loadChildren: '../pages/help-content/help-content.module#HelpContentPageModule', name: 'HelpContentPage', segment: 'help-content', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_8__pages_help_content_help_content_module__["HelpContentPageModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_9__providers_services_services__["a" /* ServicesProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    api_url: 'http://192.168.22.7:3000/app/1/',
    api_prod_url: 'https://shoppy-123456789.herokuapp.com/app/1/',
};
//# sourceMappingURL=environments.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_home_home__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MyApp = /** @class */ (function () {
    function MyApp(platform) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_2__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Projects\shoppy-api\app\views\help-centre\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Projects\shoppy-api\app\views\help-centre\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */]) === "function" && _a || Object])
    ], MyApp);
    return MyApp;
    var _a;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environments__ = __webpack_require__(252);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServicesProvider = /** @class */ (function () {
    function ServicesProvider(http) {
        this.http = http;
        this.base_url = (__WEBPACK_IMPORTED_MODULE_2__environments_environments__["a" /* environment */].production == true) ? __WEBPACK_IMPORTED_MODULE_2__environments_environments__["a" /* environment */].api_prod_url : __WEBPACK_IMPORTED_MODULE_2__environments_environments__["a" /* environment */].api_url;
        console.log('Hello ServicesProvider Provider');
    }
    ServicesProvider.prototype.getAllHelpCategory = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var callbackResponse = function (resp) {
                resolve(resp);
            };
            var errorResponse = function (error) {
                console.log('error: ', error);
                reject(error.error);
            };
            _this.http.get(_this.base_url + 'helpcentre/category', {}).subscribe(callbackResponse, errorResponse);
        });
    };
    ServicesProvider.prototype.getHelpPost = function (postId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var callbackResponse = function (resp) {
                resolve(resp);
            };
            var errorResponse = function (error) {
                console.log('error: ', error);
                reject(error.error);
            };
            _this.http.get(_this.base_url + 'helpcentre/' + postId, {}).subscribe(callbackResponse, errorResponse);
        });
    };
    ServicesProvider.prototype.getAllHelpPosts = function (categoryId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var callbackResponse = function (resp) {
                resolve(resp);
            };
            var errorResponse = function (error) {
                console.log('error: ', error);
                reject(error.error);
            };
            _this.http.get(_this.base_url + 'helpcentre/category/' + categoryId, {}).subscribe(callbackResponse, errorResponse);
        });
    };
    ServicesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ServicesProvider);
    return ServicesProvider;
}());

//# sourceMappingURL=services.js.map

/***/ })

},[201]);
//# sourceMappingURL=main.js.map