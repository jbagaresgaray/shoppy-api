(function() {
    'use strict';

    function getUrlVars() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    var code = getUrlVars()["code"];
    var action = getUrlVars()["action"];
    
    $('.img-complete').hide();
    if (action === 'user_buyer') {
        var uuid = getUrlVars()["uuid"];

        $.get("/api/1/verify/" + code + "/id/" + uuid, function(data, status) {
            if (data.success) {
                $('.circle-loader').toggleClass('load-complete');

                setTimeout(function() {
                    $('.circle-loader').hide();
                    $('.img-complete').show();

                    $('#status').text(data.msg);
                }, 600);
            } else {
                $.post("/api/1/verify/" + code + "/id/" + uuid, function(data1, status) {
                    if (data1.success) {
                        setTimeout(function() {
                            $('.circle-loader').toggleClass('load-complete');
                            $('.checkmark').toggle();

                            $('#status').text(data1.msg);
                        }, 500);
                    }
                });
            }
        });
    } else if (action === 'dash_user') {
        $.get("/app/1/users/" + code + "/verify", function(data, status) {
            if (data.success) {
                $('.circle-loader').toggleClass('load-complete');

                setTimeout(function() {
                    $('.circle-loader').hide();
                    $('.img-complete').show();

                    $('#status').text(data.msg);
                }, 600);
            } else {
                $.post("/app/1/users/" + code + "/verify", function(data1, status) {
                    if (data1.success) {
                        setTimeout(function() {
                            $('.circle-loader').toggleClass('load-complete');
                            $('.checkmark').toggle();

                            $('#status').text(data1.msg);
                        }, 500);
                    }
                });
            }
        });
    }
})();