(function() {
    'use strict';

    function getUrlVars() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    $('#loading').hide();
    $('#toHide').show();
    var action = getUrlVars()["action"];

    if (action === 'user_buyer') {
        $("#resetForm").submit(function(e) {
            $('#loading').show();
            $('#toHide').hide();
            $('#msg').text('Resetting Password');
            $.ajax({
                type: "POST",
                url: '/api/1/reset',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    password: $('#password').val(),
                    confirmpassword: $('#confirmpassword').val(),
                    code: getUrlVars()["code"],
                    uuid: getUrlVars()["uuid"]
                },
                success: function(data) {
                    if (data.success) {
                        $('.circle-loader').toggleClass('load-complete');
                        $('.checkmark').toggle();

                        $('#msg').text(data.msg);
                    } else {
                        $('#msg').text('');
                        $('#loading').hide();
                        $('#toHide').show();
                        toastr.warning(data.msg);
                    }
                },
                error: function(error) {
                    $('#msg').text('');
                    $('#loading').hide();
                    $('#toHide').show();
                    console.log('error: ', error);
                    if (error) {
                        let resp = error.responseJSON;
                        if (_.isArray(resp.result)) {
                            _.each(resp.result, function(row) {
                                toastr.warning(row.msg);
                            });
                            return;
                        }
                    }
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    } else if (action === 'dash_user') {
        $("#resetForm").submit(function(e) {
            $('#loading').show();
            $('#toHide').hide();
            $('#msg').text('Resetting Password');
            $.ajax({
                type: "POST",
                url: '/app/1/resetpassword',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: {
                    password: $('#password').val(),
                    confirmpassword: $('#confirmpassword').val(),
                    code: getUrlVars()["code"],
                    uuid: getUrlVars()["uuid"]
                },
                success: function(data) {
                    if (data.success) {
                        $('.circle-loader').toggleClass('load-complete');
                        $('.checkmark').toggle();

                        $('#msg').text(data.msg);
                    } else {
                        $('#msg').text('');
                        $('#loading').hide();
                        $('#toHide').show();
                        toastr.warning(data.msg);
                    }
                },
                error: function(error) {
                    $('#msg').text('');
                    $('#loading').hide();
                    $('#toHide').show();
                    console.log('error: ', error);
                    if (error) {
                        let resp = error.responseJSON;
                        if (_.isArray(resp.result)) {
                            _.each(resp.result, function(row) {
                                toastr.warning(row.msg);
                            });
                            return;
                        }
                    }
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    }



})()