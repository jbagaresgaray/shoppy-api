/*jshint camelcase: false */

'use strict';

// ======================== VALIDATION ============================ //
let validatorUser = require('../validation/users');
let validatorBanks = require('../validation/banks');
let validatorCourier = require('../validation/courier');
let validatorPayment = require('../validation/paymentprovider');
let validatorSharer = require('../validation/email');
let validatorContent = require('../validation/contents');
let validatorCategory = require('../validation/category');
let validatorAttribute = require('../validation/attribute');
let validatorAttributeCategory = require('../validation/attribute_category');
let validatorFormInptType = require('../validation/form_input_type');
let validatorBuyerRating = require('../validation/buyer_ratings');
let validatorDiscountPromotions = require('../validation/discount_promotions');


// ======================== ROUTING ============================ //
let users = require('./routing/app/users');
let category = require('./routing/app/category');
let attribute = require('./routing/app/attribute');
let attribute_category = require('./routing/app/attribute_category');
let forminputtype = require('./routing/app/form_input_type');
let banks = require('./routing/app/banks');
let misc = require('./routing/app/misc');
let subcategory = require('./routing/app/subcategory');
let brands = require('./routing/app/brands');
let courier = require('./routing/app/courier');
let paymentprovider = require('./routing/app/paymentprovider');
let email = require('./routing/app/email');
let buyer_seller = require('./routing/app/buyer_seller');
let mobile = require('./routing/app/mobile');
let tags = require('./routing/app/tags');
let orders = require('./routing/app/order');
let newsletter = require('./routing/app/newsletter');
let helpcentre = require('./routing/app/helpcentre');
let trackings = require('./routing/app/trackings');
let officialshop = require('./routing/app/officialshop');

let path = require("path");

module.exports = function (app, passport, config, middleware) {

    app.route('/')
        .get(function onRequest(req, res) {
            res.render('index');
        });

    app.route('/verify')
        .get(function onRequest(req, res) {
            res.render('verify');
        });

    app.route('/resetpassword')
        .get(function onRequest(req, res) {
            res.render('resetpassword');
        });

    app.route('/helpcentre')
        .get(function onRequest(req, res) {
            res.sendFile(path.resolve('app', 'views', 'help-centre', 'index.html'));
        });

    // ======================== OFFICIAL SHOP ============================ //
    // ======================== OFFICIAL SHOP ============================ //
    // ======================== OFFICIAL SHOP ============================ //

    app.route('/officialshop')
        .get(function onRequest(req, res) {
            res.sendFile(path.resolve('app', 'views', 'officialshop', 'index.html'));
        });

    app.route(config.app_version + '/officialshop/validate')
        .post(officialshop.validateSellerCode);

    // ============================
    //        OFFICIAL SHOP 
    //   Uses different authentication 
    // ============================
    app.route(config.app_version + '/officialshop/info')
        .post(middleware.authorizeSellerApplicationAuth, officialshop.saveApplicationInfo)
        .get(middleware.authorizeSellerApplicationAuth, officialshop.getApplicationInfo);

    app.route(config.app_version + '/officialshop/upload/cor')
        .post(middleware.authorizeSellerApplicationAuth, officialshop.uploadCORFile)
        .delete(middleware.authorizeSellerApplicationAuth, officialshop.deleteCORFile);

    app.route(config.app_version + '/officialshop/upload/license')
        .post(middleware.authorizeSellerApplicationAuth, officialshop.uploadBusinessLicenseFile)
        .delete(middleware.authorizeSellerApplicationAuth, officialshop.deleteLicenseFile);

    app.route(config.app_version + '/officialshop/upload/logo')
        .post(middleware.authorizeSellerApplicationAuth, officialshop.uploadBusinessLogoFile)
        .delete(middleware.authorizeSellerApplicationAuth, officialshop.deleteBusinessLogo);




    // ======================== DROPBOX TEST ============================ //
    // ======================== DROPBOX TEST ============================ //
    // ======================== DROPBOX TEST ============================ //
    app.route(config.app_version + '/testupload')
        .post(misc.testUpload);

    app.route(config.app_version + '/getallupload')
        .get(misc.getAllUploads);

    app.route(config.app_version + '/previewupload/:file')
        .get(misc.previewFileUpload);

    // ======================== PUSH NOTIFICATION TEST ============================ //
    // ======================== PUSH NOTIFICATION TEST ============================ //
    // ======================== PUSH NOTIFICATION TEST ============================ //
    app.route(config.app_version + '/push')
        .post(misc.testPushNotif)
        .get(misc.getAllNotifications);

    app.route(config.app_version + '/push/:notifId')
        .get(misc.getNotification)
        .delete(misc.deleteNotification);

    app.route(config.app_version + '/devices')
        .get(misc.getAllDevices);

    // ======================== AUTHENTICATION / AUTHORIZATION ============================ //
    // ======================== AUTHENTICATION / AUTHORIZATION ============================ //
    // ======================== AUTHENTICATION / AUTHORIZATION ============================ //
    app.route(config.app_version + '/authenticate')
        .post(validatorUser.validateBasicAuth, passport.authenticate('user'), users.login);

    app.route(config.app_version + '/logout')
        .get(users.logout);


    // ======================== SEND MAIL ============================ //
    // ======================== SEND MAIL ============================ //
    // ======================== SEND MAIL ============================ //
    app.route(config.app_version + '/shareMail')
        .post(validatorSharer.validateShare, email.shareExperience);
    app.route(config.app_version + '/sendMail')
        .post(validatorSharer.validateSend, email.sendMail);

    app.route(config.app_version + '/sendTestMail')
        .post(email.sendTestMail);

    // ======================== FORGOT PASSWORD ============================ //
    // ======================== FORGOT PASSWORD ============================ //
    // ======================== FORGOT PASSWORD ============================ //
    app.route(config.app_version + '/forgotpassword')
        .post(validatorUser.validateForgot, users.forgotPassword);

    app.route(config.app_version + '/resetpassword')
        .post(validatorUser.validateReset, users.resetPassword);

    // ======================== DASHBOARD USERS ============================ //
    // ======================== DASHBOARD USERS ============================ //
    // ======================== DASHBOARD USERS ============================ //

    app.route(config.app_version + '/users')
        .get(users.getAllUsers)
        .post(validatorUser.validateUser, users.createUser);

    app.route(config.app_version + '/users/:user_id')
        .get(users.getUser)
        .delete(users.deleteUser)

    app.route(config.app_version + '/users/:user_id/account')
        .put(validatorUser.validateUserAccount, users.updateUserAccount);

    app.route(config.app_version + '/users/:user_id/profile')
        .put(validatorUser.validateUserProfile, users.updateUser);

    app.route(config.app_version + '/users/:user_id/email')
        .put(validatorUser.validateUserEmail, users.updateUserEmail);

    app.route(config.app_version + '/users/:hashkey/verify')
        .get(users.checkIfVerified)
        .post(users.verifyUser);

    // ======================== MISC ============================ //
    // ======================== MISC ============================ //
    // ======================== MISC ============================ //

    app.route(config.app_version + '/country')
        .get(misc.getAllCountry);

    app.route(config.app_version + '/timezone')
        .get(misc.getAllTimezones);

    app.route(config.app_version + '/currency')
        .get(misc.getAllCurrencies);


    // ======================== CATEGORY ============================ //
    // ======================== CATEGORY ============================ //
    // ======================== CATEGORY ============================ //

    app.route(config.app_version + '/category')
        .get(category.getAllCatetories)
        .post(validatorCategory.validateCategory, category.createCategory);

    app.route(config.app_version + '/category/:categoryId')
        .get(category.getCategory)
        .delete(category.deleteCategory)
        .put(validatorCategory.validateCategory, category.updateCategory);

    app.route(config.app_version + '/category/:categoryId/subcategory')
        .get(subcategory.getAllCatetories)
        .post(subcategory.createCategory);

    app.route(config.app_version + '/category/:categoryId/attributes')
        .get(attribute_category.getAllAttributeCategory)
        .post(attribute_category.createAttributeCategory);

    app.route(config.app_version + '/category/:categoryId/attributes/:attributeId')
        .get(attribute_category.getAttributeCategory)
        //.put(attribute_category.updateCategory)
        .delete(attribute_category.deleteAttributeCategory);

    app.route(config.app_version + '/category/:categoryId/subcategory/:subCategoryId')
        .get(subcategory.getCategory)
        .delete(subcategory.deleteCategory)
        .put(subcategory.updateCategory);



    // ======================== ATTRIBUTES ============================ //
    // ======================== ATTRIBUTES ============================ //
    // ======================== ATTRIBUTES ============================ //

    app.route(config.app_version + '/attribute')
        .get(attribute.getAllAttribute)
        .post(validatorAttribute.validateAttribute, attribute.createAttribute);

    app.route(config.app_version + '/attribute/:attributeId')
        .get(attribute.getAttribute)
        .delete(attribute.deleteAttribute)
        .put(validatorAttribute.validateAttribute, attribute.updateAttribute);

    // ======================== FORM INPUT TYPE ============================ //
    // ======================== FORM INPUT TYPE ============================ //
    // ======================== FORM INPUT TYPE ============================ //

    app.route(config.app_version + '/forminputtype')
        .get(forminputtype.getAllFormInputType)
        .post(validatorFormInptType.validateFormInptType, forminputtype.createFormInputType);

    app.route(config.app_version + '/forminputtype/:id')
        .get(forminputtype.getFormInputType)
        .delete(forminputtype.deleteFormInputType)
        .put(validatorFormInptType.validateFormInptType, forminputtype.updateFormInputType);

    app.route(config.app_version + '/forminputtype')
        .get(forminputtype.getAllFormInputType)
        .post(validatorFormInptType.validateFormInptType, forminputtype.createFormInputType);


    // ======================== BANKS ============================ //
    // ======================== BANKS ============================ //
    // ======================== BANKS ============================ //

    app.route(config.app_version + '/banks')
        .get(banks.getAllBank)
        .post(validatorBanks.validateBanks, banks.createBank);

    app.route(config.app_version + '/banks/:bankId')
        .get(banks.getBank)
        .delete(banks.deleteBank)
        .put(validatorBanks.validateBanks, banks.updateBank);

    // ======================== BRANDS ============================ //
    // ======================== BRANDS ============================ //
    // ======================== BRANDS ============================ //

    app.route(config.app_version + '/brand')
        .get(brands.getAllBrand)
        .post(validatorCategory.validateBrand, brands.createBrand);

    app.route(config.app_version + '/brand/:brandId')
        .get(brands.getBrand)
        .delete(brands.deleteBrand)
        .put(validatorCategory.validateBrand, brands.updateBrand);

    // ======================== SHIPPING COURIER ============================ //
    // ======================== SHIPPING COURIER ============================ //
    // ======================== SHIPPING COURIER ============================ //

    app.route(config.app_version + '/courier')
        .get(courier.getAllCourier)
        .post(validatorCourier.validateCourier, courier.createCourier);

    app.route(config.app_version + '/courier/:courierId')
        .get(courier.getCourier)
        .delete(courier.deleteCourier)
        .put(validatorCourier.validateCourier, courier.updateCourier);

    // ======================== PAYMENT PROVIDER ============================ //
    // ======================== PAYMENT PROVIDER ============================ //
    // ======================== PAYMENT PROVIDER ============================ //

    app.route(config.app_version + '/paymentprovider')
        .get(paymentprovider.getAllPaymentProvider)
        .post(validatorPayment.validatePayment, paymentprovider.createPaymentProvider);

    app.route(config.app_version + '/paymentprovider/:paymentId')
        .get(paymentprovider.getPaymentProvider)
        .delete(paymentprovider.deletePaymentProvider)
        .put(validatorPayment.validatePayment, paymentprovider.updatePaymentProvider);

    // ======================== BUYER / SELLER ============================ //
    // ======================== BUYER / SELLER ============================ //
    // ======================== BUYER / SELLER ============================ //

    app.route(config.app_version + '/seller')
        .get(buyer_seller.getAllSellers);

    app.route(config.app_version + '/seller/applicants')
        .get(buyer_seller.getAllApplicants);

    app.route(config.app_version + '/seller/preferred')
        .post(buyer_seller.createPreferredSeller)
        .get(buyer_seller.getPreferredSeller);

    app.route(config.app_version + '/seller/preferred/:id')
        .put(buyer_seller.updatePreferredSeller)
        .get(buyer_seller.getPreferredSellerById);

    app.route(config.app_version + '/seller/:uuid/approval')
        .post(buyer_seller.approveSellerApplication);

    app.route(config.app_version + '/buyers')
        .get(buyer_seller.getAllBuyerUser);

    app.route(config.app_version + '/buyer/:buyerId/rating')
        .get(buyer_seller.getBuyerRatings)
        .post(validatorBuyerRating.validateBuyerRating, buyer_seller.rateBuyer);

    // app.route(config.app_version + '/shop/:userId/rating')
    //     .get(buyer_seller.getShopRatings)
    //     .post(validatorBuyerRating.validateShopRating, buyer_seller.rateShop);

    app.route(config.app_version + '/buyer/:buyerId/refund/:orderId/product/:productId')
        .post(buyer_seller.buyerRefund);



    // ======================== MOBILE CONTENTS ============================ //
    // ======================== MOBILE CONTENTS ============================ //
    // ======================== MOBILE CONTENTS ============================ //

    app.route(config.app_version + '/mobile/banner')
        .post(mobile.createMobileBanners)
        .get(mobile.getAllMobileBanners);

    app.route(config.app_version + '/mobile/banner/active')
        .get(mobile.getAllActiveMobileBanners);

    app.route(config.app_version + '/mobile/banner/:bannerId')
        .get(mobile.getBanner)
        .delete(mobile.deleteMobileBanner)
        .put(mobile.updateMobileBanners);

    app.route(config.app_version + '/mobile/banner/:bannerId/publish')
        .put(mobile.publishBanner);
    app.route(config.app_version + '/mobile/banner/:bannerId/unpublish')
        .put(mobile.unpublishBanner);

    app.route(config.app_version + '/mobile/cms')
        .post(validatorContent.validateContents, mobile.createMobileCMS)
        .get(mobile.getAllMobileCMS);

    app.route(config.app_version + '/mobile/cms/:cmsId')
        .get(mobile.getMobileCMS)
        .delete(mobile.deleteMobileCMS)
        .put(validatorContent.validateContents, mobile.updateMobileCMS);

    // ======================== TAGS ============================ //
    // ======================== TAGS ============================ //
    // ======================== TAGS ============================ //

    app.route(config.app_version + '/tags')
        .get(tags.getAllTags)
        .post(tags.createTags);

    app.route(config.app_version + '/tags/:tagId')
        .get(tags.getTag)
        .delete(tags.deleteTag)
        .put(tags.updateTag);

    // ======================== ORDERS ============================ //
    // ======================== ORDERS ============================ //
    // ======================== ORDERS ============================ //

    app.route(config.app_version + '/orders')
        .get(middleware.authorizeAPIAuth, orders.getOrdersList);

    app.route(config.app_version + '/orders/:orderId')
        .get(middleware.authorizeAPIAuth, orders.getOrderDetails);

    app.route(config.app_version + '/orders/:orderId/pay')
        .post(middleware.authorizeAPIAuth, orders.payOrder);

    app.route(config.app_version + '/orders/:orderId/shipped')
        .put(middleware.authorizeAPIAuth, orders.shippedOrder);

    app.route(config.app_version + '/orders/:orderId/receive_reminder')
        .get(middleware.authorizeAPIAuth, orders.orderReceivedReminder);

    app.route(config.app_version + '/orders/:batchId/batch')
        .get(middleware.authorizeAPIAuth, orders.getOrderDetailsByBatchNum);

    /**
     * this is to cancel the order if unshipped on deadline or onwards
     */
    app.route(config.app_version + '/orders/:orderId/autocancelunshipped')
        .post(middleware.authorizeAPIAuth, orders.autoCancelUnshippedOrders);

    app.route(config.app_version + '/orders/:orderId/autoreleasepayment')
        .post(middleware.authorizeAPIAuth, orders.autoReleasePayment);

    // ======================== NOTIFICATIONS ============================ //
    // ======================== NOTIFICATIONS ============================ //
    // ======================== NOTIFICATIONS ============================ //

    app.route(config.app_version + '/notiftype')
        .get(newsletter.getAllNotificationType);


    // ======================== NEWSLETTER ============================ //
    // ======================== NEWSLETTER ============================ //
    // ======================== NEWSLETTER ============================ //

    app.route(config.app_version + '/newsletter')
        .post(validatorContent.validateNewsletter, newsletter.createNewsletter)
        .get(newsletter.getAllNewsletter);

    app.route(config.app_version + '/newsletter/:cmsId')
        .get(newsletter.getNewsletter)
        .delete(newsletter.deleteNewsletter)
        .put(newsletter.updateNewsletter);

    app.route(config.app_version + '/newsletter/:cmsId/publish')
        .put(newsletter.publishedNewsletter);

    app.route(config.app_version + '/newsletter/:cmsId/unpublish')
        .put(newsletter.unpublishNewsletter);

    app.route(config.app_version + '/newsletter/:cmsId/push')
        .post(newsletter.publishedPushNewsletter);

    app.route(config.app_version + '/newsletter/:cmsId/email')
        .post(newsletter.publishedMailNewsletter);

    // ======================== HELP CENTRE ============================ //
    // ======================== HELP CENTRE ============================ //
    // ======================== HELP CENTRE ============================ //

    app.route(config.app_version + '/helpcentre')
        .post(middleware.authorizeAPIAuth, helpcentre.createHelpPost)
        .get(helpcentre.getAllHelpPosts);

    app.route(config.app_version + '/helpcentre/category')
        .get(helpcentre.getAllHelpCategory);

    app.route(config.app_version + '/helpcentre/:postId')
        .get(helpcentre.getPostByPostID);

    app.route(config.app_version + '/helpcentre/category/:categoryId')
        .get(helpcentre.getAllHelpByCategory);


    // ======================== SHIPPING ============================ //
    // ======================== SHIPPING ============================ //
    // ======================== SHIPPING ============================ //

    app.route(config.app_version + '/shipping_tags')
        .get(middleware.authorizeAPIAuth, trackings.getTrackingShippingTags);
    app.route(config.app_version + '/shipping_flags')
        .get(middleware.authorizeAPIAuth, trackings.getTrackingCheckingFlags);

    app.route(config.app_version + '/trackings')
        .get(middleware.authorizeAPIAuth, trackings.getAllOrderTrackings);

    app.route(config.app_version + '/trackings/:ordernum')
        .get(middleware.authorizeAPIAuth, trackings.getOrderTrackingByOrderNum);

    app.route(config.app_version + '/trackings/:order_trackingId/track')
        .put(middleware.authorizeAPIAuth, trackings.updateOrderTracking);

    app.route(config.app_version + '/trackings/:slug/:tracking_number')
        .get(middleware.authorizeAPIAuth, trackings.getOrderTrackingByTrackingNum);


    // ======================== OFFICIAL SHOP ============================ //
    // ======================== OFFICIAL SHOP ============================ //
    // ======================== OFFICIAL SHOP ============================ //

    app.route(config.app_version + '/officialshop')
        .get(middleware.authorizeAPIAuth, officialshop.getApplicationList);

    app.route(config.app_version + '/officialshop/:shopId')
        .get(middleware.authorizeAPIAuth, officialshop.getApplicationDetail);

    app.route(config.app_version + '/officialshop/:shopId/approved')
        .post(middleware.authorizeAPIAuth, officialshop.approveOfficialShopApplication);

    app.route(config.app_version + '/officialshop-evaluations')
        .get(middleware.authorizeAPIAuth, officialshop.getApplicationEvaluationList)
        .post(middleware.authorizeAPIAuth, officialshop.updateApplicationEvaluation);




};