/*jshint camelcase: false */

'use strict';

// ======================== VALIDATION ============================ //
let validatorUser = require('../validation/api/users');
let validatorProduct = require('../validation/api/product');
let validatorCategory = require('../validation/category');
let validatorDiscountPromotions = require('../validation/discount_promotions');
let validatorOrder = require('../validation/api/orders')
let validatorRating = require('../validation/api/ratings');


// ======================== ROUTING ============================ //
let users = require('./routing/api/users');
let product = require('./routing/api/product');
let variations = require('./routing/api/product_variations');
let likes = require('./routing/api/product_like');
let sales = require('./routing/api/product_wholesale');
let orders = require('./routing/api/orders');
let seller = require('./routing/api/seller');
let files = require('./routing/api/file');
let notifications = require('./routing/api/notifications');

let misc = require('./routing/app/misc');


let getAllCatetories = require('./routing/app/category').getAllCatetories;
let getAllCategoryProducts = require('./routing/app/category').getAllCategoryProducts;

let getAllSubCatetories = require('./routing/app/subcategory').getAllCatetories;
let getAllSubCategoryProducts = require('./routing/app/subcategory').getAllSubCategoryProducts;

let getAllBrand = require('./routing/app/brands').getAllBrand;
let getAllBrandProducts = require('./routing/app/brands').getAllBrandProducts;
let createBrand = require('./routing/app/brands').createBrand;

let getAllActiveMobileBanners = require('./routing/app/mobile').getAllActiveMobileBanners;
let getAllCourier = require('./routing/app/courier').getAllCourier;
let getAllBank = require('./routing/app/banks').getAllBank;
let getNewsletter = require('./routing/app/newsletter').getNewsletter;
let getOfficialShopList = require('./routing/app/officialshop').getApplicationList;


module.exports = function (app, passport, config, middleware, ejwt) {

    // ======================== AUTHENTICATION / AUTHORIZATION ============================ //
    // ======================== AUTHENTICATION / AUTHORIZATION ============================ //
    // ======================== AUTHENTICATION / AUTHORIZATION ============================ //

    var isRevokedCallback = function (req, payload, done) {
        var issuer = payload.iss;
        var tokenId = payload.jti;
        console.log('issuer: ', issuer);
        console.log('tokenId: ', tokenId);

        if (!tokenId) {
            return done(null, false);
        }
        console.log('payload: ', !!payload);
        return done(null, !!payload);

        /* data.getRevokedToken(issuer, tokenId, function(err, token) {
            if (err) { return done(err); }
            return done(null, !!token);
        }); */
    };

    app.route(config.api_version + '/login')
        .post(validatorUser.validateBasicAuth, passport.authenticate('mobile'), users.login);

    app.route(config.api_version + '/social')
        .post(validatorUser.validateSocialAuth, users.loginSocial);

    app.route(config.api_version + '/logout')
        .post(middleware.authorizeMobileBasicAuth, users.logout);

    app.route(config.api_version + '/tokenvalidity')
        .post(middleware.authorizeMobileBasicAuth, users.tokenValidity);

    /*.post(middleware.authorizeMobileBasicAuth, ejwt({
        secret: config.token_secret_mobile,
        isRevoked: isRevokedCallback
    }),users.logout);*/

    // ======================== FILE UPLOAD ============================ //
    // ======================== FILE UPLOAD ============================ //
    // ======================== FILE UPLOAD ============================ //
    app.route(config.api_version + '/file')
        .post(middleware.authorizeMobileBasicAuth, files.uploadFile);
    app.route(config.api_version + '/file/:fileId')
        .get(middleware.authorizeMobileBasicAuth, files.getFile)
        .delete(middleware.authorizeMobileBasicAuth, files.deleteFile);

    // ======================== FORGOT PASSWORD ============================ //
    // ======================== FORGOT PASSWORD ============================ //
    // ======================== FORGOT PASSWORD ============================ //
    app.route(config.api_version + '/forgot')
        .post(validatorUser.validateForgot, users.forgotPassword);

    app.route(config.api_version + '/reset')
        .post(validatorUser.validateReset, users.resetPassword);

    // ======================== REGISTRATION ============================ //
    // ======================== REGISTRATION ============================ //
    // ======================== REGISTRATION ============================ //
    app.route(config.api_version + '/register')
        .post(validatorUser.validateUserRegistration, users.createUser);

    app.route(config.api_version + '/social/signup')
        .post(validatorUser.validateSocialRegistration, users.socialSignUp);

    // ======================== VERIFY ACCOUNT ============================ //
    // ======================== VERIFY ACCOUNT ============================ //
    // ======================== VERIFY ACCOUNT ============================ //
    app.route(config.api_version + '/verify/:hashkey/id/:uuid')
        .get(users.checkIfVerified)
        .post(users.verifyUser)

    // ======================== USERS ============================ //
    // ======================== USERS ============================ //
    // ======================== USERS ============================ //

    app.route(config.api_version + '/users')
        .get(middleware.authorizeMobileBasicAuth, users.getCurrentUser);

    app.route(config.api_version + '/users/profile')
        .get(middleware.authorizeMobileBasicAuth, users.getUserProfile)
        .put(middleware.authorizeMobileBasicAuth, users.updateUserAccount);

    app.route(config.api_version + '/users/avatar')
        .post(middleware.authorizeMobileBasicAuth, users.uploadUserAvatar);

    app.route(config.api_version + '/users/banner')
        .post(middleware.authorizeMobileBasicAuth, users.uploadUserBanner);

    app.route(config.api_version + '/users/password')
        .post(middleware.authorizeMobileBasicAuth, validatorUser.validatePassword, users.validatePassword)
        .put(middleware.authorizeMobileBasicAuth, validatorUser.validateChangePassword, users.updateUserPassword);

    app.route(config.api_version + '/users/email')
        .put(middleware.authorizeMobileBasicAuth, users.updateUserEmail);

    app.route(config.api_version + '/users/currency')
        .put(middleware.authorizeMobileBasicAuth, users.updateUserCurrency);

    app.route(config.api_version + '/users/country')
        .put(middleware.authorizeMobileBasicAuth, users.updateUserCountry);

    app.route(config.api_version + '/users/addresses')
        .get(middleware.authorizeMobileBasicAuth, users.getUserAddresses)
        .post(middleware.authorizeMobileBasicAuth, validatorUser.validateUserAddress, users.createUserAddress);

    app.route(config.api_version + '/users/addresses/:_id')
        .get(middleware.authorizeMobileBasicAuth, users.getUserAddress)
        .delete(middleware.authorizeMobileBasicAuth, users.deleteUserAddress)
        .put(middleware.authorizeMobileBasicAuth, validatorUser.validateUserAddress, users.updateUserAddress);

    app.route(config.api_version + '/users/products')
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerProducts);

    app.route(config.api_version + '/users/likes')
        .get(middleware.authorizeMobileBasicAuth, likes.getAllUserProductLikes);

    app.route(config.api_version + '/users/likes/:uuid')
        .post(middleware.authorizeMobileBasicAuth, likes.likeProduct)
        .delete(middleware.authorizeMobileBasicAuth, likes.unlikeProduct);

    app.route(config.api_version + '/users/bank')
        .get(middleware.authorizeMobileBasicAuth, users.getAllUserBanks)
        .post(middleware.authorizeMobileBasicAuth, validatorUser.validateUserBank, users.saveUserBank);

    app.route(config.api_version + '/users/bank/:_id')
        .get(middleware.authorizeMobileBasicAuth, users.getUserBank)
        .delete(middleware.authorizeMobileBasicAuth, users.deleteUserBank)
        .put(middleware.authorizeMobileBasicAuth, validatorUser.validateUserBank, users.updateUserBank);

    app.route(config.api_version + '/users/shipping')
        .get(middleware.authorizeMobileBasicAuth, users.getAllUserSellerShipping)
        .post(middleware.authorizeMobileBasicAuth, users.createUserSellerShipping);

    app.route(config.api_version + '/users/vouchers')
        .get(middleware.authorizeMobileBasicAuth, users.getBuyerVouchers)
        .post(middleware.authorizeMobileBasicAuth, validatorUser.validateUserVoucher, users.claimVoucher);

    app.route(config.api_version + '/users/shipping/:_id')
        .delete(middleware.authorizeMobileBasicAuth, users.deleteUserSellerShipping);

    // ======================== NOTIFICATIONS ============================ //
    // ======================== NOTIFICATIONS ============================ //
    // ======================== NOTIFICATIONS ============================ //

    app.route(config.api_version + '/users/notification')
        .get(middleware.authorizeMobileBasicAuth, notifications.getUpdatesNotifications);

    app.route(config.api_version + '/users/notification/:notifId')
        .get(middleware.authorizeMobileBasicAuth, notifications.getNotificationDetails)
        .put(middleware.authorizeMobileBasicAuth, notifications.updateNotification)
        .delete(middleware.authorizeMobileBasicAuth, notifications.deleteNotification);


    app.route(config.api_version + '/newsletter/:cmsId')
        .get(middleware.authorizeMobileBasicAuth, getNewsletter);

    // ======================== USER SOCIAL LINK ============================ //
    // ======================== USER SOCIAL LINK ============================ //
    // ======================== USER SOCIAL LINK ============================ //
    app.route(config.api_version + '/users/social/fb')
        .post(middleware.authorizeMobileBasicAuth, users.linkFacebookAccount)
        .delete(middleware.authorizeMobileBasicAuth, users.unlinkFacebookAccount);

    app.route(config.api_version + '/users/social/google')
        .post(middleware.authorizeMobileBasicAuth, users.linkGoogleAccount)
        .delete(middleware.authorizeMobileBasicAuth, users.unlinkGoogleAccount);

    // ======================== SELLER APPLICATION ============================ //
    // ======================== SELLER APPLICATION ============================ //
    // ======================== SELLER APPLICATION ============================ //

    app.route(config.api_version + '/users/apply')
        .post(middleware.authorizeMobileBasicAuth, users.applyUserAsSeller);

    app.route(config.api_version + '/users/apply/upload/selfie')
        .post(middleware.authorizeMobileBasicAuth, users.applyUserAsSellerUploadSelfie);

    app.route(config.api_version + '/users/apply/upload/proof')
        .post(middleware.authorizeMobileBasicAuth, users.applyUserAsSellerUploadProof);

    app.route(config.api_version + '/users/apply/upload/govid')
        .post(middleware.authorizeMobileBasicAuth, users.applyUserAsSellerUploadGovId);

    app.route(config.api_version + '/users/apply/upload/personalid')
        .post(middleware.authorizeMobileBasicAuth, users.applyUserAsSellerUploadPersonalId);


    // ======================== FOLLOWERS / FOLLOWING ============================ //
    // ======================== FOLLOWERS / FOLLOWING ============================ //
    // ======================== FOLLOWERS / FOLLOWING ============================ //

    app.route(config.api_version + '/users/follow')
        .post(middleware.authorizeMobileBasicAuth, users.followUser)
        .delete(middleware.authorizeMobileBasicAuth, users.unfollowUser);
    app.route(config.api_version + '/users/following')
        .get(middleware.authorizeMobileBasicAuth, users.getCurrentUserFollowing);
    app.route(config.api_version + '/users/followers')
        .get(middleware.authorizeMobileBasicAuth, users.getCurrentUserFollowers);

    // ======================== USERS / RATING ============================ //
    app.route(config.api_version + '/users/rating')
        .get(middleware.authorizeMobileBasicAuth, users.userRatings);

    // ======================== SHOP PROFILE ============================ //
    // ======================== SHOP PROFILE ============================ //
    // ======================== SHOP PROFILE ============================ //

    app.route(config.api_version + '/shop')
        .get(middleware.authorizeMobileBasicAuth, seller.getAllSellers)
        .post(middleware.authorizeMobileBasicAuth, seller.updateShopInformation);

    app.route(config.api_version + '/shop/income')
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerIncome);

    app.route(config.api_version + '/shop/customer')
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerCustomers);

    app.route(config.api_version + '/shop/products')
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerProducts);

    app.route(config.api_version + '/shop/customers')
        .get(middleware.authorizeMobileBasicAuth, seller.geShopCustomers);

    app.route(config.api_version + '/shop/categories')
        .get(middleware.authorizeMobileBasicAuth, seller.getShopCategories);

    app.route(config.api_version + '/shop/:uuid')
        .get(middleware.authorizeMobileBasicAuth, seller.getUserShopProfile);

    app.route(config.api_version + "/shop/:uuid/info")
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerInfo);

    app.route(config.api_version + '/shop/:uuid/apply')
        .post(middleware.authorizeMobileBasicAuth, seller.applySellerAsOfficialShop)
        .get(middleware.authorizeMobileBasicAuth, seller.officialShopApplicationStatus);

    app.route(config.api_version + '/shop/:uuid/followers')
        .get(middleware.authorizeMobileBasicAuth, users.getUserFollowers);

    app.route(config.api_version + '/shop/:uuid/following')
        .get(middleware.authorizeMobileBasicAuth, users.getUserFollowing);

    app.route(config.api_version + '/shop/:uuid/products')
        .get(middleware.authorizeMobileBasicAuth, seller.getShopProducts);

    app.route(config.api_version + '/shop/:uuid/shipping')
        .get(middleware.authorizeMobileBasicAuth, seller.getAllUserSellerShipping);

    app.route(config.api_version + '/shop/:uuid/rating')
        .get(middleware.authorizeMobileBasicAuth, seller.getShopRatings)
        .post(middleware.authorizeMobileBasicAuth, validatorRating.validateShopRating, seller.rateShop);

    app.route(config.api_version + '/shop/:uuid/rating_summary')
        .get(middleware.authorizeMobileBasicAuth, seller.getShopSummaryRatings)

    app.route(config.api_version + '/shop/:uuid/categories')
        .get(middleware.authorizeMobileBasicAuth, seller.getShopCategoriesByOtherSeller);
    
    app.route(config.api_version + '/shop/:uuid/categories/:categoryId/products')
        .get(middleware.authorizeMobileBasicAuth, product.getproductsByCategory)

    // =======================SELLER VOUCHERS======================= //
    // =======================SELLER VOUCHERS======================= // 
    // =======================SELLER VOUCHERS======================= //

    app.route(config.api_version + '/shop/:uuid/vouchers')
        .post(middleware.authorizeMobileBasicAuth, seller.createSellerVoucher)
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerVouchers);

    app.route(config.api_version + '/shop/:uuid/vouchers/:voucherId')
        .put(middleware.authorizeMobileBasicAuth, seller.updateSellerVoucher)
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerProductVouchers)
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteSellerVoucher);


    app.route(config.api_version + '/shop/:uuid/vouchers/:voucherId/products')
        .post(middleware.authorizeMobileBasicAuth, seller.createSellerProductVoucher)
        .get(middleware.authorizeMobileBasicAuth, seller.getSellerProductVouchers);

    app.route(config.api_version + '/shop/:uuid/vouchers/:voucherId/products/:productId')
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteSellerProductVoucher)


    // =======================DISCOUNT PROMOTIONS======================= //
    // =======================DISCOUNT PROMOTIONS======================= //
    // =======================DISCOUNT PROMOTIONS======================= //

    app.route(config.api_version + '/shop/:uuid/discountpromotions')
        .post(middleware.authorizeMobileBasicAuth, validatorDiscountPromotions.validateDiscountPromotion, seller.createDiscountPromotion)
        .get(middleware.authorizeMobileBasicAuth, seller.getDiscountPromotions);

    app.route(config.api_version + '/shop/:uuid/discountpromotions/:id')
        .put(middleware.authorizeMobileBasicAuth, validatorDiscountPromotions.validateDiscountPromotion, seller.updateDiscountPromotion)
        .get(middleware.authorizeMobileBasicAuth, seller.getDiscountPromotion)
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteDiscountPromotion);

    app.route(config.api_version + '/shop/:uuid/discountpromotions/:id/products')
        .get(middleware.authorizeMobileBasicAuth, seller.getProductDiscountPromotions)
        .post(middleware.authorizeMobileBasicAuth, validatorDiscountPromotions.validateProductDiscountPromotion, seller.createProductDiscountPromotion);

    app.route(config.api_version + '/shop/:uuid/discountpromotions/:id/products/:productId')
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteProductDiscountPromotion);

    app.route(config.api_version + '/shop/:uuid/discountpromotions/:id/products/:productId/variant/:variantId')
        .put(middleware.authorizeMobileBasicAuth, validatorDiscountPromotions.validateProductDiscountPromotion, seller.updateProductVariantsDiscountPromotion)
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteProductVariantsDiscountPromotion);

    // ======================== SELLER TOP PICKS ============================ //
    // ======================== SELLER TOP PICKS ============================ //
    // ======================== SELLER TOP PICKS ============================ //

    app.route(config.api_version + '/shop/:uuid/toppicks')
        .post(middleware.authorizeMobileBasicAuth, seller.createTopPicks)
        .get(middleware.authorizeMobileBasicAuth, seller.getTopPicks);

    app.route(config.api_version + '/shop/:uuid/toppicks/active')
        .get(middleware.authorizeMobileBasicAuth, seller.getActiveTopPickCollection)

    app.route(config.api_version + '/shop/:uuid/toppicks/:collectionId')
        .put(middleware.authorizeMobileBasicAuth, seller.updateTopPick)
        .get(middleware.authorizeMobileBasicAuth, seller.getTopPick)
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteTopPick);

    app.route(config.api_version + '/shop/:uuid/toppicks/:collectionId/activate')
        .post(middleware.authorizeMobileBasicAuth, seller.activateTopPickCollection);

    app.route(config.api_version + '/shop/:uuid/toppicks/:collectionId/deactivate')
        .post(middleware.authorizeMobileBasicAuth, seller.deActivateTopPickCollection);

    app.route(config.api_version + '/shop/:uuid/toppicks/:collectionId/products')
        .post(middleware.authorizeMobileBasicAuth, seller.createTopPickProduct)
        .get(middleware.authorizeMobileBasicAuth, seller.getTopPickProducts);

    app.route(config.api_version + '/shop/:uuid/toppicks/:collectionId/products/:productId')
        .get(middleware.authorizeMobileBasicAuth, seller.getTopPickProduct)
        .delete(middleware.authorizeMobileBasicAuth, seller.deleteTopPickProduct)


    // ======================== CATEGORIES ============================ //
    // ======================== CATEGORIES ============================ //
    // ======================== CATEGORIES ============================ //

    app.route(config.api_version + '/category')
        .get(getAllCatetories);

    app.route(config.api_version + '/category/:categoryId/products')
        .get(getAllCategoryProducts);

    app.route(config.api_version + '/category/:categoryId/subcategory')
        .get(getAllSubCatetories);
        
    app.route(config.api_version + '/category/:categoryId/subcategory/:subCategoryId/products')
        .get(getAllSubCategoryProducts);

    // ======================== BRANDS ============================ //
    // ======================== BRANDS ============================ //
    // ======================== BRANDS ============================ //

    app.route(config.api_version + '/brand')
        .post(middleware.authorizeMobileBasicAuth, validatorCategory.validateBrand, createBrand)
        .get(getAllBrand);
    app.route(config.api_version + '/brand/:brandId/products')
        .get(getAllBrandProducts);

    // ======================== BANKS ============================ //
    // ======================== BANKS ============================ //
    // ======================== BANKS ============================ //

    app.route(config.api_version + '/banks')
        .get(getAllBank);

    // ======================== MISC ============================ //
    // ======================== MISC ============================ //
    // ======================== MISC ============================ //

    app.route(config.api_version + '/country')
        .get(misc.getAllCountry);

    app.route(config.api_version + '/currency')
        .get(misc.getAllCurrencies);

    app.route(config.api_version + '/timezone')
        .get(misc.getAllTimezones);

    // ======================== HOME BANNERS ============================ //
    // ======================== HOME BANNERS ============================ //
    // ======================== HOME BANNERS ============================ //
    app.route(config.api_version + '/mobile/banner')
        .get(getAllActiveMobileBanners);

    app.route(config.api_version + '/shipping')
        .get(getAllCourier);

    // ======================== OFFICIAL SHOP ============================ //
    // ======================== OFFICIAL SHOP ============================ //
    // ======================== OFFICIAL SHOP ============================ //

    app.route(config.api_version + '/officialshop')
        .get(getOfficialShopList);

    // ======================== PRODUCTS ============================ //
    // ======================== PRODUCTS ============================ //
    // ======================== PRODUCTS ============================ //

    app.route(config.api_version + '/products')
        .get(product.getAllProducts)
        .post(middleware.authorizeMobileBasicAuth, validatorProduct.validateProduct, product.createProduct);

    app.route(config.api_version + '/products/:uuid')
        .get(product.getProductInfo)
        .delete(middleware.authorizeMobileBasicAuth, product.deleteProduct)
        .put(middleware.authorizeMobileBasicAuth, validatorProduct.validateProduct, product.updateProduct);

    app.route(config.api_version + '/products/:uuid/vouchers')
        .get(product.getProductVouchers)

    app.route(config.api_version + '/products/:uuid/discounts')
        .get(product.getProductDiscount)

    app.route(config.api_version + '/products/:uuid/mail')
        .get(middleware.authorizeMobileBasicAuth, product.mailCreatedProduct);

    app.route(config.api_version + '/products/:uuid/more')
        .get(product.getProductOnSameShop);

    app.route(config.api_version + '/products/:uuid/image')
        .put(middleware.authorizeMobileBasicAuth, product.uploadProductImage);

    app.route(config.api_version + '/products/:uuid/image/:_id')
        .delete(middleware.authorizeMobileBasicAuth, product.deleteProductImage);

    app.route(config.api_version + '/products/:uuid/variation')
        .post(middleware.authorizeMobileBasicAuth, validatorProduct.validateProductVariation, variations.createVariations)
        .get(middleware.authorizeMobileBasicAuth, variations.getProductVariations)
        .delete(middleware.authorizeMobileBasicAuth, variations.deleteAllProductVariations);

    app.route(config.api_version + '/products/:uuid/variation/:_id')
        .delete(middleware.authorizeMobileBasicAuth, variations.deleteProductVariations)
        .put(middleware.authorizeMobileBasicAuth, validatorProduct.validateProductVariation, variations.updateProductVariations);

    app.route(config.api_version + '/products/:uuid/like')
        .post(middleware.authorizeMobileBasicAuth, likes.likeProduct)
        .delete(middleware.authorizeMobileBasicAuth, likes.unlikeProduct);

    app.route(config.api_version + '/products/:uuid/wholesale')
        .get(middleware.authorizeMobileBasicAuth, sales.getAllProductWholeSale)
        .post(middleware.authorizeMobileBasicAuth, validatorProduct.validateProductWholesale, sales.createProductWholesale)
        .delete(middleware.authorizeMobileBasicAuth, sales.deleteAllProductWholeSale);

    app.route(config.api_version + '/products/:uuid/wholesale/:_id')
        .get(middleware.authorizeMobileBasicAuth, sales.getProductWholeSale)
        .put(middleware.authorizeMobileBasicAuth, sales.updateProductWholeSale)
        .delete(middleware.authorizeMobileBasicAuth, sales.deleteProductWholeSale);

    app.route(config.api_version + '/products/:uuid/shipping')
        .post(middleware.authorizeMobileBasicAuth, product.addProductShippingFee)
        .delete(middleware.authorizeMobileBasicAuth, product.deleteProductShipping)
        .get(middleware.authorizeMobileBasicAuth, product.getProductShipping);

    app.route(config.api_version + '/products/:uuid/rating')
        .get(likes.getAllProductRatings)
        .post(middleware.authorizeMobileBasicAuth, validatorRating.validateProductRating, likes.rateProduct)
        .put(middleware.authorizeMobileBasicAuth, likes.updateProductRating);
    
    app.route(config.api_version + '/rating/:productRatingId/upload')
        .post(middleware.authorizeMobileBasicAuth, likes.uploadRatingAttachments)

    // ======================== ORDERS ============================ //
    // ======================== ORDERS ============================ //
    // ======================== ORDERS ============================ //

    app.route(config.api_version + '/orders')
        .post(middleware.authorizeMobileBasicAuth, orders.createOrder)
        .get(validatorOrder.validateOrderParams, middleware.authorizeMobileBasicAuth, orders.getOrdersList);

    app.route(config.api_version + '/orders/:orderId')
        .get(middleware.authorizeMobileBasicAuth, orders.getOrderDetails);

    /*app.route(config.api_version + '/orders/:orderId/mail')
        .get(middleware.authorizeMobileBasicAuth, orders.samplegetOrderDetails);*/

    app.route(config.api_version + '/orders/:orderId/accept')
        .post(middleware.authorizeMobileBasicAuth, orders.acceptOrder);

    app.route(config.api_version + '/orders/:orderId/pay')
        .post(middleware.authorizeMobileBasicAuth, orders.payOrder);

    app.route(config.api_version + '/orders/:orderId/ship')
        .post(middleware.authorizeMobileBasicAuth, orders.shippedOrder);

    app.route(config.api_version + '/orders/:orderId/received')
        .post(middleware.authorizeMobileBasicAuth, orders.receivedOrder);

    app.route(config.api_version + '/orders/:orderId/cancel')
        .post(middleware.authorizeMobileBasicAuth, orders.cancelOrderRequest)
        .put(middleware.authorizeMobileBasicAuth, orders.cancelOrder);

    app.route(config.api_version + '/orders/:orderId/return')
        .post(middleware.authorizeMobileBasicAuth, orders.returnOrderRequest)

    app.route(config.api_version + '/orders/:orderId/return_approved')
        .post(middleware.authorizeMobileBasicAuth, orders.returnOrder);

    app.route(config.api_version + '/orders/:orderId/return_accept')
        .post(middleware.authorizeMobileBasicAuth, orders.acceptReturnOrderRequest);

    app.route(config.api_version + '/orders/:orderId/return_shipment')
        .post(middleware.authorizeMobileBasicAuth, orders.returnOrderArrangeShipment);

    app.route(config.api_version + '/orders/:orderId/return_cancel')
        .post(middleware.authorizeMobileBasicAuth, orders.returnOrderCancelRequest);

    app.route(config.api_version + '/orders/:orderId/return/:returnId/dispute')
        .post(middleware.authorizeMobileBasicAuth);

    app.route(config.api_version + '/orders/:orderId/return/files')
        .post(middleware.authorizeMobileBasicAuth, orders.returnOrderRequestFiles);
        
    app.route(config.api_version + '/orders/:orderId/return/shipment_files')
        .post(middleware.authorizeMobileBasicAuth, orders.returnOrderRequestShippingFiles);


    app.route(config.api_version + '/orders/:batchId/batch')
        .get(middleware.authorizeMobileBasicAuth, orders.getOrderDetailsByBatchNum);

    app.route(config.api_version + '/orders/:orderId/arrangepickup')
        .post(middleware.authorizeMobileBasicAuth, orders.arrangeShipment);

    app.route(config.api_version + '/orders/:orderId/arrangepickup/:slug')
        .get(middleware.authorizeMobileBasicAuth, orders.isOrderArrangeShipment)

    app.route(config.api_version + '/orders/:orderId/trackings/:slug')
        .get(middleware.authorizeMobileBasicAuth, orders.getOrderTrackings);

    app.route(config.api_version + '/orders/:orderId/last_checkpoint/:slug')
        .get(middleware.authorizeMobileBasicAuth, orders.getOrderTrackingLastCheckPoint);


    app.route(config.api_version + '/orders/:orderId/cancel/reject')
        .post(middleware.authorizeMobileBasicAuth, orders.rejectCancelOrder);

    app.route(config.api_version + '/orders/:orderId/cancel/withdraw')
        .post(middleware.authorizeMobileBasicAuth, orders.withdrawCancelOrder);

}