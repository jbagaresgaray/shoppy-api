'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/api/product').Products;
let products = new ctrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

const multer = require('multer');
const _ = require('lodash');
const mkdirp = require('mkdirp');
const fs = require('fs');
const path = require('path');


exports.createProduct = (req, res) => {
    if (req.user) {
        products.createProduct(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.mailCreatedProduct = (req, res) => {
    if (req.user) {
        products.mailCreatedProduct(req.user.result.user, req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
}

exports.getProductInfo = (req, res) => {
    products.getProductInfo(req.params.uuid, req.query, cb.setupResponseCallback(res));
};

exports.getProductVouchers = (req, res) => {
    products.getProductVouchers(req.params.uuid, cb.setupResponseCallback(res));
};

exports.getProductDiscount = (req, res) => {
    products.getProductDiscount(req.params.uuid, cb.setupResponseCallback(res));
};

exports.getProductOnSameShop = (req, res) => {
    products.getProductOnSameShop(req.params.uuid, cb.setupResponseCallback(res));
};

exports.deleteProduct = (req, res) => {
    if (req.user) {
        products.deleteProduct(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.updateProduct = (req, res) => {
    if (req.user) {
        products.updateProduct(req.user.result.user, req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.uploadProductImage = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/products/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, req.params.uuid + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 10 // 10MB
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('image');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }
        console.log('req.files: ', req.files);
        console.log('req.file: ', req.file);
        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            // req.body.img_path = req.file.path;
            req.body.img_name = req.file.originalname;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;

            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);
        }
        console.log('req.body: ', req.body);

        if (req.user) {
            products.uploadProductImage(req.params.uuid, req.body, cb.setupResponseCallback(res));
        }
    });
};

exports.deleteProductImage = (req, res) => {
    products.deleteProductImage(req.params.uuid, req.params._id, cb.setupResponseCallback(res));
};

exports.addProductShippingFee = (req, res) => {
    products.addProductShippingFee(req.params.uuid, req.body, cb.setupResponseCallback(res));
};

exports.deleteProductShipping = (req, res) => {
    products.deleteProductShipping(req.params.uuid, cb.setupResponseCallback(res));
};

exports.getProductShipping = (req, res) => {
    products.getProductShipping(req.params.uuid, cb.setupResponseCallback(res));
};

exports.getAllProducts = (req, res) => {
    products.getAllProducts(req.query, cb.setupResponseCallback(res));
};

exports.getproductsByCategory = (req, res) => {
    products.getproductsByCategory(req.params.uuid, req.params.categoryId, req.query, cb.setupResponseCallback(res));
};
