'use strict';

const cb = require('../../../utils/callback');


const ordersCtrl = require('../../../controllers/api/orders').Orders;
const orders = new ordersCtrl();

const ordersCancelCtrl = require('../../../controllers/api/order_cancel').Orders;
const order_cancel = new ordersCancelCtrl();

const ordersShippingCtrl = require('../../../controllers/api/order_shipping').Orders;
const order_shipping = new ordersShippingCtrl();

const ordersReturnsCtrl = require('../../../controllers/api/order_return').Orders;
const order_return = new ordersReturnsCtrl();

const env = process.env.NODE_ENV || 'development';
const config = require('../../../../config/environment/' + env);
const multer = require('multer');
const _ = require('lodash');
const mkdirp = require('mkdirp');
const fs = require('fs');
const path = require('path');
const uuidv1 = require('uuid/v1');


exports.createOrder = (req, res) => {
    if (req.user) {
        req.checkBody('cartOrder').notEmpty();
        // req.checkBody('cartOrder').isArray();

        var errors = req.validationErrors();
        if (errors) {
            res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        } else {
            orders.createOrder(req.user.result.user, req.body, cb.setupResponseCallback(res));
        }
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getOrderDetails = (req, res) => {
    if (req.user) {
        req.checkQuery('type', 'Please provide user type parameters').notEmpty();
        req.checkParams('orderId', 'Please provide Order ID parameters').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        } else {
            orders.getOrderDetails(req.user.result.user, req.query.type, req.params.orderId, cb.setupResponseCallback(res));
        }
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getOrderDetailsByBatchNum = (req, res) => {
    if (req.user) {
        orders.getOrderDetailsByBatchNum(req.user.result.user, req.params.batchId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getOrdersList = (req, res) => {
    if (req.user) {
        console.log('req.query ', req.query)
        orders.getOrdersList(req.user.result.user, req.query.action, req.query.type, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.acceptOrder = (req, res) => {
    if (req.user) {
        orders.acceptOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.payOrder = (req, res) => {
    if (req.user) {
        orders.payOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.shippedOrder = (req, res) => {
    if (req.user) {
        orders.shippedOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.receivedOrder = (req, res) => {
    if (req.user) {
        orders.receivedOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.cancelOrderRequest = (req, res) => {
    if (req.user) {
        order_cancel.cancelOrderRequest(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.cancelOrder = (req, res) => {
    if (req.user) {
        order_cancel.cancelOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.rejectCancelOrder = (req, res) => {
    if (req.user) {
        order_cancel.rejectCancelOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.withdrawCancelOrder = (req, res) => {
    if (req.user) {
        order_cancel.withdrawCancelOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};


exports.isOrderArrangeShipment = (req, res) => {
    if (req.user) {
        req.checkParams('orderId', 'Please provide Order ID parameters').notEmpty();
        req.checkParams('slug', 'Please provide Shipping slug parameters').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        } else {
            order_shipping.isOrderArrangeShipment(req.params.orderId, req.params.slug, cb.setupResponseCallback(res));
        }
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.arrangeShipment = (req, res) => {
    if (req.user) {
        console.log('params : ', req.params)
        req.checkBody('orderId', 'Please provide Order ID parameters').notEmpty();
        req.checkBody('slug', 'Please provide Shipping slug parameters').notEmpty();
        req.checkBody('pickUpDate', 'Please provide Shipping pickup date').notEmpty();
        req.checkBody('shipment_pickup_date', 'Please provide Shipping pickup date').notEmpty();
        req.checkBody('shipment_package_count', 'Please provide Shipping package count').notEmpty();
        req.checkBody('shippingCourierId', 'Please provide Shipping Courier').notEmpty();
        req.checkBody('slug', 'Please provide Shipping Courier Slug').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        } else {
            order_shipping.arrangeShipment(req.params.orderId, req.body, cb.setupResponseCallback(res));
        }
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getOrderTrackings = (req, res) => {
    if (req.user) {
        req.checkParams('orderId', 'Please provide Order ID parameters').notEmpty();
        req.checkParams('slug', 'Please provide Shipping slug parameters').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        } else {
            order_shipping.getOrderTrackings(req.params.orderId, req.params.slug, cb.setupResponseCallback(res));
        }
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getOrderTrackingLastCheckPoint = (req, res) => {
    if (req.user) {
        req.checkParams('orderId', 'Please provide Order ID parameters').notEmpty();
        req.checkParams('slug', 'Please provide Shipping slug parameters').notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        } else {
            order_shipping.getOrderTrackingLastCheckPoint(req.params.orderId, req.params.slug, cb.setupResponseCallback(res));
        }
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.returnOrderRequest = (req, res) => {
    if (req.user) {
        order_return.returnOrderRequest(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.acceptReturnOrderRequest = (req, res) => {
    if (req.user) {
        order_return.acceptReturnOrderRequest(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.returnOrderArrangeShipment = (req, res) => {
    if (req.user) {
        order_return.returnOrderArrangeShipment(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.returnOrder = (req, res) => {
    if (req.user) {
        order_return.returnOrder(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.returnOrderCancelRequest = (req, res) => {
    if (req.user) {
        order_return.returnOrderCancelRequest(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.returnOrderRequestFiles = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/returns/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, uuidv1() + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 10 // 10MB
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('image');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }
        req.body.toDeleteFile = false;
        console.log('returnOrderRequestFiles req.file: ', req.file);
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_name = req.file.originalname;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;

            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);

            if (req.user) {
                order_return.uploadOrderReturnFiles(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
            }
        } else {
            return res.status(200).json({
                result: null,
                msg: 'No files to upload',
                success: false
            });
        }
    });
};

exports.returnOrderRequestShippingFiles = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/returns/shipping/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, uuidv1() + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 10 // 10MB
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('image');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }
        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_name = req.file.originalname;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;

            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);

            if (req.user) {
                order_return.returnOrderRequestShippingFiles(req.user.result.user, req.params.orderId, req.body, cb.setupResponseCallback(res));
            }
        } else {
            return res.status(200).json({
                result: null,
                msg: 'No files to upload',
                success: false
            });
        }
    });
};