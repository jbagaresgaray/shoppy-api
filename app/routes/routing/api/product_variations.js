'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/api/product_variations').Variations;
let variations = new ctrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let _ = require('lodash');


exports.createVariations = (req, res) => {
    if (req.user) {
        variations.createVariations(req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getProductVariations = (req, res) => {
    if (req.user) {
        variations.getProductVariations(req.user.result.user, req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.deleteAllProductVariations = (req, res) => {
    if (req.user) {
        variations.deleteAllProductVariations(req.user.result.user, req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.deleteProductVariations = (req, res) => {
    if (req.user) {
        variations.deleteProductVariations(req.params.uuid, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.updateProductVariations = (req, res) => {
    if (req.user) {
        variations.updateProductVariations(req.params.uuid, req.params._id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};