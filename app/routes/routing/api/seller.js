'use strict';

const cb = require('../../../utils/callback');

const usersCtrl = require('../../../controllers/api/users').Users;
const users = new usersCtrl();

const sellersCtrl = require('../../../controllers/api/seller').Seller;
const seller = new sellersCtrl();

const sellersDiscountCtrl = require('../../../controllers/api/seller-discount-promotions').Seller;
const sellerDiscount = new sellersDiscountCtrl();

const sellersCollectionCtrl = require('../../../controllers/api/seller-top-collections').Seller;
const sellerCollection = new sellersCollectionCtrl();

const sellersVouchersCtrl = require('../../../controllers/api/seller-vouchers').Seller;
const sellerVouchers = new sellersVouchersCtrl();

const shopCustomersCtrl = require('../../../controllers/api/shop_customers').ShopCustomers;
const shopCustomers = new shopCustomersCtrl();


exports.getSellerInfo = (req, res) => {
    if (req.user) {
        seller.getSellerInfo(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getAllSellers = (req, res) => {
    if (req.user) {
        seller.getAllSellers(req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getUserShopProfile = (req, res) => {
    console.log('wew')
    if (req.user) {
        seller.getUserShopProfile(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateShopInformation = (req, res) => {
    if (req.user) {
        seller.updateShopInformation(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};


exports.getSellerCustomers = (req, res) => {
    console.log('getSellerCustomers 1');
    if (req.user) {
        seller.getSellerCustomers(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getSellerIncome = (req, res) => {
    console.log('getSellerIncome 1');
    if (req.user) {
        seller.getSellerIncome(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getSellerRatings = (req, res) => {
    if (req.user) {
        seller.getSellerRatings(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getSellerProducts = (req, res) => {
    if (req.user) {
        seller.getSellerProducts(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getShopProducts = (req, res) => {
    if (req.user) {
        seller.getShopProducts(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.applySellerAsOfficialShop = (req, res) => {
    if (req.user) {
        seller.applySellerAsOfficialShop(req.params.uuid, req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getAllUserSellerShipping = (req, res) => {
    if (req.user) {
        users.getAllUserSellerShipping({
            uuid: req.params.uuid
        }, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.officialShopApplicationStatus = (req, res) => {
    if (req.user) {
        seller.officialShopApplicationStatus(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};


// ======================== SELLER DISCOUNT PROMOTIONS ============================ //
// ======================== SELLER DISCOUNT PROMOTIONS ============================ //
// ======================== SELLER DISCOUNT PROMOTIONS ============================ //

//  GEt all disount promotions
exports.getDiscountPromotions = (req, res) => {
    if (req.user) {
        sellerDiscount.getDiscountPromotions(req.params.uuid, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};


// create disount promotions
exports.createDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.createDiscountPromotion(req.params.uuid, req.body, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}

//  GEt disount promotion  by id
exports.getDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.getDiscountPromotion(req.params.uuid, req.params.id, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}

// update disount promotions
exports.updateDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.updateDiscountPromotion(req.params.id, req.body, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}

//  Delete disount promotion  by id
exports.deleteDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.deleteDiscountPromotion(req.params.uuid, req.params.id, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}

// Get all Discount Promotions by product
exports.getProductDiscountPromotions = (req, res) => {
    if (req.user) {
        sellerDiscount.getProductDiscountPromotions(req.params.uuid, req.params.id, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}


// Product discount Promotions
exports.createProductDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.createProductDiscountPromotion(req.params.uuid, req.params.id, req.body, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}

exports.updateProductVariantsDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.updateProductVariantsDiscountPromotion(req.params.uuid, req.params.id, req.params.productId, req.params.variantId, req.body, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}

exports.deleteProductVariantsDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.deleteProductVariantsDiscountPromotion(req.params.uuid, req.params.id, req.params.productId, req.params.variantId, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteProductDiscountPromotion = (req, res) => {
    if (req.user) {
        sellerDiscount.deleteProductDiscountPromotion(req.params.uuid, req.params.id, req.params.productId, cb.setupResponseCallback(res))
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
}


// ======================== SELLER TOP PICKS ============================ //
// ======================== SELLER TOP PICKS ============================ //
// ======================== SELLER TOP PICKS ============================ //

exports.createTopPicks = (req, res) => {
    if (req.user) {
        sellerCollection.createTopPicks(req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getTopPicks = (req, res) => {
    if (req.user) {
        sellerCollection.getTopPicks(req.params.uuid, req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getTopPick = (req, res) => {
    if (req.user) {
        sellerCollection.getTopPick(req.params.uuid, req.params.collectionId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateTopPick = (req, res) => {
    if (req.user) {
        sellerCollection.updateTopPick(req.params.uuid, req.params.collectionId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteTopPick = (req, res) => {
    if (req.user) {
        sellerCollection.deleteTopPick(req.params.uuid, req.params.collectionId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.activateTopPickCollection = (req, res) => {
    if (req.user) {
        sellerCollection.activateTopPickCollection(req.params.uuid, req.params.collectionId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deActivateTopPickCollection = (req, res) => {
    if (req.user) {
        sellerCollection.deActivateTopPickCollection(req.params.uuid, req.params.collectionId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getActiveTopPickCollection = (req, res) => {
    sellerCollection.getActiveTopPickCollection(req.params.uuid, cb.setupResponseCallback(res));
};

exports.createTopPickProduct = (req, res) => {
    if (req.user) {
        sellerCollection.createTopPickProduct(req.params.uuid, req.params.collectionId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getTopPickProducts = (req, res) => {
    if (req.user) {
        sellerCollection.getTopPickProducts(req.params.uuid, req.params.collectionId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getTopPickProduct = (req, res) => {
    if (req.user) {
        sellerCollection.getTopPickProduct(req.params.uuid, req.params.collectionId, req.params.productId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteTopPickProduct = (req, res) => {
    if (req.user) {
        sellerCollection.deleteTopPickProduct(req.params.uuid, req.params.collectionId, req.params.productId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

// ======================== SELLER VOUCHERS ============================ //
// ======================== SELLER VOUCHERS ============================ //
// ======================== SELLER VOUCHERS ============================ //

exports.getSellerVouchers = (req, res) => {
    sellerVouchers.getSellerVouchers(req.params.uuid, cb.setupResponseCallback(res));
};

exports.createSellerVoucher = (req, res) => {
    if (req.user) {
        sellerVouchers.createSellerVoucher(req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateSellerVoucher = (req, res) => {
    if (req.user) {
        sellerVouchers.updateSellerVoucher(req.params.uuid, req.params.voucherId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteSellerVoucher = (req, res) => {
    if (req.user) {
        sellerVouchers.deleteSellerVoucher(req.params.uuid, req.params.voucherId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getSellerProductVouchers = (req, res) => {
    if (req.user) {
        sellerVouchers.getSellerProductVouchers(req.params.uuid, req.params.voucherId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.createSellerProductVoucher = (req, res) => {
    if (req.user) {
        sellerVouchers.createSellerProductVoucher(req.params.uuid, req.params.voucherId, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteSellerProductVoucher = (req, res) => {
    if (req.user) {
        sellerVouchers.deleteSellerProductVoucher(req.params.uuid, req.params.voucherId, req.params.productId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.geShopCustomers = (req, res) => {
    if (req.user) {
        shopCustomers.geShopCustomers(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getShopRatings = (req, res) => {
    if (req.user) {
        seller.getShopRatings(req.params.uuid, req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getShopSummaryRatings = (req, res) => {
    seller.getShopSummaryRatings(req.params.uuid, cb.setupResponseCallback(res));
};

exports.rateShop = (req, res) => {
    if (req.user) {
        seller.rateShop(req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token has expired',
            result: null,
            success: false
        });
    }
};

exports.getShopCategories = (req, res) => {
    seller.getShopCategories(req.user.result.user, cb.setupResponseCallback(res));
};

exports.getShopCategoriesByOtherSeller = (req, res) => {
    seller.getShopCategoriesByOtherSeller(req.params.uuid, cb.setupResponseCallback(res));
};


