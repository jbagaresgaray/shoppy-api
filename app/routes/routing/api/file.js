'use strict';

var cb = require('../../../utils/callback');
var fileCtrl = require('../../../controllers/api/file').File;
var files = new fileCtrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');
let path = require('path');
let fs = require('fs');

exports.uploadFile = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/files/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 2
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('c_file');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            console.log('req.file: ', req.file);
            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);
            req.body.file_name = req.file.originalname;
            req.body.file_size = req.file.size;
            req.body.file_type = req.file.mimetype
        }
        console.log('req.body: ', req.body);
        files.uploadFile(req.body, cb.setupResponseCallback(res));
    });
};

exports.getFile = (req, res) => {
    files.getFile(req.params.fileId, cb.setupResponseCallback(res));
};

exports.deleteFile = (req, res) => {
    files.deleteFile(req.params.fileId, cb.setupResponseCallback(res));
};