'use strict';

let cb = require('../../../utils/callback');
let usersCtrl = require('../../../controllers/api/users').Users;
let users = new usersCtrl();

const buyerVouchersCtrl = require('../../../controllers/api/buyer-vouchers').Buyer;
const buyerVouchers = new buyerVouchersCtrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

const multer = require('multer');
const _ = require('lodash');
const mkdirp = require('mkdirp');
const fs = require('fs');
const path = require('path');

var redis = require("redis"),
    client = redis.createClient();

client.on('connect', function () {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

exports.createUser = (req, res) => {
    users.createUser(req.body, cb.setupResponseCallback(res));
};

exports.socialSignUp = (req, res) => {
    users.socialSignUp(req.body, cb.setupResponseCallback(res));
};

exports.loginSocial = (req, res) => {
    users.loginSocial(req.body, cb.setupResponseCallback(res));
};

exports.checkIfVerified = (req, res) => {
    console.log('eq.params: ', req.params)
    users.checkIfVerified(req.params.hashkey, req.params.uuid, cb.setupResponseCallback(res));
};

exports.verifyUser = (req, res) => {
    users.verifyUser(req.params.hashkey, req.params.uuid, cb.setupResponseCallback(res));
};

exports.getUserProfile = (req, res) => {
    if (req.user) {
        users.getUserProfile(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getCurrentUser = (req, res) => {
    console.log('getCurrentUser routing');
    if (req.user) {
        users.getCurrentUser(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getUserAddresses = (req, res) => {
    if (req.user) {
        users.getUserAddresses(req.user.result.user, req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getUserAddress = (req, res) => {
    if (req.user) {
        users.getUserAddress(req.user.result.user, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteUserAddress = (req, res) => {
    if (req.user) {
        users.deleteUserAddress(req.user.result.user, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserAddress = (req, res) => {
    if (req.user) {
        users.updateUserAddress(req.user.result.user, req.params._id, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.createUserAddress = (req, res) => {
    if (req.user) {
        users.createUserAddress(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.login = (req, res) => {
    if (req.isAuthenticated()) {

        if (req.user && !req.user.success) {
            res.status(200).json(req.user);
            return;
        }

        if (req.body.player_id) {
            users.saveUserDevice(req.user.result.user, req.body, () => {
                res.status(200).json(req.user);
            });
        } else {
            res.status(200).json(req.user);
        }
    } else {
        res.sendStatus(401);
    }
};


exports.logout = (req, res) => {
    if (req.user) {
        let token = req.header('Authorization').split(' ')[1];

        client.hset("forbiddenTokens", token, token, redis.print);

        users.deleteUserDevice(req.user.result.user, req.body, () => {
            res.status(200).json({
                msg: 'User successfully logout',
                success: true,
                result: null,
            }).end();
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.tokenValidity = (req, res) => {
    if (req.user) {
        let token = req.header('Authorization').split(' ')[1];
        client.hkeys("forbiddenTokens", (err, replies) => {
            if (err) {
                res.status(500).json({
                    msg: 'Problems encountered when verifying forbidden tokens. maybe the record in redis database is removed or malformatted',
                    result: null,
                    success: false
                });
            } else {
                let isExist = _.find(replies, (val) => {
                    return val == token;
                });
                res.status(200).json({
                    msg: isExist ? 'Token is INVALID' : 'Token is Valid',
                    result: isExist ? {
                        'isValid': false
                    } : {
                        'isValid': true
                    },
                    success: true
                });
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.forgotPassword = (req, res) => {
    users.forgotPassword(req.body, cb.setupResponseCallback(res));
};

exports.resetPassword = (req, res) => {
    users.resetPassword(req.body, cb.setupResponseCallback(res));
};

exports.applyUserAsSeller = (req, res) => {
    if (req.user) {
        users.applyUserAsSeller(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.applyUserAsSellerUploadSelfie = (req, res) => {
    let param = req.query;
    if (req.user) {
        let curUser = req.user.result.user;
        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = `public/uploads/selfies/${config.env}/`;
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                cb(null, curUser.uuid + '_' + file.originalname);
            }
        });

        let limits = {
            fileSize: 1024 * 1024 * 10 // 10MB
        };

        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };

        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('photo');

        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            } else {
                if (req.fileValidationError) {
                    return res.status(400).json({
                        result: null,
                        msg: req.fileValidationError,
                        success: false
                    });
                } else {}
                req.body.toDeleteFile = false;
                if (!_.isUndefined(req.file)) {
                    req.body.toDeleteFile = true;
                    req.body.img_name = req.file.originalname;
                    req.body.img_type = req.file.mimetype;
                    req.body.img_size = req.file.size;
                    req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                    req.body.uuid = curUser.uuid;
                    req.body.content = fs.readFileSync(req.body.img_path);
                    users.applyUserAsSellerUploadSelfie(param, req.body, cb.setupResponseCallback(res));
                } else {
                    return res.status(500).json({
                        msg: 'Please select a photo to upload',
                        result: null,
                        success: false
                    });
                }
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.applyUserAsSellerUploadProof = (req, res) => {
    let param = req.query;
    if (req.user) {
        let curUser = req.user.result.user;
        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = `public/uploads/proof/${config.env}/`;
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                cb(null, curUser.uuid + '_' + file.originalname);
            }
        });

        let limits = {
            fileSize: 1024 * 1024 * 10 // 10MB
        };

        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };

        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('photo');

        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            } else {
                if (req.fileValidationError) {
                    return res.status(400).json({
                        result: null,
                        msg: req.fileValidationError,
                        success: false
                    });
                } else {}
                req.body.toDeleteFile = false;
                if (!_.isUndefined(req.file)) {
                    req.body.toDeleteFile = true;
                    req.body.img_name = req.file.originalname;
                    req.body.img_type = req.file.mimetype;
                    req.body.img_size = req.file.size;
                    req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                    req.body.uuid = curUser.uuid;
                    req.body.content = fs.readFileSync(req.body.img_path);
                    users.applyUserAsSellerUploadProof(param, req.body, cb.setupResponseCallback(res));
                } else {
                    return res.status(500).json({
                        msg: 'Please select a photo to upload',
                        result: null,
                        success: false
                    });
                }
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.applyUserAsSellerUploadGovId = (req, res) => {
    let param = req.query;
    if (req.user) {
        let curUser = req.user.result.user;
        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = `public/uploads/govids/${config.env}/`;
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                cb(null, curUser.uuid + '_' + file.originalname);
            }
        });

        let limits = {
            fileSize: 1024 * 1024 * 10 // 10MB
        };

        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };

        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('photo');

        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            } else {
                if (req.fileValidationError) {
                    return res.status(400).json({
                        result: null,
                        msg: req.fileValidationError,
                        success: false
                    });
                } else {}
                req.body.toDeleteFile = false;
                if (!_.isUndefined(req.file)) {
                    req.body.toDeleteFile = true;
                    req.body.img_name = req.file.originalname;
                    req.body.img_type = req.file.mimetype;
                    req.body.img_size = req.file.size;
                    req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                    req.body.uuid = curUser.uuid;
                    req.body.content = fs.readFileSync(req.body.img_path);
                    users.applyUserAsSellerUploadGovId(param, req.body, cb.setupResponseCallback(res));
                } else {
                    return res.status(500).json({
                        msg: 'Please select a photo to upload',
                        result: null,
                        success: false
                    });
                }
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.applyUserAsSellerUploadPersonalId = (req, res) => {
    let param = req.query;
    if (req.user) {
        let curUser = req.user.result.user;
        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = `public/uploads/personal/${config.env}/`;
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                cb(null, curUser.uuid + '_' + file.originalname);
            }
        });

        let limits = {
            fileSize: 1024 * 1024 * 10 // 10MB
        };

        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };

        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('photo');

        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            } else {
                if (req.fileValidationError) {
                    return res.status(400).json({
                        result: null,
                        msg: req.fileValidationError,
                        success: false
                    });
                } else {}
                req.body.toDeleteFile = false;
                if (!_.isUndefined(req.file)) {
                    req.body.toDeleteFile = true;
                    req.body.img_name = req.file.originalname;
                    req.body.img_type = req.file.mimetype;
                    req.body.img_size = req.file.size;
                    req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                    req.body.uuid = curUser.uuid;
                    req.body.content = fs.readFileSync(req.body.img_path);
                    users.applyUserAsSellerUploadPersonalId(param, req.body, cb.setupResponseCallback(res));
                } else {
                    return res.status(500).json({
                        msg: 'Please select a photo to upload',
                        result: null,
                        success: false
                    });
                }
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.validatePassword = (req, res) => {
    if (req.user) {
        users.validatePassword(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserAccount = (req, res) => {
    if (req.user) {
        users.updateUserAccount(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserEmail = (req, res) => {
    if (req.user) {
        users.updateUserEmail(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserCurrency = (req, res) => {
    if (req.user) {
        users.updateUserCurrency(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserCountry = (req, res) => {
    if (req.user) {
        users.updateUserCountry(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserPassword = (req, res) => {
    if (req.user) {
        users.updateUserPassword(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        console.log('login at else');
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getAllUserBanks = (req, res) => {
    if (req.user) {
        users.getAllUserBanks(req.user.result.user, req.query, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.saveUserBank = (req, res) => {
    if (req.user) {
        users.saveUserBank(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getUserBank = (req, res) => {
    if (req.user) {
        users.getUserBank(req.user.result.user, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteUserBank = (req, res) => {
    if (req.user) {
        users.deleteUserBank(req.user.result.user, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.updateUserBank = (req, res) => {
    if (req.user) {
        users.updateUserBank(req.user.result.user, req.params._id, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.createUserSellerShipping = (req, res) => {
    if (req.user) {
        users.createUserSellerShipping(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getAllUserSellerShipping = (req, res) => {
    if (req.user) {
        users.getAllUserSellerShipping(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteUserSellerShipping = (req, res) => {
    if (req.user) {
        users.deleteUserSellerShipping(req.user.result.user, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.linkGoogleAccount = (req, res) => {
    if (req.user) {
        users.linkGoogleAccount(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.linkFacebookAccount = (req, res) => {
    if (req.user) {
        users.linkFacebookAccount(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.unlinkGoogleAccount = (req, res) => {
    if (req.user) {
        users.unlinkGoogleAccount(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.unlinkFacebookAccount = (req, res) => {
    if (req.user) {
        users.unlinkFacebookAccount(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.followUser = (req, res) => {
    if (req.user) {
        users.followUser(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getCurrentUserFollowing = (req, res) => {
    if (req.user) {
        users.getUserFollowing(req.user.result.user.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getCurrentUserFollowers = (req, res) => {
    if (req.user) {
        users.getUserFollowers(req.user.result.user.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};


exports.getUserFollowers = (req, res) => {
    if (req.user) {
        users.getUserFollowers(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getUserFollowing = (req, res) => {
    if (req.user) {
        users.getUserFollowing(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.unfollowUser = (req, res) => {
    if (req.user) {
        users.unfollowUser(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.uploadUserAvatar = (req, res) => {
    if (req.user) {
        let curUser = req.user.result.user;
        console.log('curUser: ', curUser);

        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = 'public/uploads/avatar/' + config.env + '/';
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                console.log('filename file: ', file);
                cb(null, curUser.uuid + '_' + file.originalname);
            }
        });

        let limits = {
            fileSize: 1024 * 1024 * 10 // 10MB
        };

        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };

        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('avatar');

        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            }

            if (req.fileValidationError) {
                return res.status(400).json({
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                });
            }
            req.body.toDeleteFile = false;
            if (!_.isUndefined(req.file)) {
                req.body.toDeleteFile = true;
                req.body.img_name = req.file.originalname;
                req.body.img_type = req.file.mimetype;
                req.body.img_size = req.file.size;
                req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                req.body.uuid = curUser.uuid;
                req.body.content = fs.readFileSync(req.body.img_path);
            }
            console.log('req.body: ', req.body);
            if (req.user) {
                users.uploadUserAvatar('avatar', req.body, cb.setupResponseCallback(res));
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.uploadUserBanner = (req, res) => {
    if (req.user) {
        let curUser = req.user.result.user;

        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = 'public/uploads/banner/' + config.env + '/';
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                console.log('filename file: ', file);
                cb(null, curUser.uuid + '_' + file.originalname);
            }
        });

        let limits = {
            fileSize: 1024 * 1024 * 10 // 10MB
        };

        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };

        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('banner');

        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            }

            if (req.fileValidationError) {
                return res.status(400).json({
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                });
            }
            req.body.toDeleteFile = false;
            if (!_.isUndefined(req.file)) {
                req.body.toDeleteFile = true;
                req.body.img_name = req.file.originalname;
                req.body.img_type = req.file.mimetype;
                req.body.img_size = req.file.size;
                req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                req.body.uuid = curUser.uuid;
                req.body.content = fs.readFileSync(req.body.img_path);
            }
            console.log('req.body: ', req.body);
            if (req.user) {
                users.uploadUserAvatar('banner', req.body, cb.setupResponseCallback(res));
            }
        });
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getBuyerVouchers = (req, res) => {
    if (req.user) {
        buyerVouchers.getBuyerVouchers(req.user.result.user, req.params, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.claimVoucher = (req, res) => {
    if (req.user) {
        buyerVouchers.claimVoucher(req.user.result.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.userRatings = (req, res) => {
    if (req.user) {
        users.userRatings(req.user.result.user, req.params, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};