'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/api/product_wholesale').ProductWholeSale;
let sales = new ctrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let _ = require('lodash');


exports.createProductWholesale = (req, res) => {
    if (req.user) {
        sales.createProductWholesale(req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.deleteAllProductWholeSale = (req, res) => {
    if (req.user) {
        sales.deleteAllProductWholeSale(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllProductWholeSale = (req, res) => {
    if (req.user) {
        sales.getAllProductWholeSale(req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.deleteProductWholeSale = (req, res) => {
    if (req.user) {
        sales.deleteProductWholeSale(req.params.uuid, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getProductWholeSale = (req, res) => {
    if (req.user) {
        sales.getProductWholeSale(req.params.uuid, req.params._id, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.updateProductWholeSale = (req, res) => {
    if (req.user) {
        sales.updateProductWholeSale(req.params.uuid, req.params._id, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};