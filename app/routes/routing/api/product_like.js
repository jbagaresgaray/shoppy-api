'use strict';

let cb = require('../../../utils/callback');

let ctrl = require('../../../controllers/api/product_like').ProductLike;
let likes = new ctrl();

let ctrl1 = require('../../../controllers/api/product_like').ProductRating;
let rating = new ctrl1();


let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');
let path = require('path');
let fs = require('fs');


exports.likeProduct = (req, res) => {
    if (req.user) {
        likes.likeProduct(req.user.result.user, req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.unlikeProduct = (req, res) => {
    if (req.user) {
        likes.unlikeProduct(req.user.result.user, req.params.uuid, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllUserProductLikes = (req, res) => {
    if (req.user) {
        likes.getAllUserProductLikes(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};


// ======================== PRODUCTS RATING ============================ //
// ======================== PRODUCTS RATING ============================ //
// ======================== PRODUCTS RATING ============================ //

exports.rateProduct = (req, res) => {
    if (req.user) {
        rating.rateProduct(req.user.result.user, req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};


exports.updateProductRating = (req, res) => {
    if (req.user) {
        rating.updateRating(req.user.result.user, req.params.uuid, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.uploadRatingAttachments = (req, res) => {
    if (req.user) {
        let storage = multer.diskStorage({
            destination: (req, file, cb) => {
                let dir = 'public/uploads/files/' + config.env + '/';
                mkdirp(dir, (err) => {
                    cb(err, dir);
                });
            },
            filename: (req, file, cb) => {
                cb(null, file.originalname);
            }
        });
    
        let limits = {
            fileSize: 1024 * 1024 * 2
        };
    
        let fileFilter = (req, file, cb) => {
            if ((file.mimetype !== 'image/png') &&
                (file.mimetype !== 'image/jpeg') &&
                (file.mimetype !== 'image/jpg') &&
                (file.mimetype !== 'image/gif') &&
                (file.mimetype !== 'image/x-ms-bmp')) {
                req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
                return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
            }
            cb(null, true);
        };
    
        let uploadImg = multer({
            storage: storage,
            fileFilter: fileFilter,
            limits: limits
        }).single('file');
    
        uploadImg(req, res, (err) => {
            if (err) {
                if (err.code == 'LIMIT_FILE_SIZE') {
                    return res.status(400).json({
                        result: 'LIMIT_FILE_SIZE',
                        success: false,
                        msg: 'File Size Limit Exceeded'
                    });
                } else {
                    return res.status(500).json({
                        result: err,
                        success: false,
                        msg: err.inner
                    });
                }
            }
    
            if (req.fileValidationError) {
                return res.status(400).json({
                    result: null,
                    msg: req.fileValidationError,
                    success: false
                });
            }
    
            req.body.toDeleteFile = false;
            if (!_.isUndefined(req.file)) {
                req.body.img_path = path.resolve(req.file.destination, req.file.filename);
                req.body.content = fs.readFileSync(req.body.img_path);
                req.body.img_name = req.file.originalname;
                req.body.img_size = req.file.size;
                req.body.img_type = req.file.mimetype
            }
            rating.uploadRatingAttachments(req.params.productRatingId, req.body, cb.setupResponseCallback(res));
        });
    } else {
        res.sendStatus(401);
    }
};

exports.getAllProductRatings = (req, res) => {
    rating.getAllProductRatings(req.params.uuid, req.query, cb.setupResponseCallback(res));
};