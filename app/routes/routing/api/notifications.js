'use strict';

var cb = require('../../../utils/callback');
var notificationCtrl = require('../../../controllers/api/notifications').Notification;
var ctrl = new notificationCtrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);


exports.getUpdatesNotifications = (req, res) => {
    if (req.user) {
        ctrl.getUpdatesNotifications(req.user.result.user, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.getNotificationDetails = (req, res) => {
    if (req.user) {
        ctrl.getNotificationDetails(req.user.result.user, req.params.notifId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};


exports.updateNotification = (req, res) => {
    if (req.user) {
        ctrl.updateNotification(req.user.result.user, req.params.notifId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};

exports.deleteNotification = (req, res) => {
    if (req.user) {
        ctrl.deleteNotification(req.user.result.user, req.params.notifId, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({
            msg: 'User not found or Token had expired',
            result: null,
            success: false
        });
    }
};