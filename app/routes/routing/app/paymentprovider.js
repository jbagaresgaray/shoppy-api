'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/app/paymentprovider').PaymentProvider;
let payment = new ctrl();

exports.createPaymentProvider = (req, res) => {
    payment.createPaymentProvider(req.body, cb.setupResponseCallback(res));
};

exports.getAllPaymentProvider = (req, res) => {
    payment.getAllPaymentProvider(req.query, cb.setupResponseCallback(res));
};

exports.getPaymentProvider = (req, res) => {
    payment.getPaymentProvider(req.params.paymentId, cb.setupResponseCallback(res));
};

exports.deletePaymentProvider = (req, res) => {
    payment.deletePaymentProvider(req.params.paymentId, cb.setupResponseCallback(res));
};

exports.updatePaymentProvider = (req, res) => {
    payment.updatePaymentProvider(req.params.paymentId, req.body, cb.setupResponseCallback(res));
};