'use strict';

let cb = require('../../../utils/callback');
let attributeCategoryCtrl = require('../../../controllers/app/attribute_category').AttributeCategory;
let attributeCategory = new attributeCategoryCtrl();

exports.createAttributeCategory = (req, res) => {
    attributeCategory.createAttributeCategory(req.params.categoryId, req.body, cb.setupResponseCallback(res));
};

exports.getAllAttributeCategory = (req, res) => {
    attributeCategory.getAllAttributeCategory(req.params.categoryId, req.query, cb.setupResponseCallback(res));
};

exports.getAttributeCategory = (req, res) => {
    attributeCategory.getAttributeCategory(req.params.categoryId, req.params.attributeId, cb.setupResponseCallback(res));
};

exports.deleteAttributeCategory = (req, res) => {
    attributeCategory.deleteAttributeCategory(req.params.categoryId, req.params.attributeId, cb.setupResponseCallback(res));
};

// exports.updateAttributeCategory = (req, res) => {
//     attributeCategory.updateAttributeCategory(req.params.attributeId, req.body, cb.setupResponseCallback(res));
// };