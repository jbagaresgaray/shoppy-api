'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/app/courier').Courier;
let courier = new ctrl();

exports.createCourier = (req, res) => {
    courier.createCourier(req.body, cb.setupResponseCallback(res));
};

exports.getAllCourier = (req, res) => {
    courier.getAllCourier(req.query, cb.setupResponseCallback(res));
};

exports.getCourier = (req, res) => {
    courier.getCourier(req.params.courierId, cb.setupResponseCallback(res));
};

exports.deleteCourier = (req, res) => {
    courier.deleteCourier(req.params.courierId, cb.setupResponseCallback(res));
};

exports.updateCourier = (req, res) => {
    courier.updateCourier(req.params.courierId, req.body, cb.setupResponseCallback(res));
};