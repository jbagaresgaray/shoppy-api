'use strict';

var cb = require('../../../utils/callback');
var mobileCtrl = require('../../../controllers/app/mobile').Mobile;
var mobile = new mobileCtrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');
const path = require('path');
let fs = require('fs');

exports.createMobileBanners = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/mobile/banners/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 2
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('c_file');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            // req.body.img_path = req.file.path;
            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);
            req.body.img_name = req.file.originalname;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        mobile.createMobileBanner(req.body, cb.setupResponseCallback(res));
    });
};

exports.updateMobileBanners = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/mobile/banners/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 2
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('c_file');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        console.log('req.file: ', req.file);
        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            // req.body.img_path = req.file.path;
            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);
            req.body.img_name = req.file.filename;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        mobile.updateMobileBanner(req.params.bannerId, req.body, cb.setupResponseCallback(res));
    });
};

exports.getBanner = (req, res) => {
    mobile.getBanner(req.params.bannerId, cb.setupResponseCallback(res));
};

exports.getAllMobileBanners = (req, res) => {
    mobile.getAllMobileBanners(cb.setupResponseCallback(res));
};

exports.getAllActiveMobileBanners = (req, res) => {
    mobile.getAllActiveMobileBanners(cb.setupResponseCallback(res));
};

exports.deleteMobileBanner = (req, res) => {
    mobile.deleteMobileBanner(req.params.bannerId, cb.setupResponseCallback(res));
};

exports.publishBanner = (req, res) => {
    mobile.publishBanner(req.params.bannerId, cb.setupResponseCallback(res));
};

exports.unpublishBanner = (req, res) => {
    mobile.unpublishBanner(req.params.bannerId, cb.setupResponseCallback(res));
};



exports.createMobileCMS = (req, res) => {
    if (req.user) {
        mobile.createMobileCMS(req.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.sendStatus(401);
    }
};

exports.getAllMobileCMS = (req, res) => {
    mobile.getAllMobileCMS(cb.setupResponseCallback(res));
};

exports.getMobileCMS = (req, res) => {
    mobile.getMobileCMS(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.deleteMobileCMS = (req, res) => {
    mobile.deleteMobileCMS(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.updateMobileCMS = (req, res) => {
    mobile.updateMobileCMS(req.params.cmsId, req.body, cb.setupResponseCallback(res));
};