'use strict'

var cb = require('../../../utils/callback');

var sharerCtrl = require('../../../controllers/app/email').Sharer;
var sharer = new sharerCtrl();


exports.shareExperience = function onRequest(req, res) {
    sharer.shareExperience(req.body, cb.setupResponseCallback(res));
};

exports.htmltoPDF = function onRequest(req, res) {
    sharer.htmltoPDF(req.body, cb.setupResponseCallback(res));
};

exports.sendMail = function onRequest(req, res) {
    sharer.sendMail(req.body, cb.setupResponseCallback(res));
};

exports.sendTestMail = function onRequest(req, res) {
    sharer.sendTestMail(req.body, cb.setupResponseCallback(res));
};