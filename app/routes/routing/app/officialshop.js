"use strict";

var cb = require("../../../utils/callback");
var shopCtrl = require("../../../controllers/app/officialshop").OfficialShop;
var shop = new shopCtrl();

let env = process.env.NODE_ENV || "development";
let config = require("../../../../config/environment/" + env);
let func = require('../../../../app/utils/functions');

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');

exports.validateSellerCode = (req, res) => {
    req.checkBody('username', 'Please provide shop username').notEmpty();
    req.checkBody('code', 'Please provide Official shop application code').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        shop.validateSellerCode(req.body, cb.setupResponseCallback(res));
    }
};

exports.saveApplicationInfo = (req, res) => {
    req.checkBody('shop_name', 'Please provide shop name').notEmpty();
    req.checkBody('shop_email', 'Please provide shop email').notEmpty();
    req.checkBody('shop_email', 'Shop email address is not a valid email format').isEmail();
    req.checkBody('shop_mobileno', 'Please provide shop mobile number').notEmpty();
    req.checkBody('owner_fname', 'Please provide shop owner first name').notEmpty();
    req.checkBody('owner_mname', 'Please provide shop owner middle name').notEmpty();
    req.checkBody('owner_lname', 'Please provide shop owner last name').notEmpty();
    req.checkBody('owner_email', 'Please provide shop owner email').notEmpty();
    req.checkBody('owner_email', 'Owner email address is not a valid email format').isEmail();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        console.log('saveApplicationInfo: ', req.body);
        shop.saveApplicationInfo(req.body, cb.setupResponseCallback(res));
    }
};

exports.getApplicationInfo = (req, res) => {
    req.checkQuery('token', 'Please provide shop token parameter').notEmpty();
    req.checkQuery('code', 'Please provide shop code parameter').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        shop.getApplicationInfo(req.query, cb.setupResponseCallback(res));
    }
};

exports.uploadCORFile = (req, res) => {
    let dir = 'public/uploads/officialshop/' + config.env + '/';

    let fileUploadFilter = (req, file, cb) => {
        if ((file.mimetype !== 'application/pdf') &&
            (file.mimetype !== 'application/octet-stream') &&
            (file.mimetype !== 'application/msword') &&
            (file.mimetype !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') &&
            (file.mimetype !== 'application/vnd.ms-excel') &&
            (file.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') &&
            (file.mimetype !== 'application/vnd.ms-powerpoint') &&
            (file.mimetype !== 'application/vnd.openxmlformats-officedocument.presentationml.presentation')) {
            req.fileValidationError = 'Invalid file type. Must be .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx';
            return cb(null, false, new Error('Invalid file type. Must be .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: func.uploadDirectory(dir, req, res),
        fileFilter: fileUploadFilter,
        limits: func.uploadFileLimits()
    }).single('file');

    uploadImg(req, res, (err) => {
        req.checkBody('shop_cor_number', 'Please provide shop registration number').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            return res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_path = req.file.path;
            req.body.img_name = req.file.filename;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        shop.uploadCORFile(req.body, cb.setupResponseCallback(res));
    });
};

exports.uploadBusinessLicenseFile = (req, res) => {
    let dir = 'public/uploads/officialshop/' + config.env + '/';

    let fileUploadFilter = (req, file, cb) => {
        if ((file.mimetype !== 'application/pdf') &&
            (file.mimetype !== 'application/octet-stream') &&
            (file.mimetype !== 'application/msword') &&
            (file.mimetype !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') &&
            (file.mimetype !== 'application/vnd.ms-excel') &&
            (file.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') &&
            (file.mimetype !== 'application/vnd.ms-powerpoint') &&
            (file.mimetype !== 'application/vnd.openxmlformats-officedocument.presentationml.presentation')) {
            req.fileValidationError = 'Invalid file type. Must be .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx';
            return cb(null, false, new Error('Invalid file type. Must be .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: func.uploadDirectory(dir, req, res),
        fileFilter: fileUploadFilter,
        limits: func.uploadFileLimits()
    }).single('file');

    uploadImg(req, res, (err) => {
        req.checkBody('shop_distributor_license_number', 'Please provide shop license number').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            return res.status(400).send({
                result: errors,
                msg: '',
                success: false
            });
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_path = req.file.path;
            req.body.img_name = req.file.filename;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        shop.uploadBusinessLicenseFile(req.body, cb.setupResponseCallback(res));
    });
};

exports.uploadBusinessLogoFile = (req, res) => {
    let dir = 'public/uploads/officialshop/' + config.env + '/';

    let imageUploadFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: func.uploadDirectory(dir, req, res),
        fileFilter: imageUploadFilter,
        limits: func.uploadFileLimits()
    }).single('logo');

    uploadImg(req, res, (err) => {
        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_path = req.file.path;
            req.body.img_name = req.file.filename;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        shop.uploadBusinessLogoFile(req.body, cb.setupResponseCallback(res));
    });
};

exports.deleteCORFile = (req, res) => {
    req.checkQuery('ref_code', 'Please provide shop url referrence code parameter').notEmpty();
    req.checkQuery('code', 'Please provide shop code parameter').notEmpty();
    req.checkQuery('userId', 'Please provide shop userId parameter').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        console.log('deleteCORFile 1');
        shop.deleteCORFile(req.query, cb.setupResponseCallback(res));
    }
};

exports.deleteLicenseFile = (req, res) => {
    req.checkQuery('ref_code', 'Please provide shop url referrence code parameter').notEmpty();
    req.checkQuery('code', 'Please provide shop code parameter').notEmpty();
    req.checkQuery('userId', 'Please provide shop userId parameter').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        shop.deleteBusinessLicenseFile(req.query, cb.setupResponseCallback(res));
    }
};

exports.deleteBusinessLogo = (req, res) => {
    req.checkQuery('ref_code', 'Please provide shop url referrence code parameter').notEmpty();
    req.checkQuery('code', 'Please provide shop code parameter').notEmpty();
    req.checkQuery('userId', 'Please provide shop userId parameter').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        shop.deleteBusinessLogoFile(req.query, cb.setupResponseCallback(res));
    }
};


exports.getApplicationList = (req, res) => {
    shop.getApplicationList(req.query, cb.setupResponseCallback(res));
};

exports.getApplicationDetail = (req, res) => {
    shop.getApplicationDetail(req.params.shopId, cb.setupResponseCallback(res));
};

exports.getApplicationEvaluationList = (req, res) => {
    shop.getApplicationEvaluationList(req.query, cb.setupResponseCallback(res));
};

exports.updateApplicationEvaluation = (req, res) => {
    req.checkBody('official_shop_id', 'Please provide Shop ID').notEmpty();
    req.checkBody('date', 'Please provide evaluation date').notEmpty();
    req.checkBody('statusClass', 'Please provide status class').notEmpty();
    req.checkBody('content', 'Please provide evaluation content').notEmpty();
    req.checkBody('eval_categoryID', 'Please provide evaluation category').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        shop.updateApplicationEvaluation(req.body, cb.setupResponseCallback(res));
    }
};

exports.approveOfficialShopApplication = (req, res) => {
    req.checkBody('official_shop_id', 'Please provide Shop ID').notEmpty();
    req.checkParams('shopId', 'Please provide Shop ID').notEmpty();
    req.checkBody('date', 'Please provide evaluation date').notEmpty();
    req.checkBody('statusClass', 'Please provide status class').notEmpty();
    req.checkBody('content', 'Please provide evaluation content').notEmpty();
    req.checkBody('eval_categoryID', 'Please provide evaluation category').notEmpty();
    req.checkBody('shop_appoved_date', 'Please provide approved date').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        shop.approveOfficialShopApplication(req.user.result.user, req.body, cb.setupResponseCallback(res));
    }
};