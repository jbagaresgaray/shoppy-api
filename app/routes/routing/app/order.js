'use strict';

let cb = require('../../../utils/callback');
let ordersCtrl = require('../../../controllers/app/orders').Orders;
let orders = new ordersCtrl();



exports.getOrderDetails = (req, res) => {
    orders.getOrderDetails(req.params.orderId, cb.setupResponseCallback(res));
}

exports.getOrderDetailsByBatchNum = (req, res) => {
    orders.getOrderDetailsByBatchNum(req.params.batchId, cb.setupResponseCallback(res));
};

exports.getOrdersList = (req, res) => {
    orders.getOrdersList(req.query.action, req.query.type, cb.setupResponseCallback(res));
};

exports.payOrder = (req, res) => {
    orders.payOrder(req.params.orderId, req.body, cb.setupResponseCallback(res));
};

exports.shippedOrder = (req, res) => {
    req.checkParams('orderId', 'Please provide Order ID').notEmpty();
    req.checkBody('shippedDateTime', 'Please provide Shipping time').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        orders.shippedOrder(req.params.orderId, req.body, cb.setupResponseCallback(res));
    }
};

exports.orderReceivedReminder = (req, res) => {
    req.checkParams('orderId', 'Please provide Order ID').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        orders.orderReceivedReminder(req.params.orderId, cb.setupResponseCallback(res));
    }
};

exports.autoCancelUnshippedOrders = (req, res) => {
    orders.autoCancelUnshippedOrders(req.params.orderId, cb.setupResponseCallback(res));
};

exports.autoReleasePayment = (req, res) => {
    orders.autoReleasePayment(req.params.orderId, cb.setupResponseCallback(res));
};