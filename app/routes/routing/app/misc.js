'use strict';

var cb = require('../../../utils/callback');
var miscCtrl = require('../../../controllers/app/misc').Misc;
var misc = new miscCtrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');
let path = require('path');
let fs = require('fs');

exports.getAllTimezones = (req, res) => {
    misc.getAllTimezones(cb.setupResponseCallback(res));
};


exports.getAllCountry = (req, res) => {
    misc.getAllCountry(cb.setupResponseCallback(res));
};

exports.getAllCurrencies = (req, res) => {
    misc.getAllCurrencies(cb.setupResponseCallback(res));
};

exports.testUpload = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/category/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 2
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('c_file');

    uploadImg(req, res, (err) => {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.img_path = path.resolve(req.file.destination, req.file.filename);
            req.body.content = fs.readFileSync(req.body.img_path);
            req.body.img_name = req.file.filename;
        }
        // console.log('req.body: ', req.body);
        misc.testUpload(req.body, cb.setupResponseCallback(res));
    });
};

exports.getAllUploads = (req, res) => {
    misc.getAllUploads(cb.setupResponseCallback(res));
};

exports.previewFileUpload = (req, res) => {
    misc.previewFileUpload(req.params.file, cb.setupResponseCallback(res));
};

exports.testPushNotif = (req, res) => {
    misc.testPushNotif(req.body, cb.setupResponseCallback(res));
};

exports.getAllNotifications = (req, res) => {
    misc.getAllNotifications(cb.setupResponseCallback(res));
};

exports.getNotification = (req, res) => {
    misc.getNotification(req.params.notifId, cb.setupResponseCallback(res));
};

exports.getAllDevices = (req, res) => {
    misc.getAllDevices(cb.setupResponseCallback(res));
};

exports.deleteNotification = (req, res) => {
    console.log("deleteNotification 1");
    misc.deleteNotification(req.params.notifId, cb.setupResponseCallback(res));
};