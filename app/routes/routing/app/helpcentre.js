'use strict';

let cb = require('../../../utils/callback');
let helpCtrl = require('../../../controllers/app/helpcentre').Help;
let help = new helpCtrl();

exports.getAllHelpCategory = (req, res) => {
    help.getAllHelpCategory(cb.setupResponseCallback(res));
};

exports.createHelpPost = (req, res) => {
    req.checkBody('title', 'Please provide Content title').notEmpty();
    req.checkBody('content', 'Please provide Content details').notEmpty();
    req.checkBody('categoryId', 'Please provide Content summary').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        help.createHelpPost(req.user.result.user, req.body, cb.setupResponseCallback(res));
    }
};

exports.getAllHelpPosts = (req,res)=>{
	help.getAllHelpPosts(cb.setupResponseCallback(res));
};

exports.getPostByPostID = (req,res)=>{
	help.getPostByPostID(req.params.postId, cb.setupResponseCallback(res));
};

exports.getAllHelpByCategory = (req,res) =>{
	help.getAllHelpByCategory(req.params.categoryId, cb.setupResponseCallback(res));
};