'use strict';

var cb = require('../../../utils/callback');
var cmsCtrl = require('../../../controllers/app/newsletter').CMS;
var mobile = new cmsCtrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');
const path = require('path');
let fs = require('fs');


exports.createNewsletter = (req, res) => {
    if (req.user) {
        mobile.createNewsletter(req.user, req.body, cb.setupResponseCallback(res));
    } else {
        res.status(401).json({ msg: 'User not found or Token had expired', result: null, success: false });
    }
};

exports.getAllNewsletter = (req, res) => {
    mobile.getAllNewsletter(cb.setupResponseCallback(res));
};

exports.getNewsletter = (req, res) => {
    console.log('getNewsletter: 1');
    mobile.getNewsLetter(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.deleteNewsletter = (req, res) => {
    mobile.deleteNewsletter(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.updateNewsletter = (req, res) => {
    mobile.updateNewsletter(req.params.cmsId, req.user, req.body, cb.setupResponseCallback(res));
};

exports.publishedNewsletter = (req, res) => {
    mobile.publishedNewsletter(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.unpublishNewsletter = (req, res) => {
    mobile.unpublishNewsletter(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.getAllNotificationType = (req, res) => {
    mobile.getAllNotificationType(cb.setupResponseCallback(res));
};

exports.publishedMailNewsletter = (req, res) => {
    mobile.publishedMailNewsletter(req.params.cmsId, cb.setupResponseCallback(res));
};

exports.publishedPushNewsletter = (req, res) => {
    mobile.publishedPushNewsletter(req.params.cmsId, cb.setupResponseCallback(res));
};