'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/app/trackings').Trackings;
let trackings = new ctrl();


exports.getTrackingCheckingFlags = (req, res) => {
    trackings.getTrackingCheckingFlags(cb.setupResponseCallback(res));
};

exports.getTrackingShippingTags = (req, res) => {
    trackings.getTrackingShippingTags(cb.setupResponseCallback(res));
};

exports.getAllOrderTrackings = (req, res) => {
    trackings.getAllOrderTrackings(req.query, cb.setupResponseCallback(res));
};

exports.getOrderTrackingByOrderNum = (req, res) => {
    trackings.getOrderTrackingByOrderNum(req.params.ordernum, cb.setupResponseCallback(res));
};

exports.getOrderTrackingByTrackingNum = (req, res) => {
    trackings.getOrderTrackingByTrackingNum(req.params.slug, req.params.tracking_number, cb.setupResponseCallback(res));
};

exports.updateOrderTracking = (req, res) => {
    req.checkParams('order_trackingId', 'Please provide Order Tracking ID').notEmpty();
    if (req.body.flags.order_checking_flagsId == 1) {
        req.checkBody('tracking_number', 'Please provide Tracking Number').notEmpty();
        req.checkBody('signed_by', 'Please provide Courier pickup personnel').notEmpty();
    }
    req.checkBody('message', 'Please provide Tracking event').notEmpty();
    req.checkBody('slug', 'Please provide Shipping courier slug').notEmpty();
    req.checkBody('tag', 'Please provide Shipping event tag').notEmpty();
    req.checkBody('subtag', 'Please provide Shipping event subtag').notEmpty();
    req.checkBody('created_at', 'Please provide Shipping event checkpoint time').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        trackings.updateOrderTracking(req.params.order_trackingId, req.body, cb.setupResponseCallback(res));
    }
};