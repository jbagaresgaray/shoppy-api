'use strict';

let cb = require('../../../utils/callback');
let banksCtrl = require('../../../controllers/app/banks').Banks;
let banks = new banksCtrl();

exports.createBank = (req, res) => {
    banks.createBank(req.body, cb.setupResponseCallback(res));
};

exports.getAllBank = (req, res) => {
    banks.getAllBank(req.query, cb.setupResponseCallback(res));
};

exports.getBank = (req, res) => {
    banks.getBank(req.params.bankId, cb.setupResponseCallback(res));
};

exports.deleteBank = (req, res) => {
    banks.deleteBank(req.params.bankId, cb.setupResponseCallback(res));
};

exports.updateBank = (req, res) => {
    banks.updateBank(req.params.bankId, req.body, cb.setupResponseCallback(res));
};