'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/app/tags').Tags;
let tags = new ctrl();

exports.createTags = (req, res) => {
    tags.createTags(req.body, cb.setupResponseCallback(res));
};

exports.getAllTags = (req, res) => {
    tags.getAllTags(cb.setupResponseCallback(res));
};

exports.getTag = (req, res) => {
    tags.getTag(req.params.tagId, cb.setupResponseCallback(res));
};

exports.deleteTag = (req, res) => {
    tags.deleteTag(req.params.tagId, cb.setupResponseCallback(res));
};

exports.updateTag = (req, res) => {
    tags.updateTag(req.params.tagId, req.body, cb.setupResponseCallback(res));
};