'use strict';

let cb = require('../../../utils/callback');
let usersCtrl = require('../../../controllers/app/users').Users;
let users = new usersCtrl();

exports.createUser = (req, res) => {
    users.createUser(req.body, cb.setupResponseCallback(res));
};

exports.getAllUsers = (req, res) => {
    users.getAllUsers(req.query, cb.setupResponseCallback(res));
};

exports.getUser = (req, res) => {
    users.getUser(req.params.user_id, cb.setupResponseCallback(res));
};

exports.deleteUser = (req, res) => {
    users.deleteUser(req.params.user_id, cb.setupResponseCallback(res));
};

exports.updateUser = (req, res) => {
    users.updateUser(req.params.user_id, req.body, cb.setupResponseCallback(res));
};

exports.updateUserAccount = (req, res) => {
    users.updateUserAccount(req.params.user_id, req.body, cb.setupResponseCallback(res));
};

exports.updateUserEmail = (req, res) => {
    users.updateUserEmail(req.params.user_id, req.body, cb.setupResponseCallback(res));
};

exports.checkIfVerified = (req, res) => {
    users.checkIfVerified(req.params.hashkey, cb.setupResponseCallback(res));
};

exports.verifyUser = (req, res) => {
    users.verifyUser(req.params.hashkey, cb.setupResponseCallback(res));
};

exports.logout = (req, res) => {
    req.logout();
    res.status(200).json({
        msg: 'User successfully logout',
        success: true,
        result: null,
    }).end();
};

exports.login = (req, res) => {
    if (req.isAuthenticated()) {
        res.status(200).json(req.user);
    } else {
        console.log('login at else');
        res.sendStatus(401);
    }
};

exports.forgotPassword = (req, res) => {
    users.forgotPassword(req.body, cb.setupResponseCallback(res));
};

exports.resetPassword = (req, res) => {
    users.resetPassword(req.body, cb.setupResponseCallback(res));
};