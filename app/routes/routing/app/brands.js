'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/app/brands').Brands;
let brands = new ctrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');

exports.createBrand = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/brands/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 2
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('b_file');

    uploadImg(req, res, (err) => {
        req.checkBody('name', 'Please provide Brand name').notEmpty();
        let errors = req.validationErrors();
        if (errors) {
            return res.status(400).json({
                result: errors,
                msg: '',
                success: false
            });

        }

        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_path = req.file.path;
            req.body.img_name = req.file.filename;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        brands.createBrand(req.body, cb.setupResponseCallback(res));
    });
};

exports.getAllBrand = (req, res) => {
    brands.getAllBrand(req.query, cb.setupResponseCallback(res));
};

exports.getBrand = (req, res) => {
    brands.getBrand(req.params.brandId, cb.setupResponseCallback(res));
};

exports.deleteBrand = (req, res) => {
    brands.deleteBrand(req.params.brandId, cb.setupResponseCallback(res));
};

exports.updateBrand = (req, res) => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
            let dir = 'public/uploads/brands/' + config.env + '/';
            mkdirp(dir, (err) => {
                cb(err, dir);
            });
        },
        filename: (req, file, cb) => {
            cb(null, Math.floor(Date.now() / 1000) + '_' + file.originalname);
        }
    });

    let limits = {
        fileSize: 1024 * 1024 * 2
    };

    let fileFilter = (req, file, cb) => {
        if ((file.mimetype !== 'image/png') &&
            (file.mimetype !== 'image/jpeg') &&
            (file.mimetype !== 'image/jpg') &&
            (file.mimetype !== 'image/gif') &&
            (file.mimetype !== 'image/x-ms-bmp')) {
            req.fileValidationError = 'Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp';
            return cb(null, false, new Error('Invalid file type. Must be .png, .jpg, .jpeg, .gif, .bmp'));
        }
        cb(null, true);
    };

    let uploadImg = multer({
        storage: storage,
        fileFilter: fileFilter,
        limits: limits
    }).single('b_file');

    uploadImg(req, res, (err) => {
        req.checkParams('brandId', 'Please provide Brand Id parameter').notEmpty();
        req.checkBody('name', 'Please provide Brand name').notEmpty();
        let errors = req.validationErrors();
        if (errors) {
            return res.status(400).json({
                result: errors,
                msg: '',
                success: false
            });

        }

        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                return res.status(400).json({
                    result: 'LIMIT_FILE_SIZE',
                    success: false,
                    msg: 'File Size Limit Exceeded'
                });
            } else {
                return res.status(500).json({
                    result: err,
                    success: false,
                    msg: err.inner
                });
            }
        }

        if (req.fileValidationError) {
            return res.status(400).json({
                result: null,
                msg: req.fileValidationError,
                success: false
            });
        }

        req.body.toDeleteFile = false;
        if (!_.isUndefined(req.file)) {
            req.body.toDeleteFile = true;
            req.body.img_path = req.file.path;
            req.body.img_name = req.file.filename;
            req.body.img_type = req.file.mimetype;
            req.body.img_size = req.file.size;
        }
        console.log('req.body: ', req.body);
        brands.updateBrand(req.params.brandId, req.body, cb.setupResponseCallback(res));
    });
};

exports.getAllBrandProducts = (req, res) => {
    brands.getAllBrandProducts(req.params.brandId, req.query, cb.setupResponseCallback(res));
};