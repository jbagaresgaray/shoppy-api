'use strict';

let cb = require('../../../utils/callback');
let ctrl = require('../../../controllers/app/buyer_seller').BuyerSeller;
let buyer_seller = new ctrl();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../../config/environment/' + env);

let multer = require('multer');
let _ = require('lodash');
let mkdirp = require('mkdirp');

exports.approveSellerApplication = (req, res) => {
    req.checkParams('uuid', 'Please provide Buyer UUID').notEmpty();
    req.checkBody('code', 'Please provide Seller Verification Code').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        buyer_seller.approveSellerApplication(req.params.uuid, req.body, cb.setupResponseCallback(res));
    }
};

exports.getAllApplicants = (req, res) => {
    buyer_seller.getAllApplicants(cb.setupResponseCallback(res));
};

exports.getAllSellers = (req, res) => {
    buyer_seller.getAllSellers(cb.setupResponseCallback(res));
};

exports.getAllBuyerUser = (req, res) => {
    buyer_seller.getAllBuyerUser(req.query, cb.setupResponseCallback(res));
};

exports.officialShopSubmision = (req, res) => {

};

exports.rateBuyer = (req, res) => {
    buyer_seller.rateBuyer(req.params.buyerId, req.body, cb.setupResponseCallback(res));
};

exports.getBuyerRatings = (req, res) => {
    buyer_seller.getBuyerRatings(req.params.buyerId, req.query, cb.setupResponseCallback(res));
};


/**
 * This is for shop ratings from buyers perspective
 */
exports.rateShop = (req, res) => {
    buyer_seller.rateShop(req.params.userId, req.body, cb.setupResponseCallback(res));
};

exports.getShopRatings = (req, res) => {
    buyer_seller.getShopRatings(req.params.userId, cb.setupResponseCallback(res));
};
/**
 * End of shop ratings from buyers perspective
 */

exports.buyerRefund = (req, res) => {
    buyer_seller.buyerRefund(req.params.buyerId, req.params.orderId, req.params.productId, cb.setupResponseCallback(res));
};

exports.getPreferredSeller = (req, res) => {
    buyer_seller.getPreferredSeller(cb.setupResponseCallback(res));
};

exports.getPreferredSellerById = (req, res) => {
    buyer_seller.getPreferredSellerById(req.params.id, cb.setupResponseCallback(res));
};

exports.createPreferredSeller = (req, res) => {
    buyer_seller.createPreferredSeller(req.body, cb.setupResponseCallback(res));
};

exports.updatePreferredSeller = (req, res) => {
    buyer_seller.updatePreferredSeller(req.params.id, req.body, cb.setupResponseCallback(res));
};

exports.deletePreferredSeller = (req, res) => {
    buyer_seller.deletePreferredSeller(req.params.id, cb.setupResponseCallback(res));
};