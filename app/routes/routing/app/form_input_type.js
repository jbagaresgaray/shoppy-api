'use strict';

let cb = require('../../../utils/callback');
let formInputTypeCtrl = require('../../../controllers/app/form_input_type').FormInputType;
let formInputType = new formInputTypeCtrl();

exports.createFormInputType = (req, res) => {
    formInputType.createFormInputType(req.body, cb.setupResponseCallback(res));
};

exports.getAllFormInputType = (req, res) => {
    formInputType.getAllFormInputType(req.query, cb.setupResponseCallback(res));
};

exports.getFormInputType = (req, res) => {
    formInputType.getFormInputType(req.params.id, cb.setupResponseCallback(res));
};

exports.deleteFormInputType = (req, res) => {
    formInputType.deleteFormInputType(req.params.id, cb.setupResponseCallback(res));
};

exports.updateFormInputType = (req, res) => {
    formInputType.updateFormInputType(req.params.id, req.body, cb.setupResponseCallback(res));
};