'use strict';

let cb = require('../../../utils/callback');
let attributeCtrl = require('../../../controllers/app/attribute').Attribute;
let attribute = new attributeCtrl();

exports.createAttribute = (req, res) => {
    attribute.createAttribute(req.body, cb.setupResponseCallback(res));
};

exports.getAllAttribute = (req, res) => {
    attribute.getAllAttribute(req.query, cb.setupResponseCallback(res));
};

exports.getAttribute = (req, res) => {
    attribute.getAttribute(req.params.attributeId, cb.setupResponseCallback(res));
};

exports.deleteAttribute = (req, res) => {
    attribute.deleteAttribute(req.params.attributeId, cb.setupResponseCallback(res));
};

exports.updateAttribute = (req, res) => {
    attribute.updateAttribute(req.params.attributeId, req.body, cb.setupResponseCallback(res));
};