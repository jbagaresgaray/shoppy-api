'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let async = require('async');
let _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.getTrackingCheckingFlags = (next) => {
    knex
        .select("*")
        .from("order_checking_flags")
        .asCallback(next);

    /**
     * -----------------------------------------------------------------------------------------
     * THIS IS THE PREVIOUS VERSION
     * -----------------------------------------------------------------------------------------
     *
     let strSQL = mysql.format('SELECT * FROM order_checking_flags;');
     db.query(strSQL, next);
     *
     */
};

exports.getTrackingShippingTags = (next) => {
    knex
        .select("*")
        .from("order_tracking_tags")
        .asCallback(next);

    /**
     * -----------------------------------------------------------------------------------------
     * THIS IS THE PREVIOUS VERSION
     * -----------------------------------------------------------------------------------------
     *
      let strSQL = mysql.format('SELECT * FROM order_tracking_tags;');
      db.query(strSQL, next);
     *
     */
};

exports.getAllOrderTrackings = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex("order_tracking")
                .count("* as numRows")
                .innerJoin("orders", "order_tracking.orderId", "=", "orders.orderId")
                .innerJoin("shipping_courier", "shipping_courier.shippingCourierId", "=", "order_tracking.shippingCourierId")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                });

            /**
             * 
             * --------------------------------------------------------------------------------------------------
             * THIS IS THE PREVIOUS VERSION
             * --------------------------------------------------------------------------------------------------
             * 
             let strSQL = mysql.format(`
                SELECT COUNT(*) AS numRows FROM order_tracking ot 
                INNER JOIN orders o ON ot.orderId = o.orderId 
                INNER JOIN shipping_courier sc ON sc.shippingCourierId = ot.shippingCourierId;`);
             console.log('strSQL: ', strSQL);
             db.query(strSQL, function (err, results) {
                 if (err) {
                     next(err, null);
                 } else {
                     numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                     numPages = Math.ceil(numRows / numPerPage);
                     callback(null, numRows, numPages);
                 }
             });
             * 
             */
        },
        (numRows, numPages, callback) => {
            /**--------------------------------------------------------------------------------------------------
             * THIS IS THE PREVIOUS VERSION
             * --------------------------------------------------------------------------------------------------
             *
             let strSQL = mysql.format(`
                    SELECT 
                        o.batchID,
                        o.orderNum,
                        sc.name AS courier_name, 
                        sc.description AS courier_description, 
                        ot.* 
                    FROM order_tracking ot 
                        INNER JOIN orders o 
                            ON ot.orderId = o.orderId 
                        INNER JOIN shipping_courier sc 
                            ON sc.shippingCourierId = ot.shippingCourierId`);

             if (params.limit && params.startWith) {
                 strSQL = strSQL + ' LIMIT ' + limit
             } else if (params.limit) {
                 strSQL = strSQL + ' LIMIT ' + numPerPage
             }
             strSQL = strSQL + ';';
             console.log('strSQL: ', strSQL);

             db.query(strSQL, function (err, results) {
                 if (err) {
                     next(err, null);
                 } else {
                     console.log('results ', results);
                     async.eachSeries(results, (item, cb) => {
                         let strSQL2 = mysql.format('SELECT * FROM order_tracking_checkpoints WHERE pk_order_trackingId =? ORDER BY checkpoint_time DESC LIMIT 1;', [item.order_trackingId]);
                         console.log('strSQL2: ', strSQL2);
                         db.query(strSQL2, (err, checkpoints) => {
                             if (err) {
                                 return next(err, null);
                             } else {
                                 _.each(checkpoints, (row) => {
                                     delete row.pk_order_trackingId;
                                 });

                                 if (checkpoints && checkpoints.length > 0) {
                                     checkpoints = checkpoints[0];
                                 }

                                 item.checkpoint = checkpoints;
                                 cb(null, item);
                             }
                         });
                     }, () => {
                         callback(null, numRows, numPages, results);
                     });
                 }
             });
             *
             */


            /**
             * --------------------------------------------------------------------------------------------------
             * THIS IS THE OTHER IMPLEMENTATION, but i will not going to use it for now
             * --------------------------------------------------------------------------------------------------
             * knex
                 .select("*", {
                     batchID: "orders.batchID",
                     orderNum: "orders.orderNum",
                     courier_name: "shipping_courier.name",
                     courier_description: "shipping_courier.description"
                 })
                 .from("order_tracking")
                 .innerJoin("orders", "order_tracking.orderId", "=", "orders.orderId")
                 .innerJoin("shipping_courier", "shipping_courier.shippingCourierId", "=", "order_tracking.shippingCourierId")
                 .asCallback(() => {

                 });
             */
            let strSQL = `SELECT 
                        o.batchID,
                        o.orderNum,
                        sc.name AS courier_name, 
                        sc.description AS courier_description, 
                        ot.* 
                    FROM order_tracking ot 
                        INNER JOIN orders o 
                            ON ot.orderId = o.orderId 
                        INNER JOIN shipping_courier sc 
                            ON sc.shippingCourierId = ot.shippingCourierId`;

            if (params.limit && params.startWith) {
                strSQL = strSQL + ' LIMIT ' + limit
            } else if (params.limit) {
                strSQL = strSQL + ' LIMIT ' + numPerPage
            }
            strSQL = strSQL + ';';
            console.log('strSQL: ', strSQL);

            knex.raw(strSQL)
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        /**
                         * The resultset after applying knex raw query in the above inline query differs from the previous one.
                         * so instead of using results, we use results[0] to ommit the unneccessary fields.
                         *  */
                        async.eachSeries(results[0], (item, cb) => {
                            /**
                             * ----------------------------------------------------------------------------------
                             * THIS IS PREVIOUS VERSION
                             * ----------------------------------------------------------------------------------
                             *
                             let strSQL2 = mysql.format(`
                                SELECT * FROM order_tracking_checkpoints 
                                    WHERE pk_order_trackingId = ? 
                                        ORDER BY checkpoint_time
                                    DESC LIMIT 1;`, [item.order_trackingId]);

                             console.log('strSQL2: ', strSQL2);
                             db.query(strSQL2, (err, checkpoints) => {
                                 if (err) {
                                     return next(err, null);
                                 } else {
                                     _.each(checkpoints, (row) => {
                                         delete row.pk_order_trackingId;
                                     });
                                     if (checkpoints && checkpoints.length > 0) {
                                         checkpoints = checkpoints[0];
                                     }
                                     item.checkpoint = checkpoints;
                                     cb(null, item);
                                 }
                             });
                             * 
                             */

                            knex("order_tracking_checkpoints")
                                .where("pk_order_trackingId", "=", item.order_trackingId)
                                .orderBy("checkpoint_time", "des")
                                .limit(1)
                                .asCallback((err, checkpoints) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        _.each(checkpoints, (row) => {
                                            delete row.pk_order_trackingId;
                                        });
                                        if (checkpoints && checkpoints.length > 0) {
                                            checkpoints = checkpoints[0];
                                        }
                                        item.checkpoint = checkpoints;
                                        cb(null, item);
                                    }
                                });
                        }, () => {
                            callback(null, numRows, numPages, results[0]);
                        });
                    }
                })
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getOrderTrackingByOrderNum = (orderNum, next) => {
    /**
     * ----------------------------------------------------------------------------------------------------------
     * THIS IS THE PREVIOUS VERSION
     * ----------------------------------------------------------------------------------------------------------
     *
     let strSQL = mysql.format(`
        SELECT 
            o.batchID,
            o.orderNum,
            sc.name AS courier_name,
            sc.description AS courier_description,
            ot.* 
                FROM order_tracking ot 
                    INNER JOIN orders o 
                        ON ot.orderId = o.orderId 
                        INNER JOIN shipping_courier sc 
                            ON sc.shippingCourierId = ot.shippingCourierId  
                        WHERE o.orderNum = ? LIMIT 1;`, [orderNum]);
     console.log('getOrderTrackingByOrderNum: ', strSQL);
     db.query(strSQL, (err, order_tracking) => {
         if (err) {
             return next(err, null);
         } else {
             if (order_tracking && order_tracking.length > 0) {
                 order_tracking = order_tracking[0];

                 let strSQL = mysql.format('SELECT * FROM order_tracking_checkpoints WHERE pk_order_trackingId = ? ORDER BY checkpoint_time DESC;', [order_tracking.order_trackingId]);
                 console.log('strSQL: ', strSQL);
                 db.query(strSQL, (err, order_tracking_checkpoints) => {
                     if (err) {
                         return next(err, null);
                     } else {
                         order_tracking.checkpoints = order_tracking_checkpoints;
                         next(null, order_tracking);
                     }
                 });
             } else {
                 next(null, order_tracking);
             }
         }
     });
     */

    knex
        .select("*", {
            batchID: "orders.batchID",
            orderNum: "orders.orderNum",
            courier_name: "shipping_courier.name",
            courier_description: "shipping_courier.description",
        })
        .from("order_tracking")
        .innerJoin("orders", "order_tracking.orderId", "=", "orders.orderId")
        .innerJoin("shipping_courier", "shipping_courier.shippingCourierId", "=", "order_tracking.shippingCourierId")
        .where("orders.orderNum", "=", orderNum)
        .asCallback((err, order_tracking) => {
            if (err) {
                return next(err, null);
            } else {
                if (order_tracking && order_tracking.length > 0) {
                    order_tracking = order_tracking[0];

                    /**
                     * ------------------------------------------------------------------------------------------
                     * THIS IS THE PREVIOUS VERSION
                     * ------------------------------------------------------------------------------------------
                     *
                     let strSQL = mysql.format(`
                        SELECT * FROM order_tracking_checkpoints 
                            WHERE pk_order_trackingId = ? 
                            ORDER BY checkpoint_time DESC;`, [order_tracking.order_trackingId]);

                     console.log('strSQL: ', strSQL);
                     db.query(strSQL, (err, order_tracking_checkpoints) => {
                         if (err) {
                             return next(err, null);
                         } else {
                             order_tracking.checkpoints = order_tracking_checkpoints;
                             next(null, order_tracking);
                         }
                     });
                     */

                    knex("order_tracking_checkpoints")
                        .where("pk_order_trackingId", "=", order_tracking.order_trackingId)
                        .orderBy("checkpoint_time", "desc")
                        .asCallback((err, order_tracking_checkpoints) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                order_tracking.checkpoints = order_tracking_checkpoints;
                                next(null, order_tracking);
                            }
                        })
                } else {
                    next(null, order_tracking);
                }
            }
        })
};

exports.getOrderTrackingByTrackingNum = (slug, tracking_number, next) => {

    /**
     * ----------------------------------------------------------------------------------------------------------
     * THIS IS THE PREVIOUS VERSION
     * ----------------------------------------------------------------------------------------------------------
     *
     let strSQL = mysql.format(`
        SELECT 
            o.batchID,
            o.orderNum,
            sc.name AS courier_name,
            sc.description AS courier_description,
            ot.* 
        FROM order_tracking ot 
            INNER JOIN orders o
                ON ot.orderId = o.orderId 
            INNER JOIN shipping_courier sc
                ON sc.shippingCourierId = ot.shippingCourierId 
        WHERE o.tracking_number = ? AND o.slug =? LIMIT 1;`, [tracking_number, slug]);

     db.query(strSQL, (err, order_tracking) => {
         if (err) {
             return next(err, null);
         } else {
             if (order_tracking && order_tracking.length > 0) {
                 order_tracking = order_tracking[0];

                 let strSQL = mysql.format('SELECT * FROM order_tracking_checkpoints WHERE pk_order_trackingId = ? ORDER BY checkpoint_time DESC;', [order_tracking.order_trackingId]);
                 db.query(strSQL, (err, order_tracking_checkpoints) => {
                     if (err) {
                         return next(err, null);
                     } else {
                         order_tracking.checkpoints = order_tracking_checkpoints;
                         next(null, order_tracking);
                     }
                 });
             } else {
                 next(null, order_tracking);
             }
         }
     });
     */

    knex
        .select("*", {
            batchID: "orders.batchID",
            orderNum: "orders.orderNum",
            courier_name: "shipping_courier.name",
            courier_description: "shipping_courier.description"
        })
        .from("order_tracking")
        .innerJoin("orders", "order_tracking.orderId", "=", "orders.orderId")
        .innerJoin("shipping_courier", "shipping_courier.shippingCourierId", "=", "order_tracking.shippingCourierId")
        .where("order_tracking.tracking_number", "=", tracking_number)
        .andWhere("order_tracking.slug", "=", slug)
        .asCallback((err, order_tracking) => {
            if (err) {
                return next(err, null);
            } else {
                if (order_tracking && order_tracking.length > 0) {
                    order_tracking = order_tracking[0];

                    knex
                        .select('*')
                        .from('order_tracking_checkpoints')
                        .where('pk_order_trackingId', '=', order_tracking.order_trackingId)
                        .orderBy('checkpoint_time', 'desc')
                        .asCallback((err, order_tracking_checkpoints) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                order_tracking.checkpoints = order_tracking_checkpoints;
                                next(null, order_tracking);
                            }
                        })
                } else {
                    next(null, order_tracking);
                }
            }
        });
};

exports.updateOrderTracking = (order_trackingId, data, next) => {
    async.waterfall([
        (callback) => {
            /**
             * -----------------------------
             * THIS IS THE PREVIOUS VERSION
             * -----------------------------
             *
             let strSQL = mysql.format(`UPDATE order_tracking SET 
                                            tracking_number=?,
                                            signed_by=?,
                                            isPickUp=? 
                                            WHERE order_trackingId=?`,
                 [data.tracking_number, data.signed_by, data.isPickUp, order_trackingId]);

             console.log('strSQL: ', strSQL);
             db.actionQuery(strSQL, (err, response) => {
                 if (err) {
                     return next(err, null);
                 } else {
                     console.log('response : ', response);
                     callback(null, response);
                 }
             });
             */

            knex('order_tracking')
                .where('order_trackingId', '=', order_trackingId)
                .update({
                    tracking_number: data.tracking_number,
                    signed_by: data.signed_by,
                    isPickUp: data.isPickUp
                })
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        callback(null, response);
                    }
                });
        },
        (order_tracking, callback) => {
            console.log('second function..')
            let strOjb = {
                'pk_order_trackingId': order_trackingId,
                'slug': data.slug,
                'city': data.city,
                'created_at': data.created_at,
                'location': data.location,
                'country_name': data.country_name,
                'message': data.message,
                'country_iso3': data.country_iso3,
                'tag': data.tag,
                'subtag': data.subtag,
                'checkpoint_time': data.created_at,
                'state': data.state,
                'zip': data.zip,
            };

            /**
             * ------------------------------------------------------------------------------------
             * THIS IS PREVIOUS VERSION
             * ------------------------------------------------------------------------------------
             *
             let strSQL = mysql.format('INSERT INTO order_tracking_checkpoints SET ? ', strOjb);
             console.log('strSQL: ', strSQL);
             if (data.tag == 'Delivered') {
                 async.waterfall([
                     (callback1) => {
                         db.actionQuery(strSQL, (err, response) => {
                             if (err) {
                                 return next(err, null);
                             } else {
                                 callback1(null, order_tracking);
                             }
                         });
                     },
                     (order_tracking, callback1) => {
                         let paymentDeadline = new Date(data.created_at);
                         paymentDeadline.setDate(paymentDeadline.getDate() + 2);
                         let strSQL1 = mysql.format('UPDATE orders SET paymentDeadline=? WHERE orderId = (SELECT orderId FROM order_tracking WHERE order_trackingId=? LIMIT 1);', [
                             paymentDeadline, order_trackingId
                         ]);
                         db.actionQuery(strSQL1, (err, response) => {
                             if (err) {
                                 return next(err, null);
                             } else {
                                 callback1(null, order_tracking);
                             }
                         });
                     }
                 ], callback);
             } else {
                 db.actionQuery(strSQL, (err, response) => {
                     if (err) {
                         return next(err, null);
                     } else {
                         callback(null, order_tracking);
                     }
                 });
             }
             */

            if (data.tag == 'Delivered') {
                async.waterfall([
                    (callback1) => {
                        // knex('order_tracking_checkpoints')
                        //     .insert(strOjb)
                        //     .asCallback((err, response) => {
                        //         console.log('status at first callback function ', {
                        //             err,
                        //             response
                        //         });
                        //         if (err) {
                        //             return next(err, null);
                        //         } else {
                        //             callback1(null, order_tracking);
                        //         }
                        //     })
                        knex.transaction((trx) => {
                                knex('order_tracking_checkpoints')
                                    .transacting(trx).insert(strOjb)
                                    .then((resp) => {
                                        console.log('resp in transaction -->', resp)
                                        return resp;
                                    })
                                    .then(trx.commit)
                                    .catch(trx.rollback);
                            })
                            .then((resp) => {
                                console.log('Transaction complete.', resp);
                                callback1(null, order_tracking);
                            })
                            .catch((err) => {
                                console.log('err ', err);
                                return next(err, err);
                            });
                    },
                    (order_tracking, callback1) => {
                        let paymentDeadline = new Date(data.created_at);
                        paymentDeadline.setDate(paymentDeadline.getDate() + 2);
                        knex.transaction((trx) => {
                                knex('orders')
                                    .update({
                                        paymentDeadline: paymentDeadline,
                                    })
                                    .where('orderId', '=', knex.raw(`(SELECT orderId 
                                                                        FROM 
                                                                        order_tracking 
                                                                        WHERE order_trackingId=${order_trackingId} 
                                                                        or orderTrackingNumber=${order_trackingId} 
                                                                        LIMIT 1)`))
                                    .then((resp) => {
                                        return resp
                                    })
                                    .then(trx.commit)
                                    .catch(trx.rollback)
                            })
                            .then((resp) => {
                                console.log('succeessfully updated, resp is ', resp)
                                callback1(null, order_tracking);
                            })
                            .catch((err) => {
                                console.log('err ', err)
                                return next(err, null);
                            })

                        // knex('orders')
                        //     .update({
                        //         paymentDeadline: paymentDeadline,
                        //     })
                        //     .where('orderId', '=', knex.raw("(SELECT orderId FROM order_tracking WHERE order_trackingId = ? LIMIT 1 )", [order_trackingId]))
                        //     .asCallback((err, response) => {
                        //         if (err) {
                        //             return next(err, null);
                        //         } else {
                        //             callback1(null, order_tracking);
                        //         }
                        //     });
                    }
                ], callback);
            } else {
                // knex('order_tracking_checkpoints')
                //     .insert(strOjb)
                //     .asCallback((err, response) => {
                //         if (err) {
                //             return next(err, null);
                //         } else {
                //             callback(null, order_tracking);
                //         }
                //     })

                knex.transaction((trx) => {
                        knex('order_tracking_checkpoints')
                            .insert(strOjb)
                            .then((resp) => {
                                return resp
                            })
                            .then(trx.commit)
                            .catch(trx.rollback)
                    })
                    .then((resp) => {
                        callback(null, order_tracking);
                    })
                    .catch((err) => {
                        return next(err, null);
                    })
            }
        }
    ], next);
};