'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.createTags = (data, next) => {
    /**
     * ----------------------------------------------------------------------------
     * This is the previous version
     * ----------------------------------------------------------------------------
     *
     let strSQL = mysql.format('INSERT INTO tags(tags,uuid) VALUES (?,UUID());', [
         data.tags
     ]);
     db.insertWithId(strSQL, next);
     */

    knex.transaction((trx) => {
            knex('tags')
                .insert({
                    tags: data.tags,
                    uuid: knex.raw("UUID()")
                }, 'tagId')
                .then((resp) => {
                    return resp
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp && resp.length > 0 ? resp[0] : resp)
        })
        .catch((err) => {
            next(err, null)
        })
};

exports.getAllTags = (next) => {
    /**
     * ----------------------------------------------------------------------------
     * this is the previous version
     * ----------------------------------------------------------------------------
     *
     let strSQL = mysql.format('SELECT * FROM tags');
     db.query(strSQL, next);
     * 
     */

    knex
        .select('*')
        .from('tags')
        .asCallback(next);
};

exports.getTag = (tag, next) => {
    /**
     * ----------------------------------------------------------------------------
     * this is the previous version
     * ----------------------------------------------------------------------------
     *
     let strSQL = mysql.format('SELECT * FROM tags WHERE tags = ? LIMIT 1;', [tag]);
     db.query(strSQL, next);
     */

    knex
        .select('*')
        .from('tags')
        .where('tagId', tag)
        .orWhere("uuid", tag)
        .first()
        .asCallback(next);
};

exports.deleteTag = (tagId, next) => {
    /**
     * ----------------------------------------------------------------------------
     * this is the previous version
     * ----------------------------------------------------------------------------
     *
     let strSQL = mysql.format('DELETE FROM tags WHERE (tagId=? OR uuid=?);', [
         tagId, tagId
     ]);
     db.actionQuery(strSQL, next);
     */

    // knex.transaction((trx) => {
    //         knex('tags')
    //             .where('tagId', tagId)
    //             .orWhere('uuid', tagId)
    //             .del()
    //             .then((resp) => {
    //                 return resp
    //             })
    //             .then(trx.commit)
    //             .catch(trx.rollback)
    //     })
    //     .asCallback(next)

    knex.transaction(trx => {
        return trx("tags")
            .where('tagId', tagId)
            .orWhere('uuid', tagId)
            .del()
            .asCallback(next)
    })
};

exports.updateTag = (tagId, data, next) => {
    /**
     * ----------------------------------------------------------------------------
     * this is the previous version
     * ----------------------------------------------------------------------------
     *
     let strSQL = mysql.format('UPDATE tags SET tags=? WHERE (tagId=? OR uuid=?);', [
         data.tags, tagId, tagId
     ]);
     db.actionQuery(strSQL, next);
     */

    knex.transaction((trx) => {
            knex('tags')
                .update({
                    tags: data.tags,
                })
                .where('tagId', tagId)
                .orWhere('uuid', tagId)
                .then((resp) => {
                    return resp
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, null)
        })
};