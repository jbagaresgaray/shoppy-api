'use strict';

const mysql = require('mysql');
const Database = require('../../../app/utils/database').Database;
const db = new Database();

const async = require('async');
const _ = require('lodash');
const uuidv1 = require('uuid/v1');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.getAllHelpCategory = (next) => {
    // let strSQL = mysql.format('SELECT * FROM help_centre_category;');
    // db.query(strSQL, next);

    knex.select("*")
        .from("help_centre_category")
        .asCallback(next)
};

exports.createHelpPost = (user, data, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('SELECT duserId FROM dashboard_users WHERE (duserId = ? OR uuid = ?) LIMIT 1;', [user.uuid, user.uuid]);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null)
            //     } else {
            //         if (response && response.length > 0) {
            //             response = response[0];
            //         }
            //         callback(null, response);
            //     }
            // });

            knex.select("duserId").from("dashboard_users").where("duserId", user.uuid).orWhere("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null)
                    } else {
                        callback(null, response);
                    }
                })
        },
        (duser, callback) => {
            // let strSQL = mysql.format('INSERT INTO help_centre(title,content,categoryId,post_created,isPublished,createdBy,uuid) VALUES (?,?,?,NOW(),0,?,UUID())', [
            //     data.title, data.content, data.categoryId, duser.duserId
            // ]);
            // db.insertWithId(strSQL, callback);

            knex.transaction((trx) => {
                    knex("help_centre")
                        .insert({
                            title: data.title,
                            content: data.content,
                            categoryId: data.categoryId,
                            post_created: knex.raw("NOW()"),
                            isPublished: 0,
                            createdBy: duser.duserId,
                            uuid: knex.raw("UUID()")
                        })
                        .then((resp) => {
                            return resp
                        })
                        .then(trx.commit)
                        .catch(trx.rollback)
                })
                .asCallback(callback)
        }
    ], next);
};

exports.getAllHelpPosts = (next) => {
    // let strSQL = mysql.format('SELECT h.*,hc.name as `category_name` FROM help_centre h INNER JOIN help_centre_category hc ON h.categoryID = hc.categoryID;');
    // db.query(strSQL, next);

    knex.select("h.*", "hc.name as category_name")
        .from("help_centre as h")
        .innerJoin("help_centre_category as hc", "h.categoryID", "hc.categoryID")
        .asCallback(next)
};

exports.getPostByPostID = (postId, next) => {
    // let strSQL = mysql.format('SELECT h.*,hc.name as `category_name` FROM help_centre h INNER JOIN help_centre_category hc ON h.categoryID = hc.categoryID WHERE (h.postId=? OR h.uuid=?) LIMIT 1;', [postId, postId]);
    // console.log('strSQL: ', strSQL);
    // db.query(strSQL, next);

    knex.select("h.*", "hc.name as category_name")
        .from("help_centre as h")
        .innerJoin("help_centre_category as hc", "h.categoryID", "hc.categoryID")
        .where("h.postId", postId)
        .orWhere("h.uuid", postId)
        .asCallback(next)
};

exports.getAllHelpByCategory = (categoryId, next) => {
    // let strSQL = mysql.format('SELECT h.*,hc.name as `category_name` FROM help_centre h INNER JOIN help_centre_category hc ON h.categoryID = hc.categoryID WHERE (h.categoryId=?) LIMIT 1;', [categoryId]);
    // console.log('strSQL: ', strSQL);
    // db.query(strSQL, next);

    knex.select("h.*", "hc.name as category_name")
        .from("help_centre as h")
        .innerJoin("help_centre_category as hc", "h.categoryID", "hc.categoryID")
        .where("h.categoryId", categoryId)
        .asCallback(next)
};