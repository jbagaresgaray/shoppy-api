'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.getOrderDetails = (orderId, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT *,
            //     (SELECT sellerName 
            //         FROM users 
            //             WHERE userId = o.sellerId LIMIT 1) AS sellerName,
            //     (SELECT email 
            //         FROM users 
            //             WHERE userId = o.sellerId LIMIT 1) AS sellerEmail, 
            //     (SELECT username 
            //         FROM users 
            //             WHERE userId = o.userId LIMIT 1) AS buyerName,
            //     (SELECT email 
            //         FROM users 
            //             WHERE userId = o.userId LIMIT 1) AS buyerEmail, 
            //     sc.name AS courier_name,
            //     sc.description AS courier_desc,
            //     sc.slug AS courier_slug,
            //     sc.phone  AS courier_phone,
            //     sc.web_url  AS courier_web_url 
            //         FROM orders o 
            //             LEFT JOIN shipping_courier  sc 
            //                 ON o.shippingCourierId = sc.shippingCourierId 
            //             WHERE (o.orderNum = ? 
            //                 OR o.orderId=? 
            //                 OR o.uuid = ?)`, [
            //     orderId, orderId, orderId
            // ]);

            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (orders && orders.length > 0) {
            //             orders = orders[0];
            //         }
            //         callback(null, orders);
            //     }
            // });

            knex
                .select(
                    "*",
                    knex.raw(`(SELECT sellerName 
                                FROM users 
                                    WHERE userId = o.sellerId LIMIT 1) AS sellerName`),
                    knex.raw(`(SELECT email 
                                FROM users 
                                    WHERE userId = o.sellerId LIMIT 1) AS sellerEmail`),
                    knex.raw(`(SELECT username 
                                FROM users 
                                    WHERE userId = o.userId LIMIT 1) AS buyerName`),
                    knex.raw(`(SELECT email 
                                FROM users 
                                    WHERE userId = o.userId LIMIT 1) AS buyerEmail`),
                    "sc.name AS courier_name",
                    "sc.description AS courier_desc",
                    "sc.slug AS courier_slug",
                    "sc.phone  AS courier_phone",
                    "sc.web_url  AS courier_web_url"
                )
                .from("orders as o")
                .leftJoin("shipping_courier as sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("o.orderNum", "=", orderId)
                .orWhere("o.orderId", "=", orderId)
                .orWhere("o.uuid", "=", orderId)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            // let strSQL = mysql.format('SELECT od.*, p.productDesc FROM order_details od ' +
            //     'INNER JOIN products p ON od.productId = p.productId WHERE orderId = ?', [
            //         orders.orderId
            //     ]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         orders.details = response;
            //         callback(null, orders);
            //     }
            // });

            knex
                .select("od.*", "p.productDesc")
                .from("order_details as od")
                .innerJoin("products as p", "od.productId", "=", "p.productId")
                .where("orderId", "=", orders.orderId)
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        orders.details = response;
                        callback(null, orders);
                    }
                })
        }
    ], next);
};

exports.getOrderDetailsByBatchNum = (batchNum, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                             *,
            //                             (SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName,
            //                             (SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail,
            //                             (SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName, 
            //                             (SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail,
            //                             sc.name AS courier_name,
            //                             sc.description AS courier_desc,
            //                             sc.slug AS courier_slug,
            //                             sc.phone AS courier_phone,
            //                             sc.web_url AS courier_web_url
            //                                 FROM orders o 
            //                                     LEFT JOIN shipping_courier  sc 
            //                                     ON o.shippingCourierId = sc.shippingCourierId 
            //                                     WHERE o.batchId = ?`, [
            //     batchNum
            // ]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (orders && orders.length > 0) {
            //             orders = orders[0];
            //         }
            //         callback(null, orders);
            //     }
            // });

            knex
                .select(
                    "*",
                    knex.raw(`(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName`),
                    knex.raw(`(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail,`),
                    knex.raw(`(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName, `),
                    knex.raw(`(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail,`),
                    "sc.name AS courier_name",
                    "sc.description AS courier_desc",
                    "sc.slug AS courier_slug",
                    "sc.phone AS courier_phone",
                    "sc.web_url AS courier_web_url"
                )
                .from("orders as o")
                .leftJoin("shipping_courier as sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("o.batchId", "=", batchNum)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            // let strSQL = mysql.format('SELECT od.*,p.productDesc FROM order_details od ' +
            //     'INNER JOIN products p ON od.productId = p.productId WHERE orderId = ?', [
            //         orders.orderId
            //     ]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         orders.details = response;
            //         callback(null, orders);
            //     }
            // });

            knex
                .select("od.*", "p.productDesc")
                .from("order_details as od")
                .innerJoin("products as p", "od.productId", "=", "p.productId")
                .where("orderId", "=", orders.orderId)
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        orders.details = response;
                        callback(null, orders);
                    }
                })
        }
    ], next);
};

exports.getAllOrdersList = (next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT *,
            //                                 (SELECT username 
            //                                     FROM users 
            //                                         WHERE userID = o.userId LIMIT 1) AS orderBy,
            //                                 (SELECT sellerName 
            //                                     FROM users 
            //                                         WHERE userID = o.sellerId LIMIT 1) AS sellerName 
            //                                 FROM orders o;`);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });

            knex
                .select(
                    "*",
                    knex.raw(`(SELECT username FROM users WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName FROM users WHERE userID = o.sellerId LIMIT 1) AS sellerName`))
                .from("orders as o")
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format(`SELECT 
                //                             od.*,
                //                             p.productDesc 
                //                                 FROM order_details od
                //                                     INNER JOIN products p 
                //                                         ON od.productId = p.productId 
                //                                     WHERE orderId = ?`, [
                //     order.orderId
                // ]);
                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex
                    .select("od.*", "p.productDesc")
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "=", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        async.eachSeries(response, (item, cb) => {
                            // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                            // console.log('strSQL: ', strSQL);
                            // db.query(strSQL, (err, response) => {
                            //     if (err) {
                            //         next(err, null);
                            //     } else {
                            //         if (response && response.length > 0) {
                            //             let img = response[0];
                            //             item.image = img;
                            //             cb();
                            //         } else {
                            //             item.image = {};
                            //             cb();
                            //         }
                            //     }
                            // });

                            knex
                                .select("*")
                                .from("product_images")
                                .where("productId", "=", item.productId)
                                .limit(1)
                                .asCallback((err, response) => {
                                    if (response && response.length > 0) {
                                        let img = response[0];
                                        item.image = img;
                                        cb();
                                    } else {
                                        item.image = {};
                                        cb();
                                    }
                                })
                        }, () => {
                            order.details = response;
                            orderCallback(null, order);
                        });
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getPayableOrdersList = (type, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                                     *,
            //                                     (SELECT username FROM users WHERE userID = o.userId LIMIT 1) AS orderBy,
            //                                     (SELECT sellerName FROM users WHERE userID = o.sellerId LIMIT 1) AS sellerName 
            //                                     FROM orders o 
            //                                         WHERE o.isPrepared = 0 
            //                                             AND o.isShipped = 0 
            //                                             AND o.isPaid = 0 
            //                                             AND o.isReceived = 0 
            //                                             AND o.isCancelled = 0 
            //                                             AND o.isReturned = 0;`);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });

            knex
                .select(
                    "*",
                    knex.raw(`(SELECT username FROM users WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName FROM users WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                )
                .from("orders as o")
                .where("o.isPrepared", "=", "0")
                .andWhere("o.isShipped", "=", "0")
                .andWhere("o.isPaid", "=", "0")
                .andWhere("o.isReceived", "=", "0")
                .andWhere("o.isCancelled", "=", "0")
                .andWhere("o.isReturned", "=", "0") // Please be noted that in the previous code, field "isReturned" is present in this query but that field does not appear in the Official DB table
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format('SELECT od.*,p.productDesc FROM order_details od ' +
                //     'INNER JOIN products p ON od.productId = p.productId WHERE orderId = ?', [
                //         order.orderId
                //     ]);
                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex
                    .select("od.*", "p.productDesc")
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "=", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                //let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // console.log('strSQL: ', strSQL);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             cb();
                                //         } else {
                                //             item.image = {};
                                //             cb();
                                //         }
                                //     }
                                // });

                                knex
                                    .select("product_images")
                                    .where("productId", "=", item.productId)
                                    .limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                let img = response[0];
                                                item.image = img;
                                                cb();
                                            } else {
                                                item.image = {};
                                                cb();
                                            }
                                        }
                                    })
                            }, () => {
                                order.details = response;
                                orderCallback(null, order);
                            });
                        }
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getPreparedOrderList = (next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`
            //         SELECT 
            //             *,
            //             (SELECT username 
            //                 FROM users 
            //                 WHERE userID = o.userId LIMIT 1) AS orderBy,
            //             (SELECT sellerName 
            //                 FROM users 
            //                 WHERE userID = o.sellerId LIMIT 1) AS sellerName 
            //         FROM orders o 
            //             WHERE o.isPrepared = 1 
            //             AND o.isCancelled = 0;`);

            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });

            knex
                .select(
                    "*",
                    knex.raw(`(SELECT username 
                            FROM users 
                            WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName 
                            FROM users 
                            WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                )
                .from("orders as o")
                .where("o.isPrepared", "=", 1)
                .andWhere("o.isCancelled", "=", 0)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format(`
                //         SELECT 
                //             od.*,
                //             p.productDesc 
                //             FROM order_details od 
                //                 INNER JOIN products p 
                //                     ON od.productId = p.productId 
                //                 WHERE orderId = ?`, [
                //     order.orderId
                // ]);

                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex
                    .select("od.*", "p.productDesc")
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "=", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // console.log('strSQL: ', strSQL);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             cb();
                                //         } else {
                                //             item.image = {};
                                //             cb();
                                //         }
                                //     }
                                // });
                                knex
                                    .select("*")
                                    .from("product_images")
                                    .where("productId", "=", item.productId)
                                    .limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                let img = response[0];
                                                item.image = img;
                                                cb();
                                            } else {
                                                item.image = {};
                                                cb();
                                            }
                                        }
                                    })
                            }, () => {
                                order.details = response;
                                orderCallback(null, order);
                            });
                        }
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getShippedOrdersList = (action, type, next) => {
    async.waterfall([
        (callback) => {
            let sQl;
            let strSQL;
            if (action == 'ship') {
                // strSQL = mysql.format(`
                //             SELECT 
                //             *,
                //             (SELECT username 
                //                 FROM users 
                //                 WHERE userID = o.userId LIMIT 1) AS orderBy,
                //             (SELECT sellerName 
                //                 FROM users 
                //                 WHERE userID = o.sellerId LIMIT 1) AS sellerName
                //             FROM orders o 
                //                 WHERE o.isPrepared = 1 
                //                 AND o.isShipped = 0 
                //                 AND o.isCancelled = 0;`);
                sQl = knex.select(
                        "*",
                        knex.raw(`(SELECT username 
                                FROM users 
                                WHERE userID = o.userId LIMIT 1) AS orderBy`),
                        knex.raw(`(SELECT sellerName 
                                FROM users 
                                WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                    )
                    .from("orders as o")
                    .where("o.isPrepared", "=", 1)
                    .andWhere("o.isShipped", "=", 0)
                    .andWhere("o.isCancelled", "=", 0);

            } else if (action == 'shipping') {
                // strSQL = mysql.format(`
                //             SELECT 
                //                 *,
                //                 (SELECT username FROM users WHERE userID = o.userId LIMIT 1) AS orderBy,
                //                 (SELECT sellerName FROM users WHERE userID = o.sellerId LIMIT 1) AS sellerName 
                //             FROM orders o 
                //                 WHERE o.isPrepared = 1 
                //                     AND o.isShipped = 1 
                //                     AND o.isReceived = 0 
                //                     AND o.isCancelled = 0;`);
                sQl = knex.select(
                        "*",
                        knex.raw(`(SELECT username FROM users WHERE userID = o.userId LIMIT 1) AS orderBy`),
                        knex.raw(`(SELECT sellerName FROM users WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                    )
                    .from("orders as o")
                    .where("o.isPrepared", "=", 1)
                    .andWhere("o.isShipped", "=", 1)
                    .andWhere("o.isReceived", "=", 0)
                    .andWhere("isCancelled", "=", 0)
            }
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });
            sQl.asCallback((err, orders) => {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, orders);
                }
            })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format(`
                //             SELECT 
                //                 od.*,
                //                 p.productDesc 
                //                     FROM order_details od 
                //                         INNER JOIN products p 
                //                             ON od.productId = p.productId 
                //                         WHERE orderId = ?`, [
                //     order.orderId
                // ]);

                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex
                    .select("od.*", "p.productDesc")
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             cb();
                                //         } else {
                                //             item.image = {};
                                //             cb();
                                //         }
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("product_images")
                                    .where("productId", "=", item.productId)
                                    .limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                let img = response[0];
                                                item.image = img;
                                                cb();
                                            } else {
                                                item.image = {};
                                                cb();
                                            }
                                        }
                                    })
                            }, () => {
                                order.details = response;
                                orderCallback(null, order);
                            });
                        }
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getReceivableOrdersList = (next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                 *,
            //                 (SELECT username 
            //                     FROM users 
            //                     WHERE userID = o.userId LIMIT 1) AS orderBy,
            //                 (SELECT sellerName 
            //                     FROM users 
            //                     WHERE userID = o.sellerId LIMIT 1) AS sellerName
            //                 FROM orders o 
            //                     WHERE 
            //                         o.isPrepared = 1 
            //                         AND o.isShipped = 1 
            //                         AND o.isReceived = 0 
            //                         AND o.isCancelled = 0;`);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });

            knex.select(
                    "*",
                    knex.raw(`(SELECT username 
                                FROM users 
                                WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName 
                                FROM users 
                                WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                )
                .from("orders as o")
                .where("o.isPrepared", "=", 1)
                .andWhere("o.isShipped", "=", 1)
                .andWhere("o.isReceived", "=", 0)
                .andWhere("o.isCancelled", "=", 0)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format(`SELECT 
                //                                 od.*,
                //                                 p.productDesc 
                //                             FROM order_details od
                //                             INNER JOIN products p 
                //                             ON od.productId = p.productId 
                //                             WHERE orderId = ?`, [
                //     order.orderId
                // ]);

                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex
                    .select(
                        "od.*",
                        "p.productDesc")
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             cb();
                                //         } else {
                                //             item.image = {};
                                //             cb();
                                //         }
                                //     }
                                // });

                                knex.select("*").from("product_images").where("productId", "=", item.productId).limit(1).asCallback((err, response) => {
                                    if (err) {
                                        next(err, null);
                                    } else {
                                        if (response && response.length > 0) {
                                            let img = response[0];
                                            item.image = img;
                                            cb();
                                        } else {
                                            item.image = {};
                                            cb();
                                        }
                                    }
                                })
                            }, () => {
                                order.details = response;
                                orderCallback(null, order);
                            });
                        }
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getCompletedOrdersList = (type, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`
            //     SELECT 
            //         *,
            //         (SELECT username 
            //             FROM users 
            //             WHERE userID = o.userId LIMIT 1) AS orderBy,
            //         (SELECT sellerName 
            //             FROM users 
            //             WHERE userID = o.sellerId LIMIT 1) AS sellerName 
            //     FROM orders o 
            //         WHERE o.isPrepared = 1 
            //         AND o.isShipped = 1 
            //         AND o.isPaid = 1 
            //         AND o.isReceived = 1 
            //         AND o.isCancelled = 0 
            //         AND o.isReturned = 0;`);

            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });

            knex.select(
                    "*",
                    knex.raw(`(SELECT username 
                        FROM users 
                        WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName 
                        FROM users 
                        WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                )
                .from("orders as o")
                .where("o.isPrepared", "=", 1)
                .andWhere("o.isShipped", "=", 1)
                .andWhere("o.isPaid", "=", 1)
                .andWhere("o.isReceived", "=", 1)
                .andWhere("o.isCancelled", "=", 0)
                .andWhere("o.isReturned", "=", 0) //Plese be noted that the field isReturned is not present in the orders table
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                let strSQL = mysql.format('SELECT od.*,p.productDesc FROM order_details od ' +
                    'INNER JOIN products p ON od.productId = p.productId WHERE orderId = ?', [
                        order.orderId
                    ]);
                db.query(strSQL, (err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                            console.log('strSQL: ', strSQL);
                            db.query(strSQL, (err, response) => {
                                if (err) {
                                    next(err, null);
                                } else {
                                    if (response && response.length > 0) {
                                        let img = response[0];
                                        item.image = img;
                                        cb();
                                    } else {
                                        item.image = {};
                                        cb();
                                    }
                                }
                            });
                        }, () => {
                            order.details = response;
                            orderCallback(null, order);
                        });
                    }
                });
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getCancelledOrdersList = (type, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                                 *,
            //                                 (SELECT username 
            //                                     FROM users 
            //                                     WHERE userID = o.userId LIMIT 1) AS orderBy,
            //                                 (SELECT sellerName 
            //                                     FROM users 
            //                                     WHERE userID = o.sellerId LIMIT 1) AS sellerName 
            //                                 FROM orders o WHERE o.isCancelled = 1 AND o.isReturned = 0;`);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });
            knex
                .select(
                    "*",
                    knex.raw(`(SELECT username 
                                            FROM users 
                                            WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName 
                                            FROM users 
                                            WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                )
                .from("orders as o")
                .where("o.isCancelled", "=", 1)
                .andWhere("o.isReturned", "=", 0) //Please be noted that isReturned field in not present in orders table
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format(`SELECT 
                //                                 od.*,
                //                                 p.productDesc 
                //                             FROM order_details od
                //                                 INNER JOIN products p 
                //                                     ON od.productId = p.productId 
                //                                 WHERE orderId = ?`, [
                //     order.orderId
                // ]);
                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex.select("od.*", "p.productDesc")
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // console.log('strSQL: ', strSQL);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             cb();
                                //         } else {
                                //             item.image = {};
                                //             cb();
                                //         }
                                //     }
                                // });
                                knex.select("*")
                                    .from("product_images")
                                    .where("productId", "=", item.productId)
                                    .limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                let img = response[0];
                                                item.image = img;
                                                cb();
                                            } else {
                                                item.image = {};
                                                cb();
                                            }
                                        }
                                    })
                            }, () => {
                                order.details = response;
                                orderCallback(null, order);
                            });
                        }
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getReturnedOrdersList = (type, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                                 *,
            //                                 (SELECT username 
            //                                     FROM users 
            //                                         WHERE userID = o.userId LIMIT 1) AS orderBy,
            //                                 (SELECT sellerName 
            //                                     FROM users 
            //                                         WHERE userID = o.sellerId LIMIT 1) AS sellerName 
            //                             FROM orders o 
            //                                 WHERE o.isReturned = 1;`);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, orders);
            //     }
            // });
            knex.select(
                    "*",
                    knex.raw(`(SELECT username FROM users  WHERE userID = o.userId LIMIT 1) AS orderBy`),
                    knex.raw(`(SELECT sellerName FROM users WHERE userID = o.sellerId LIMIT 1) AS sellerName`)
                )
                .from("orders as o")
                .where("o.isReturned", "=", 1)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                // let strSQL = mysql.format('SELECT od.*,p.productDesc FROM order_details od ' +
                //     'INNER JOIN products p ON od.productId = p.productId WHERE orderId = ?', [
                //         order.orderId
                //     ]);
                // db.query(strSQL, (err, response) => {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         async.eachSeries(response, (item, cb) => {
                //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                //             console.log('strSQL: ', strSQL);
                //             db.query(strSQL, (err, response) => {
                //                 if (err) {
                //                     next(err, null);
                //                 } else {
                //                     if (response && response.length > 0) {
                //                         let img = response[0];
                //                         item.image = img;
                //                         cb();
                //                     } else {
                //                         item.image = {};
                //                         cb();
                //                     }
                //                 }
                //             });
                //         }, () => {
                //             order.details = response;
                //             orderCallback(null, order);
                //         });
                //     }
                // });

                knex.select(
                        "od.*",
                        "p.productDesc"
                    )
                    .from("order_details as od")
                    .innerJoin("products as p", "od.productId", "p.productId")
                    .where("orderId", "=", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             cb();
                                //         } else {
                                //             item.image = {};
                                //             cb();
                                //         }
                                //     }
                                // });

                                knex.select("*")
                                    .from("product_images")
                                    .where("productId", "=", item.productId)
                                    .limit(1)
                                    .asCallback((err_1, response_1) => {
                                        if (err_1) {
                                            next(err_1, null);
                                        } else {
                                            if (response_1 && response_1.length > 0) {
                                                let img = response_1[0];
                                                item.image = img;
                                                cb();
                                            } else {
                                                item.image = {};
                                                cb();
                                            }
                                        }
                                    })
                            }, () => {
                                order.details = response;
                                orderCallback(null, order);
                            });
                        }
                    })
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};


exports.isOrderPaid = (orderId, next) => {
    // let strSQL = mysql.format(`SELECT * FROM orders 
    //                                 WHERE isPaid = 1 
    //                                     AND paymentDateTime IS NOT NULL 
    //                                     AND (orderNum = ? OR orderID = ? OR uuid= ?);`, [
    //     orderId, orderId, orderId
    // ]);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("orders")
        .where("isPaid", "=", 1)
        .andWhereNot("paymentDateTime")
        .andWhere("orderNum", "=", orderId)
        .orWhere("orderID", "=", orderId)
        .orWhere("uuid", "=", orderId)
        .asCallback(next)
};

exports.payOrder = (orderId, data, next) => {
    // let strSQL = mysql.format('UPDATE orders SET isPaid=1,paymentDateTime=? WHERE (orderNum = ? OR orderID = ? OR uuid= ?);', [
    //     data.paymentDateTime, orderId, orderId, orderId
    // ]);
    // db.actionQuery(strSQL, next);

    knex("orders")
        .update({
            "isPaid": 1,
            "paymentDateTime": data.paymentDateTime
        })
        .where("orderNum", "=", orderId)
        .orWhere("orderID", "=", orderId)
        .orWhere("uuid", "=", orderId)
        .asCallback(next)
};

exports.shippedOrder = (orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`UPDATE orders SET isShipped=1,shippedDateTime=? WHERE (orderNum = ? OR orderID = ? OR uuid= ?);`, [
            //     data.shippedDateTime, orderId, orderId, orderId
            // ]);
            // db.actionQuery(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback();
            //     }
            // });

            knex("orders")
                .update({
                    "isShipped": 1,
                    "shippedDateTime": data.shippedDateTime
                })
                .where("orderNum", orderId)
                .orWhere("orderID", orderId)
                .orWhere("uuid", orderId)
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback();
                    }
                })
        },
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                 *,
            //                 (SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName,
            //                 (SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail,
            //                 (SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName,
            //                 (SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail
            //                     FROM orders o 
            //                         WHERE (orderNum = ? OR orderId=?)`, [orderId, orderId]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (orders && orders.length > 0) {
            //             orders = orders[0];
            //         }
            //         callback(null, orders);
            //     }
            // });

            knex.select(
                    "*",
                    knex.raw(`(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName`),
                    knex.raw(`(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail`),
                    knex.raw(`(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName`),
                    knex.raw(`(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail`)
                )
                .from("orders as o")
                .where("orderNum", orderId)
                .orWhere("orderId", orderId)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                     userId,
            //                     username,
            //                     email,
            //                     phone,
            //                     user_img_path as img_path, 
            //                     user_img_name AS img_name, 
            //                     uuid 
            //                         FROM users 
            //                             WHERE (userId = ? OR uuid=?) LIMIT 1;`, [orders.sellerId, orders.sellerId]);
            // db.query(strSQL, (err, user) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (user && user.length > 0) {
            //             user = user[0];
            //         }
            //         callback(null, user, orders);
            //     }
            // });

            knex.select(
                    "userId",
                    "username",
                    "email",
                    "phone",
                    "user_img_path as img_path",
                    "user_img_name AS img_name",
                    "uuid")
                .from("users")
                .where("userId", orders.sellerId)
                .orWhere("uuid", orders.sellerId)
                .asCallback((err, user) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (user && user.length > 0) {
                            user = user[0];
                        }
                        callback(null, user, orders);
                    }
                })
        },
        (seller, orders, callback) => {
            // let strSQL = mysql.format(`SELECT 
            //     userId,
            //     username,
            //     email,
            //     phone,
            //     user_img_path as img_path, 
            //     user_img_name AS img_name, 
            //     uuid 
            //         FROM users 
            //             WHERE (userId =? OR uuid= ?) LIMIT 1;`, [orders.userId, orders.userId]);
            // db.query(strSQL, (err, user) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (user && user.length > 0) {
            //             user = user[0];
            //         }
            //         callback(null, seller, user, orders);
            //     }
            // });

            knex.select(
                    "userId",
                    "username",
                    "email",
                    "phone",
                    "user_img_path as img_path",
                    "user_img_name AS img_name",
                    "uuid")
                .from("users")
                .where("userId", orders.userId)
                .orWhere("uuid", orders.userId)
                .limit(1)
                .asCallback((err, user) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (user && user.length > 0) {
                            user = user[0];
                        }
                        callback(null, seller, user, orders);
                    }
                })
        },
        (seller, buyer, orders, callback) => {
            let strSQL = mysql.format(`
                            SELECT 
                                od.*,
                                p.productDesc 
                                    FROM order_details od 
                                    INNER JOIN products p 
                                        ON od.productId = p.productId 
                                    WHERE orderId = ?`, [orders.orderId]);
            db.query(strSQL, (err, response) => {
                if (err) {
                    next(err, null);
                } else {
                    async.eachSeries(response, (item, cb) => {
                        // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                        // db.query(strSQL, (err, response) => {
                        //     if (err) {
                        //         next(err, null);
                        //     } else {
                        //         if (response && response.length > 0) {
                        //             let img = response[0];
                        //             item.image = img;
                        //             cb();
                        //         } else {
                        //             item.image = {};
                        //             cb();
                        //         }
                        //     }
                        // });

                        knex.select("*")
                            .from("product_images")
                            .where("productId", item.productId)
                            .limit(1)
                            .asCallback((err, response) => {
                                if (err) {
                                    next(err, null);
                                } else {
                                    if (response && response.length > 0) {
                                        let img = response[0];
                                        item.image = img;
                                        cb();
                                    } else {
                                        item.image = {};
                                        cb();
                                    }
                                }
                            })
                    }, () => {
                        orders.details = response;
                        callback(null, orders, seller, buyer);
                    });
                }
            });
        },
        (orders, seller, buyer, callback) => {
            // let sSQL = mysql.format('SELECT onesignal_player_id FROM user_devices WHERE userId = ?;', [buyer.userId]);
            // db.query(sSQL, (err, devices) => {
            //     if (err) {
            //         // callbackWater(err, null); // This is the previous code. i dont understand what's callbackWater is. :)
            //         callback(err, null);
            //     } else {
            //         _.each(devices, (row) => {
            //             storesPlayerID.push(row);
            //         });
            //         console.log('storesPlayerID: ', storesPlayerID);
            //         callback(null, orders, seller, buyer);
            //     }
            // });

            knex
                .select("onesignal_player_id")
                .from("user_devices")
                .where("userId", buyer.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        // callbackWater(err, null); // This is the previous code. i dont understand what's callbackWater is. :)
                        callback(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        console.log('storesPlayerID: ', storesPlayerID);
                        callback(null, orders, seller, buyer);
                    }
                })
        },
        (orders, seller, buyer, callback) => {
            let linkObj = {
                action: 'order_receive',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
            };
            let title = 'Shipped Out'
            let content = 'Order <b>' + orders.orderNum + '</b> has beend shipped out by <b>' + seller.username + '</b>. Kindly check ShoppyApp for more info on tracking your order.';

            // let strSQL = mysql.format('INSERT INTO notifications(title,content,datetime,link,notifTypeId) VALUES (?,?,NOW(),?,?);', [
            //     title, content, JSON.stringify(linkObj), 5
            // ]);

            // db.insertWithId(strSQL, (err, notifId) => {
            //     if (err) {
            //         return next(err, null);
            //     } else {
            //         let sSQL = mysql.format('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
            //             notifId, buyer.userId, buyer.userId
            //         ]);
            //         console.log('user_notifications sSQL: ', sSQL);
            //         db.actionQuery(sSQL, (err, resp) => {
            //             if (err) {
            //                 return next(err, null);
            //             } else {
            //                 let notification = {
            //                     notifId: notifId,
            //                     title: title,
            //                     content: content,
            //                     notification_data: linkObj,
            //                     devices: storesPlayerID
            //                 };
            //                 callback(null, {
            //                     orders: orders,
            //                     seller: seller,
            //                     buyer: buyer,
            //                     notification: notification
            //                 });
            //             }
            //         });
            //     }
            // });

            knex("notifications")
                .insert({
                    "title": title,
                    "content": content,
                    "datetime": knex.fn.now(),
                    "link": JSON.stringify(linkObj),
                    "notifTypeId": 5
                }).asCallback((err, notifId) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        let sSQL = mysql.format(`INSERT 
                                INTO user_notifications(
                                    notificationId,
                                    isRead,
                                    userId,
                                    user_player_ids,
                                    date_created,
                                    uuid
                                ) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;`, [
                            notifId, buyer.userId, buyer.userId
                        ]);

                        db.actionQuery(sSQL, (err, resp) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                let notification = {
                                    notifId: notifId,
                                    title: title,
                                    content: content,
                                    notification_data: linkObj,
                                    devices: storesPlayerID
                                };

                                callback(null, {
                                    orders: orders,
                                    seller: seller,
                                    buyer: buyer,
                                    notification: notification
                                });
                            }
                        });

                        // TO DO
                        // knex("user_notifications")
                        //     .insert(knex.raw(`select 
                        //                         ${notifId}, 
                        //                         0, 
                        //                         ${buyer.userId},
                        //                         ud.onesignal_player_id,
                        //                         ${knex.fn.now},
                        //                         ${knex.raw("UUID()")}
                        //                         FROM user_devices ud 
                        //                         WHERE ud.userId = ${buyer.userId}
                        //                         `)).asCallback((err, resp) => {
                        //         if (err) {
                        //             return next(err, null);
                        //         } else {
                        //             let notification = {
                        //                 notifId: notifId,
                        //                 title: title,
                        //                 content: content,
                        //                 notification_data: linkObj,
                        //                 devices: storesPlayerID
                        //             };

                        //             callback(null, {
                        //                 orders: orders,
                        //                 seller: seller,
                        //                 buyer: buyer,
                        //                 notification: notification
                        //             });
                        //         }
                        //     })
                    }
                })
        }
    ], next);
};

exports.orderReceivedReminder = (orderId, next) => {
    let storesPlayerID = [];

    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                 *,
            //                 (SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName,
            //                 (SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail,
            //                 (SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName, 
            //                 (SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail
            //                     FROM orders o 
            //                     WHERE (orderNum = ? OR orderId=?)`, [orderId, orderId]);
            // db.query(strSQL, (err, orders) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (orders && orders.length > 0) {
            //             orders = orders[0];
            //         }
            //         callback(null, orders);
            //     }
            // });

            knex.select(
                    "*",
                    knex.raw(`(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName`),
                    knex.raw(`(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail`),
                    knex.raw(`(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName`),
                    knex.raw(`(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail`)
                )
                .from("orders as o")
                .where("orderNum", orderId)
                .orWhere("orderId", orderId)
                .asCallback((err, orders) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        callback(null, orders);
                    }
                })
        },
        (orders, callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                 userId,
            //                 username,
            //                 email,
            //                 phone,
            //                 user_img_path as img_path, 
            //                 user_img_name AS img_name, 
            //                 uuid 
            //                 FROM users 
            //                     WHERE (userId = ? OR uuid=?) 
            //                     LIMIT 1;`, [orders.sellerId, orders.sellerId]);
            // db.query(strSQL, (err, user) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (user && user.length > 0) {
            //             user = user[0];
            //         }
            //         callback(null, user, orders);
            //     }
            // });
            knex.select(
                    "userId",
                    "username",
                    "email",
                    "phone",
                    "user_img_path as img_path",
                    "user_img_name AS img_name",
                    "uuid"
                )
                .from("users")
                .where("userId", orders.sellerId)
                .orWhere("uuid", orders.sellerId)
                .limit(1)
                .asCallback((err, user) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (user && user.length > 0) {
                            user = user[0];
                        }
                        callback(null, user, orders);
                    }
                })
        },
        (seller, orders, callback) => {
            // let strSQL = mysql.format(`SELECT 
            //         userId,
            //         username,
            //         email,
            //         phone,
            //         user_img_path as img_path, 
            //         user_img_name AS img_name, 
            //         uuid
            //             FROM users 
            //                 WHERE (userId =? OR uuid= ?) 
            //             LIMIT 1;`, [orders.userId, orders.userId]);

            // db.query(strSQL, (err, user) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (user && user.length > 0) {
            //             user = user[0];
            //         }
            //         callback(null, seller, user, orders);
            //     }
            // });

            knex.select(
                    "userId",
                    "username",
                    "email",
                    "phone",
                    "user_img_path as img_path",
                    "user_img_name AS img_name",
                    "uuid"
                )
                .from("users")
                .where("userId", orders.userId)
                .orWhere("uuid", orders.userId)
                .limit(1)
                .asCallback((err, user) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (user && user.length > 0) {
                            user = user[0];
                        }
                        callback(null, seller, user, orders);
                    }
                })

        },
        (seller, buyer, orders, callback) => {
            // let strSQL = mysql.format(`SELECT 
            //                                 od.*,
            //                                 p.productDesc 
            //                                     FROM order_details od 
            //                                 INNER JOIN 
            //                                     products p 
            //                                         ON od.productId = p.productId 
            //                                     WHERE orderId = ?`, [orders.orderId]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         async.eachSeries(response, (item, cb) => {
            //             let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
            //             db.query(strSQL, (err, response) => {
            //                 if (err) {
            //                     next(err, null);
            //                 } else {
            //                     if (response && response.length > 0) {
            //                         let img = response[0];
            //                         item.image = img;
            //                         cb();
            //                     } else {
            //                         item.image = {};
            //                         cb();
            //                     }
            //                 }
            //             });
            //         }, () => {
            //             orders.details = response;
            //             callback(null, orders, seller, buyer);
            //         });
            //     }
            // });

            knex.select(
                    "od.*",
                    "p.productDesc"
                )
                .from("order_details as od")
                .innerJoin("products as p", "od.productId", "p.productId")
                .where("orderId", orders.orderId)
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            // let strSQL = mysql.format(`SELECT 
                            //         * FROM product_images 
                            //             WHERE productId=? 
                            //             LIMIT 1;`, [item.productId]);
                            // db.query(strSQL, (err, response) => {
                            //     if (err) {
                            //         next(err, null);
                            //     } else {
                            //         if (response && response.length > 0) {
                            //             let img = response[0];
                            //             item.image = img;
                            //             cb();
                            //         } else {
                            //             item.image = {};
                            //             cb();
                            //         }
                            //     }
                            // });
                            knex.select("*")
                                .from("product_images")
                                .where("productId", item.productId)
                                .limit(1)
                                .asCallback((err, response) => {
                                    if (err) {
                                        next(err, null);
                                    } else {
                                        if (response && response.length > 0) {
                                            let img = response[0];
                                            item.image = img;
                                            cb();
                                        } else {
                                            item.image = {};
                                            cb();
                                        }
                                    }
                                })
                        }, () => {
                            orders.details = response;
                            callback(null, orders, seller, buyer);
                        });
                    }
                })
        },
        (orders, seller, buyer, callback) => {
            // let sSQL = mysql.format('SELECT onesignal_player_id FROM user_devices WHERE userId = ?;', [buyer.userId]);
            // console.log('sSQL: ', sSQL);
            // db.query(sSQL, (err, devices) => {
            //     if (err) {
            //         callbackWater(err, null);
            //     } else {
            //         _.each(devices, (row) => {
            //             storesPlayerID.push(row);
            //         });
            //         console.log('storesPlayerID: ', storesPlayerID);
            //         callback(null, orders, seller, buyer);
            //     }
            // });
            knex.select("onesignal_player_id")
                .from("user_devices")
                .where("userId", buyer.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        callbackWater(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        console.log('storesPlayerID: ', storesPlayerID);
                        callback(null, orders, seller, buyer);
                    }
                })
        },
        (orders, seller, buyer, callback) => {
            let linkObj = {
                action: 'order_receive',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
            };
            let title = 'Received Order?'
            let content = 'Your payment for order <b>' + orders.orderNum + '</b> will be transfered to the seller soon. Kindly confirm by <b>' + orders.paymentDeadline + '</b> if you received the order.';

            // let strSQL = mysql.format(`INSERT INTO 
            //         notifications(
            //             title,
            //             content,
            //             datetime,
            //             link,
            //             notifTypeId) VALUES (?,?,NOW(),?,?);`, [
            //     title, content, JSON.stringify(linkObj), 5
            // ]);

            // db.insertWithId(strSQL, (err, notifId) => {
            //     if (err) {
            //         return next(err, null);
            //     } else {
            //         let sSQL = mysql.format(`INSERT INTO 
            //         user_notifications(
            //             notificationId,
            //             isRead,
            //             userId,
            //             user_player_ids,
            //             date_created,
            //             uuid) 
            //                 SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;`, [
            //             notifId, buyer.userId, buyer.userId
            //         ]);

            //         db.actionQuery(sSQL, (err, resp) => {
            //             if (err) {
            //                 return next(err, null);
            //             } else {
            //                 let notification = {
            //                     notifId: notifId,
            //                     title: title,
            //                     content: content,
            //                     notification_data: linkObj,
            //                     devices: storesPlayerID
            //                 };

            //                 callback(null, {
            //                     orders: orders,
            //                     seller: seller,
            //                     buyer: buyer,
            //                     notification: notification
            //                 });
            //             }
            //         });
            //     }
            // })

            knex(`notifications`)
                .insert({
                    title: title,
                    content: content,
                    datetime: knex.fn.now(),
                    link: JSON.stringify(linkObj),
                    notifTypeId: 5
                }).asCallback((err, notifId) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        // let sSQL = mysql.format(`INSERT INTO 
                        // user_notifications(
                        //     notificationId,
                        //     isRead,
                        //     userId,
                        //     user_player_ids,
                        //     date_created,
                        //     uuid) 
                        //         SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;`, [
                        //     notifId, buyer.userId, buyer.userId
                        // ]);

                        // db.actionQuery(sSQL, (err, resp) => {
                        //     if (err) {
                        //         return next(err, null);
                        //     } else {
                        //         let notification = {
                        //             notifId: notifId,
                        //             title: title,
                        //             content: content,
                        //             notification_data: linkObj,
                        //             devices: storesPlayerID
                        //         };

                        //         callback(null, {
                        //             orders: orders,
                        //             seller: seller,
                        //             buyer: buyer,
                        //             notification: notification
                        //         });
                        //     }
                        // });

                        knex.raw(`INSERT INTO 
                                    user_notifications(
                                        notificationId,
                                        isRead,
                                        userId,
                                        user_player_ids,
                                        date_created,
                                        uuid) 
                                            SELECT ?, 0, ?,
                                            ud.onesignal_player_id,
                                            NOW(),
                                            UUID() 
                                            FROM user_devices ud 
                                            WHERE ud.userId = ? ;
                                            `, [notifId, buyer.userId, buyer.userId]).asCallback((err, resp) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                let notification = {
                                    notifId: notifId,
                                    title: title,
                                    content: content,
                                    notification_data: linkObj,
                                    devices: storesPlayerID
                                };

                                callback(null, {
                                    orders: orders,
                                    seller: seller,
                                    buyer: buyer,
                                    notification: notification
                                });
                            }
                        })
                    }
                })
        }
    ], next);
};

exports.autoCancelUnshippedOrders = (orderId, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                isAutoCancelled: 1,
                cancelledDateTime: knex.fn.now(),
                cancelledReason: `The order wasn 't shipped on deadline`
            })
            .from("orders")
            .where("shippingDeadline", "<=", knex.fn.now())
            .andWhere(sql => {
                sql
                    .where("orderId", orderId)
                    .orWhere("uuid", orderId)
            })
            .asCallback(next);
    });
};

exports.autoReleasePayment = (orderId, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                isReceived: 1,
                receivedDateTime: knex.fn.now()
            })
            .from("orders")
            .where("orderId", orderId)
            .orWhere("uuid", orderId)
            .asCallback(next);
    });
};