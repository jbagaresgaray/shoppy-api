'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

let async = require('async');
let _ = require('lodash');
let fs = require('fs');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkCategoryName = (categoryId, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM subcategory WHERE name = ? AND categoryId = ? LIMIT 1;', [data.name, categoryId]);
    // db.query(strSQL, next);

    knex
        .select('*')
        .from('subcategory')
        .where('name', '=', data.name)
        .andWhere('categoryId', '=', categoryId)
        .limit(1)
        .asCallback(next)
};

exports.checkCategoryForUpdate = (categoryId, _id, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM subcategory WHERE name = ? AND categoryId = ? AND subcategoryId <> ? LIMIT 1;', [data.name, categoryId, _id]);
    // db.query(strSQL, next);

    knex.select("*").from("subcategory").where("name", data.name).andWhere("categoryId", categoryId).andWhere("subcategoryId", _id)
        .asCallback(next)
};

exports.createCategory = (data, next) => {
    // let strSQL = mysql.format(`INSERT INTO subcategory(
    //         name,
    //         img_path,
    //         img_name,
    //         img_type,
    //         img_size,
    //         categoryId,
    //         uuid) VALUES (?,?,?,?,?,?,UUID());`, [
    //     data.name, data.img_path, data.img_name, data.img_type, data.img_size, data.categoryId
    // ]);
    // db.insertWithId(strSQL, next);

    knex('subcategory')
        .insert({
            name: data.name,
            img_path: data.img_path,
            img_name: data.img_name,
            img_type: data.img_type,
            img_size: data.img_size,
            categoryId: data.categoryId
        }, 'subcategoryId')
        .asCallback((err, resp) => {
            if (err) {
                next(err, null);
            } else {
                next(err, resp[0]);
            }
        })
};

exports.getAllCatetories = (categoryId, params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('SELECT COUNT(*) AS numRows FROM subcategory WHERE categoryId=?;', [categoryId]);
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });

            knex('subcategory')
                .count('* AS numRows')
                .where('categoryId', categoryId)
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                })
        },
        (numRows, numPages, callback) => {
            // let strSQL = mysql.format('SELECT * FROM subcategory WHERE categoryId = ?;', [categoryId]);
            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' LIMIT ' + limit
            // } else if (params.limit) {
            //     strSQL = strSQL + ' LIMIT ' + numPerPage
            // }
            // strSQL = strSQL + ';';

            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, numRows, numPages, results);
            //     }
            // });

            let sQl = knex.select('*').from('subcategory').where('categoryId', '=', categoryId);
            if (params.limit && params.startWith) {
                sQl.limit(limit)
            } else if (params.limit) {
                sQl.limit(numPerPage)
            }

            sQl.asCallback((err, results) => {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, numRows, numPages, results);
                }
            })
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getCategory = (categoryId, _id, next) => {
    // let strSQL = mysql.format('SELECT * FROM subcategory WHERE (subcategoryId=? OR uuid=?) AND categoryId =? LIMIT 1;', [_id, _id, categoryId]);
    // db.query(strSQL, next);

    knex
        .select('*')
        .from('subcategory')
        .where('subcategoryId', _id)
        .orWhere('uuid', _id)
        .andWhere('categoryId', categoryId)
        .asCallback(next)
};

exports.deleteCategory = (categoryId, _id, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            // let sql1 = mysql.format(`SELECT 
            //     img_path,
            //     img_name,
            //     img_type,
            //     img_size 
            //         FROM subcategory 
            //         WHERE (subcategoryId=? OR uuid=?)  
            //         LIMIT 1;`, [_id, _id]);
            // console.log('sql1: ', sql1);

            // db.query(sql1, function (err, res) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (res && res.length > 0) {
            //             res = res[0];
            //             if (res) {
            //                 dbx.filesDelete({
            //                     path: res.img_path
            //                 }).then((resp) => {
            //                     console.log('Delete CategoryImage: ', resp);
            //                     callback();
            //                 }).catch((error) => {
            //                     console.error('DropBox error 4: ', error);
            //                     callback();
            //                 });
            //             } else {
            //                 callback();
            //             }
            //         } else {
            //             callback();
            //         }
            //     }
            // });

            knex
                .select('img_path', 'img_name', 'img_type', 'img_size')
                .from('subcategory')
                .where('subcategoryId', _id)
                .orWhere('uuid', _id)
                .limit(1)
                .asCallback((err, res) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (res && res.length > 0) {
                            res = res[0];
                            if (res) {
                                dbx.filesDelete({
                                    path: res.img_path
                                }).then((resp) => {
                                    console.log('Delete CategoryImage: ', resp);
                                    callback();
                                }).catch((error) => {
                                    console.error('DropBox error 4: ', error);
                                    callback();
                                });
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    }
                })
        },
        (callback) => {
            // let strSQL = mysql.format('DELETE FROM subcategory WHERE (subcategoryId=? OR uuid=?);', [_id, _id]);
            // console.log('strSQL: ', strSQL);
            // db.actionQuery(strSQL, function (err, res) {
            //     if (err) {
            //         callback(err, null);
            //     } else {
            //         callback(null, res);
            //     }
            // });

            // knex('subcategory')
            //     .where('subcategoryId', _id)
            //     .orWhere('uuid', _id)
            //     .delete()
            //     .asCallback((err, res) => {
            //         if (err) {
            //             callback(err, null);
            //         } else {
            //             callback(null, res);
            //         }
            //     })

            knex.transaction(trx => {
                return trx('subcategory')
                    .where('subcategoryId', _id)
                    .orWhere('uuid', _id)
                    .delete()
                    .asCallback(callback)
            })
        }
    ], next);
};

exports.updateCategory = (categoryId, _id, data, next) => {
    if (data.toDeleteFile) {
        async.waterfall([
            (callback) => {
                // let sql1 = mysql.format('SELECT img_path,img_name,img_type,img_size FROM subcategory WHERE (subcategoryId=? OR uuid=?) LIMIT 1;', [_id, _id]);
                // console.log('sql1: ', sql1);

                // db.query(sql1, function (err, res) {
                //     if (err) {
                //         next(err, null);
                //     }
                //     if (res && res.length > 0) {
                //         res = res[0];
                //         if (res) {
                //             if (func.existsSync(res.img_path)) {
                //                 fs.unlinkSync(res.img_path);
                //                 console.log('unlink');
                //                 callback(null, {
                //                     img_name: data.img_name,
                //                     img_path: data.img_path,
                //                     img_type: data.img_type,
                //                     img_size: data.img_size
                //                 })
                //             } else {
                //                 callback(null, {
                //                     img_name: data.img_name,
                //                     img_path: data.img_path,
                //                     img_type: data.img_type,
                //                     img_size: data.img_size
                //                 });
                //             }
                //         } else {
                //             callback(null, {
                //                 img_name: data.img_name,
                //                 img_path: data.img_path,
                //                 img_type: data.img_type,
                //                 img_size: data.img_size
                //             });
                //         }
                //     } else {
                //         callback(null, {
                //             img_name: data.img_name,
                //             img_path: data.img_path,
                //             img_type: data.img_type,
                //             img_size: data.img_size
                //         });
                //     }
                // });

                knex
                    .select('img_path', 'img_name', 'img_type', 'img_size')
                    .from('subcategory')
                    .where('subcategoryId', _id)
                    .orWhere('uuid', _id)
                    .asCallback((err, res) => {
                        if (err) {
                            next(err, null);
                        } else {
                            if (res && res.length > 0) {
                                res = res[0];
                                if (res) {
                                    if (func.existsSync(res.img_path)) {
                                        fs.unlinkSync(res.img_path);
                                        console.log('unlink');
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        })
                                    } else {
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        });
                                    }
                                } else {
                                    callback(null, {
                                        img_name: data.img_name,
                                        img_path: data.img_path,
                                        img_type: data.img_type,
                                        img_size: data.img_size
                                    });
                                }
                            } else {
                                callback(null, {
                                    img_name: data.img_name,
                                    img_path: data.img_path,
                                    img_type: data.img_type,
                                    img_size: data.img_size
                                });
                            }
                        }
                    })
            },
            (result, callback) => {
                let app = {
                    img_name: result.img_name,
                    img_path: result.img_path,
                    img_type: result.img_type,
                    img_size: result.img_size
                }
                // let strSQL = mysql.format(`UPDATE subcategory 
                //         SET name=?,
                //             img_path=?,
                //             img_name=?,
                //             img_type=?,
                //             img_size=?,
                //             categoryId=? 
                //         WHERE (subcategoryId=? OR uuid=?);`, [
                //     data.name, app.img_path, app.img_name, app.img_type, app.img_size, categoryId, _id, _id
                // ]);
                // console.log('strSQL: ', strSQL);
                // db.actionQuery(strSQL, function (err, res) {
                //     if (err) {
                //         callback(err, null);
                //     } else {
                //         callback(null, res);
                //     }
                // });

                knex('subcategory')
                    .update({
                        name: data.name,
                        img_path: app.img_path,
                        img_name: app.img_name,
                        img_type: app.img_type,
                        img_size: app.img_size,
                        categoryId: categoryId
                    })
                    .where('subcategoryId', _id)
                    .orWhere('uuid', _id)
                    .asCallback((err, res) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, res);
                        }
                    })
            }
        ], next);
    } else {
        // let strSQL = mysql.format('UPDATE subcategory SET name=?, categoryId=? WHERE (subcategoryId=? OR uuid=?);', [
        //     data.name, categoryId, _id, _id
        // ]);
        // db.actionQuery(strSQL, next);

        knex('subcategory')
            .update({
                name: data.name,
                categoryId: categoryId
            })
            .where('subcategoryId', _id)
            .orWhere('uuid', _id)
            .asCallback(next)
    }
};

exports.getAllSubCategoryProducts = (categoryId, _id, params, next) => {
    console.log('params: ', params);
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format(`SELECT COUNT(*) AS numRows 
            //                                 FROM products p 
            //                                 LEFT JOIN product_rating pr 
            //                                 ON p.productId = pr.productId 
            //                                 WHERE productCategoryID =? 
            //                                 AND productSubCategoryId = ?;`, [categoryId, _id]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });

            knex
                .count("* AS numRows")
                .from("products as p")
                .leftJoin("product_rating as pr", "p.productId", "pr.productId")
                .where("p.productCategoryID", categoryId)
                .andWhere("p.productSubCategoryId", _id)
                .asCallback((err, results) => {
                    if (err) {
                        console.log({err})
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                });
        },
        (numRows, numPages, callback) => {
            // let strSQL = mysql.format(`SELECT
            //                              p.*, IF(pr.rating,AVG(pr.rating),0)  AS rating,
            //                              (SELECT COUNT(o.orderId) 
            //                                 FROM  order_details o 
            //                                     WHERE p.productId = o.productId) AS orderscount 
            //                                         FROM products p 
            //                                     LEFT JOIN product_rating pr 
            //                                         ON p.productId = pr.productId 
            //                                     WHERE productCategoryID=?  
            //                                         AND productSubCategoryId = ? `, [categoryId, _id]);

            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' GROUP BY p.productId LIMIT ' + limit
            //     console.log('strSQL1: ', strSQL);
            // } else if (params.limit) {
            //     strSQL = strSQL + ' GROUP BY p.productId LIMIT ' + numPerPage
            //     console.log('strSQL2: ', strSQL);
            // } else {
            //     strSQL = strSQL + ' GROUP BY p.productId LIMIT ' + numPerPage
            // }

            // strSQL = strSQL + ';';
            // console.log('strSQL: ', strSQL);

            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         async.eachSeries(results, (item, cb) => {
            //             async.waterfall([
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM categories WHERE categoryId =? LIMIT 1;', [item.productCategoryID]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         if (response && response.length > 0) {
            //                             item.category = response[0];
            //                         } else {
            //                             item.category = {};
            //                         }
            //                         callback1();
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM subcategory WHERE subcategoryId = ?;', [item.productSubCategoryId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         if (response && response.length > 0) {
            //                             item.subcategory = response[0];
            //                         } else {
            //                             item.subcategory = {};
            //                         }
            //                         callback1();
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM brand WHERE brandId =?;', [item.productBrandId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         if (response && response.length > 0) {
            //                             item.brand = response[0];
            //                         } else {
            //                             item.brand = {};
            //                         }
            //                         callback1();
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM product_variation WHERE productId=?;', [item.productId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         item.variants = response;
            //                         callback1();
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM product_wholesale WHERE productId=?;', [item.productId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         item.wholesale = response;
            //                         callback1();
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT pse.*,sc.name FROM product_shipping_fee pse INNER JOIN shipping_courier sc ON pse.shippingCourierId = sc.shippingCourierId WHERE pse.productId=?;', [item.productId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         item.shippingFee = response;
            //                         callback1();
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
            //                     console.log('strSQL: ', strSQL);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         }
            //                         if (response && response.length > 0) {
            //                             let img = response[0];
            //                             item.image = img;
            //                             callback1();
            //                         } else {
            //                             item.image = {};
            //                             callback1();
            //                         }
            //                     });
            //                 }
            //             ], cb);
            //         }, () => {
            //             callback(null, numRows, numPages, results);
            //         });
            //     }
            // });


            let sQl = knex.select(
                        "p.*", 
                        knex.raw("(IF(pr.rating,AVG(pr.rating),0) )  AS rating"), 
                        knex.raw("(SELECT COUNT(o.orderId) from order_details as o where p.productId = o.productId) as orderscount")
                )
                .from("products as p")
                .leftJoin("product_rating as pr", "p.productId", "pr.productId")
                .where("p.productCategoryID", categoryId)
                .andWhere("p.productSubCategoryId", _id);


            if (params.limit && params.startWith) {
                sQl.groupBy("p.productId")
                    .limit(limit);
            } else if (params.limit) {
                sQl.groupBy("p.productId")
                    .limit(numPerPage);
            } else {
                sQl.groupBy("p.productId")
                    .limit(numPerPage);
            }


            sQl.asCallback((err, results) => {
                if (err) {
                    console.log({err})
                    next(err, null);
                } else {
                    async.eachSeries(results, (item, cb) => {
                        async.waterfall([
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format('SELECT * FROM categories WHERE categoryId =? LIMIT 1;', [item.productCategoryID]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             item.category = response[0];
                                //         } else {
                                //             item.category = {};
                                //         }
                                //         callback1();
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("categories")
                                    .where("categoryId", item.productCategoryID)
                                    .limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                item.category = response[0];
                                            } else {
                                                item.category = {};
                                            }
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format('SELECT * FROM subcategory WHERE subcategoryId = ?;', [item.productSubCategoryId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             item.subcategory = response[0];
                                //         } else {
                                //             item.subcategory = {};
                                //         }
                                //         callback1();
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("subcategory")
                                    .where("subcategoryId", item.productSubCategoryId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                item.subcategory = response[0];
                                            } else {
                                                item.subcategory = {};
                                            }
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format('SELECT * FROM brand WHERE brandId =?;', [item.productBrandId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             item.brand = response[0];
                                //         } else {
                                //             item.brand = {};
                                //         }
                                //         callback1();
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("brand")
                                    .where("brandId", item.productBrandId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                item.brand = response[0];
                                            } else {
                                                item.brand = {};
                                            }
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format('SELECT * FROM product_variation WHERE productId=?;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         item.variants = response;
                                //         callback1();
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("product_variation")
                                    .where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            item.variants = response;
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format('SELECT * FROM product_wholesale WHERE productId=?;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         item.wholesale = response;
                                //         callback1();
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("product_wholesale")
                                    .where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            item.wholesale = response;
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format(`SELECT 
                                //     pse.*,
                                //     sc.name 
                                //         FROM product_shipping_fee pse 
                                //         INNER JOIN shipping_courier sc 
                                //             ON pse.shippingCourierId = sc.shippingCourierId 
                                //         WHERE pse.productId=?;`, [item.productId]);

                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         item.shippingFee = response;
                                //         callback1();
                                //     }
                                // });

                                knex
                                    .select("pse.*", "sc.name")
                                    .from("product_shipping_fee as pse")
                                    .innerJoin("shipping_courier as sc", "pse.shippingCourierId", "sc.shippingCourierId")
                                    .where("pse.productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            item.shippingFee = response;
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // This is the old version
                                // let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                // console.log('strSQL: ', strSQL);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             callback1();
                                //         } else {
                                //             item.image = {};
                                //             callback1();
                                //         }
                                //     }
                                // });

                                knex
                                    .select("*")
                                    .from("product_images")
                                    .where("productId", item.productId)
                                    .limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                let img = response[0];
                                                item.image = img;
                                                callback1();
                                            } else {
                                                item.image = {};
                                                callback1();
                                            }
                                        }
                                    })
                            }
                        ], cb);
                    }, () => {
                        callback(null, numRows, numPages, results);
                    });
                }
            })
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};