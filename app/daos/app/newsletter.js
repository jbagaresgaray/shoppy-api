'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const fs = require('fs');
const async = require('async');
const _ = require('lodash');

var knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.createNewsletter = (user, data, next) => {
    if (_.isArray(data.tags) && !_.isEmpty(data.tags)) {
        data.tags = _.map(data.tags, (row) => {
            return row.text
        }).join(',');
    }
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('SELECT duserId FROM dashboard_users WHERE (duserId <> ? OR uuid = ?) LIMIT 1;', [user.uuid, user.uuid]);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         console.log('response ', response)
            //         if (response && response.length > 0) {
            //             response = response[0];
            //         }
            //         callback(null, response)
            //     }
            // });

            knex
                .select("duserId")
                .from("dashboard_users")
                .where("duserId", "<>", user.uuid)
                .orWhere("uuid", user.uuid)
                .first()
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, response);
                    }
                })
        },
        (duser, callback) => {
            // let strSQL = mysql.format(`INSERT INTO newsletter(title,
            //                                                     summary,
            //                                                     content,
            //                                                     date_created,
            //                                                     created_by,
            //                                                     tags,
            //                                                     type,
            //                                                     isPublished,
            //                                                     uuid) VALUES (?,?,?,NOW(),?,?,?,0,UUID())`, [
            //     data.title, data.summary, data.content, duser.duserId, data.tags, data.type
            // ]);
            // console.log('strSQL: ', strSQL);
            // db.insertWithId(strSQL, (err, result) => {
            //     console.log('result ', result);
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, result)
            //     }
            // });

            knex.transaction((trx) => {
                    knex("newsletter")
                        .insert({
                            title: data.title,
                            summary: data.summary,
                            content: data.content,
                            date_created: knex.fn.now(),
                            created_by: duser.duserId,
                            tags: data['tags'], // i used this style of object value difinition instead of 'data.tags' to prevent issues with builtin nodejs tags variable
                            type: data.type,
                            isPublished: 0,
                            uuid: knex.raw("UUID()")
                        }, "cmsId")
                        .then((resp) => {
                            return resp
                        })
                        .then(trx.commit)
                        .catch(trx.rollback)
                })
                .then((resp) => {
                    callback(null, resp.length > 0 ? resp[0] : resp)
                })
                .catch((err) => {
                    next(err, err)
                })
        }
    ], next);
};

exports.updateNewsletter = (cmsId, user, data, next) => {
    console.log('data ', data)
    if (_.isArray(data.tags) && !_.isEmpty(data.tags)) {
        data.tags = _.map(data.tags, (row) => {
            return row.text
        }).join(',');
    }

    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('SELECT duserId FROM dashboard_users WHERE (duserId <> ? OR uuid = ?) LIMIT 1;', [user_id, user_id]);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     if (response && response.length > 0) {
            //         response = response[0];
            //     }
            //     callback(null, response)
            // });
            knex
                .select("duserId")
                .from("dashboard_users")
                .where("duserId", "<>", user.uuid)
                .orWhere("uuid", user.uuid)
                .first()
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, response);
                    }
                })
        },
        (duser, callback) => {
            // let strSQL = mysql.format('UPDATE newsletter SET title=?,summary=?,content=?,tags=?,type=? WHERE (cmsId = ? OR uuid=?);', [
            //     data.title, data.summary, data.content, data.tags, data.type, cmsId, cmsId
            // ]);
            // console.log('strSQL: ', strSQL);
            // db.actionQuery(strSQL, (err, result) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     callback(null, result)
            // });

            knex.transaction((trx) => {
                    knex("newsletter")
                        .update({
                            title: data.title,
                            summary: data.summary,
                            content: data.content,
                            tags: data.tags,
                            type: data.type
                        })
                        .where("cmsId", cmsId)
                        .orWhere("uuid", cmsId)
                        .then((resp) => {
                            return resp
                        })
                        .then(trx.commit)
                        .catch(trx.rollback)
                })
                .then((resp) => {
                    callback(null, resp);
                })
                .catch((err) => {
                    next(err, err)
                })
        }
    ], next);
};

exports.getNewsLetter = (cmsId, next) => {
    // let strSQL = mysql.format('SELECT * FROM newsletter WHERE (cmsId = ? OR uuid=?) LIMIT 1;', [cmsId, cmsId]);
    // db.query(strSQL, next);
    knex
        .select("*")
        .from("newsletter")
        .where("cmsId", cmsId)
        .orWhere("uuid", cmsId)
        .asCallback(next);
};

exports.getAllNewsletter = (next) => {
    // let strSQL = mysql.format('SELECT * FROM newsletter;');
    // db.query(strSQL, next);
    knex.select("*").from("newsletter").asCallback(next);
};

exports.deleteNewsletter = (cmsId, next) => {
    // let strSQL = mysql.format('DELETE FROM newsletter WHERE (cmsId=? OR uuid=?);', [cmsId, cmsId]);
    // db.actionQuery(strSQL, next);
    knex.transaction(trx => {
        return trx("newsletter")
            .where("cmsId", cmsId)
            .orWhere("uuid", cmsId)
            .delete()
            .asCallback(next)
    })
    // knex.transaction((trx) => {
    //         knex("newsletter")
    //             .where("cmsId", cmsId)
    //             .orWhere("uuid", cmsId)
    //             .delete()
    //             .then((resp) => {
    //                 return resp
    //             })
    //             .then(trx.commit)
    //             .catch(trx.rollback)
    //     })
    //     .then((resp) => {
    //         next(null, resp)
    //     })
    //     .catch((err) => {
    //         next(err, err)
    //     })
};

exports.isPublished = (cmsId, next) => {
    // let strSQL = mysql.format('SELECT * FROM newsletter WHERE cmsId=? AND isPublished = 1 AND date_published IS NOT NULL LIMIT 1;', [cmsId]);
    // db.query(strSQL, next);
    knex.select("*")
        .from("newsletter")
        .where("cmsId", cmsId)
        .andWhere("isPublished", 1)
        .andWhereRaw("date_published IS NOT NULL")
        .limit(1)
        .asCallback(next)
};

exports.publishedNewsletter = (cmsId, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('UPDATE newsletter SET isPublished = 1,date_published=NOW() WHERE (cmsId=? OR uuid=?);', [cmsId, cmsId]);
            // console.log('strSQL: ', strSQL);
            // db.actionQuery(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     callback();
            // });
            knex("newsletter")
                .update({
                    isPublished: 1,
                    date_published: knex.raw("NOW()")
                })
                .where("cmsId", cmsId)
                .orWhere("uuid", cmsId)
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback();
                    }
                })
        },
        (callback) => {
            // let strSQL = mysql.format('SELECT * FROM newsletter WHERE (cmsId=? OR uuid=?) LIMIT 1;', [cmsId, cmsId]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     }
            //     if (response && response.length > 0) {
            //         response = response[0];
            //     }
            //     callback(null, response);
            // });
            knex.select("*")
                .from("newsletter")
                .where("cmsId", cmsId)
                .orWhere("uuid", cmsId)
                .limit(1)
                .first()
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, response);
                    }
                })
        }
    ], next);
};

exports.unpublishNewsletter = (cmsId, next) => {
    // let strSQL = mysql.format('UPDATE newsletter SET isPublished = 0 WHERE (cmsId=? OR uuid=?);', [cmsId, cmsId]);
    // db.actionQuery(strSQL, next);

    knex("newsletter")
        .update({
            isPublished: 0
        })
        .where("cmsId", cmsId)
        .orWhere("uuid", cmsId)
        .asCallback(next);
};

exports.notifyNewsletter = (cmsId, next) => {
    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('SELECT * FROM newsletter WHERE (cmsId=? OR uuid=?) LIMIT 1;', [cmsId, cmsId]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (response && response.length > 0) {
            //             response = response[0];
            //         }
            //         callback(null, response);
            //     }
            // });

            knex
                .select("*")
                .from("newsletter")
                .where("cmsId", cmsId)
                .orWhere("uuid", cmsId)
                .limit(1)
                .first()
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, response);
                    }
                })
        },
        (newsletter, callback) => {
            // db.query(mysql.format('SELECT email FROM users WHERE isVerify = 1;'), (err, response) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, newsletter, response);
            //     }
            // });
            knex
                .select('email')
                .from("users")
                .where("isVerify", 1)
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        callback(null, newsletter, response);
                    }
                })
        },
        (newsletter, users, callback) => {
            let linkObj = {
                action: 'newsletter',
                Id: cmsId
            };
            // let strSQL = mysql.format('INSERT INTO notifications(title,content,datetime,link,notifTypeId) VALUES (?,?,NOW(),?,?);', [
            //     newsletter.title, newsletter.summary, JSON.stringify(linkObj), newsletter.type
            // ]);
            // console.log('strSQL: ', strSQL);
            // db.insertWithId(strSQL, (err, notifId) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         console.log('insert of notif: ', notifId);
            //         let sSQL = mysql.format(`INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,ud.userId,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud;`, [
            //             notifId
            //         ]);
            //         console.log('sSQL: ', sSQL);
            //         db.actionQuery(sSQL, (err, resp) => {
            //             callback(null, newsletter, users, linkObj, notifId);
            //         });
            //     }
            // });

            knex("notifications")
                .insert({
                    title: newsletter.title,
                    content: newsletter.summary,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: newsletter.type
                })
                .asCallback((err, notifId) => {
                    if (err) {
                        next(err, null);
                    } else {
                        // console.log('insert of notif: ', notifId);
                        // let sSQL = mysql.format(`INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,ud.userId,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud;`, [
                        //     notifId
                        // ]);
                        // console.log('sSQL: ', sSQL);
                        // db.actionQuery(sSQL, (err, resp) => {
                        //     callback(null, newsletter, users, linkObj, notifId);
                        // });

                        knex.raw(`INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,ud.userId,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud;`)
                            .asCallback((err, resp) => {
                                callback(null, newsletter, users, linkObj, notifId);
                            })
                    }
                })
        },
        (newsletter, users, linkObj, notifId, callback) => {
            // let strSQL = mysql.format('SELECT * FROM notifications WHERE notificationId=? LIMIT 1;', [notifId]);
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, (err, notifications) => {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (notifications && notifications.length > 0) {
            //             notifications = notifications[0];

            //             let strSQL1 = mysql.format('SELECT userId,user_player_ids FROM user_notifications WHERE notificationId=?', [notifId]);
            //             console.log('strSQL1: ', strSQL1);
            //             db.query(strSQL1, (err, user_notifications) => {
            //                 if (err) {
            //                     return next(err, null);
            //                 } else {
            //                     notifications.user_notifications = user_notifications;
            //                     notifications.notification_data = linkObj;

            //                     callback(null, {
            //                         notifications: notifications,
            //                         newsletter: newsletter,
            //                         users: users
            //                     });
            //                 }
            //             });
            //         }
            //     }
            // });

            knex
                .select("*")
                .from("notifications")
                .where("notificationId", notifId)
                .limit(1)
                .asCallback((err, notifications) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (notifications && notifications.length > 0) {
                            notifications = notifications[0];

                            // let strSQL1 = mysql.format('SELECT userId,user_player_ids FROM user_notifications WHERE notificationId=?', [notifId]);
                            // console.log('strSQL1: ', strSQL1);
                            // db.query(strSQL1, (err, user_notifications) => {
                            //     if (err) {
                            //         return next(err, null);
                            //     } else {
                            //         notifications.user_notifications = user_notifications;
                            //         notifications.notification_data = linkObj;

                            //         callback(null, {
                            //             notifications: notifications,
                            //             newsletter: newsletter,
                            //             users: users
                            //         });
                            //     }
                            // });

                            knex.select("userId", "user_player_ids")
                                .from("user_notifications")
                                .where("notificationId", notifId)
                                .asCallback((err, user_notifications) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        notifications.user_notifications = user_notifications;
                                        notifications.notification_data = linkObj;

                                        callback(null, {
                                            notifications: notifications,
                                            newsletter: newsletter,
                                            users: users
                                        });
                                    }
                                })
                        }
                    }
                })
        }
    ], next);
};


exports.getAllNotificationType = (next) => {
    // let strSQL = mysql.format('SELECT  * FROM notificationtype;');
    // db.query(strSQL, next);

    knex.select("*").from("notificationtype").asCallback(next);
};