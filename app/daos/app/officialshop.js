"use strict";

let mysql = require('mysql');
let Database = require("../../../app/utils/database").Database;
let db = new Database();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);
let constants = require('../../../config/constants');

let async = require("async");
let _ = require("lodash");

var knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.validateSellerCode = (data, next) => {
    knex.select('os.official_shop_id', 'os.shop_application_date', 'os.shop_ref_code', 'os.shop_url_code', 'os.shop_url_token', 'os.userId')
        .table("official_shop as os")
        .innerJoin("users as u", "u.userId", "=", "os.userId")
        .where({
            shop_ref_code: data.code,
            username: data.username
        })
        .first()
        .asCallback(next);
};

exports.validateSellerCodePromise = data => {
    // Promise
    return knex.select().table("official_shop");
};

exports.getApplicationInfo = (data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("os.*", "u.username", "u.sellerName", "u.sellerDescription")
                .table("official_shop as os")
                .innerJoin("users as u", "u.userId", "=", "os.userId")
                .where({
                    shop_url_code: data.code,
                    shop_url_token: data.token
                })
                .first()
                .then((result) => {
                    callback(null, result);
                }, (error) => {
                    return next(error, null);
                })
        },
        (shop, callback) => {
            knex.select('*').from('official_shop_evaluation').where({
                    official_shop_id: shop.official_shop_id
                }).orderBy('date', 'desc')
                .then((evaluation) => {
                    if (!_.isEmpty(evaluation)) {
                        shop.evaluation = evaluation[0];
                    } else {
                        shop.evaluation = {};
                    }
                    shop.activity = evaluation;
                    callback(null, shop);
                }, (error) => {
                    return next(error, null);
                });
        }
    ], next);
};

exports.saveApplicationInfo = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_name: data.shop_name,
                owner_fname: data.owner_fname,
                owner_mname: data.owner_mname,
                owner_lname: data.owner_lname,
                owner_email: data.owner_email,
                shop_email: data.shop_email,
                shop_mobileno: data.shop_mobileno,
                shop_website: data.shop_website,
                shop_instagram: data.shop_instagram,
                shop_facebook: data.shop_facebook,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.code,
                userId: data.userId
            })
    }).debug(true).asCallback(next);
};

exports.deleteCORFile = (data, next) => {
    console.log('deleteCORFile 3');
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    knex.select('shop_cor_file AS img_path', 'shop_cor_filename AS img_name')
        .table('official_shop')
        .where({
            shop_url_code: data.urlcode,
            shop_ref_code: data.refcode,
            userId: data.userId
        })
        .first()
        .then((res) => {
            console.log('result: ', res);
            if (res) {
                dbx.filesDelete({
                    path: res.img_name
                }).then((resp) => {
                    console.log('deleteCORFile: ', resp);
                    next(null, {
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size
                    })
                }, (error) => {
                    next(null, {
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size
                    });
                });
            } else {
                next(null, {
                    img_name: data.img_name,
                    img_path: data.img_path,
                    img_type: data.img_type,
                    img_size: data.img_size
                });
            }
        }, (error) => {
            next(null, {
                img_name: data.img_name,
                img_path: data.img_path,
                img_type: data.img_type,
                img_size: data.img_size
            });
        });
};

exports.uploadCORFile = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_cor_number: data.shop_cor_number,
                shop_cor_file: data.img_path,
                shop_cor_filename: data.img_name,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.urlcode,
                shop_ref_code: data.refcode,
                userId: data.userId
            })
    }).asCallback(next);
};

exports.emptyCORFile = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_cor_file: null,
                shop_cor_filename: null,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.urlcode,
                shop_ref_code: data.refcode,
                userId: data.userId
            })
    }).asCallback(next);
};

exports.deleteBusinessLicenseFile = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    knex.select('shop_distributor_license AS img_path', 'shop_distributor_license_filename AS img_name')
        .table('official_shop')
        .where({
            shop_url_code: data.urlcode,
            shop_ref_code: data.refcode,
            userId: data.userId
        })
        .first()
        .then((res) => {
            console.log('result: ', res);
            if (res) {
                dbx.filesDelete({
                    path: res.img_name
                }).then((resp) => {
                    console.log('deleteBusinessLicenseFile: ', resp);
                    next(null, {
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size
                    })
                }, (error) => {
                    next(null, {
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size
                    });
                });
            } else {
                next(null, {
                    img_name: data.img_name,
                    img_path: data.img_path,
                    img_type: data.img_type,
                    img_size: data.img_size
                });
            }
        }, (error) => {
            next(null, {
                img_name: data.img_name,
                img_path: data.img_path,
                img_type: data.img_type,
                img_size: data.img_size
            });
        });
};

exports.uploadBusinessLicenseFile = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_distributor_license_number: data.shop_distributor_license_number,
                shop_distributor_license: data.img_path,
                shop_distributor_license_filename: data.img_name,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.urlcode,
                shop_ref_code: data.refcode,
                userId: data.userId
            })
    }).asCallback(next);
};

exports.emptyBusinessLicenseFile = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_distributor_license: null,
                shop_distributor_license_filename: null,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.urlcode,
                shop_ref_code: data.refcode,
                userId: data.userId
            })
    }).asCallback(next);
};

exports.deleteBusinessLogoFile = (data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    knex.select('shop_official_logo AS img_path', 'shop_official_logo_filename AS img_name')
        .table('official_shop')
        .where({
            shop_url_code: data.urlcode,
            shop_ref_code: data.refcode,
            userId: data.userId
        })
        .first()
        .then((res) => {
            console.log('result: ', res);
            if (res) {
                dbx.filesDelete({
                    path: res.img_name
                }).then((resp) => {
                    console.log('deleteBusinessLogoFile: ', resp);
                    next(null, {
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size
                    })
                }, (error) => {
                    next(null, {
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size
                    })
                });
            } else {
                next(null, {
                    img_name: data.img_name,
                    img_path: data.img_path,
                    img_type: data.img_type,
                    img_size: data.img_size
                });
            }
        }, (error) => {
            next(null, {
                img_name: data.img_name,
                img_path: data.img_path,
                img_type: data.img_type,
                img_size: data.img_size
            });
        });
};

exports.uploadBusinessLogoFile = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_official_logo: data.img_path,
                shop_official_logo_filename: data.img_name,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.urlcode,
                shop_ref_code: data.refcode,
                userId: data.userId
            })
    }).asCallback(next);
};

exports.emptyBusinessLogoFile = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_official_logo: null,
                shop_official_logo_filename: null,
            })
            .from('official_shop')
            .where({
                shop_url_code: data.urlcode,
                shop_ref_code: data.refcode,
                userId: data.userId
            })
    }).asCallback(next);
};

exports.approvedOfficialShop = (user, data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                shop_isapproved: 1,
                shop_appoved_date: new Date().toISOString(),
                shop_approved_by: user.userId
            })
            .from('official_shop')
            .where({
                official_shop_id: data.shopId
            })
    }).asCallback(next);
};


exports.getApplicationList = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            let strSQL = knex.count('* AS numRows').table("official_shop as os").innerJoin("users as u", "u.userId", "=", "os.userId");

            if (params.isApproved) {
                strSQL.whereNotNull("shop_appoved_date")
            }

            strSQL.then((results) => {
                numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                numPages = Math.ceil(numRows / numPerPage);
                callback(null, numRows, numPages);
            }, (err) => {
                return next(err, null);
            });
        },
        (numRows, numPages, callback) => {
            let strSQL = knex.select("os.*", "u.username", "u.sellerName", "u.sellerDescription")
                .table("official_shop as os").innerJoin("users as u", "u.userId", "=", "os.userId");

            if (params.isApproved) {
                strSQL.whereNotNull("shop_appoved_date")
            }

            if (params.limit && params.startWith) {
                strSQL.limit(numPerPage).offset(skip);
            } else if (params.limit) {
                strSQL.limit(numPerPage);
            }
            strSQL.then((results) => {
                callback(null, numRows, numPages, results)
            }, (error) => {
                return next(err, null);
            });
        },
        (numRows, numPages, results, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: results,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: results,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: results,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getApplicationDetail = (shopId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("os.*", "u.username", "u.sellerName", "u.sellerDescription")
                .table("official_shop as os")
                .innerJoin("users as u", "u.userId", "=", "os.userId")
                .where({
                    official_shop_id: shopId
                })
                .first()
                .then((result) => {
                    return callback(null, result);
                }, (error) => {
                    return next(error, null);
                });
        },
        (shop, callback) => {
            knex.select('*').from('official_shop_evaluation').where({
                    official_shop_id: shopId
                }).orderBy('date', 'desc')
                .then((evaluation) => {
                    if (!_.isEmpty(evaluation)) {
                        shop.evaluation = evaluation[0];
                    } else {
                        shop.evaluation = {};
                    }
                    shop.activity = evaluation;
                    return callback(null, shop);
                }, (error) => {
                    return next(error, null);
                });
        },
    ], next);
};

exports.getApplicationEvaluationList = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex.count('* AS numRows')
                .table("official_shop as os")
                .innerJoin("users as u", "u.userId", "=", "os.userId")
                .then((results) => {
                    numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    console.log('numRows: ', numRows);
                    console.log('numPages: ', numPages);

                    return callback(null, numRows, numPages);
                }, (err) => {
                    return next(err, null);
                })
        },
        (numRows, numPages, callback) => {
            let strSQL = knex.select("os.*", "u.username", "u.sellerName", "u.sellerDescription")
                .table("official_shop as os")
                .innerJoin("users as u", "u.userId", "=", "os.userId")

            if (params.limit && params.startWith) {
                strSQL.limit(numPerPage).offset(skip);
            } else if (params.limit) {
                strSQL.limit(numPerPage);
            }
            strSQL.then((results) => {
                return callback(null, numRows, numPages, results)
            }, (error) => {
                return next(error, null);
            });
        },
        (numRows, numPages, results, callback) => {
            async.eachSeries(results, (shop, shopCallback) => {
                knex.select('*').from('official_shop_evaluation').where({
                        official_shop_id: shop.official_shop_id
                    }).orderBy('date', 'desc')
                    .then((evaluation) => {
                        if (!_.isEmpty(evaluation)) {
                            shop.evaluation = evaluation[0];
                        } else {
                            shop.evaluation = {};
                        }
                        shop.activity = evaluation;
                        return shopCallback(null, shop);
                    }, (error) => {
                        return next(error, null);
                    });
            }, () => {
                callback(null, numRows, numPages, results);
            });
        },
        (numRows, numPages, results, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    return callback(null, {
                        result: results,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    return callback(null, {
                        result: results,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                return callback(null, {
                    result: results,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.updateApplicationEvaluation = (data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            console.log("callback 1");
            knex.select('u.userId', 'u.username', 'u.email', 'u.phone', 'u.user_img_path as img_path', 'u.user_img_name AS img_name', 'u.uuid').from('official_shop as os')
                .join('users AS u')
                .where('os.official_shop_id', '=', data.official_shop_id)
                .first()
                .then((result) => {
                    return callback(null, result);
                }, (error) => {
                    return next(error, null);
                });
        },
        (seller, callback) => {
            console.log("callback 2");
            knex.select('u.duserId', 'u.username', 'u.email', 'u.phone', 'u.user_img_path as img_path', 'u.user_img_name AS img_name', 'u.uuid')
                .from('dashboard_users as u')
                .where('u.uuid', '=', user.uuid)
                .first()
                .then((result) => {
                    return callback(null, result, seller);
                }, (error) => {
                    return next(error, null);
                });
        },
        (duser, seller, callback) => {
            console.log("callback 3");
            knex.transaction((trx) => {
                return trx
                    .insert({
                        official_shop_id: data.official_shop_id,
                        date: data.date,
                        statusClass: data.statusClass,
                        tagName: data.tagName,
                        content: data.content,
                        eval_categoryID: data.eval_categoryID
                    })
                    .into('official_shop_evaluation')
            }).then(() => {
                return callback(null, duser, seller);
            }, (error) => {
                return next(error, null);
            });
        },
        (duser, seller, callback) => {
            console.log("callback 4");
            knex.select('shop_name', 'owner_fname', 'owner_mname', 'owner_lname', 'owner_email', 'shop_email', 'shop_mobileno', 'shop_website', 'shop_instagram', 'shop_facebook', 'shop_application_date', 'shop_appoved_date')
                .from('official_shop').where({
                    official_shop_id: data.official_shop_id
                }).first().then((result) => {
                    return callback(null, duser, seller, result);
                }, (error) => {
                    return next(error, null);
                });
        },
        (duser, seller, officialshop, callback) => {
            console.log("callback 5");
            knex.select('onesignal_player_id').from('user_devices').where({
                    userId: seller.userId
                })
                .first()
                .then((result) => {
                    _.each(result, (row) => {
                        storesPlayerID.push(row);
                    });
                    return callback(null, duser, seller, officialshop);
                }, (error) => {
                    return next(error, null);
                });
        },
        (duser, seller, officialshop, callback) => {
            console.log("callback 6");
            let linkObj = {
                action: 'officialshop',
                officialShopId: data.official_shop_id,
                officialshop: officialshop,
                status: data.eval_categoryID,
                seller: seller,
            };
            let title = 'Official Shop Application Status!'
            let content = 'Official Shop Application for <b>' + officialshop.shop_name + '</b>  STATUS: <b>' + data.eval_categoryID + '</b>';
            knex('notifications').insert({
                title: title,
                content: content,
                datetime: knex.raw('NOW()'),
                link: JSON.stringify(linkObj),
                notifTypeId: 2
            }).returning('notificationId').then((result) => {
                console.log('result: ', result[0]);
                const notifId = result[0];
                let sSQL = mysql.format('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                    notifId, seller.userId, seller.userId
                ]);
                console.log("sSQL: ", sSQL);
                db.actionQuery(sSQL, (err, resp) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        let notification = {
                            notifId: notifId,
                            title: title,
                            content: content,
                            notification_data: linkObj,
                            devices: storesPlayerID
                        };
                        callback(null, {
                            officialshop: officialshop,
                            seller: seller,
                            notification: notification
                        });
                    }
                });
            }, (error) => {
                return next(error, null);
            });
        }
    ], next);
};

exports.approveOfficialShopApplication = (user, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            console.log("callback 1");
            knex.select('u.userId', 'u.username', 'u.email', 'u.phone', 'u.user_img_path as img_path', 'u.user_img_name AS img_name', 'u.uuid').from('official_shop as os')
                .join('users AS u')
                .where('os.official_shop_id', '=', data.official_shop_id)
                .first()
                .then((result) => {
                    return callback(null, result);
                }, (error) => {
                    return next(error, null);
                });
        },
        (seller, callback) => {
            console.log("callback 2");
            knex.select('u.duserId', 'u.username', 'u.email', 'u.phone', 'u.user_img_path as img_path', 'u.user_img_name AS img_name', 'u.uuid')
                .from('dashboard_users as u')
                .where('u.uuid', '=', user.uuid)
                .first()
                .then((result) => {
                    return callback(null, result, seller);
                }, (error) => {
                    return next(error, null);
                });
        },
        (duser, seller, callback) => {
            console.log("callback 3");
            knex.transaction((trx) => {
                return trx
                    .update({
                        shop_isapproved: 1,
                        shop_appoved_date: data.shop_appoved_date,
                        shop_approved_by: duser.duserId,
                    })
                    .from('official_shop')
                    .where({
                        official_shop_id: data.official_shop_id
                    })
            }).then(() => {
                return callback(null, duser, seller);
            }, (error) => {
                return next(error, null);
            });
        },
        (duser, seller, callback) => {
            console.log("callback 4");
            knex.transaction((trx) => {
                return trx
                    .insert({
                        official_shop_id: data.official_shop_id,
                        date: data.date,
                        statusClass: data.statusClass,
                        tagName: data.tagName,
                        content: data.content,
                        eval_categoryID: data.eval_categoryID
                    })
                    .into('official_shop_evaluation')
            }).then(() => {
                return callback(null, duser, seller);
            }, (error) => {
                return next(error, null);
            });
        },
        (duser, seller, callback) => {
            console.log("callback 5");
            knex.select('shop_name', 'owner_fname', 'owner_mname', 'owner_lname', 'owner_email', 'shop_email', 'shop_mobileno', 'shop_website', 'shop_instagram', 'shop_facebook', 'shop_application_date', 'shop_appoved_date')
                .from('official_shop').where({
                    official_shop_id: data.official_shop_id
                }).first().then((result) => {
                    return callback(null, duser, seller, result);
                }, (error) => {
                    return next(error, null);
                });
        },
        (duser, seller, officialshop, callback) => {
            console.log("callback 6");
            knex.select('onesignal_player_id').from('user_devices').where({
                    userId: seller.userId
                })
                .first()
                .then((result) => {
                    _.each(result, (row) => {
                        storesPlayerID.push(row);
                    });
                    return callback(null, duser, seller, officialshop);
                }, (error) => {
                    return next(error, null);
                });
        },
        (duser, seller, officialshop, callback) => {
            console.log("callback 7");
            let linkObj = {
                action: 'officialshop',
                officialShopId: data.official_shop_id,
                officialshop: officialshop,
                status: data.eval_categoryID,
                seller: seller,
                approvedBy: duser.duserId,
            };
            let title = 'Official Shop Application Approved!'
            let content = 'Your Official Shop Application as <b>' + officialshop.shop_name + '</b> has been approved on ' + officialshop.shop_application_date;
            knex('notifications').insert({
                title: title,
                content: content,
                datetime: knex.raw('NOW()'),
                link: JSON.stringify(linkObj),
                notifTypeId: 2
            }).returning('notificationId').then((result) => {
                console.log('result: ', result[0]);
                const notifId = result[0];
                let sSQL = mysql.format('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                    notifId, seller.userId, seller.userId
                ]);
                console.log("sSQL: ", sSQL);
                db.actionQuery(sSQL, (err, resp) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        let notification = {
                            notifId: notifId,
                            title: title,
                            content: content,
                            notification_data: linkObj,
                            devices: storesPlayerID
                        };
                        callback(null, {
                            officialshop: officialshop,
                            seller: seller,
                            notification: notification
                        });
                    }
                });
            }, (error) => {
                return next(error, null);
            });
        }
    ], next);
};