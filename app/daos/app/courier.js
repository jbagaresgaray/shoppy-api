'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let async = require('async');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkCourier = (data, next) => {
    //let strSQL = mysql.format('SELECT * FROM shipping_courier WHERE name = ? LIMIT 1;', [data.name]);
    //db.query(strSQL, next);
    knex.select("*").from("shipping_courier").where("name", data.name).limit(1).asCallback(next)
};

exports.checkCourierforUpdate = (_id, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM shipping_courier WHERE name = ? AND shippingCourierId <> ? LIMIT 1;', [data.name, _id]);
    // db.query(strSQL, next);
    knex.select("*").from("shipping_courier").where("name", data.name).andWhere("shippingCourierId", _id).limit(1).asCallback(next);
};

exports.createCourier = (data, next) => {
    // let strSQL = mysql.format('INSERT INTO shipping_courier(name,description,slug,phone,web_url,uuid) VALUES (?,?,?,?,?,UUID());', [
    //     data.name,data.description,data.slug,data.phone,data.web_url
    // ]);
    // db.insertWithId(strSQL, next);

    knex.transaction((trx) => {
            knex("shipping_courier")
                .insert({
                    name: data.name,
                    description: data.description,
                    slug: data.slug,
                    phone: data.phone,
                    web_url: data.web_url,
                    uuid: knex.raw("UUID()")
                })
                .then((resp) => {
                    return resp
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp && resp.length > 0 ? resp[0] : resp)
        })
        .catch((err) => {
            next(err, err)
        })

};

exports.getAllCourier = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // var strSQL = mysql.format('SELECT COUNT(*) AS numRows FROM shipping_courier;');
            // db.query(strSQL, function(err, results) {
            //     console.log('results : ', results)
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });
            knex.count("* as numRows").from("shipping_courier").asCallback(function (err, results) {
                console.log('results : ', results)
                if (err) {
                    next(err, null);
                } else {
                    numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    callback(null, numRows, numPages);
                }
            })
        },
        (numRows, numPages, callback) => {
            // var strSQL = mysql.format('SELECT * FROM shipping_courier');
            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' LIMIT ' + limit
            // } else if (params.limit) {
            //     strSQL = strSQL + ' LIMIT ' + numPerPage
            // }
            // strSQL = strSQL + ';';
            // db.query(strSQL, function(err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, numRows, numPages, results);
            //     }
            // });

            let sQl = knex.select("*").from("shipping_courier");
            if (params.limit && params.startWith) {
                sQl = sQl.limit(limit)
            } else if (params.limit) {
                sQl = sQl.limit(numPerPage)
            }
            sQl.asCallback(function (err, results) {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, numRows, numPages, results);
                }
            })
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getCourier = (_id, next) => {
    // let strSQL = mysql.format('SELECT * FROM shipping_courier WHERE (shippingCourierId=? OR uuid=?) LIMIT 1;', [_id, _id]);
    // db.query(strSQL, next);
    knex.select("*").from("shipping_courier").where("shippingCourierId", _id).orWhere("uuid", _id).asCallback(next);
};

exports.deleteCourier = (_id, next) => {
    // let strSQL = mysql.format('DELETE FROM shipping_courier WHERE (shippingCourierId=? OR uuid=?);', [_id, _id]);
    // db.actionQuery(strSQL, next);
    knex.transaction((trx) => {
            knex("shipping_courier")
                .where("shippingCourierId", _id)
                .orWhere("uuid", _id)
                .delete()
                .then((resp) => {
                    return resp
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.updateCourier = (_id, data, next) => {
    // let strSQL = mysql.format('UPDATE shipping_courier SET name=?, description=?,slug=?,phone=?,web_url=? WHERE (shippingCourierId=? OR uuid=?);', [
    //     data.name,data.description,data.slug,data.phone,data.web_url, _id, _id
    // ]);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("shipping_courier")
                .update({
                    name: data.name,
                    description: data.description,
                    slug: data.slug,
                    phone: data.phone,
                    web_url: data.web_url
                })
                .where("shippingCourierId", _id)
                .orWhere("uuid", _id)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)

        })
        .then((resp) => {
            next(null, next)
        })
        .catch((err) => {
            next(err, err)
        })
};