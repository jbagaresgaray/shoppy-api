'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const fs = require('fs');
const async = require('async');

var knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkMobileBanner = (name, next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_banners WHERE name = ?;', [name]);
    // console.log('checkMobileBanner: ', strSQL);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("mobile_banners")
        .where('name', name)
        .asCallback(next)
};

exports.checkMobileBannerUpdate = (bannerId, name, next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_banners WHERE (bannerId <> ?) AND name = ?;', [bannerId, name]);
    // console.log('checkMobileBannerUpdate: ', strSQL);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("mobile_banners")
        .where("bannerId", "<>", bannerId)
        .andWhere("name", name)
        .asCallback(next)
};

exports.createMobileBanner = (data, next) => {
    // let strSQL = mysql.format(`INSERT INTO mobile_banners(
    //     name,
    //     img_path,
    //     img_name,
    //     img_type,
    //     img_size,
    //     link,
    //     sortorder,
    //     isPublished,
    //     uuid) VALUES (?,?,?,?,?,?,?,0,UUID());`, [
    //     data.name, data.img_path, data.img_name, data.img_type, data.img_size, data.link, data.sortorder
    // ]);
    // db.insertWithId(strSQL, next);

    knex.transaction((trx) => {
            knex("mobile_banners")
                .insert({
                    name: data.name,
                    img_path: data.img_path,
                    img_name: data.img_name,
                    img_type: data.img_type,
                    img_size: data.img_size,
                    link: data.link,
                    sortorder: data.sortorder,
                    isPublished: 0,
                    uuid: knex.raw("UUID()")
                })
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp && resp.length > 0 ? resp[0] : resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.updateMobileBanner = (bannerId, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    if (data.toDeleteFile) {
        async.waterfall([
            (callback) => {
                // let sql1 = mysql.format(`SELECT 
                //     img_path,
                //     img_name,
                //     img_type,
                //     img_size 
                //     FROM mobile_banners 
                //     WHERE (bannerId=? OR uuid=?) 
                //     LIMIT 1;`,[bannerId, bannerId]);
                // console.log('sql1: ', sql1);
                // db.query(sql1, function(err, res) {
                //     if (err) {
                //         next(err, null);
                //     }
                //     if (res && res.length > 0) {
                //         res = res[0];
                //         if (res) {
                //             dbx.filesDelete({
                //                 path: res.img_name
                //             }).then((resp) => {
                //                 console.log('Delete bannerImage: ', resp);
                //                 callback(null, {
                //                     img_name: data.img_name,
                //                     img_path: data.img_path,
                //                     img_type: data.img_type,
                //                     img_size: data.img_size
                //                 })
                //             }).catch((error) => {
                //                 console.error('DropBox error 4: ', error);
                //                 callback(null, {
                //                     img_name: data.img_name,
                //                     img_path: data.img_path,
                //                     img_type: data.img_type,
                //                     img_size: data.img_size
                //                 })
                //             });
                //         } else {
                //             callback(null, {
                //                 img_name: data.img_name,
                //                 img_path: data.img_path,
                //                 img_type: data.img_type,
                //                 img_size: data.img_size
                //             });
                //         }
                //     } else {
                //         callback(null, {
                //             img_name: data.img_name,
                //             img_path: data.img_path,
                //             img_type: data.img_type,
                //             img_size: data.img_size
                //         });
                //     }
                // });

                //--------------------------------------
                knex.select("img_path", "img_name", "img_type", "img_size")
                    .from("mobile_banners")
                    .where("bannerId", bannerId)
                    .orWhere("uuid", bannerId)
                    .asCallback((err, res) => {
                        if (err) {
                            next(err, null);
                        } else {
                            if (res && res.length > 0) {
                                res = res[0];
                                if (res) {
                                    dbx.filesDelete({
                                        path: res.img_name
                                    }).then((resp) => {
                                        console.log('Delete bannerImage: ', resp);
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        })
                                    }).catch((error) => {
                                        console.error('DropBox error 4: ', error);
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        })
                                    });
                                } else {
                                    callback(null, {
                                        img_name: data.img_name,
                                        img_path: data.img_path,
                                        img_type: data.img_type,
                                        img_size: data.img_size
                                    });
                                }
                            } else {
                                callback(null, {
                                    img_name: data.img_name,
                                    img_path: data.img_path,
                                    img_type: data.img_type,
                                    img_size: data.img_size
                                });
                            }
                        }
                    })
            },
            (result, callback) => {
                let app = {
                    img_name: result.img_name,
                    img_path: result.img_path,
                    img_type: result.img_type,
                    img_size: result.img_size
                }
                // let strSQL = mysql.format(`UPDATE mobile_banners SET 
                //     name=?,
                //     img_path=?,
                //     img_name=?,
                //     img_type=?,
                //     img_size=?,
                //     link=?,
                //     sortorder=? 
                //     WHERE (bannerId = ? OR uuid = ?);`, [
                //     data.name, 
                //     app.img_path, 
                //     app.img_name, 
                //     app.img_type, 
                //     app.img_size, 
                //     data.link, 
                //     data.sortorder, 
                //     bannerId, 
                //     bannerId
                // ]);
                // console.log('strSQL: ', strSQL);
                // db.actionQuery(strSQL, function(err, res) {
                //     if (err) {
                //         callback(err, null);
                //     } else {
                //         callback(null, res);
                //     }
                // });

                //----------------------------------
                knex.transaction((trx) => {
                        knex("mobile_banners")
                            .update({
                                name: data.name,
                                img_path: app.img_path,
                                img_name: app.img_name,
                                img_type: app.img_type,
                                img_size: app.img_size,
                                link: data.link,
                                sortorder: data.sortorder,
                            })
                            .where("bannerId", bannerId)
                            .orWhere("uuid", bannerId)
                            .then((resp) => {
                                return resp
                            })
                            .then(trx.commit)
                            .catch(trx.rollback)
                    })
                    .then((resp) => {
                        callback(null, res);
                    })
                    .catch((err) => {
                        callback(err, null);
                    })
            }
        ], next);
    } else {
        // let strSQL = mysql.format(`UPDATE mobile_banners SET 
        //     name=?,
        //     img_path=?,
        //     img_name=?,
        //     img_type=?,
        //     img_size=?,
        //     link=?,
        //     sortorder=? 
        //     WHERE (bannerId = ? OR uuid = ?);`, [
        //     data.name, 
        //     data.img_path, 
        //     data.img_name, 
        //     data.img_type, 
        //     data.img_size, 
        //     data.link, 
        //     data.sortorder, 
        //     bannerId, 
        //     bannerId
        // ]);
        // db.actionQuery(strSQL, next);

        //--------------------------------------
        knex.transaction((trx) => {
                knex("mobile_banners")
                    .update({
                        name: data.name,
                        img_path: data.img_path,
                        img_name: data.img_name,
                        img_type: data.img_type,
                        img_size: data.img_size,
                        link: data.link,
                        sortorder: data.sortorder
                    })
                    .where("bannerId", bannerId)
                    .orWhere("uuid", bannerId)
                    .then((resp) => {
                        return resp
                    })
                    .then(trx.commit)
                    .catch(trx.rollback)
            })
            .then((resp) => {
                next(null, resp)
            })
            .catch((err) => {
                next(err, err)
            })
    }
};

exports.getBanner = (bannerId, next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_banners WHERE (bannerId=? OR uuid=?);', [bannerId, bannerId]);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("mobile_banners")
        .where("bannerId", bannerId)
        .orWhere("uuid", bannerId)
        .asCallback(next)
};

exports.getAllMobileBanners = (next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_banners;');
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("mobile_banners")
        .asCallback(next)
};

exports.getAllActiveMobileBanners = (next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_banners WHERE isPublished=1;');
    // db.query(strSQL, next);

    knex.select("*").from("mobile_banners").where("isPublished", 1).asCallback(next)
};

exports.deleteMobileBanner = (bannerId, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            // let sql1 = mysql.format('SELECT img_path,img_name,img_type,img_size FROM mobile_banners WHERE (bannerId=? OR uuid=?)  LIMIT 1;', [bannerId, bannerId]);
            // console.log('sql1: ', sql1);
            // db.query(sql1, function(err, res) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (res && res.length > 0) {
            //             res = res[0];
            //             if (res) {
            //                 dbx.filesDelete({
            //                     path: res.img_name
            //                 }).then((resp) => {
            //                     console.log('Delete deleteMobileBanner: ', resp);
            //                     callback();
            //                 }).catch((error) => {
            //                     console.error('DropBox error 4: ', error);
            //                     callback();
            //                 });
            //             } else {
            //                 callback();
            //             }
            //         } else {
            //             callback();
            //         }
            //     }
            // });

            knex.select("img_path", "img_name", "img_type", "img_size")
                .from("mobile_banners")
                .where("bannerId", bannerId)
                .orWhere("uuid", bannerId)
                .asCallback((err, res) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (res && res.length > 0) {
                            res = res[0];
                            if (res) {
                                dbx.filesDelete({
                                    path: res.img_name
                                }).then((resp) => {
                                    console.log('Delete deleteMobileBanner: ', resp);
                                    callback();
                                }).catch((error) => {
                                    console.error('DropBox error 4: ', error);
                                    callback();
                                });
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    }
                })

        },
        (callback) => {
            // let strSQL = mysql.format('DELETE FROM mobile_banners WHERE (bannerId=? OR uuid=?);', [bannerId, bannerId]);
            // console.log('strSQL: ', strSQL);
            // db.actionQuery(strSQL, function(err, res) {
            //     if (err) {
            //         callback(err, null);
            //     } else {
            //         callback(null, res);
            //     }
            // });

            knex.transaction((trx) => {
                    knex("mobile_banners")
                        .where("bannerId", bannerId)
                        .orWhere("uuid", bannerId)
                        .delete()
                        .then((resp) => {
                            return resp;
                        })
                        .then(trx.commit)
                        .catch(trx.rollback)
                })
                .then((resp) => {
                    callback(null, resp);
                })
                .catch((err) => {
                    callback(err, err)
                })
        }
    ], next);
};

exports.publishBanner = (bannerId, next) => {
    // let strSQL = mysql.format('UPDATE mobile_banners SET isPublished = 1 WHERE (bannerId=? OR uuid=?);', [bannerId, bannerId]);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("mobile_banners")
                .update({
                    isPublished: 1
                })
                .where("bannerId", bannerId)
                .orWhere("uuid", bannerId)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.unpublishBanner = (bannerId, next) => {
    // let strSQL = mysql.format('UPDATE mobile_banners SET isPublished = 0 WHERE (bannerId=? OR uuid=?);', [bannerId, bannerId]);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("mobile_banners")
                .update({
                    isPublished: 0
                })
                .where("bannerId", bannerId)
                .orWhere("uuid", bannerId)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.createMobileCMS = (user, data, next) => {
    // let strSQL = mysql.format('INSERT INTO mobile_cms(title,summary,content,date_created,created_by,tags,isPublished,uuid) VALUES (?,?,?,NOW(),?,?,0,UUID())', [
    //     data.title, data.summary, data.content, user.duserId, data.tags
    // ]);
    // db.insertWithId(strSQL, next);

    knex.transaction((trx) => {
            knex("mobile_cms")
                .insert({
                    title: data.title,
                    summary: data.summary,
                    content: data.content,
                    date_created: knex.raw("NOW()"),
                    //created_by: user.duserId,
                    created_by: user.uuid,
                    tags: data.tags,
                    isPublished: 0,
                    uuid: knex.raw("UUID()")
                })
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp && resp.length > 0 ? resp[0] : resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.updateMobileCMS = (cmsId, data, next) => {
    // let strSQL = mysql.format('UPDATE mobile_cms SET title=?,summary=?,content=? WHERE (cmsId = ? OR uuid=?);', [
    //     data.title, data.summary, data.content, cmsId, cmsId
    // ]);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("mobile_cms")
                .update({
                    title: data.title,
                    summary: data.summary,
                    content: data.content
                })
                .where("cmsId", cmsId)
                .orWhere("uuid", cmsId)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.getMobileCMS = (cmsId, next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_cms WHERE (cmsId = ? OR uuid=?) LIMIT 1;', [cmsId, cmsId]);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("mobile_cms")
        .where("cmsId", cmsId)
        .orWhere("uuid", cmsId)
        .limit(1)
        .first()
        .asCallback(next)
};

exports.getAllMobileCMS = (next) => {
    // let strSQL = mysql.format('SELECT * FROM mobile_cms;');
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("mobile_cms")
        .asCallback(next)
};

exports.deleteMobileCMS = (cmsId, next) => {
    // let strSQL = mysql.format('DELETE FROM mobile_cms WHERE (cmsId=? OR uuid=?);', [cmsId, cmsId]);
    // db.actionQuery(strSQL, next);

    knex.transaction(trx => {
        return trx("mobile_cms")
            .where("cmsId", cmsId)
            .orWhere("uuid", cmsId)
            .delete()
            .asCallback(next)
    })
};