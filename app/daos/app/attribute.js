"use strict";

const Database = require("../../../app/utils/database").Database;
const db = new Database();
const async = require("async");

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkAttribute = (data, next) => {
    knex
        .select("*")
        .from("attributes")
        .where("name", data.name)
        .first()
        .asCallback(next);
};

exports.checkAttributeforUpdate = (_id, data, next) => {
    knex
        .select("*")
        .from("attributes")
        .where("name", data.name)
        .andWhere("attribute_id", _id)
        .first()
        .asCallback(next);
};

exports.createAttribute = (data, next) => {
    knex.transaction(trx => {
        return trx
            .insert({
                    name: data.name,
                    label: data.label,
                    default_value: data.default_value,
                    is_visible: data.is_visible,
                    is_search: data.is_search,
                    is_required: data.is_required,
                    is_customizable: data.is_customizable,
                    is_unique: data.is_unique,
                    is_comparable: data.is_comparable,
                    sub_id: data.sub_id,
                    form_input_type_id: data.form_input_type_id,
                    _id: knex.raw("UUID()")
                },
                "attribute_id"
            )
            .into("attributes")
            .asCallback(next);
    });
};

exports.getAllAttribute = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + "," + numPerPage;

    async.waterfall(
        [
            callback => {
                knex("attributes")
                    .count("* as numRows")
                    .then(results => {
                        numRows =
                            results && results[0] && results[0].numRows ?
                            results[0].numRows :
                            0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("*").from("attributes");
                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }

                strSQL
                    .then(results => {
                        return callback(null, numRows, numPages, results);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, attributes, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            result: attributes,
                            pagination: {
                                total: numRows,
                                startAt: page + 1,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            result: attributes,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        result: attributes,
                        pagination: {
                            total: numRows,
                            startAt: 0,
                            page_size: 0,
                            maxResult: 0
                        }
                    });
                }
            }
        ],
        next
    );
};

exports.getAttribute = (_id, next) => {
    knex
        .select("*")
        .from("attributes")
        .where("attribute_id", _id)
        .orWhere("_id", _id)
        .first()
        .asCallback(next);
};

exports.deleteAttribute = (_id, next) => {
    knex.transaction(trx => {
        return trx("attributes")
            .where("attribute_id", _id)
            .orWhere("_id", _id)
            .delete()
            .asCallback(next);
    })
};

exports.updateAttribute = (_id, data, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                name: data.name,
                label: data.label,
                default_value: data.default_value,
                is_visible: data.is_visible,
                is_search: data.is_search,
                is_required: data.is_required,
                is_customizable: data.is_customizable,
                is_unique: data.is_unique,
                is_comparable: data.is_comparable,
                sub_id: data.sub_id,
                form_input_type_id: data.form_input_type_id,
            })
            .from("attributes")
            .where("attribute_id", _id)
            .orWhere("_id", _id)
            .asCallback(next);
    });
};