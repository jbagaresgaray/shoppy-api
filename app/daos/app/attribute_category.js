"use strict";

const Database = require("../../../app/utils/database").Database;
const db = new Database();
const async = require("async");

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.createAttributeCategory = (categoryId, data, next) => {
    let tmpArr = [];
    for (let i = 0; i < data.attributes.length; i++) {
        tmpArr.push({
            attribute_id: data.attributes[i],
            category_id: categoryId,
        })
    }
    knex.transaction(trx => {
        return trx
            .insert(tmpArr,
                "att_cat_id"
            )
            .into("attribute_category")
            .asCallback(next);
    });
};

exports.getAllAttributeCategory = (categoryId, params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + "," + numPerPage;

    async.waterfall(
        [
            callback => {
                knex("attribute_category")
                    .count("* as numRows")
                    .where("category_id", "=", categoryId)
                    .then(results => {
                        numRows =
                            results && results[0] && results[0].numRows ?
                            results[0].numRows :
                            0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("*").from("attribute_category").where("category_id", "=", categoryId);
                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }

                strSQL
                    .then(results => {
                        return callback(null, numRows, numPages, results);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, attributes, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            result: attributes,
                            pagination: {
                                total: numRows,
                                startAt: page + 1,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            result: attributes,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        result: attributes,
                        pagination: {
                            total: numRows,
                            startAt: 0,
                            page_size: 0,
                            maxResult: 0
                        }
                    });
                }
            }
        ],
        next
    );
};

exports.getAttributeCategory = (category_id, attribute_id, next) => {
    knex
        .select("*")
        .from("attribute_category")
        .innerJoin('attributes', 'attribute_category.attribute_id', 'attributes.attribute_id')
        .where("attribute_category.attribute_id", "=", attribute_id)
        .andWhere("attribute_category.category_id", "=", category_id)
        .first()
        .asCallback(next);
};

exports.deleteAttributeCategory = (category_id, attribute_id, next) => {
    knex.transaction(trx => {
        return trx("attribute_category")
            .where("attribute_id", attribute_id)
            .andWhere("category_id", category_id)
            .delete()
            .asCallback(next);
    })
};

// exports.updateAttributeCategory = (_id, data, next) => {
//     knex.transaction(trx => {
//         return trx
//             .update({
//                 name: data.name,
//                 label: data.label,
//                 default_value: data.default_value,
//                 is_visible: data.is_visible,
//                 is_search: data.is_search,
//                 is_required: data.is_required,
//                 is_customizable: data.is_customizable,
//                 is_unique: data.is_unique,
//                 is_comparable: data.is_comparable,
//                 sub_id: data.sub_id,
//                 form_input_type_id: data.form_input_type_id,
//             })
//             .from("attribute_category")
//             .where("attribute_id", "=", _id)
//             .orWhere("_id", "=", _id)
//             .asCallback(next);
//     });
// };