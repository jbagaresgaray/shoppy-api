'use strict';

var func = require('../../../app/utils/functions');
var path = require('path');

exports.getAllTimezones = (next) => {
    var filepath = path.resolve('app/data');

    var newPath = filepath + '/timezone.json';
    var timeZoneData = func.readJsonFileSync(newPath);

    next(null, timeZoneData);
};

exports.getAllCountry = (next) => {
    var filepath = path.resolve('app/data');

    var newPath = filepath + '/country.json';
    var timeZoneData = func.readJsonFileSync(newPath);

    next(null, timeZoneData);
};

exports.getAllCurrencies = (next) => {
    var filepath = path.resolve('app/data');

    var newPath = filepath + '/currency.json';
    var timeZoneData = func.readJsonFileSync(newPath);

    next(null, timeZoneData);
};