"use strict";

const Database = require("../../../app/utils/database").Database;
const db = new Database();
const async = require("async");

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkFormInputType = (data, next) => {
    knex
        .select("*")
        .from("form_input_type")
        .where("name", "=", data.name)
        .first()
        .asCallback(next);
};

exports.checkFormInputTypeforUpdate = (_id, data, next) => {
    knex
        .select("*")
        .from("form_input_type")
        .where("name", "=", data.name)
        .andWhere("form_input_type_id", "=", _id)
        .first()
        .asCallback(next);
};

exports.createFormInputType = (data, next) => {
    knex.transaction(trx => {
        return trx
            .insert({
                    name: data.name,
                    label: data.label,
                    sub_id: data.sub_id,
                    _id: knex.raw("UUID()")
                },
                "form_input_type_id"
            )
            .into("form_input_type")
            .asCallback(next);
    });
};

exports.getAllFormInputType = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + "," + numPerPage;

    async.waterfall(
        [
            callback => {
                knex("form_input_type")
                    .count("* as numRows")
                    .then(results => {
                        numRows =
                            results && results[0] && results[0].numRows ?
                            results[0].numRows :
                            0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex.select("*").from("form_input_type");
                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }

                strSQL
                    .then(results => {
                        return callback(null, numRows, numPages, results);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, attributes, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            result: attributes,
                            pagination: {
                                total: numRows,
                                startAt: page + 1,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            result: attributes,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        result: attributes,
                        pagination: {
                            total: numRows,
                            startAt: 0,
                            page_size: 0,
                            maxResult: 0
                        }
                    });
                }
            }
        ],
        next
    );
};

exports.getFormInputType = (_id, next) => {
    knex
        .select("*")
        .from("form_input_type")
        .where("form_input_type_id", "=", _id)
        .orWhere("_id", "=", _id)
        .first()
        .asCallback(next);
};

exports.deleteFormInputType = (_id, next) => {
    knex.transaction(trx => {
        return trx("form_input_type")
            .where("form_input_type_id", "=", _id)
            .orWhere("_id", "=", _id)
            .delete()
            .asCallback(next);
    })
};

exports.updateFormInputType = (_id, data, next) => {
    knex.transaction(trx => {
        return trx
            .update({
                name: data.name,
                label: data.label,
                sub_id: data.sub_id,
            })
            .from("form_input_type")
            .where("form_input_type_id", "=", _id)
            .orWhere("_id", "=", _id)
            .asCallback(next);
    });
};