"use strict";

const Database = require("../../../app/utils/database").Database;
const db = new Database();
const async = require("async");

const knex = require("knex")({
  client: "mysql",
  connection: db.configuration
});

exports.checkBank = (data, next) => {
  knex
    .select("*")
    .from("bank")
    .where("name", data.name)
    .first()
    .asCallback(next);
};

exports.checkBankforUpdate = (_id, data, next) => {
  knex
    .select("*")
    .from("bank")
    .where("name", data.name)
    .andWhere("bankId", "<>", _id)
    .first()
    .asCallback(next);
};

exports.createBank = (data, next) => {
  knex.transaction(trx => {
    return trx
      .insert({
          name: data.name,
          address: data.address,
          city: data.city,
          suburb: data.suburb,
          state: data.state,
          zipcode: data.zipcode,
          country: data.country,
          uuid: knex.raw("UUID()")
        },
        "bankId"
      )
      .into("bank")
      .asCallback(next);
  });
};

exports.getAllBank = (params, next) => {
  if (params.limit <= 10) params.limit = 10;

  let numRows;
  let queryPagination;
  let numPerPage = parseInt(params.limit, 10) || 10;
  let page = parseInt(params.startWith, 10) || 0;
  let numPages;
  let skip = page * numPerPage;
  let limit = skip + "," + numPerPage;

  async.waterfall(
    [
      callback => {
        knex("bank")
          .count("* as numRows")
          .then(results => {
            numRows =
              results && results[0] && results[0].numRows ?
              results[0].numRows :
              0;
            numPages = Math.ceil(numRows / numPerPage);
            return callback(null, numRows, numPages);
          })
          .catch(err => {
            return next(err, null);
          });
      },
      (numRows, numPages, callback) => {
        let strSQL = knex.select("*").from("bank");
        if (params.limit && params.startWith) {
          strSQL = strSQL.limit(numPerPage).offset(skip);
        } else if (params.limit) {
          strSQL = strSQL.limit(numPerPage);
        }

        strSQL
          .then(results => {
            return callback(null, numRows, numPages, results);
          })
          .catch(err => {
            return next(err, null);
          });
      },
      (numRows, numPages, banks, callback) => {
        if (params.limit && params.startWith) {
          if (page < numPages) {
            callback(null, {
              result: banks,
              pagination: {
                total: numRows,
                startAt: page + 1,
                page_size: numPerPage,
                maxResult: numPages
              }
            });
          } else {
            callback(null, {
              result: banks,
              pagination: {
                total: numRows,
                startAt: page,
                page_size: numPerPage,
                maxResult: numPages || 0
              }
            });
          }
        } else {
          callback(null, {
            result: banks,
            pagination: {
              total: numRows,
              startAt: 0,
              page_size: 0,
              maxResult: 0
            }
          });
        }
      }
    ],
    next
  );
};

exports.getBank = (_id, next) => {
  knex
    .select("*")
    .from("bank")
    .where("bankId", _id)
    .orWhere("uuid", _id)
    .first()
    .asCallback(next);
};

exports.deleteBank = (_id, next) => {
  knex.transaction(trx => {
    return trx("bank")
      .where("bankId", _id)
      .orWhere("uuid", _id)
      .delete()
      .asCallback(next);
  })
};

exports.updateBank = (_id, data, next) => {
  knex.transaction(trx => {
    return trx
      .update({
        name: data.name,
        address: data.address,
        city: data.city,
        suburb: data.suburb,
        state: data.state,
        zipcode: data.zipcode,
        country: data.country
      })
      .from("bank")
      .where("bankId", _id)
      .orWhere("uuid", _id)
      .asCallback(next);
  });
};