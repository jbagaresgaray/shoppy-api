'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || "development";
let config = require("../../../config/environment/" + env);

const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
let async = require('async');
let fs = require('fs');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkUserUsername = (data, next) => {
    // let strSQL = mysql.format('SELECT * FROM dashboard_users WHERE username = ? LIMIT 1;', [data.username]);
    // db.query(strSQL, next);

    // I used .limit(1) instead of .first() because the controller already expects the return to be an array.
    knex.select("*")
        .from("dashboard_users")
        .where("username", data.username)
        .limit(1)
        .asCallback(next)
};

exports.checkUserUsernameForUpdate = (user_id, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM dashboard_users WHERE duserId <> ? AND username = ? LIMIT 1;', [user_id, data.username]);
    // db.query(strSQL, next);

    // I used .limit(1) instead of .first() because the controller already expects the return to be an array.
    knex.select("*")
        .from("dashboard_users")
        .where("duserId", "<>", user_id)
        .andWhere("username", data.username)
        .limit(1)
        .asCallback(next);
};

exports.checkUserEmail = (data, next) => {
    // let strSQL = mysql.format('SELECT * FROM dashboard_users WHERE email = ? LIMIT 1;', [data.email]);
    // console.log('strSQL: ', strSQL);
    // db.query(strSQL, next);

    // I used .limit(1) instead of .first() because the controller already expects the return to be an array.
    knex.select("*")
        .from("dashboard_users")
        .where("email", data.email)
        .limit(1)
        .asCallback(next)
};

exports.checkUserEmailForUpdate = (user_id, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM dashboard_users WHERE duserId <> ? AND email = ? LIMIT 1;', [user_id, data.email]);
    // db.query(strSQL, next);

    // I used .limit(1) instead of .first() because the controller already expects the return to be an array.
    knex.select("*")
        .from("dashboard_users")
        .where("duserId", "<>", user_id)
        .andWhere("email", data.email)
        .limit(1)
        .asCallback(next)
};

exports.getUserByUsernameAndEmail = (email, next) => {
    // let strSQL = mysql.format('SELECT username,email,firstname,lastname,phone,fax,verificationCode,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid FROM dashboard_users WHERE email =? LIMIT 1;', [email]);
    // db.query(strSQL, next);

    // I used .limit(1) instead of .first() because the controller already expects the return to be an array.
    knex.select("username", "email", "firstname", "lastname", "phone", "fax", "verificationCode", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
        .from("dashboard_users")
        .limit(1)
        .asCallback(next)
};

exports.getUserByUUID = (uuid, next) => {
    // let strSQL = mysql.format('SELECT username,email,firstname,lastname,verificationCode,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid FROM dashboard_users WHERE uuid =? LIMIT 1;', [uuid]);
    // console.log('strSQL: ', strSQL);
    // db.query(strSQL, next);

    knex.select("username", "email", "firstname", "lastname", "verificationCode", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
        .from("dashboard_users")
        .where("uuid", uuid)
        .limit(1)
        .asCallback(next)
};

exports.createUser = (data, next) => {
    const SendBird = require("sendbird-nodejs");
    let sb = SendBird(config.sendbird_app_token);

    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    data.uuid = uuidv1();
    data.verificationCode = func.generateString(36);

    const payload = {
        "user_id": data.uuid,
        "nickname": data.username,
        "profile_url": "",
        "issue_access_token": true // (Optional)
    };

    let strSQL;
    sb.users.create(payload).then((response) => {
        // console.log("SendBird response: ", response);
        // strSQL = mysql.format('INSERT INTO dashboard_users(firstname,lastname,username,password,email,isVerify,verificationCode,sendBirdToken,uuid) VALUES (?,?,?,?,?,0,?,?,?);', [
        //     data.firstname,
        //     data.lastname,
        //     data.username,
        //     data.password,
        //     data.email,
        //     data.verificationCode,
        //     response.access_token,
        //     data.uuid
        // ]);
        // console.log("strSQL: ", strSQL);
        // db.insertWithId(strSQL, (err, user) => {
        //     if (err) {
        //         next(err, null);
        //     } else {
        //         if (user && user.length > 0) {
        //             user = user[0];
        //         }
        //         console.log('user: ', user);
        //         var sqlStrUser = mysql.format('SELECT username,email,firstname,lastname,phone,fax,verificationCode,uuid ' +
        //             'FROM dashboard_users WHERE duserId =? LIMIT 1', [user]);
        //         console.log('sqlStrUser: ', sqlStrUser);
        //         db.query(sqlStrUser, next);
        //     }
        // });

        knex.transaction((trx) => {
                knex("dashboard_users")
                    .insert({
                        firstname: data.firstname,
                        lastname: data.lastname,
                        username: data.username,
                        password: data.password,
                        email: data.email,
                        isVerify: 0,
                        verificationCode: data.verificationCode,
                        sendBirdToken: response.access_token,
                        uuid: knex.raw("UUID()")
                    })
                    .then((resp) => {
                        return resp
                    })
                    .then(trx.commit)
                    .catch(trx.rollback)
            })
            .then((user) => {
                if (user && user.length > 0) {
                    user = user[0];
                }

                knex.select("username", "email", "firstname", "lastname", "phone", "fax", "verificationCode", "uuid")
                    .from("dashboard_users")
                    .where("duserId", user)
                    .limit(1)
                    .asCallback(next)
            })
            .catch((err) => {
                next(err, null);
            })
    }, (error) => {
        console.log('SendBird error: ', error);
    });
};

exports.getAllUsers = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // var strSQL = mysql.format('SELECT COUNT(*) AS numRows FROM dashboard_users;');
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });

            knex.count("* as numRows")
                .from("dashboard_users")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                })
        },
        (numRows, numPages, callback) => {
            // var strSQL = mysql.format('SELECT username,email,firstname,lastname,phone,fax,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid FROM dashboard_users');
            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' LIMIT ' + limit
            // } else if (params.limit) {
            //     strSQL = strSQL + ' LIMIT ' + numPerPage
            // }
            // strSQL = strSQL + ';';

            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, numRows, numPages, results);
            //     }
            // });

            let sQl = knex.select("username", "email", "firstname", "lastname", "phone", "fax", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
                .from("dashboard_users");
            if (params.limit && params.startWith) {
                sQl = sQl.limit(limit)
            } else if (params.limit) {
                sQl = sQl.limit(numPerPage)
            }

            sQl.asCallback((err, results) => {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, numRows, numPages, results);
                }
            })
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getUser = (user_id, next) => {
    // let strSQL = mysql.format(`SELECT 
    //                             username,
    //                             email,
    //                             firstname,
    //                             lastname,
    //                             phone,fax,
    //                             user_img_path AS img_path, 
    //                             user_img_name AS img_name, 
    //                             user_img_type AS img_type, 
    //                             user_img_size AS img_size,
    //                             uuid FROM dashboard_users WHERE (duserId=? OR uuid=?) LIMIT 1;`, [user_id, user_id]);
    // db.query(strSQL, next);

    knex.select("username", "email", "firstname", "lastname", "phone", "fax", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
        .from("dashboard_users")
        .where("duserId", user_id)
        .orWhere("uuid", user_id)
        .limit(1)
        .asCallback(next)
};

exports.deleteUser = (user_id, next) => {
    async.waterfall([
        (callback) => {
            // let sql1 = mysql.format('SELECT user_img_path,user_img_name,user_img_type,user_img_size FROM dashboard_users WHERE (duserId=? OR uuid=?)  LIMIT 1;', [user_id, user_id]);
            // console.log('sql1: ', sql1);
            // db.query(sql1, function (err, res) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (res && res.length > 0) {
            //             res = res[0];
            //             if (res) {
            //                 if (func.existsSync(res.img_path)) {
            //                     fs.unlinkSync(res.img_path);
            //                     callback()
            //                 } else {
            //                     callback();
            //                 }
            //             } else {
            //                 callback();
            //             }
            //         } else {
            //             callback();
            //         }
            //     }
            // });

            knex.select("user_img_path", "user_img_name", "user_img_type", "user_img_size")
                .from("dashboard_users")
                .where("duserId", user_id)
                .orWhere("uuid", user_id)
                .limit(1)
                .asCallback((err, res) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (res && res.length > 0) {
                            res = res[0];
                            if (res) {
                                if (func.existsSync(res.img_path)) {
                                    fs.unlinkSync(res.img_path);
                                    callback()
                                } else {
                                    callback();
                                }
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    }
                })
        },
        (callback) => {
            // let strSQL = mysql.format('DELETE FROM dashboard_users WHERE (duserId=? OR uuid=?);', [user_id, user_id]);
            // console.log('strSQL: ', strSQL);
            // db.actionQuery(strSQL, function (err, res) {
            //     if (err) {
            //         callback(err, null);
            //     } else {
            //         callback(null, res);
            //     }
            // });

            knex.transaction(trx => {
                return trx("dashboard_users")
                    .where("duserId", user_id)
                    .orWhere("uuid", user_id)
                    .delete()
                    .asCallback(callback)
            })

            // knex.transaction((trx) => {
            //         knex("dashboard_users")
            //             .where("duserId", user_id)
            //             .orWhere("uuid", user_id)
            //             .delete()
            //             .then((resp) => {
            //                 return resp
            //             })
            //             .then(trx.commit)
            //             .catch(trx.rollback)
            //     })
            //     .then((res) => {
            //         callback(null, res);
            //     })
            //     .catch((err) => {
            //         callback(err, null);
            //     })
        }
    ], next);
};

exports.updateUserAccount = (user_id, data, next) => {
    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    // let strSQL = mysql.format('UPDATE dashboard_users SET username=?, password=? WHERE (duserId=? OR uuid=?);', [
    //     data.username, data.password, user_id, user_id
    // ]);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("dashboard_users")
                .update({
                    username: data.username,
                    password: data.password
                })
                .where("duserId", user_id)
                .orWhere("uuid", user_id)
                .then((resp) => {
                    return resp
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })
};

exports.updateUserPassword = (data, next) => {
    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    // let strSQL = mysql.format('UPDATE dashboard_users SET password=? WHERE (duserId=? OR uuid=?);', [
    //     data.password, data.uuid, data.uuid
    // ]);
    // console.log('strSQL: ', strSQL);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("dashboard_users")
                .update({
                    password: data.password
                })
                .where("duserId", data.uuid)
                .orWhere("uuid", data.uuid)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })

};

exports.updateUserEmail = (user_id, data, next) => {
    // let strSQL = mysql.format('UPDATE dashboard_users SET email=? WHERE (duserId=? OR uuid=?);', [
    //     data.email, user_id, user_id
    // ]);
    // console.log('updateUserEmail: ', strSQL);
    // db.actionQuery(strSQL, next);

    knex.transaction((trx) => {
            knex("dashboard_users")
                .update({
                    email: data.email
                })
                .where("duserId", user_id)
                .orWhere("uuid", user_id)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .then(trx.rollback)
        })
        .then((resp) => {
            next(null, resp);
        })
        .catch((err) => {
            next(err, err);
        })
};

exports.updateUser = (user_id, data, next) => {
    if (data.toDeleteFile) {
        async.waterfall([
            (callback) => {
                // let sql1 = mysql.format('SELECT user_img_path,user_img_name,user_img_type,user_img_size FROM dashboard_users WHERE (duserId=? OR uuid=?) LIMIT 1;', [_id, _id]);
                // console.log('sql1: ', sql1);
                // db.query(sql1, function (err, res) {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         if (res && res.length > 0) {
                //             res = res[0];
                //             if (res) {
                //                 if (func.existsSync(res.img_path)) {
                //                     fs.unlinkSync(res.img_path);
                //                     callback(null, {
                //                         img_name: data.img_name,
                //                         img_path: data.img_path,
                //                         img_type: data.img_type,
                //                         img_size: data.img_size
                //                     })
                //                 } else {
                //                     callback(null, {
                //                         img_name: data.img_name,
                //                         img_path: data.img_path,
                //                         img_type: data.img_type,
                //                         img_size: data.img_size
                //                     });
                //                 }
                //             } else {
                //                 callback(null, {
                //                     img_name: data.img_name,
                //                     img_path: data.img_path,
                //                     img_type: data.img_type,
                //                     img_size: data.img_size
                //                 });
                //             }
                //         } else {
                //             callback(null, {
                //                 img_name: data.img_name,
                //                 img_path: data.img_path,
                //                 img_type: data.img_type,
                //                 img_size: data.img_size
                //             });
                //         }
                //     }
                // });

                knex.select("user_img_path", "user_img_name", "user_img_type", "user_img_size")
                    .from("dashboard_users")
                    .where("duserId", _id)
                    .orWhere("uuid", _id)
                    .limit(1)
                    .asCallback((err, res) => {
                        if (err) {
                            next(err, null);
                        } else {
                            if (res && res.length > 0) {
                                res = res[0];
                                if (res) {
                                    if (func.existsSync(res.img_path)) {
                                        fs.unlinkSync(res.img_path);
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        })
                                    } else {
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        });
                                    }
                                } else {
                                    callback(null, {
                                        img_name: data.img_name,
                                        img_path: data.img_path,
                                        img_type: data.img_type,
                                        img_size: data.img_size
                                    });
                                }
                            } else {
                                callback(null, {
                                    img_name: data.img_name,
                                    img_path: data.img_path,
                                    img_type: data.img_type,
                                    img_size: data.img_size
                                });
                            }
                        }
                    })

            },
            (result, callback) => {
                let app = {
                    img_name: result.img_name,
                    img_path: result.img_path,
                    img_type: result.img_type,
                    img_size: result.img_size
                }

                // let strSQL = mysql.format('UPDATE dashboard_users SET firstname=?,lastname=?,phone=?,fax=?,user_img_path=?,user_img_name=?,user_img_type=?,user_img_size=? WHERE duserId=?;', [
                //     data.firstname, data.lastname, data.phone, data.fax, app.img_path, app.img_name, app.img_type, app.img_size, user_id
                // ]);
                // db.actionQuery(strSQL, function (err, res) {
                //     if (err) {
                //         callback(err, null);
                //     } else {
                //         callback(null, res);
                //     }
                // });

                knex.transaction((trx) => {
                        knex("dashboard_users")
                            .update({
                                firstname: data.firstname,
                                lastname: data.lastname,
                                phone: data.phone,
                                fax: data.fax,
                                user_img_path: app.img_path,
                                user_img_name: app.img_name,
                                user_img_type: app.img_type,
                                user_img_size: app.img_size
                            })
                            .where("duserId", user_id)
                            .then((resp) => {
                                return resp;
                            })
                            .then(trx.commit)
                            .catch(trx.rollback)
                    })
                    .then((res) => {
                        callback(null, res);
                    })
                    .catch((err) => {
                        callback(err, null);
                    })
            }
        ], next);
    } else {
        // let strSQL = mysql.format('UPDATE dashboard_users SET firstname=?,lastname=?,phone=?,fax=? WHERE duserId=?;', [
        //     data.firstname, data.lastname, data.phone, data.fax, user_id
        // ]);
        // db.actionQuery(strSQL, next);

        knex.transaction((trx) => {
                knex("dashboard_users")
                    .update({
                        firstname: data.firstname,
                        lastname: data.lastname,
                        phone: data.phone,
                        fax: data.fax
                    })
                    .where("duserId", user_id)
                    .then((resp) => {
                        return resp;
                    })
                    .then(trx.commit)
                    .catch(trx.rollback)
            })
            .then((resp) => {
                next(null, resp)
            })
            .catch((err) => {
                next(err, err)
            })
    }
};

exports.checkIfVerified = (hash_key, next) => {
    // var sqlStr = mysql.format('SELECT username,email,firstname,lastname,phone,fax,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
    //     'FROM dashboard_users WHERE verificationCode =? AND dateVerify IS NOT NULL LIMIT 1', [hash_key]);
    // db.query(sqlStr, next);

    knex.select("username", "email", "firstname", "lastname", "phone", "fax", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
        .from("dashboard_users")
        .whereNotNull("dateVerify")
        .andWhere("verificationCode", hash_key)
        .limit(1)
        .asCallback(next)
};

exports.verifyUser = (hash_key, next) => {
    // console.log('verifying user..')
    // var sqlStr = mysql.format('UPDATE dashboard_users SET dateVerify=NOW(), isVerify=1 WHERE verificationCode = ?;', [hash_key]);
    // db.query(sqlStr, (err, result) => {
    //     if (result) {
    //         var sqlStrs = mysql.format('SELECT username,email,firstname,lastname,phone,fax,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
    //             'FROM dashboard_users WHERE activation_hash =?;', [hash_key]);
    //         db.query(sqlStrs, function (err, response) {
    //             if (response && response.length > 0) {
    //                 response = response[0];
    //             }
    //             next(null, response);
    //         });
    //     }
    // });

    knex.transaction((trx) => {
            knex("dashboard_users")
                .update({
                    dateVerify: knex.raw("NOW()"),
                    isVerify: 1
                })
                .where("verificationCode", hash_key)
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((result) => {
            // let sqlStrs = mysql.format('SELECT username,email,firstname,lastname,phone,fax,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
            //     'FROM dashboard_users WHERE activation_hash =?;', [hash_key]);
            // db.query(sqlStrs, function (err, response) {
            //     if (response && response.length > 0) {
            //         response = response[0];
            //     }
            //     next(null, response);
            // });

            knex.select("username", "email", "firstname", "lastname", "phone", "fax", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
                .from("dashboard_users")
                .where("activation_hash", hash_key)
                .asCallback((err, response) => {
                    if (response && response.length > 0) {
                        response = response[0];
                    } else {}
                    next(null, response);
                })
        })
        .catch((err) => {
            next(err, err)
        })
};