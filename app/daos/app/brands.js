'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkBrandName = (data, next) => {
    knex("brand").where("name", data.name).first().asCallback(next);
};

exports.checkBrandForUpdate = (_id, data, next) => {
    knex("brand").where("name", data.name).andWhere("brandId", "<>", _id).first().asCallback(next);
};

exports.createBrand = (data, next) => {
    knex.transaction(trx => {
        return trx
            .insert({
                    name: data.name,
                    img_path: data.img_path,
                    img_name: data.img_name,
                    img_type: data.img_type,
                    img_size: data.img_size,
                    uuid: knex.raw("UUID()")
                },
                "brandId"
            )
            .into("brand")
            .asCallback(next);
    });
};

exports.getAllBrand = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex("brand").count("* as numRows").then(results => {
                numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                numPages = Math.ceil(numRows / numPerPage);

                return callback(null, numRows, numPages);
            }).catch(err => {
                return next(err, null);
            });
        },
        (numRows, numPages, callback) => {
            let strSQL = knex.select("*").from("brand");
            if (params.limit && params.startWith) {
                strSQL = strSQL.limit(numPerPage).offset(skip);
            } else if (params.limit) {
                strSQL = strSQL.limit(numPerPage)
            }
            strSQL.then(results => {
                return callback(null, numRows, numPages, results);
            }).catch(err => {
                return next(err, null);
            });
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getBrand = (_id, next) => {
    knex("brand").where("brandId", _id).orWhere("uuid", _id).first().asCallback(next);
};

exports.deleteBrand = (_id, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            knex.select("img_path", "img_name", "img_type", "img_size").from("brand")
                .where("brandId", _id).orWhere("uuid", _id)
                .first()
                .then(results => {
                    if (results) {
                        dbx.filesDelete({
                            path: results.img_name
                        }).then((resp) => {
                            console.log('Delete BrandImage: ', resp);
                            return callback();
                        }).catch((error) => {
                            console.error('DropBox error 4: ', error);
                            return callback();
                        });
                    } else {
                        return callback();
                    }
                }).catch(err => {
                    return next(err, null);
                });
        },
        (callback) => {
            //let strSQL = mysql.format('DELETE FROM brand WHERE (brandId=? OR uuid=?);', [_id, _id]);
            // knex("brand")
            //     .where("brandId", _id)
            //     .orWhere("uuid", _id)
            //     .delete()
            //     .then(results => {
            //         return callback(null, results);
            //     }).catch(err => {
            //         return callback(err, null);
            //     });
            knex.transaction(trx => {
                return trx("brand")
                    .where("brandId", _id)
                    .orWhere("uuid", _id)
                    .delete()
                    .asCallback(callback)
            })
        }
    ], next);
};

exports.updateBrand = (_id, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    if (data.toDeleteFile) {
        async.waterfall([
            (callback) => {
                let sql1 = mysql.format('SELECT img_path,img_name,img_type,img_size FROM brand WHERE (brandId=? OR uuid=?) LIMIT 1;', [_id, _id]);
                knex.select("img_path", "img_name", "img_type", "img_size").from("brand")
                    .where("brandId", "=", _id).orWhere("uuid", "=", _id)
                    .first()
                    .then(results => {
                        if (results) {
                            dbx.filesDelete({
                                path: results.img_name
                            }).then((resp) => {
                                console.log('updateBrand BrandImage: ', resp);
                                return callback(null, {
                                    img_name: data.img_name,
                                    img_path: data.img_path,
                                    img_type: data.img_type,
                                    img_size: data.img_size
                                })
                            }).catch((error) => {
                                console.error('DropBox error 4: ', error);
                                return callback(null, {
                                    img_name: data.img_name,
                                    img_path: data.img_path,
                                    img_type: data.img_type,
                                    img_size: data.img_size
                                });
                            });
                        } else {
                            return callback(null, {
                                img_name: data.img_name,
                                img_path: data.img_path,
                                img_type: data.img_type,
                                img_size: data.img_size
                            });
                        }
                    }).catch(err => {
                        return next(err, null);
                    });
            },
            (result, callback) => {
                const app = {
                    img_name: result.img_name,
                    img_path: result.img_path,
                    img_type: result.img_type,
                    img_size: result.img_size
                };

                knex.transaction(trx => {
                    return trx
                        .update(app)
                        .from("brand")
                        .where("brandId", "=", _id)
                        .orWhere("uuid", "=", _id)
                        .then(res => {
                            return callback(null, res);
                        }).catch(err => {
                            return callback(err, null);
                        });
                });
            }
        ], next);
    } else {
        knex.transaction(trx => {
            return trx
                .update({
                    name: data.name
                })
                .from("brand")
                .where("brandId", "=", _id)
                .orWhere("uuid", "=", _id)
                .asCallback(next);
        });
    }
};


exports.getAllBrandProducts = (_id, params, next) => {
    console.log('params: ', params);
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex("products AS p").count("* as numRows")
                .where("productBrandId", "=", _id)
                .then(results => {
                    numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);

                    return callback(null, numRows, numPages);
                }).catch(err => {
                    console.log("err: ", err);
                    return next(err, null);
                });
        },
        (numRows, numPages, callback) => {
            let strSQL = knex.select("p.*",
                    knex.raw('(SELECT IF(pr.rating,AVG(pr.rating),0) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rating'),
                    knex.raw('(SELECT COUNT(*) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rated'),
                    knex.raw('(SELECT COUNT(pl.productId) FROM product_like pl WHERE pl.productId = p.productId) AS liked'),
                    knex.raw("(SELECT COUNT(o.orderId) FROM  order_details o WHERE p.productId = o.productId) AS orderscount"))
                .from("products AS p")
                .where("productBrandId", "=", _id);

            if (params.limit && params.startWith) {
                strSQL = strSQL.groupBy("p.productId").limit(numPerPage).offset(skip);
            } else if (params.limit) {
                strSQL = strSQL.groupBy("p.productId").limit(numPerPage);
            } else {
                strSQL = strSQL.groupBy("p.productId").limit(numPerPage);
            }

            strSQL.then(results => {
                async.eachSeries(results, (item, cb) => {
                    async.waterfall([
                        (callback1) => {
                            knex("categories").where("categoryId", "=", item.productCategoryID).first()
                                .then(results => {
                                    if (results) {
                                        item.category = results;
                                    } else {
                                        item.category = {};
                                    }
                                    return callback1();
                                }).catch(err => {
                                    return next(err, null);
                                });
                        },
                        (callback1) => {
                            knex("subcategory").where("subcategoryId", "=", item.productSubCategoryId).first()
                                .then(results => {
                                    if (results) {
                                        item.subcategory = results;
                                    } else {
                                        item.subcategory = {};
                                    }
                                    return callback1();
                                }).catch(err => {
                                    return next(err, null);
                                });
                        },
                        (callback1) => {
                            knex("brand").where("brandId", "=", item.productBrandId).first()
                                .then(results => {
                                    if (results) {
                                        item.brand = results;
                                    } else {
                                        item.brand = {};
                                    }
                                    callback1();
                                }).catch(err => {
                                    return next(err, null);
                                });
                        },
                        (callback1) => {
                            knex("product_variation").where("productId", "=", item.productId)
                                .then(results => {
                                    item.variants = results;
                                    return callback1();
                                }).catch(err => {
                                    return next(err, null);
                                });
                        },
                        (callback1) => {
                            knex("product_wholesale").where("productId", "=", item.productId)
                                .then(results => {
                                    item.wholesale = results;
                                    return callback1();
                                }).catch(err => {
                                    console.log("err: ", err);
                                    return next(err, null);
                                });
                        },
                        (callback1) => {
                            knex.select("pse.*", "sc.name").from("product_shipping_fee AS pse")
                                .innerJoin("shipping_courier AS sc", "pse.shippingCourierId", "=", "sc.shippingCourierId")
                                .where("pse.productId", "=", item.productId)
                                .then(results => {
                                    item.shippingFee = results;
                                    return callback1();
                                }).catch(err => {
                                    return next(err, null);
                                });
                        },
                        (callback1) => {
                            knex("product_images").where("productId", "=", item.productId).first()
                                .then(results => {
                                    if (results) {
                                        item.image = results;
                                        return callback1();
                                    } else {
                                        item.image = {};
                                        return callback1();
                                    }
                                }).catch(err => {
                                    return next(err, null);
                                });
                        }
                    ], cb);
                }, () => {
                    callback(null, numRows, numPages, results);
                });
            }).catch(err => {
                return next(err, null);
            });
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};