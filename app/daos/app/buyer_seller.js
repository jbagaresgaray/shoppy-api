'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();
let func = require('../../../app/utils/functions');

const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
let async = require('async');
let fs = require('fs');
let _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.approveSellerApplication = (user_id, data, next) => {
    async.waterfall([
        (callback) => {
            let strSQL = mysql.format(`UPDATE users SET isSeller=1,sellerName = username, sellerApprovalDate=NOW() WHERE (userId=? OR uuid=?) AND sellerApprovalCode=?;`, [
                user_id, user_id, data.code
            ]);
            console.log('approveSellerApplication: ', strSQL);
            db.actionQuery(strSQL, (err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    callback();
                }
            });
        },
        (callback) => {
            // let strSQL = mysql.format('SELECT username,email,firstname,lastname,sellerApprovalDate,sellerApplicationDate,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid FROM users WHERE (uuid =? OR userId=?) LIMIT 1;', [
            //     user_id, user_id
            // ]);
            // db.query(strSQL, (err, response) => {
            //     if (err) {
            //         return next(err, null);
            //     } else {
            //         if (response && response.length > 0) {
            //             response = response[0];
            //         }
            //         callback(null, response);
            //     }
            // });

            knex.select("username", "email", "firstname", "lastname", "sellerApprovalDate", "sellerApplicationDate", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
                .from("users")
                .where("uuid", user_id)
                .orWhere("userId", user_id)
                .first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        callback(null, response);
                    }
                })
        }
    ], next);
};

exports.getAllApplicants = (next) => {
    console.log('getAllApplicants 2');
    // let strSQL = mysql.format('SELECT username,email,firstname,lastname,sellerApprovalDate,sellerApplicationDate,sellerApprovalCode,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
    //     'FROM users WHERE sellerApplicationDate IS NOT NULL AND sellerApprovalDate IS NULL;');
    // db.query(strSQL, next);

    knex.select("username", "email", "firstname", "lastname", "sellerApprovalDate", "sellerApplicationDate", "sellerApprovalCode", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid")
        .from("users")
        .whereNotNull("sellerApplicationDate")
        .andWhere(function () {
            this.whereNull("sellerApprovalDate")
        })
        .asCallback(next)
};

exports.getAllSellers = (next) => {
    // let strSQL = mysql.format('SELECT username,email,firstname,lastname,sellerApprovalDate,sellerApplicationDate,sellerApprovalCode,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,uuid ' +
    //     'FROM users WHERE (isSeller = 1 AND sellerApprovalDate IS NOT NULL);');
    // db.query(strSQL, next);

    knex.select(
            "userId", "username", "email", "firstname", "lastname", "sellerApprovalDate", 
            "sellerApplicationDate", "sellerApprovalCode", "user_img_path AS img_path", 
            "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "uuid",
            knex.raw("(select IF(count(*) > 0, 1,0) from preferred_seller where sellerId = userId) as isPreferred")
        )
        .from("users")
        .where("isSeller", 1)
        .andWhere(function () {
            this.whereNotNull("sellerApprovalDate")
        })
        .asCallback(next)
};

exports.getAllBuyerUser = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // var strSQL = mysql.format('SELECT COUNT(*) AS numRows FROM users;');
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });

            knex
                .count("* as numRows")
                .from("users")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                })
        },
        (numRows, numPages, callback) => {
            // var strSQL = mysql.format('SELECT username,email,firstname,lastname,phone,fax,user_img_path AS img_path, user_img_name AS img_name, user_img_type AS img_type, user_img_size AS img_size,isVerify,uuid FROM users');
            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' LIMIT ' + limit
            // } else if (params.limit) {
            //     strSQL = strSQL + ' LIMIT ' + numPerPage
            // }
            // strSQL = strSQL + ';';

            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, numRows, numPages, results);
            //     }
            // });

            let sQl = knex.select("username", "email", "firstname", "lastname", "phone", "fax", "user_img_path AS img_path", "user_img_name AS img_name", "user_img_type AS img_type", "user_img_size AS img_size", "isVerify", "uuid").from("users");
            if (params.limit && params.startWith) {
                sQl = sQl.limit(limit)
            } else if (params.limit) {
                sQl = sQl.limit(numPerPage)
            }
            sQl.asCallback((err, results) => {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, numRows, numPages, results);
                }
            })
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};


exports.officialShopSubmision = (shopId, data, next) => {
    let sSQLOjb = {
        shop_name: data.shop_name,
        shop_email: data.shop_email,
        shop_mobileno: data.shop_mobileno,
        shop_website: data.shop_website,
        owner_fname: data.owner_fname,
        owner_mname: data.owner_mname,
        owner_lname: data.owner_lname,
        owner_email: data.owner_email,
        shop_cor_number: data.shop_cor_number,
        shop_cor_file: data.shop_cor_file,
        shop_cor_filename: data.shop_cor_filename,
        shop_distributor_license_number: data.shop_distributor_license_number,
        shop_distributor_license: data.shop_distributor_license,
        shop_distributor_license_filename: data.shop_distributor_license_filename,
        shop_official_logo: data.shop_official_logo,
        shop_official_logo_filename: data.shop_official_logo_filename
    };

    // let strSQL = mysql.format('UPDATE official_shop SET ? WHERE official_shop_id=? AND userId=?', [sSQLOjb, shopId, userId]);
    // console.log('strSQL: ', strSQL);
    // db.actionQuery(strSQL, next);

    knex.transaction(trx => {
        return trx("official_shop")
            .update(sSQLOjb)
            .where("official_shop_id", shopId)
            .andWhere("userId", userId)
            .asCallback(next)
    })
};

/**
 * This is for buyer ratings from sellers perspective
 */
exports.rateBuyer = (buyerId, data, next) => {
    knex.transaction(trx => {
        return trx
            .insert({
                    rating: data.rating,
                    comment: data.comment,
                    orderId: data.orderId,
                    buyer_userId: buyerId,
                    seller_userId: data.seller_userId,
                    ratedOn: knex.fn.now(),
                    uuid: knex.raw("UUID()")
                },
                "buyerRatingId"
            )
            .into("buyer_rating")
            .asCallback(next);
    });
};

exports.getBuyerRatings = (buyerId, params, next) => {
    knex
        .avg({
            total_rate: 'rating'
        })
        .from("buyer_rating")
        .where("buyer_userId", "=", buyerId)
        .asCallback((err, resp) => {
            if (err) {
                cb(err, resp);
            } else {
                let rating_as_buyer = resp[0].total_rate;
                knex
                    .select("*")
                    .from("product_rating")
                    .where("userId", "=", buyerId)
                    .asCallback((e, r) => {
                        /**
                         * TO Review. must include buyer as a seller in calculation
                         */
                        if (r.length) {
                            resp[0].isAlsoAseller = true
                            knex.avg({
                                    'average': 'rating'
                                })
                                .from('product_rating')
                                .where('userId', "=", buyerId)
                                .asCallback((e2, r2) => {
                                    let rating_as_seller = r2[0].average;
                                    const arrSum = arr => arr.reduce((a, b) => a + b, 0);
                                    let uniqueVal = _.compact([rating_as_seller, rating_as_buyer]);
                                    let averageOfBoth = arrSum(uniqueVal) / uniqueVal.length;
                                    next(e2, {
                                        total_rate: averageOfBoth,
                                        averageAsBuyer: rating_as_buyer,
                                        averageAsSeller: rating_as_seller
                                    });
                                });
                        } else {
                            resp[0].isAlsoAseller = false
                            next(err, resp[0]);
                        }
                    });
            }
        });
};
/**
 * End of buyer ratings from sellers perspective
 */


/**
 * This is for shop ratings from buyers perspective
 */
exports.rateShop = (userId, data, next) => {
    knex.transaction(trx => {
        return trx
            .insert({
                    rating: data.rating,
                    comment: data.comment,
                    orderId: data.orderId,
                    userId: userId,
                    productId: data.productId,
                    ratedOn: knex.fn.now(),
                    uuid: knex.raw("UUID()")
                },
                "productRatingId"
            )
            .into("product_rating")
            .asCallback(next);
    });
};

exports.getShopRatings = (userId, next) => {
    async.waterfall([
        (cb) => {
            knex
                .select("*")
                .from("products")
                .where("userId", "=", userId)
                .asCallback(cb);
        },
        (products, cb) => {
            let x = _.map(products, function (object) {
                return _.pick(object, 'productId').productId;
            });
            knex.avg({
                    'average': 'rating'
                })
                .from('product_rating')
                .whereIn('productId', x)
                .asCallback(cb)
        }
    ], next)
};
/**
 * End of shop ratings from buyers perspective
 */


exports.buyerRefund = (userId, orderId, productId, next) => {
    let x = {
        userId,
        orderId,
        productId
    };
    next(null, x);
    // async.waterfall([
    //     (cb) => {
    //         knex
    //             .update("*")
    //             .from("products")
    //             .where("userId", "=", userId)
    //             .asCallback(cb);
    //     },
    //     (products, cb) => {
    //         let x = _.map(products, function (object) {
    //             return _.pick(object, 'productId').productId;
    //         });
    //         knex.avg({
    //                 'average': 'rating'
    //             })
    //             .from('product_rating')
    //             .whereIn('productId', x)
    //             .asCallback(cb)
    //     }
    // ], next)
};

exports.getPreferredSeller = (next) => {
    knex
        .select("u.username", "u.email", "u.firstname", "u.lastname", "u.gender", "u.birthday", "u.phone",
                "u.user_img_path", "u.user_banner_path", "u.sellerName")
        .from("users as u")
        .whereIn("u.userId", knex.raw("SELECT ps.sellerId FROM preferred_seller ps"))
        .asCallback(next)
};

exports.getPreferredSellerById = (preferredSellerId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("*")
                .from("preferred_seller")
                .where("preferred_sellerId", preferredSellerId)
                .orWhere("uuid", preferredSellerId)
                .first()
                .asCallback((err, resp) => {
                    if(err) {
                        next(err, null)
                    } else {
                        callback(null, resp && resp[0] ? resp[0].preferred_sellerId : resp)
                    }
                })
        },
        (resp, callback) => {
            knex
                .select("u.username", "u.email", "u.firstname", "u.lastname", "u.gender", "u.birthday", "u.phone",
                        "u.user_img_path", "u.user_banner_path", "u.sellerName")
                .from("users as u")
                .whereIn("u.userId", knex.raw("SELECT ps.sellerId FROM preferred_seller ps where ps.preferred_sellerId = ?", resp.preferred_sellerId))
                .limit(1)
                .asCallback(callback)
        }
    ], next)
};

exports.createPreferredSeller = (data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("*")
            .from("users")
            .where("uuid", data.sellerId)
            .first()
            .asCallback((err, resp) => {
                if(err){
                    next(err, null)
                } else {
                    return callback(null, resp.userId)
                }
            })
        },
        (userId, callback) => {
            knex.transaction(trx => {
                return trx
                    .insert({
                        sellerId : userId,
                        preferredOn : knex.raw("NOW()"),
                        date_created : knex.raw("NOW()"),
                        uuid : knex.raw("UUID()"),
                        approvedBy : null,
                        approvedOn: null
                    })
                    .into("preferred_seller")
                    .asCallback((err, resp) => {
                        if(err) {
                            callback(err, null)
                        } else {
                            callback(null, resp && resp[0] ? resp[0] : resp)
                        }
                    })
            })
        }
    ], next)
};

exports.updatePreferredSeller = (id, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("*")
            .from("users")
            .where("uuid", data.sellerId)
            .first()
            .asCallback((err, resp) => {
                if(err){
                    next(err, null)
                } else {
                    return callback(null, resp.userId)
                }
            })
        },
        (userId, callback) => {
            knex.transaction(trx => {
                return trx("preferred_seller")
                    .update({
                        sellerId : userId,
                        preferredOn : data.preferredOn,
                        approvedBy : data.approvedBy,
                        approvedOn: data.approvedOn
                    })
                    .where("preferred_sellerId", id)
                    .asCallback(callback)
            })
        }
    ], next)
};

exports.deletePreferredSeller = (id, next) => {
    knex.transaction(trx => {
        return trx("preferred_seller")
            .where("preferred_sellerId", id)
            .delete()
            .asCallback(callback)
    })
};