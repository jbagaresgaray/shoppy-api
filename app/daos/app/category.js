'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const fs = require('fs');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkCategoryName = (data, next) => {
    // let strSQL = mysql.format('SELECT * FROM categories WHERE name = ? LIMIT 1;', [data.name]);
    // db.query(strSQL, next);

    knex.select("*").from("categories").where("name", data.name).limit(1).asCallback(next)
};

exports.checkCategoryForUpdate = (_id, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM categories WHERE name = ? AND categoryId <> ? LIMIT 1;', [data.name, _id]);
    // db.query(strSQL, next);

    knex.select("*").from("categories").where("name", data.name).andWhere("categoryId", "<>", _id).asCallback(next);
};

exports.createCategory = (data, next) => {
    // let strSQL = mysql.format('INSERT INTO categories(name,img_path,img_name,img_type,img_size,uuid) VALUES (?,?,?,?,?,UUID());', [
    //     data.name, data.img_path, data.img_name, data.img_type, data.img_size
    // ]);
    // db.insertWithId(strSQL, next);

    knex.transaction((trx) => {
            knex("categories")
                .insert({
                    name: data.name,
                    img_path: data.img_path,
                    img_name: data.img_name,
                    img_type: data.img_type,
                    img_size: data.img_size,
                    uuid: knex.raw("UUID()")
                })
                .then((resp) => {
                    return resp
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp && resp.length > 0 ? resp[0] : resp)
        })
        .catch((err) => {
            next(null, err)
        })
};

exports.getAllCatetories = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // let strSQL = mysql.format('SELECT COUNT(*) AS numRows FROM categories;');
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });
            knex.count("* as numRows").from("categories")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                })
        },
        (numRows, numPages, callback) => {
            // let strSQL = mysql.format('SELECT * FROM categories');
            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' LIMIT ' + limit
            // } else if (params.limit) {
            //     strSQL = strSQL + ' LIMIT ' + numPerPage
            // }
            // strSQL = strSQL + ';';

            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, numRows, numPages, results);
            //     }
            // });

            let sQl = knex.select("*").from("categories");
            if (params.limit && params.startWith) {
                sQl = sQl.limit(limit)
            } else if (params.limit) {
                sQl = sQl.limit(numPerPage)
            }
            sQl.asCallback((err, results) => {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, numRows, numPages, results);
                }
            })
        },
        (numRows, numPages, categories, callback) => {
            // let strSQL = mysql.format('SELECT * FROM subcategory');
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         _.each(categories, (row) => {
            //             let res = _.filter(results, {
            //                 'categoryId': row.categoryId
            //             });
            //             if (res) {
            //                 row.subcategory = res;
            //             } else {
            //                 row.subcategory = {};
            //             }
            //         });
            //         callback(null, numRows, numPages, categories);
            //     }
            // });

            knex.select("*").from("subcategory")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        _.each(categories, (row) => {
                            let res = _.filter(results, {
                                'categoryId': row.categoryId
                            });
                            if (res) {
                                row.subcategory = res;
                            } else {
                                row.subcategory = {};
                            }
                        });
                        callback(null, numRows, numPages, categories);
                    }
                })
        },
        (numRows, numPages, categories, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: categories,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: categories,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: categories,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getCategory = (_id, next) => {
    // let strSQL = mysql.format('SELECT * FROM categories WHERE (categoryId=? OR uuid=?) LIMIT 1;', [_id, _id]);
    // db.query(strSQL, next);

    knex.select("*").from("categories").where("categoryId", _id).orWhere("uuid", _id).limit(1)
        .asCallback(next)
};

exports.deleteCategory = (_id, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            // let sql1 = mysql.format('SELECT img_path,img_name,img_type,img_size FROM categories WHERE (categoryId=? OR uuid=?)  LIMIT 1;', [_id, _id]);
            // console.log('sql1: ', sql1);
            // db.query(sql1, function (err, res) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         if (res && res.length > 0) {
            //             res = res[0];
            //             dbx.filesDelete({
            //                 path: res.img_name
            //             }).then((resp) => {
            //                 console.log('Delete CategoryImage: ', resp);
            //                 callback();
            //             }).catch((error) => {
            //                 console.error('DropBox error 4: ', error);
            //                 callback();
            //             });
            //         } else {
            //             callback();
            //         }
            //     }
            // });

            knex.select("img_path", "img_name", "img_type", "img_size")
                .from("categories")
                .where("categoryId", _id)
                .orWhere("uuid", _id)
                .asCallback((err, res) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (res && res.length > 0) {
                            res = res[0];
                            dbx.filesDelete({
                                path: res.img_name
                            }).then((resp) => {
                                console.log('Delete CategoryImage: ', resp);
                                callback();
                            }).catch((error) => {
                                console.error('DropBox error 4: ', error);
                                callback();
                            });
                        } else {
                            callback();
                        }
                    }
                })
        },
        (callback) => {
            // let strSQL = mysql.format('DELETE FROM categories WHERE (categoryId=? OR uuid=?);', [_id, _id]);
            // console.log('strSQL: ', strSQL);
            // db.actionQuery(strSQL, function (err, res) {
            //     if (err) {
            //         callback(err, null);
            //     } else {
            //         callback(null, res);
            //     }
            // });

            knex.transaction(trx => {
                return trx("categories")
                    .where("categoryId", _id)
                    .orWhere("uuid", _id)
                    .delete()
                    .asCallback(callback)
            })
        }
    ], next);
};

exports.updateCategory = (_id, data, next) => {
    require('isomorphic-fetch');
    var Dropbox = require('dropbox').Dropbox;
    var dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    if (data.toDeleteFile) {
        async.waterfall([
            (callback) => {
                // let sql1 = mysql.format('SELECT img_path,img_name,img_type,img_size FROM categories WHERE (categoryId=? OR uuid=?) LIMIT 1;', [_id, _id]);
                // console.log('sql1: ', sql1);
                // db.query(sql1, function (err, res) {
                //     if (err) {
                //         next(err, null);
                //     } else {
                //         if (res && res.length > 0) {
                //             res = res[0];
                //             if (res) {
                //                 dbx.filesDelete({
                //                     path: res.img_name
                //                 }).then((resp) => {
                //                     console.log('updateCategory CategoryImage: ', resp);
                //                     callback(null, {
                //                         img_name: data.img_name,
                //                         img_path: data.img_path,
                //                         img_type: data.img_type,
                //                         img_size: data.img_size
                //                     })
                //                 }).catch((error) => {
                //                     console.error('DropBox error 4: ', error);
                //                     callback(null, {
                //                         img_name: data.img_name,
                //                         img_path: data.img_path,
                //                         img_type: data.img_type,
                //                         img_size: data.img_size
                //                     })
                //                 });
                //             } else {
                //                 callback(null, {
                //                     img_name: data.img_name,
                //                     img_path: data.img_path,
                //                     img_type: data.img_type,
                //                     img_size: data.img_size
                //                 });
                //             }
                //         } else {
                //             callback(null, {
                //                 img_name: data.img_name,
                //                 img_path: data.img_path,
                //                 img_type: data.img_type,
                //                 img_size: data.img_size
                //             });
                //         }
                //     }
                // });

                knex.select("img_path", "img_name", "img_type", "img_size")
                    .from("categories")
                    .where("categoryId", _id)
                    .orWhere("uuid", _id)
                    .limit(1)
                    .asCallback((err, res) => {
                        if (err) {
                            next(err, null);
                        } else {
                            if (res && res.length > 0) {
                                res = res[0];
                                if (res) {
                                    dbx.filesDelete({
                                        path: res.img_name
                                    }).then((resp) => {
                                        console.log('updateCategory CategoryImage: ', resp);
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        })
                                    }).catch((error) => {
                                        console.error('DropBox error 4: ', error);
                                        callback(null, {
                                            img_name: data.img_name,
                                            img_path: data.img_path,
                                            img_type: data.img_type,
                                            img_size: data.img_size
                                        })
                                    });
                                } else {
                                    callback(null, {
                                        img_name: data.img_name,
                                        img_path: data.img_path,
                                        img_type: data.img_type,
                                        img_size: data.img_size
                                    });
                                }
                            } else {
                                callback(null, {
                                    img_name: data.img_name,
                                    img_path: data.img_path,
                                    img_type: data.img_type,
                                    img_size: data.img_size
                                });
                            }
                        }
                    })
            },
            (result, callback) => {
                let app = {
                    img_name: result.img_name,
                    img_path: result.img_path,
                    img_type: result.img_type,
                    img_size: result.img_size
                }
                // let strSQL = mysql.format('UPDATE categories SET name=?,img_path=?,img_name=?,img_type=?,img_size=? WHERE (categoryId=? OR uuid=?);', [
                //     data.name, app.img_path, app.img_name, app.img_type, app.img_size, _id, _id
                // ]);
                // db.actionQuery(strSQL, function (err, res) {
                //     if (err) {
                //         callback(err, null);
                //     } else {
                //         callback(null, res);
                //     }
                // });

                knex.transaction((trx) => {
                        knex("categories")
                            .update({
                                name: data.name,
                                img_path: app.img_path,
                                img_name: app.img_name,
                                img_type: app.img_type,
                                img_size: app.img_size
                            })
                            .where("categoryId", _id)
                            .orWhere("uuid", _id)
                            .then((ersp) => {
                                return resp
                            })
                            .then(trx.commit)
                            .catch(trx.rollback)
                    })
                    .then((resp) => {
                        callback(null, res);
                    })
                    .then((err) => {
                        callback(err, null);
                    })
            }
        ], next);
    } else {
        // let strSQL = mysql.format('UPDATE categories SET name=? WHERE (categoryId=? OR uuid=?);', [
        //     data.name, _id, _id
        // ]);
        // db.actionQuery(strSQL, next);
        knex.transaction((trx) => {
                knex("categories")
                    .update({
                        name: data.name
                    })
                    .where("categoryId", _id)
                    .orWhere("uuid", _id)
                    .then((resp) => {
                        return resp
                    })
                    .then(trx.commit)
                    .catch(trx.rollback)
            })
            .then((resp) => {
                next(null, resp)
            })
            .catch((err) => {
                next(err, err)
            })
    }
};

exports.getAllCategoryProducts = (_id, params, next) => {
    console.log('params: ', params);
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex.count("* as numRows")
                .from("products as p")
                .where("p.productCategoryID", _id)
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                })
        },
        (numRows, numPages, callback) => {
            let sQl = knex.select(
                    "p.*",
                    knex.raw('(SELECT IF(pr.rating,AVG(pr.rating),0) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rating'),
                    knex.raw('(SELECT COUNT(*) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rated'),
                    knex.raw('(SELECT COUNT(pl.productId) FROM product_like pl WHERE pl.productId = p.productId) AS liked'),
                    knex.raw("(SELECT COUNT(o.orderId) FROM order_details o WHERE p.productId = o.productId) AS orderscount")
                )
                .from("products as p")
                .where("productCategoryID", _id);
            if (params.limit && params.startWith) {
                sQl = sQl.groupBy("p.productId").limit(limit);
            } else if (params.limit) {
                sQl = sQl.groupBy("p.productId").limit(numPerPage);
            } else {
                sQl = sQl.groupBy("p.productId").limit(numPerPage);
            }

            sQl.asCallback((err, results) => {
                if (err) {
                    next(err, null);
                } else {
                    async.eachSeries(results, (item, cb) => {
                        async.waterfall([
                            (callback1) => {
                                // let strSQL = mysql.format('SELECT * FROM categories WHERE categoryId =? LIMIT 1;', [item.productCategoryID]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             item.category = response[0];
                                //         } else {
                                //             item.category = {};
                                //         }
                                //         callback1();
                                //     }
                                // });

                                knex.select("*").from("categories").where("categoryId", item.productCategoryID).limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                item.category = response[0];
                                            } else {
                                                item.category = {};
                                            }
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // let strSQL = mysql.format('SELECT * FROM subcategory WHERE subcategoryId = ?;', [item.productSubCategoryId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             item.subcategory = response[0];
                                //         } else {
                                //             item.subcategory = {};
                                //         }
                                //         callback1();
                                //     }
                                // });

                                knex("subcategory").where("subcategoryId", item.productSubCategoryId).limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                item.subcategory = response[0];
                                            } else {
                                                item.subcategory = {};
                                            }
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // let strSQL = mysql.format('SELECT * FROM brand WHERE brandId =?;', [item.productBrandId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             item.brand = response[0];
                                //         } else {
                                //             item.brand = {};
                                //         }
                                //         callback1();
                                //     }
                                // });

                                knex.select("*").from("brand").where("brandId", item.productBrandId).limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                item.brand = response[0];
                                            } else {
                                                item.brand = {};
                                            }
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // let strSQL = mysql.format('SELECT * FROM product_variation WHERE productId=?;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         item.variants = response;
                                //         callback1();
                                //     }
                                // });

                                knex.select("*").from("product_variation").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            item.variants = response;
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // let strSQL = mysql.format('SELECT * FROM product_wholesale WHERE productId=?;', [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         item.wholesale = response;
                                //         callback1();
                                //     }
                                // });

                                knex.select("*").from("product_wholesale").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            item.wholesale = response;
                                            callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                // let strSQL = mysql.format(`SELECT pse.*,sc.name 
                                //                                 FROM product_shipping_fee pse 
                                //                                 INNER JOIN shipping_courier sc 
                                //                                     ON pse.shippingCourierId = sc.shippingCourierId 
                                //                                 WHERE pse.productId=?;`, [item.productId]);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         item.shippingFee = response;
                                //         callback1();
                                //     }
                                // });

                                knex.select("pse.*", "sc.name")
                                    .from("product_shipping_fee as pse")
                                    .innerJoin("shipping_courier as sc", "pse.shippingCourierId", "sc.shippingCourierId")
                                    .where("pse.productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            item.shippingFee = response;
                                            callback1();
                                        }
                                    })

                            },
                            (callback1) => {
                                // let strSQL = mysql.format(`SELECT * FROM product_images WHERE productId=? LIMIT 1;`, [item.productId]);
                                // console.log('strSQL: ', strSQL);
                                // db.query(strSQL, (err, response) => {
                                //     if (err) {
                                //         next(err, null);
                                //     } else {
                                //         if (response && response.length > 0) {
                                //             let img = response[0];
                                //             item.image = img;
                                //             callback1();
                                //         } else {
                                //             item.image = {};
                                //             callback1();
                                //         }
                                //     }
                                // });

                                knex.select("*").from("product_images").where("productId", item.productId).limit(1)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            next(err, null);
                                        } else {
                                            if (response && response.length > 0) {
                                                let img = response[0];
                                                item.image = img;
                                                callback1();
                                            } else {
                                                item.image = {};
                                                callback1();
                                            }
                                        }
                                    })
                            }
                        ], cb);
                    }, () => {
                        callback(null, numRows, numPages, results);
                    });
                }
            });

            // let strSQL = mysql.format('SELECT p.*,IF(pr.rating,AVG(pr.rating),0)  AS rating, ' +
            //     '(SELECT COUNT(o.orderId) FROM order_details o WHERE p.productId = o.productId) AS orderscount ' +
            //     'FROM products p LEFT JOIN product_rating pr ON p.productId = pr.productId WHERE productCategoryID = ? ', [_id]);
            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' GROUP BY p.productId LIMIT ' + limit
            //     console.log('strSQL1: ', strSQL);
            // } else if (params.limit) {
            //     strSQL = strSQL + ' GROUP BY p.productId LIMIT ' + numPerPage
            //     console.log('strSQL2: ', strSQL);
            // } else {
            //     strSQL = strSQL + ' GROUP BY p.productId LIMIT ' + numPerPage
            // }

            // strSQL = strSQL + ';';
            // console.log('strSQL: ', strSQL);
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         async.eachSeries(results, (item, cb) => {
            //             async.waterfall([
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM categories WHERE categoryId =? LIMIT 1;', [item.productCategoryID]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             if (response && response.length > 0) {
            //                                 item.category = response[0];
            //                             } else {
            //                                 item.category = {};
            //                             }
            //                             callback1();
            //                         }
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM subcategory WHERE subcategoryId = ?;', [item.productSubCategoryId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             if (response && response.length > 0) {
            //                                 item.subcategory = response[0];
            //                             } else {
            //                                 item.subcategory = {};
            //                             }
            //                             callback1();
            //                         }
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM brand WHERE brandId =?;', [item.productBrandId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             if (response && response.length > 0) {
            //                                 item.brand = response[0];
            //                             } else {
            //                                 item.brand = {};
            //                             }
            //                             callback1();
            //                         }
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM product_variation WHERE productId=?;', [item.productId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             item.variants = response;
            //                             callback1();
            //                         }
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM product_wholesale WHERE productId=?;', [item.productId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             item.wholesale = response;
            //                             callback1();
            //                         }
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT pse.*,sc.name FROM product_shipping_fee pse INNER JOIN shipping_courier sc ON pse.shippingCourierId = sc.shippingCourierId WHERE pse.productId=?;', [item.productId]);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             item.shippingFee = response;
            //                             callback1();
            //                         }
            //                     });
            //                 },
            //                 (callback1) => {
            //                     let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
            //                     console.log('strSQL: ', strSQL);
            //                     db.query(strSQL, (err, response) => {
            //                         if (err) {
            //                             next(err, null);
            //                         } else {
            //                             if (response && response.length > 0) {
            //                                 let img = response[0];
            //                                 item.image = img;
            //                                 callback1();
            //                             } else {
            //                                 item.image = {};
            //                                 callback1();
            //                             }
            //                         }
            //                     });
            //                 }
            //             ], cb);
            //         }, () => {
            //             callback(null, numRows, numPages, results);
            //         });
            //     }
            // });
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};