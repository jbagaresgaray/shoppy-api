'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

let async = require('async');
const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});



exports.checkPaymentProvider = (data, next) => {
    // let strSQL = mysql.format('SELECT * FROM payment_providers WHERE name = ? LIMIT 1;', [data.name]);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("payment_providers")
        .where("name", "=", data.name)
        .limit(1)
        .asCallback(next)
};

exports.checkPaymentProviderforUpdate = (_id, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM payment_providers WHERE name = ? AND paymentProvidersId <> ? LIMIT 1;', [data.name, _id]);
    // db.query(strSQL, next);

    knex
        .select("*")
        .from("payment_providers")
        .where("name", "=", data.name)
        .andWhere("paymentProvidersId", "<>", _id)
        .limit(1)
        .asCallback()
};

exports.createPaymentProvider = (data, next) => {
    // let strSQL = mysql.format('INSERT INTO payment_providers(name,uuid) VALUES (?,UUID());', [
    //     data.name
    // ]);
    // db.insertWithId(strSQL, next);

    knex("payment_providers")
        .insert({
            name: data.name,
            uuid: kknex.raw("UUID()")
        })
        .asCallback((err, resp) => {
            if (err) {
                next(err, resp)
            } else {
                next(null, resp[0])
            }
        })
};

exports.getAllPaymentProvider = (params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            // var strSQL = mysql.format('SELECT COUNT(*) AS numRows FROM payment_providers;');
            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
            //         numPages = Math.ceil(numRows / numPerPage);
            //         callback(null, numRows, numPages);
            //     }
            // });
            knex
                .count("* as numRows")
                .from("payment_providers")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        callback(null, numRows, numPages);
                    }
                })
        },
        (numRows, numPages, callback) => {
            // var strSQL = mysql.format('SELECT * FROM payment_providers;');

            // if (params.limit && params.startWith) {
            //     strSQL = strSQL + ' LIMIT ' + limit
            // } else if (params.limit) {
            //     strSQL = strSQL + ' LIMIT ' + numPerPage
            // }
            // strSQL = strSQL + ';';

            // db.query(strSQL, function (err, results) {
            //     if (err) {
            //         next(err, null);
            //     } else {
            //         callback(null, numRows, numPages, results);
            //     }
            // });

            let sQl = knex.select("*").from("payment_providers");

            if (params.limit && params.startWith) {
                sQl.limit(limit)
            } else if (params.limit) {
                sQl.limit(numPerPage)
            }
            sQl.asCallback((err, results) => {
                if (err) {
                    next(err, null);
                } else {
                    callback(null, numRows, numPages, results);
                }
            })
        },
        (numRows, numPages, banks, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: banks,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getPaymentProvider = (_id, next) => {
    // let strSQL = mysql.format('SELECT * FROM payment_providers WHERE (paymentProvidersId=? OR uuid=?) LIMIT 1;', [_id, _id]);
    // db.query(strSQL, next);

    knex.select("*")
        .from("payment_providers")
        .where("paymentProvidersId", "=", _id)
        .orWhere("uuid", "=", _id)
        .asCallback(next)
};

exports.deletePaymentProvider = (_id, next) => {
    // let strSQL = mysql.format('DELETE FROM payment_providers WHERE (paymentProvidersId=? OR uuid=?);', [_id, _id]);
    // db.actionQuery(strSQL, next);

    knex.transaction(trx => {
        return trx("payment_providers")
            .where("paymentProvidersId", _id)
            .orWhere("uuid", _id)
            .asCallback(next)
    })
};

exports.updatePaymentProvider = (_id, data, next) => {
    // let strSQL = mysql.format('UPDATE payment_providers SET name=? WHERE (paymentProvidersId=? OR uuid=?);', [
    //     data.name, _id, _id
    // ]);
    // db.actionQuery(strSQL, next);

    knex("payment_providers")
        .update({
            name: data.name
        })
        .where("paymentProvidersId", "=", _id)
        .orWhere("uuid", "=", _id)
        .asCallback(next)

};