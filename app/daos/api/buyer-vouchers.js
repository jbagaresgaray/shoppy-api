'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.getBuyerVouchers = (user, params, next) => {
    async.waterfall([
        (cb)=>{
            knex.select("userId").from("users").where("uuid", user.uuid).first().asCallback(cb)
        },
        (user, cb) => {
            knex.select("*").from("user_vouchers").where("userId",user.userId).asCallback(cb)
        }
    ], next)
}

exports.claimVoucher = (user, data, next) => {
    async.waterfall([
        (cb) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first().asCallback(cb)
        },
        (user, cb) => {
            knex.transaction((trx)=>{
                return trx("user_vouchers")
                    .insert({
                        userId: user.userId,
                        voucherId: data.voucherId,
                        claimedOn: '',
                        isUsed: 0,
                        uuid: knex.raw("UUID()")
                    })
                    .asCallback((err, resp) => {
                        if(err) {
                            return next(err, null)
                        } else {
                            return cb(null, resp && resp.length > 0 ? resp[0] : resp)
                        }
                    })
            })
        }
    ], next)
}

//This is the method to be called right after successfull order was made using any vouchers
exports.useVoucher = (userId, data, next) => {
    async.waterfall([
        (cb) => {
            knex.select("userId").from("users").where("uuid", userId).orWhere("userId", userId).first().asCallback(cb)
        },
        (user, cb) => {
            knex.transaction((trx)=>{
                return trx("user_vouchers")
                    .update({
                        userId: user.userId,
                        voucherId: data.voucherId,
                        claimedOn: knex.raw("NOW()"),
                        isUsed: data.isUsed //should be 1 by default.
                    })
                    .asCallback(cb)
            })
        }
    ], next)
}