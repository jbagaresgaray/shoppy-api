'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const func = require('../../../app/utils/functions');
const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
const async = require('async');
const fs = require('fs');
const _ = require('lodash');
const constant = require("../../../config/constants");
const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

const isSellerFirstOrder = (user, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId", "email", "phone").from("users").where(sql => {
                sql.where("userId", user.uuid).orWhere("uuid", user.uuid);
            }).first().asCallback((err, user) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, user);
                }
            });
        },
        (_user, callback) => {
            knex.select("orderId").from("orders").where("sellerId", _user.userId)
                .asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders);
                    }
                });
        }
    ], next);
};
exports.isSellerFirstOrder = isSellerFirstOrder;

exports.createOrder = (user, data, next) => {
    const batchNum = func.generateOrderNumber(15);
    const orderObj = {
        batchId: batchNum,
        orderDateTime: data.orderDateTime,
        orderShipName: data.orderShipName,
        orderShipAddress: data.orderShipAddress,
        orderShipCity: data.orderShipCity,
        orderShipState: data.orderShipState,
        orderShipZipCode: data.orderShipZipCode,
        orderShipCountry: data.orderShipCountry,
        orderShipSuburb: data.orderShipSuburb,
        orderPhone: data.orderPhone,
        orderFax: data.orderFax,
        orderEmail: data.orderEmail,
        modeofPayment: data.modePayment
    };
    let notifications = [];
    let orderIds = [];

    async.waterfall([
        (callbackWater) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", user.uuid).orWhere("uuid", user.uuid);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callbackWater(null, user);
                    }
                });
        },
        (currentUser, callbackWater) => {
            async.eachSeries(data.cartOrder, (row, callback) => {
                let storesPlayerID = [];

                const orderNum = func.generateOrderNumber(15);
                orderObj.orderNote = row.orderNote;
                orderObj.orderNum = orderNum;
                orderObj.orderAmt = data.orderAmt;
                orderObj.orderShippingFee = data.shippingFee || 0;
                orderObj.isCOD = data.isCOD || (data.modePayment == 'cod' ? 1 : 0);
                orderObj.isShipped = data.isShipped || 0;
                orderObj.isPaid = data.isPaid || 0;
                orderObj.isPrepared = data.isPrepared || 0;
                orderObj.isReceived = data.isReceived || 0;
                orderObj.isCancelled = data.isCancelled || 0;
                orderObj.orderTax = data.orderTax || 0;
                orderObj.uuid = uuidv1();
                orderObj.sellerId = row.storeId;
                orderObj.userId = currentUser.userId;
                orderObj.shippingCourierId = row.selected_shipping.shippingCourierId;
                orderObj.shippingFee = row.selected_shipping.fee || row.shippingFee;
                orderObj.shippingSlug = row.selected_shipping.slug;

                knex.transaction(trx => {
                    return trx.insert(orderObj, 'orderId').into("orders").asCallback((err, orderId) => {
                        if (err) {
                            next(err, null);
                        } else {
                            orderId = orderId[0];
                            async.waterfall([
                                (callbackss) => {
                                    knex.select("onesignal_player_id").from("user_devices").where("userId", orderObj.sellerId)
                                        .asCallback((err, devices) => {
                                            if (err) {
                                                return callbackWater(err, null);
                                            } else {
                                                _.each(devices, (row) => {
                                                    storesPlayerID.push(row);
                                                });
                                                return callbackss();
                                            }
                                        })
                                },
                                (callbackss) => {
                                    async.eachSeries(row.details, (product, callback1) => {
                                        const details = {
                                            orderId: orderId,
                                            productId: product.productId,
                                            detailName: product.details.productName,
                                            detailPrice: product.details.productPrice,
                                            detailSKU: product.details.productSKU,
                                            detailQty: product.quantity
                                        };

                                        if (product.variantId) {
                                            details.variantId = product.variantId;
                                            details.variantName = product.variant.name;
                                        }

                                        knex.transaction(trx => {
                                            return trx.insert(details, 'detailsId')
                                                .into("order_details")
                                                .asCallback((err, response) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        return callback1(null, response);
                                                    }
                                                });
                                        });
                                    }, () => {
                                        callbackss(null, orderId);
                                    });
                                },
                                (orderId, callbackss) => {
                                    this.isSellerFirstOrder({
                                        uuid: row.storeId
                                    }, (err, response1) => {
                                        if (err) {
                                            console.log('err ', err)
                                            return next({
                                                msg: err,
                                                result: err,
                                                success: false
                                            }, null);
                                        } else {
                                            let title, content;
                                            const linkObj = {
                                                action: 'order',
                                                orderBy: currentUser,
                                                orderId: orderId,
                                                batchNum: batchNum
                                            };
                                            if (response1 && response1.length == 1) {
                                                title = 'CONGRATS on your first order! Follow these simple steps to fulfill this order.';
                                                content = '<b>CONGRATS</b> on your first order! Follow these simple steps to fulfill this order.';
                                            } else if (response1 && response1.length > 1) {
                                                title = 'You have received a new order from ' + currentUser.username + ' for the amount of ' + data.orderAmt;
                                                content = 'You have received a new order from <b>' + currentUser.username + '</b> for the amount of ' + data.orderAmt;
                                            }

                                            knex.transaction(trx => {
                                                return trx.insert({
                                                        title: title,
                                                        content: content,
                                                        datetime: knex.raw("NOW()"),
                                                        link: JSON.stringify(linkObj),
                                                        notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                                                    }).into("notifications")
                                                    .asCallback((err, notifId) => {
                                                        if (err) {
                                                            return next(err, null);
                                                        } else {
                                                            knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                                                                notifId, orderObj.sellerId, orderObj.sellerId
                                                            ]).asCallback((err, resp) => {
                                                                if (err) {
                                                                    return next(err, null);
                                                                } else {
                                                                    notifications.push({
                                                                        notifId: notifId,
                                                                        title: title,
                                                                        content: content,
                                                                        notification_data: linkObj,
                                                                        devices: storesPlayerID
                                                                    });
                                                                    return callbackss(null, orderId);
                                                                }
                                                            });
                                                        }
                                                    });
                                            });
                                        }
                                    });
                                },
                            ], () => {
                                orderIds.push(orderId);
                                return callback(null, orderId);
                            });
                        }
                    })
                });
            }, () => {
                return callbackWater(null, {
                    batchNum: batchNum,
                    orderIds: orderIds,
                    notifications: notifications
                });
            });
        }
    ], next);
};

exports.getOrderDetails = (uuid, type, orderId, next) => {
    async.waterfall([
        (callback) => {
            let strSQL;
            if (type == 'buyer') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        'sc.name AS courier_name', 'sc.description AS courier_desc', 'sc.slug AS courier_slug', 'sc.phone  AS courier_phone', 'sc.web_url  AS courier_web_url'
                    )
                    .from("orders AS o")
                    .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where(sql => {
                        sql.where("o.userId", knex.select("userId").from("users").where("uuid", uuid).first())
                            .andWhere(sql1 => {
                                sql1.where("orderNum", orderId).orWhere("orderId", orderId);
                            });
                    });
            } else if (type == 'seller') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        'sc.name AS courier_name', 'sc.description AS courier_desc', 'sc.slug AS courier_slug', 'sc.phone  AS courier_phone', 'sc.web_url  AS courier_web_url'
                    )
                    .from("orders AS o")
                    .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where(sql => {
                        sql.where("o.sellerId", knex.select("userId").from("users").where("uuid", uuid).first())
                            .andWhere(sql1 => {
                                sql1.where("orderNum", orderId).orWhere("orderId", orderId);
                            });
                    });
            }

            strSQL.asCallback((err, orders) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (orders && orders.length > 0) {
                        orders = orders[0];
                    }
                    return callback(null, orders);
                }
            });
        },
        (orders, callback) => {
            async.parallel([
                (callback1) => {
                    knex.select("od.*", "p.productDesc").from("order_details AS od")
                        .innerJoin("products AS p", "od.productId", "p.productId").where("orderId", orderId)
                        .asCallback((err, response) => {
                            if (err) {
                                next(err, null);
                            } else {
                                async.eachSeries(response, (item, cb) => {
                                    knex("product_images").where("productId", item.productId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                if (response) {
                                                    item.image = response;
                                                    return cb();
                                                } else {
                                                    item.image = {};
                                                    return cb();
                                                }
                                            }
                                        });
                                }, () => {
                                    orders.details = response;
                                    callback1(null, orders);
                                });
                            }
                        })
                },
                (callback1) => {
                    knex.select("ot.*",
                            knex.raw("(SELECT sellerName FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerEmail"),
                            knex.raw("(SELECT phone FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerPhone"),
                            knex.raw("(SELECT username FROM users WHERE userId = ot.userId LIMIT 1) AS buyerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = ot.userId LIMIT 1) AS buyerEmail"),
                            knex.raw("(SELECT phone FROM users WHERE userId = ot.userId LIMIT 1) AS buyerPhone")
                        )
                        .from("order_return AS ot").where("ot.orderId", orderId)
                        .asCallback((err, order_return) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                if (order_return && order_return.length > 0) {
                                    order_return = order_return[0];
                                    async.parallel([
                                        (callback2) => {
                                            knex.select("od.*", "p.productDesc")
                                                .from("order_return_details AS od")
                                                .innerJoin("products AS p", "od.productId", "p.productId")
                                                .where("od.returnId", order_return.returnId)
                                                .asCallback((err, order_return_details) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        async.eachSeries(order_return_details, (item, cb) => {
                                                            let strSQL = mysql.format('SELECT * FROM product_images WHERE productId=? LIMIT 1;', [item.productId]);
                                                            knex("product_images").where("productId", item.productId).first()
                                                                .asCallback((err, response) => {
                                                                    if (err) {
                                                                        return next(err, null);
                                                                    } else {
                                                                        if (response) {
                                                                            item.image = response;
                                                                            return cb();
                                                                        } else {
                                                                            item.image = {};
                                                                            return cb();
                                                                        }
                                                                    }
                                                                });
                                                        }, () => {
                                                            order_return.return_details = order_return_details;
                                                            return callback2(null, order_return);
                                                        });
                                                    }

                                                });
                                        },
                                        (callback2) => {
                                            knex("shipping_courier").where("shippingCourierId", order_return.returnCarrierId).first()
                                                .asCallback((err, shipping_courier) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        if (shipping_courier) {
                                                            order_return.shipping_courier = shipping_courier;
                                                        } else {
                                                            order_return.shipping_courier = {};
                                                        }
                                                        return callback2(null, order_return);
                                                    }
                                                });
                                        },
                                        (callback2) => {
                                            knex("order_return_files").where("orderReturnId", order_return.returnId)
                                                .asCallback((err, order_return_files) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        if (order_return_files && order_return_files.length > 0) {
                                                            order_return.return_files = order_return_files;
                                                        } else {
                                                            order_return.return_files = [];
                                                        }
                                                        return callback2(null, order_return);
                                                    }
                                                });
                                        },
                                        (callback2) => {
                                            knex("order_return_shipping_files").where("orderReturnId", order_return.returnId)
                                                .asCallback((err, order_return_shipping_files) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        if (order_return_shipping_files && order_return_shipping_files.length > 0) {
                                                            order_return.return_shipping_files = order_return_shipping_files;
                                                        } else {
                                                            order_return.return_shipping_files = [];
                                                        }
                                                        return callback2(null, order_return);
                                                    }
                                                });
                                        },
                                        (callback2) => {
                                            knex.select('userAddressId AS _id', 'name', 'detailAddress', 'city', 'suburb', 'state', 'country', 'zipcode', 'mobilenum AS mobile', 'isDefaultAddress', 'isPickUpAddress', 'uuid')
                                                .from("user_address").where("userId", order_return.sellerId).andWhere("isDefaultAddress", 1)
                                                .asCallback((err, user_address) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        if (user_address) {
                                                            user_address = user_address;
                                                        } else {
                                                            user_address = {};
                                                        }
                                                        order_return.seller_address = user_address;
                                                        return callback2(null, order_return);
                                                    }
                                                });
                                        },
                                        (callback2) => {
                                            knex.select('userAddressId AS _id', 'name', 'detailAddress', 'city', 'suburb', 'state', 'country', 'zipcode', 'mobilenum AS mobile', 'isDefaultAddress', 'isPickUpAddress', 'uuid')
                                                .from("user_address").where("userId", order_return.userId).andWhere("isDefaultAddress", 1)
                                                .asCallback((err, user_address) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    } else {
                                                        if (user_address) {
                                                            user_address = user_address;
                                                        } else {
                                                            user_address = null;
                                                        }
                                                        order_return.buyer_address = user_address;
                                                        return callback2(null, order_return);
                                                    }
                                                })
                                        }
                                    ], () => {
                                        orders.order_return_info = order_return;
                                        return callback1(null, orders);
                                    });
                                } else {
                                    order_return = null;
                                    orders.order_return_info = order_return;
                                    return callback1(null, orders);
                                }
                            }
                        });
                }
            ], () => {
                callback(null, orders);
            })
        }
    ], next);
};

exports.getOrderDetailsByBatchNum = (uuid, batchNum, next) => {
    async.waterfall([
        (callback) => {
            knex.select()
                .from("orders AS o")
                .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]))
                .andWhere("batchId", batchNum).first()
                .asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders);
                    }
                });
        },
        (orders, callback) => {
            async.parallel([
                (callback1) => {
                    let strSQL = mysql.format('SELECT od.*,p.productDesc FROM order_details od INNER JOIN products p ON od.productId = p.productId WHERE od.orderId = ?', [orders.orderId]);
                    knex.select("od.*", "p.productDesc")
                        .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                        .where("od.orderId", orders.orderId).asCallback((err, response) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                async.eachSeries(response, (item, cb) => {
                                    knex("product_images").where("productId", item.productId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                if (response) {
                                                    item.image = response;
                                                    return cb();
                                                } else {
                                                    item.image = {};
                                                    return cb();
                                                }
                                            }
                                        });
                                }, () => {
                                    orders.details = response;
                                    return callback1(null, orders);
                                });
                            }
                        });
                },
                (callback1) => {
                    knex.select("ot.*",
                            knex.raw("(SELECT sellerName FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerEmail"),
                            knex.raw("(SELECT phone FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerPhone"),
                            knex.raw("(SELECT username FROM users WHERE userId = ot.userId LIMIT 1) AS buyerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = ot.userId LIMIT 1) AS buyerEmail"),
                            knex.raw("(SELECT phone FROM users WHERE userId = ot.userId LIMIT 1) AS buyerPhone")
                        )
                        .from("order_return AS ot").where("ot.orderId", orders.orderId)
                        .asCallback((err, order_return) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                if (order_return && order_return.length > 0) {
                                    order_return = order_return[0];

                                    knex.select("od.*", "p.productDesc")
                                        .from("order_return_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                                        .where("od.returnId", order_return.returnId)
                                        .asCallback((err, order_return_details) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                async.eachSeries(order_return_details, (item, cb) => {
                                                    knex("product_images").where("productId", item.productId).first()
                                                        .asCallback((err, response) => {
                                                            if (err) {
                                                                return next(err, null);
                                                            } else {
                                                                if (response) {
                                                                    item.image = response;
                                                                    return cb();
                                                                } else {
                                                                    item.image = {};
                                                                    return cb();
                                                                }
                                                            }
                                                        });
                                                }, () => {
                                                    order_return.return_details = order_return_details;
                                                    orders.order_return_info = order_return;
                                                    return callback1(null, orders);
                                                });
                                            }
                                        });
                                } else {
                                    order_return = {};
                                    orders.order_return_info = order_return;
                                    return callback1(null, orders);
                                }
                            }
                        })
                }
            ], () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getPayableOrdersList = (uuid, type, next) => {
    async.waterfall([
        (callback) => {
            let strSQL;
            if (type == 'pay') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where("o.isPrepared", 0).andWhere("o.isShipped", 0).andWhere("o.isPaid", 0).andWhere("o.isReceived", 0).andWhere("o.isCancelled", 0)
                    .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            } else if (type == 'unpaid') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where("o.isPrepared", 0).andWhere("o.isShipped", 0).andWhere("o.isPaid", 0).andWhere("o.isReceived", 0).andWhere("o.isCancelled", 0)
                    .andWhere("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            }

            strSQL.asCallback((err, orders) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, orders);
                }
            });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                knex.select("od.*", "p.productDesc")
                    .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                    .where("od.orderId", order.orderId)
                    .asCallback((err, response) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            async.eachSeries(response, (item, cb) => {
                                knex("product_images").where("productId", item.productId).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            if (response) {
                                                item.image = response;
                                                return cb();
                                            } else {
                                                item.image = {};
                                                return cb();
                                            }
                                        }
                                    });
                            }, () => {
                                order.details = response;
                                return orderCallback(null, order);
                            });
                        }
                    });
            }, () => {
                return callback(null, orders);
            });
        }
    ], next);
};

exports.getPreparedOrderList = (uuid, next) => {
    async.waterfall([
        (callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                    "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("o.isPrepared", 1).andWhere("o.isCancelled", 0)
                .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]))
                .asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders);
                    }
                });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                async.parallel([
                    (callback1) => {
                        knex.select("od.*", "p.productDesc")
                            .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                            .where("od.orderId", order.orderId)
                            .asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    async.eachSeries(response, (item, cb) => {
                                        knex("product_images").where("productId", item.productId).first()
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    if (response) {
                                                        item.image = response;
                                                        return cb();
                                                    } else {
                                                        item.image = {};
                                                        return cb();
                                                    }
                                                }
                                            });
                                    }, () => {
                                        order.details = response;
                                        return callback1(null, order);
                                    });
                                }
                            });
                    },
                    (callback1) => {
                        knex.select("ot.orderId", "otc.*")
                            .from("order_tracking AS ot").innerJoin("order_tracking_checkpoints AS otc", "ot.order_trackingId", "otc.pk_order_trackingId")
                            .where("ot.orderId", order.orderId).orderBy("checkpoint_time", "desc").first()
                            .asCallback((err, checkpoints) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (checkpoints) {
                                        delete checkpoints.pk_order_trackingId;
                                        checkpoints = checkpoints;
                                    } else {
                                        checkpoints = {};
                                    }
                                    order.checkpoint = checkpoints;
                                    return callback1(null, order);
                                }
                            });
                    }
                ], () => {
                    orderCallback(null, order);
                });
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getShippedOrdersList = (uuid, action, type, next) => {
    async.waterfall([
        (callback) => {
            let strSQL;
            if (action == 'ship') {
                if (type == 'seller') {
                    strSQL = knex.select("o.*",
                            knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                            knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                            "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                        .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                        .where("o.isPrepared", 1).andWhere("o.isShipped", 0).andWhere("o.isCancelled", 0)
                        .andWhere("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));

                } else if (type == 'buyer') {
                    strSQL = knex.select("o.*",
                            knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                            knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                            "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                        .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                        .where("o.isPrepared", 1).andWhere("o.isShipped", 0).andWhere("o.isCancelled", 0)
                        .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
                }
            } else if (action == 'shipping') {
                if (type == 'seller') {
                    strSQL = knex.select("o.*",
                            knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                            knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                            "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                        .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                        .where("o.isPrepared", 1).andWhere("o.isShipped", 1).andWhere("o.isReceived", 0).andWhere("o.isCancelled", 0)
                        .andWhere("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));

                } else if (type == 'buyer') {
                    strSQL = knex.select("o.*",
                            knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                            knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                            knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                            "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                        .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                        .where("o.isPrepared", 1).andWhere("o.isShipped", 1).andWhere("o.isReceived", 0).andWhere("o.isCancelled", 0)
                        .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
                }
            }
            strSQL.asCallback((err, orders) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, orders);
                }
            });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, cb) => {
                async.parallel([
                    (callback1) => {
                        knex.select("od.*", "p.productDesc")
                            .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                            .where("od.orderId", order.orderId)
                            .asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    async.eachSeries(response, (item, cb) => {
                                        knex("product_images").where("productId", item.productId).first()
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    if (response) {
                                                        item.image = response;
                                                        return cb();
                                                    } else {
                                                        item.image = {};
                                                        return cb();
                                                    }
                                                }
                                            });
                                    }, () => {
                                        order.details = response;
                                        return callback1(null, order);
                                    });
                                }
                            });
                    },
                    (callback1) => {
                        knex.select("ot.orderId", "otc.*")
                            .from("order_tracking AS ot").innerJoin("order_tracking_checkpoints AS otc", "ot.order_trackingId", "otc.pk_order_trackingId")
                            .where("ot.orderId", order.orderId).orderBy("checkpoint_time", "desc").first()
                            .asCallback((err, checkpoints) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (checkpoints) {
                                        delete checkpoints.pk_order_trackingId;
                                        checkpoints = checkpoints;
                                    } else {
                                        checkpoints = {};
                                    }
                                    order.checkpoint = checkpoints;
                                    return callback1(null, order);
                                }
                            });
                    },
                    (callback1) => {
                        knex("order_return").where("orderId", order.orderId).first()
                            .asCallback((err, order_return) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (order_return) {
                                        knex("order_return_details").where("returnId", order_return.returnId)
                                            .asCallback((err, order_return_details) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    order_return.return_details = order_return_details;
                                                    order.order_return_info = order_return;
                                                    return callback1(null, order);
                                                }
                                            });
                                    } else {
                                        order_return = {};
                                        order.order_return_info = order_return;
                                        return callback1(null, order);
                                    }
                                }
                            });
                    }
                ], cb)
            }, () => {
                return callback(null, orders);
            });
        }
    ], next);
};

exports.getReceivableOrdersList = (uuid, next) => {
    async.waterfall([
        (callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                    "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("o.isPrepared", 1).andWhere("o.isShipped", 1).andWhere("o.isReceived", 0).andWhere("o.isCancelled", 0)
                .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]))
                .asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders);
                    }
                });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, cb) => {
                async.parallel([
                    (callback1) => {
                        knex.select("od.*", "p.productDesc")
                            .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                            .where("od.orderId", order.orderId)
                            .asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    async.eachSeries(response, (item, cb) => {
                                        knex("product_images").where("productId", item.productId).first()
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    if (response) {
                                                        item.image = response;
                                                        return cb();
                                                    } else {
                                                        item.image = {};
                                                        return cb();
                                                    }
                                                }
                                            });
                                    }, () => {
                                        order.details = response;
                                        return callback1(null, order);
                                    });
                                }
                            });
                    },
                    (callback1) => {
                        knex.select("ot.orderId", "otc.*")
                            .from("order_tracking AS ot").innerJoin("order_tracking_checkpoints AS otc", "ot.order_trackingId", "otc.pk_order_trackingId")
                            .where("ot.orderId", order.orderId).orderBy("checkpoint_time", "desc").first()
                            .asCallback((err, checkpoints) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (checkpoints) {
                                        delete checkpoints.pk_order_trackingId;
                                        checkpoints = checkpoints;
                                    } else {
                                        checkpoints = {};
                                    }
                                    order.checkpoint = checkpoints;
                                    return callback1(null, order);
                                }
                            });
                    },
                    (callback1) => {
                        knex("order_return").where("orderId", order.orderId).first()
                            .asCallback((err, order_return) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (order_return) {
                                        knex("order_return_details").where("returnId", order_return.returnId)
                                            .asCallback((err, order_return_details) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    order_return.return_details = order_return_details;
                                                    order.order_return_info = order_return;
                                                    return callback1(null, order);
                                                }
                                            });
                                    } else {
                                        order_return = {};
                                        order.order_return_info = order_return;
                                        return callback1(null, order);
                                    }
                                }
                            });
                    }
                ], cb);
            }, () => {
                return callback(null, orders);
            });
        }
    ], next);
};

exports.getCompletedOrdersList = (uuid, type, next) => {
    async.waterfall([
        (callback) => {
            let strSQL;
            if (type == 'seller') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where("o.isPrepared", 1).andWhere("o.isShipped", 1).andWhere("o.isReceived", 1).andWhere("o.isCancelled", 0)
                    .andWhere("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));

            } else if (type == 'buyer') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where("o.isPrepared", 1).andWhere("o.isShipped", 1).andWhere("o.isReceived", 1).andWhere("o.isCancelled", 0)
                    .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            }
            strSQL.asCallback((err, orders) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, orders);
                }
            });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                async.parallel([
                    (callback1) => {
                        knex.select("od.*", "p.productDesc")
                            .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                            .where("od.orderId", order.orderId)
                            .asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    async.eachSeries(response, (item, cb) => {
                                        knex("product_images").where("productId", item.productId).first()
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    if (response) {
                                                        item.image = response;
                                                        return cb();
                                                    } else {
                                                        item.image = {};
                                                        return cb();
                                                    }
                                                }
                                            });
                                    }, () => {
                                        order.details = response;
                                        return callback1(null, order);
                                    });
                                }
                            });
                    },
                    (callback1) => {
                        knex.select("ot.orderId", "otc.*")
                            .from("order_tracking AS ot").innerJoin("order_tracking_checkpoints AS otc", "ot.order_trackingId", "otc.pk_order_trackingId")
                            .where("ot.orderId", order.orderId).orderBy("checkpoint_time", "desc").first()
                            .asCallback((err, checkpoints) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (checkpoints) {
                                        delete checkpoints.pk_order_trackingId;
                                        checkpoints = checkpoints;
                                    } else {
                                        checkpoints = {};
                                    }
                                    order.checkpoint = checkpoints;
                                    return callback1(null, order);
                                }
                            });
                    },
                    (callback1) => {
                        knex("order_return").where("orderId", order.orderId).first()
                            .asCallback((err, order_return) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (order_return) {
                                        knex("order_return_details").where("returnId", order_return.returnId)
                                            .asCallback((err, order_return_details) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    order_return.return_details = order_return_details;
                                                    order.order_return_info = order_return;
                                                    return callback1(null, order);
                                                }
                                            });
                                    } else {
                                        order_return = {};
                                        order.order_return_info = order_return;
                                        return callback1(null, order);
                                    }
                                }
                            });
                    }
                ], () => {
                    return orderCallback(null, order);
                })

            }, () => {
                return callback(null, orders);
            });
        }
    ], next);
};

exports.getCancelledOrdersList = (uuid, type, next) => {
    async.waterfall([
        (callback) => {
            let strSQL;

            if (type == 'seller') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where("o.isCancelled", 1)
                    .andWhere("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));

            } else if (type == 'buyer') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .where("o.isCancelled", 1)
                    .andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            }
            strSQL.asCallback((err, orders) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, orders);
                }
            });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                async.parallel([
                    (callback1) => {
                        knex("order_return").where("orderId", order.orderId).first()
                            .asCallback((err, order_return) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (order_return) {
                                        knex("order_return_details").where("returnId", order_return.returnId)
                                            .asCallback((err, order_return_details) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    order_return.return_details = order_return_details;
                                                    order.order_return_info = order_return;
                                                    return callback1(null, order);
                                                }
                                            });
                                    } else {
                                        order_return = {};
                                        order.order_return_info = order_return;
                                        return callback1(null, order);
                                    }
                                }
                            });
                    },
                    (callback1) => {
                        knex.select("od.*", "p.productDesc")
                            .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                            .where("od.orderId", order.orderId)
                            .asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    async.eachSeries(response, (item, cb) => {
                                        knex("product_images").where("productId", item.productId).first()
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    if (response) {
                                                        item.image = response;
                                                        return cb();
                                                    } else {
                                                        item.image = {};
                                                        return cb();
                                                    }
                                                }
                                            });
                                    }, () => {
                                        order.details = response;
                                        return callback1(null, order);
                                    });
                                }
                            });
                    }
                ], () => {
                    return orderCallback(null, orders);
                });
            }, () => {
                return callback(null, orders);
            });
        }
    ], next);
};

exports.getReturnedOrdersList = (uuid, type, next) => {
    async.waterfall([
        (callback) => {
            let strSQL;
            if (type == 'seller') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").innerJoin("order_return AS rt", "rt.orderId", "o.orderId")
                    .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .whereRaw("rt.returnedRequested IS NOT NULL").andWhere("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            } else if (type == 'buyer') {
                strSQL = knex.select("o.*",
                        knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                        knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                        knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail"),
                        "sc.name AS courier_name", "sc.description AS courier_desc", "sc.slug AS courier_slug", "sc.phone AS courier_phone", "sc.web_url  AS courier_web_url")
                    .from("orders AS o").innerJoin("order_return AS rt", "rt.orderId", "o.orderId")
                    .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                    .whereRaw("rt.returnedRequested IS NOT NULL").andWhere("o.userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            }
            strSQL.asCallback((err, orders) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, orders);
                }
            });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                async.parallel([
                    (callback1) => {
                        knex.select("od.*", "p.productDesc")
                            .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                            .where("od.orderId", order.orderId)
                            .asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    async.eachSeries(response, (item, cb) => {
                                        knex("product_images").where("productId", item.productId).first()
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    if (response) {
                                                        item.image = response;
                                                        return cb();
                                                    } else {
                                                        item.image = {};
                                                        return cb();
                                                    }
                                                }
                                            });
                                    }, () => {
                                        order.details = response;
                                        return callback1(null, order);
                                    });
                                }
                            });
                    },
                    (callback1) => {
                        knex.select("ot.orderId", "otc.*")
                            .from("order_tracking AS ot").innerJoin("order_tracking_checkpoints AS otc", "ot.order_trackingId", "otc.pk_order_trackingId")
                            .where("ot.orderId", order.orderId).orderBy("checkpoint_time", "desc").first()
                            .asCallback((err, checkpoints) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (checkpoints) {
                                        delete checkpoints.pk_order_trackingId;
                                        checkpoints = checkpoints;
                                    } else {
                                        checkpoints = {};
                                    }
                                    order.checkpoint = checkpoints;
                                    return callback1(null, order);
                                }
                            });
                    },
                    (callback1) => {
                        knex("order_return").where("orderId", order.orderId).first()
                            .asCallback((err, order_return) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (order_return) {
                                        knex("order_return_details").where("returnId", order_return.returnId)
                                            .asCallback((err, order_return_details) => {
                                                if (err) {
                                                    return next(err, null);
                                                } else {
                                                    order_return.return_details = order_return_details;
                                                    order.order_return_info = order_return;
                                                    return callback1(null, order);
                                                }
                                            });
                                    } else {
                                        order_return = {};
                                        order.order_return_info = order_return;
                                        return callback1(null, order);
                                    }
                                }
                            });
                    }
                ], orderCallback);
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.extendOrderGuarantee = (uuid, orderId, next) => {

};

exports.isOrderAcceptedSeller = (sellerUUID, orderId, next) => {
    knex("orders")
        .where("isPrepared", 1).andWhereRaw("preparedDateTime IS NOT NULL").andWhere(sql => {
            sql.where("orderNum", orderId).orWhere("orderID", orderId).orWhere("uuid", orderId);
        }).andWhere("sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [sellerUUID]))
        .asCallback(next);
};

exports.isOrderPrepared = (orderId, next) => {
    knex("orders")
        .where("isPrepared", 1).andWhereRaw("preparedDateTime IS NOT NULL").andWhere(sql => {
            sql.where("orderNum", orderId).orWhere("orderID", orderId).orWhere("uuid", orderId);
        })
        .asCallback(next);
};

exports.acceptOrder = (sellerUUID, orderId, data, next) => {
    let storesPlayerID = [];

    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", sellerUUID).orWhere("uuid", sellerUUID)
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (seller, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", knex.raw("(SELECT userId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1)", [orderId, orderId, orderId]))
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, seller, user);
                    }
                });
        },
        (seller, buyer, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        isPrepared: 1,
                        preparedDateTime: data.preparedDateTime,
                        shippingDeadline: data.shippingDeadline
                    }).from("orders")
                    .where(sql => {
                        sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
                    }).asCallback((err, response) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            return callback(null, seller, buyer);
                        }
                    })
            });
        },
        (seller, buyer, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o")
                .where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [sellerUUID]))
                .andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).first().asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders, seller, buyer);
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("od.*", "p.productDesc")
                .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("od.orderId", orderId)
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        if (response) {
                                            item.image = response;
                                            return cb();
                                        } else {
                                            item.image = {};
                                            return cb();
                                        }
                                    }
                                });
                        }, () => {
                            order.details = response;
                            return callback(null, orders, seller, buyer);
                        });
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", buyer.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return callbackWater(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, orders, seller, buyer);
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            const linkObj = {
                action: 'order_ship',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
                approvedBy: seller,
            };
            const title = 'COD Request Confirmed'
            const content = 'The COD payment method of your order <b>' + orders.orderNum + '</b> has been approved ' + orders.preparedDateTime;
            knex.transaction(trx => {
                return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw("NOW()"),
                        link: JSON.stringify(linkObj),
                        notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                    }, 'notificationId')
                    .into("notifications")
                    .asCallback((err, notifId) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            notifId = notifId[0];
                            knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                                notifId, buyer.userId, buyer.userId
                            ]).asCallback((err, resp) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    const notification = {
                                        notifId: notifId,
                                        title: title,
                                        content: content,
                                        notification_data: linkObj,
                                        devices: storesPlayerID
                                    };
                                    return callback(null, {
                                        orders: orders,
                                        seller: seller,
                                        buyer: buyer,
                                        notification: notification
                                    });
                                }
                            });
                        }
                    })
            });
        }
    ], next);
};

exports.isOrderPaid = (uuid, orderId, next) => {
    knex("orders")
        .where("isPaid", 1).andWhereRaw("paymentDateTime IS NOT NULL").andWhere(sql => {
            sql.where("orderNum", orderId).andWhere("orderId", orderId).andWhere("uuid", orderId)
        }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [uuid]))
        .asCallback(next);
};

exports.payOrder = (uuid, orderId, data, next) => {
    knex.transaction(trx => {
        return trx.update({
                isPaid: 1,
                paymentDateTime: data.paymentDateTime
            }).from("orders")
            .where(sql => {
                sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId)
            }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [uuid]))
            .asCallback(next);
    });
};

exports.isOrderShipped = (uuid, orderId, next) => {
    knex("orders")
        .where("isShipped", 1).andWhereRaw("shippedDateTime IS NOT NULL").andWhere(sql => {
            sql.where("orderNum", orderId).andWhere("orderId", orderId).andWhere("uuid", orderId)
        }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [uuid]))
        .asCallback(next);
};

exports.shippedOrder = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", uuid).orWhere("uuid", uuid)
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (seller, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", knex.raw("(SELECT userId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1)", [orderId, orderId, orderId]))
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, seller, user);
                    }
                });
        },
        (seller, buyer, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        orderTrackingNumber: data.orderTrackingNumber,
                        isShipped: 1,
                        shippedDateTime: data.shippedDateTime,
                        deliveryDeadline: data.deliveryDeadline
                    }).from("orders")
                    .where(sql => {
                        sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId)
                    }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [uuid]))
                    .asCallback((err, response) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            return callback(null, seller, buyer);
                        }
                    });
            });
        },
        (seller, buyer, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o")
                .where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [sellerUUID]))
                .andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).first().asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders, seller, buyer);
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("od.*", "p.productDesc")
                .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("od.orderId", orderId)
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        if (response) {
                                            item.image = response;
                                            return cb();
                                        } else {
                                            item.image = {};
                                            return cb();
                                        }
                                    }
                                });
                        }, () => {
                            order.details = response;
                            return callback(null, orders, seller, buyer);
                        });
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", buyer.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return callbackWater(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, orders, seller, buyer);
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            const linkObj = {
                action: 'order_receive',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
                approvedBy: seller,
            };
            const title = 'Shipped Out'
            const content = '<b>' + seller.username + '</b> has shipped your order <b>' + orders.orderNum + '</b>. Your order\'s tracking ID is <b>' + orders.orderTrackingNumber + '</b>. ' + orders.shippedDateTime;
            knex.transaction(trx => {
                return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw("NOW()"),
                        link: JSON.stringify(linkObj),
                        notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                    }, 'notificationId')
                    .into("notifications")
                    .asCallback((err, notifId) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            notifId = notifId[0];
                            knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                                notifId, buyer.userId, buyer.userId
                            ]).asCallback((err, resp) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    const notification = {
                                        notifId: notifId,
                                        title: title,
                                        content: content,
                                        notification_data: linkObj,
                                        devices: storesPlayerID
                                    };
                                    return callback(null, {
                                        orders: orders,
                                        seller: seller,
                                        buyer: buyer,
                                        notification: notification
                                    });
                                }
                            });
                        }
                    });
            });
        }
    ], next);
};

exports.isOrderReceived = (uuid, orderId, next) => {
    knex("orders")
        .where("isReceived", 1).andWhereRaw("receivedDateTime IS NOT NULL").andWhere(sql => {
            sql.where("orderNum", orderId).andWhere("orderId", orderId).andWhere("uuid", orderId)
        }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [uuid]))
        .asCallback(next);
};

exports.receivedOrder = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", uuid).orWhere("uuid", uuid)
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", knex.raw("(SELECT userId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1)", [orderId, orderId, orderId]))
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        isReceived: 1,
                        receivedDateTime: data.receivedDateTime,
                        isPaid: data.isPaid,
                        paymentDateTime: data.paymentDateTime
                    }).from("orders")
                    .where(sql => {
                        sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId)
                    }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1);", [uuid]))
                    .asCallback((err, response) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            return callback(null, buyer, seller);
                        }
                    });
            });
        },
        (buyer, seller, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o")
                .where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [buyer.userId]))
                .andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).first().asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, orders, buyer, seller);
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("od.*", "p.productDesc")
                .from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("od.orderId", orderId)
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        if (response) {
                                            item.image = response;
                                            return cb();
                                        } else {
                                            item.image = {};
                                            return cb();
                                        }
                                    }
                                });
                        }, () => {
                            order.details = response;
                            return callback(null, orders, buyer, seller);
                        });
                    }

                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", seller.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return callbackWater(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, orders, buyer, seller);
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            const linkObj = {
                action: 'order_completed',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
            };
            const title = 'Order Received'
            const content = '<b>' + buyer.username + '</b> has confirmed that he received and accepted the product.';

            let strSQL = mysql.format('INSERT INTO notifications(title,content,datetime,link,notifTypeId) VALUES (?,?,NOW(),?,?);', [
                title, content, JSON.stringify(linkObj), 5
            ]);
            db.insertWithId(strSQL, (err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    let sSQL = mysql.format('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, buyer.userId, buyer.userId
                    ]);
                    db.actionQuery(sSQL, (err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            let notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            callback(null, {
                                orders: orders,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });

            knex.transaction(trx => {
                return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw("NOW()"),
                        link: JSON.stringify(linkObj),
                        notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                    }, 'notificationId')
                    .into("notifications")
                    .asCallback((err, notifId) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            notifId = notifId[0];
                            knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                                notifId, buyer.userId, buyer.userId
                            ]).asCallback((err, resp) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    const notification = {
                                        notifId: notifId,
                                        title: title,
                                        content: content,
                                        notification_data: linkObj,
                                        devices: storesPlayerID
                                    };
                                    return callback(null, {
                                        orders: orders,
                                        seller: seller,
                                        buyer: buyer,
                                        notification: notification
                                    });
                                }
                            });
                        }
                    });
            });
        }
    ], next);
};