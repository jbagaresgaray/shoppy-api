'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();

const uuidv1 = require('uuid/v1');
const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.saveFileUpload = (data, next) => {
    let fileObj = {
        file_name: data.file_name,
        file_type: data.file_type,
        file_size: data.file_size,
        file_orig_path: data.file_orig_path,
        file_100_path: data.file_100_path,
        file_200_path: data.file_200_path,
        file_400_path: data.file_400_path,
        file_800_path: data.file_800_path,
        file_thumb_path: data.file_thumb_path,
        file_preview_path: data.file_preview_path,
        file_large_path: data.file_large_path,
        file_extra_large_path: data.file_extra_large_path,
        dp_id: data.dp_id,
        dp_rev: data.dp_rev,
        dp_file_hash: data.dp_file_hash,
        uuid: uuidv1()
    };

    let strSQL = mysql.format('INSERT INTO files SET ?;', fileObj);
    console.log('strSQL: ', strSQL);
    db.insertWithId(strSQL, next);
};


exports.getFile = (Id, next) => {
    // let strSQL = mysql.format('SELECT * FROM files WHERE (fileId = ? OR uuid=?) LIMIT 1;', [Id, Id]);
    // db.query(strSQL, next);

    knex.select("*")
        .from("files")
        .where("fileId", Id)
        .orWhere("uuid", Id)
        .limit(1)
        .asCallback(next)
};

exports.deleteFile = (Id, next) => {
    // let strSQL = mysql.format('DELETE FROM files WHERE (fileId = ? OR uuid = ?);', [Id, Id]);
    // db.query(strSQL, next);

    knex.transaction(trx => {
        return trx("files")
            .where("fileId", Id)
            .orWhere("uuid", Id)
            .delete()
            .asCallback(next)
    })
};