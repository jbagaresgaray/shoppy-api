'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.geShopCustomers = (user, next) => {
    async.waterfall([
        (cb)=>{
            knex
                .select("userId")
                .from("users")
                .where("uuid", user.uuid)
                .first()
                .asCallback(cb)
        },
        (user, cb)=>{
            knex
                .distinct("o.userId")
                .from("orders as o ")
                .where("o.sellerid", user.userId)
                .asCallback((err, resp)=>{
                    if(err){
                        next(err, null)
                    } else {
                        if(resp && resp.length > 0) {
                            cb(null, resp)
                        } else {
                            next(null, [])
                        }
                    }
                })
        },
        (resp, cb) => {
            knex.select("*")
                .from("users")
                .asCallback((err, users) => {
                    if(err) {
                        cb(null, resp)
                    } else {
                        _.each(resp, (value, index) => {
                            value['info'] = _.find(users, (user) => {
                                if(user.userId == value.userId) {
                                    // confidential details are omitted
                                    delete user.password
                                    delete user.sendBirdToken
                                    delete user.facebookId
                                    delete user.googleId
                                    delete user.uuid
                                    delete user.verificationCode
                                    delete user.sellerIsVacationMode
                                    return user
                                }
                            })
                            if(index == resp.length - 1) {
                                cb(null, resp)
                            }
                        })
                    }
                })
        }
    ], next)
}