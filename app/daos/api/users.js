"use strict";

const Database = require("../../../app/utils/database").Database;
const db = new Database();
const func = require("../../../app/utils/functions");

const env = process.env.NODE_ENV || "development";
const config = require("../../../config/environment/" + env);

const uuidv1 = require("uuid/v1");
const bcrypt = require("bcryptjs");
const async = require("async");
const fs = require("fs");
const _ = require("lodash");
const moment = require("moment");

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.checkUserUsername = (data, next) => {
    knex("users")
        .where({
            username: data.username
        })
        .first()
        .asCallback(next);
};

exports.checkUserUsernameForUpdate = (user_id, data, next) => {
    knex("users")
        .where({
            username: data.username
        })
        .andWhere("userId", "<>", user_id)
        .first()
        .asCallback(next);
};

exports.checkUserEmail = (data, next) => {
    knex("users")
        .where({
            email: data.email
        })
        .first()
        .asCallback(next);
};

exports.checkUserEmailForUpdate = (user_id, data, next) => {
    knex("users")
        .where({
            email: data.email
        })
        .andWhere("userId", "<>", user_id)
        .first()
        .asCallback(next);
};

exports.checkIfUserIsSeller = (user_id, next) => {
    knex("users")
        .where(sql => {
            sql
                .where({
                    userId: user_id
                })
                .orWhere({
                    uuid: user_id
                });
        })
        .andWhere(sql => {
            sql.whereNotNull("sellerApprovalDate").andWhere({
                isSeller: 1
            });
        })
        .first()
        .asCallback(next);
};

exports.checkIfUserHasSellerApplication = (user_id, next) => {
    knex("users")
        .where(sql => {
            sql
                .where({
                    userId: user_id
                })
                .orWhere({
                    uuid: user_id
                });
        })
        .andWhere(sql => {
            sql
                .whereNotNull("sellerApprovalDate")
                .and.whereNotNull("sellerApplicationDate")
                .andWhere({
                    isSeller: 0
                });
        })
        .first()
        .asCallback(next);
};

exports.getUserByUsernameAndEmail = (email, next) => {
    knex
        .select(
            "username",
            "email",
            "firstname",
            "lastname",
            "verificationCode",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid"
        )
        .from("users")
        .where({
            email: email
        })
        .first()
        .asCallback(next);
};

exports.getUserByUUID = (uuid, next) => {
    knex
        .select(
            "username",
            "email",
            "firstname",
            "lastname",
            "verificationCode",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid"
        )
        .from("users")
        .where({
            uuid: uuid
        })
        .first()
        .asCallback(next);
};

exports.createUser = (data, next) => {
    const SendBird = require("sendbird-nodejs");
    const sb = SendBird(config.sendbird_app_token);

    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    data.uuid = uuidv1();
    data.verificationCode = func.generateString(36);

    const payload = {
        user_id: data.uuid,
        nickname: data.username,
        profile_url: "",
        issue_access_token: true // (Optional)
    };

    sb.users.create(payload).then(
        response => {
            console.log("SendBird response: ", response);
            knex
                .transaction(trx => {
                    return trx
                        .insert({
                                firstname: data.firstname,
                                lastname: data.lastname,
                                username: data.username,
                                password: data.password,
                                email: data.email,
                                isVerify: 0,
                                verificationCode: data.verificationCode,
                                registrationDate: knex.raw("NOW()"),
                                sendBirdToken: response.access_token,
                                uuid: data.uuid
                            },
                            "userId"
                        )
                        .into("users")
                        .then(ids => {
                            return knex
                                .select(
                                    "username",
                                    "email",
                                    "firstname",
                                    "lastname",
                                    "phone",
                                    "fax",
                                    "verificationCode",
                                    "uuid"
                                )
                                .from("users")
                                .where({
                                    userId: ids[0]
                                })
                                .first();
                        });
                })
                .asCallback(next);
        },
        error => {
            console.log("SendBird error: ", error);
        }
    );
};

exports.createSendBirdUser = (data, next) => {
    const sb = SendBird(config.sendbird_app_token);
    const payload = {
        user_id: string,
        nickname: string,
        profile_url: string,
        issue_access_token: boolean // (Optional)
    };
    sb.users.create(payload).then(function (response) {
        // do something with SendBird response
        // {
        //     "user_id": string,
        //     "nickname": string,
        //     "profile_url": string,
        //     "access_token": string,
        //     "last_seen_at": long,
        //     "is_online": boolean
        // }
    });
};

exports.checkIfVerified = (hash_key, uuid, next) => {
    knex
        .select(
            "username",
            "email",
            "firstname",
            "lastname",
            "phone",
            "fax",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid"
        )
        .from("users")
        .where({
            uuid: uuid,
            verificationCode: hash_key
        })
        .and.whereNotNull("dateVerify")
        .first()
        .asCallback(next);
};

exports.verifyUser = (hash_key, uuid, next) => {
    knex
        .transaction(trx => {
            return trx
                .update({
                    dateVerify: knex.raw("NOW()"),
                    isVerify: 1
                })
                .from("users")
                .where({
                    verificationCode: hash_key,
                    uuid: uuid
                })
                .then(() => {
                    return knex
                        .select(
                            "username",
                            "email",
                            "firstname",
                            "lastname",
                            "phone",
                            "fax",
                            "user_img_path AS img_path",
                            "user_img_name AS img_name",
                            "user_img_type AS img_type",
                            "user_img_size AS img_size",
                            "uuid"
                        )
                        .from("users")
                        .where({
                            verificationCode: hash_key,
                            uuid: uuid
                        })
                        .first();
                });
        })
        .asCallback(next);
};

exports.getUserProfile = (uuid, next) => {
    knex
        .select(
            "username",
            "email",
            "firstname",
            "lastname",
            "gender",
            "birthday",
            "phone",
            "fax",
            "facebookId",
            "googleId",
            "registrationDate",
            "isSeller",
            "user_img_path AS img_path",
            "user_img_name AS img_name",
            "user_img_type AS img_type",
            "user_img_size AS img_size",
            "uuid"
        )
        .from("users")
        .where({
            uuid: uuid
        })
        .first()
        .asCallback(next);
};

exports.getCurrentUser = (uuid, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select(
                        "userId",
                        "username",
                        "email",
                        "firstname",
                        "lastname",
                        "gender",
                        "birthday",
                        "phone",
                        "fax",
                        "facebookId",
                        "googleId",
                        "sendBirdToken",
                        "currencyId",
                        "countryId",
                        "isSeller",
                        "sellerName",
                        "sellerDescription",
                        "sellerIsVacationMode",
                        "sellerApprovalCode",
                        "sellerApprovalDate",
                        "sellerApplicationDate",
                        "user_img_path AS img_path",
                        "user_img_name AS img_name",
                        "user_img_type AS img_type",
                        "user_img_size AS img_size",
                        "user_banner_path AS banner_path",
                        "user_banner_name AS banner_name",
                        "user_banner_type AS banner_type",
                        "user_banner_size AS banner_size",
                        "uuid"
                    )
                    .from("users")
                    .where({
                        userId: uuid
                    })
                    .orWhere({
                        uuid: uuid
                    })
                    .first()
                    .then(response => {
                        if (response) {
                            response.isSeller = _.isNull(response.isSeller) ? 0 : response.isSeller;
                            response.isSeller = response.isSeller == 0 ? false : true;
                        }
                        return callback(null, response);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                knex.select('device_model', 'device_version', 'device_manufacturer', 'device_serial', 'device_uuid', 'onesignal_player_id')
                    .from("user_devices").where("userId", user.userId).then(response => {
                        user.devices = response;
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            }
        ],
        next
    );
};

exports.getUserAddresses = (uuid, params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + "," + numPerPage;

    async.waterfall(
        [
            callback => {
                knex("user_address")
                    .count("* as numRows")
                    .where("userId", knex.select("userId").from("users").where({
                        uuid: uuid
                    }).first())
                    .then(results => {
                        numRows =
                            results && results[0] && results[0].numRows ?
                            results[0].numRows :
                            0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex
                    .select(
                        "userAddressId AS _id",
                        "name",
                        "detailAddress",
                        "city",
                        "suburb",
                        "state",
                        "country",
                        "zipcode",
                        "mobilenum AS mobile",
                        "isDefaultAddress",
                        "isPickUpAddress",
                        "uuid"
                    )
                    .from("user_address")
                    .where("userId", knex.select("userId").from("users").where({
                        uuid: uuid
                    }).first());

                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }

                strSQL
                    .then(results => {
                        _.each(results, row => {
                            row.isDefaultAddress =
                                _.isNull(row.isDefaultAddress) || row.isDefaultAddress == 0 ?
                                false :
                                true;
                            row.isPickUpAddress =
                                _.isNull(row.isPickUpAddress) || row.isPickUpAddress == 0 ?
                                false :
                                true;
                        });

                        return callback(null, numRows, numPages, results);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, banks, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            result: banks,
                            pagination: {
                                total: numRows,
                                startAt: page + 1,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            result: banks,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: 0,
                            page_size: 0,
                            maxResult: 0
                        }
                    });
                }
            }
        ],
        next
    );
};

exports.getUserAddress = (uuid, _id, next) => {
    knex
        .select(
            "userAddressId AS _id",
            "name",
            "detailAddress",
            "city",
            "suburb",
            "state",
            "country",
            "zipcode",
            "mobilenum AS mobile",
            "isDefaultAddress",
            "isPickUpAddress",
            "uuid"
        )
        .from("user_address")
        .where(str => {
            str.where({
                userAddressId: _id
            }).orWhere({
                uuid: _id
            });
        })
        .andWhere("userId", knex.select("userId").from("users").where("uuid", uuid).first())
        .then(results => {
            _.each(results, row => {
                row.isDefaultAddress =
                    _.isNull(row.isDefaultAddress) || row.isDefaultAddress == 0 ?
                    false :
                    true;
                row.isPickUpAddress =
                    _.isNull(row.isPickUpAddress) || row.isPickUpAddress == 0 ?
                    false :
                    true;
            });

            return next(null, results);
        })
        .catch(err => {
            return next(err, null);
        });
};

exports.deleteUserAddress = (uuid, _id, next) => {
    knex("user_address")
        .where(str => {
            str.where({
                userAddressId: _id
            }).orWhere({
                uuid: _id
            });
        })
        .andWhere("userId", knex.select("userId").from("users").where("uuid", uuid).first())
        .delete()
        .asCallback(next);
};

exports.createUserAddress = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                if (user) {
                    console.log("user: ", user);
                    knex("user_address")
                        .insert({
                            name: data.name,
                            detailAddress: data.detailAddress,
                            city: data.city,
                            suburb: data.suburb,
                            state: data.state,
                            country: data.country,
                            zipcode: data.zipcode,
                            mobilenum: data.mobile,
                            isDefaultAddress: data.isDefault || 0,
                            isPickUpAddress: data.isPickup || 0,
                            userId: user.userId,
                            uuid: knex.raw("UUID()")
                        })
                        .asCallback(callback);
                } else {
                    return next({
                            message: "No user or token associated"
                        },
                        null
                    );
                }
            }
        ],
        next
    );
};

exports.updateUserAddress = (uuid, _id, data, next) => {
    async.waterfall(
        [
            callback => {
                knex("user_address")
                    .where(str => {
                        str.where("userAddressId", "<>", _id).orWhere("uuid", "<>", _id);
                    })
                    .andWhere(
                        "userId",
                        knex
                        .select("userId")
                        .from("users")
                        .where("uuid", uuid)
                        .first()
                    )
                    .update({
                        isDefaultAddress: !data.isDefault,
                        isPickUpAddress: !data.isPickup
                    })
                    .asCallback(callback);
            },
            callback => {
                knex
                    .transaction(trx => {
                        return trx
                            .update({
                                name: data.name,
                                detailAddress: data.detailAddress,
                                city: data.city,
                                suburb: data.suburb,
                                state: data.state,
                                country: data.country,
                                zipcode: data.zipcode,
                                mobilenum: data.mobilenum,
                                isDefaultAddress: data.isDefault || 0,
                                isPickUpAddress: data.isPickup || 0
                            })
                            .from("user_address")
                            .where(str => {
                                str
                                    .where("userAddressId", "<>", _id)
                                    .orWhere("uuid", "<>", _id);
                            })
                            .andWhere(
                                "userId",
                                knex
                                .select("userId")
                                .from("users")
                                .where("uuid", uuid)
                                .first()
                            );
                    })
                    .asCallback(callback);
            }
        ],
        next
    );
};

exports.updateUserPassword = (data, next) => {
    data.password = bcrypt.hashSync(data.password, bcrypt.genSaltSync(8), null);
    knex
        .transaction(trx => {
            return trx
                .update({
                    password: data.password
                })
                .from("users")
                .where("userId", data.uuid)
                .orWhere("uuid", data.uuid);
        })
        .asCallback(next);
};

exports.updateUserEmail = (user_id, data, next) => {
    knex
        .transaction(trx => {
            return trx
                .update({
                    email: data.email
                })
                .from("users")
                .where("userId", user_id)
                .orWhere("uuid", user_id);
        })
        .asCallback(next);
};

exports.updateUserAccount = (user_id, data, next) => {
    knex
        .transaction(trx => {
            return trx
                .update({
                    gender: data.gender,
                    birthday: data.birthday,
                    phone: data.phone,
                    fax: data.fax
                })
                .from("users")
                .where("userId", user_id)
                .orWhere("uuid", user_id);
        })
        .asCallback(next);
};

exports.updateUserCurrency = (user_id, data, next) => {
    knex
        .transaction(trx => {
            return trx
                .update({
                    currencyId: data.currency
                })
                .from("users")
                .where("userId", user_id)
                .orWhere("uuid", user_id);
        })
        .asCallback(next);
};

exports.updateUserCountry = (user_id, data, next) => {
    knex
        .transaction(trx => {
            return trx
                .update({
                    countryId: data.country
                })
                .from("users")
                .where("userId", user_id)
                .orWhere("uuid", user_id);
        })
        .asCallback(next);
};

exports.applyUserAsSeller = (user_id, next) => {
    let sellerApprovalCode = func.generateNumberString(14);
    async.waterfall(
        [
            callback => {
                knex("users")
                    .where(str => {
                        str.where("userId", user_id).orWhere("uuid", user_id);
                    })
                    .update({
                        isSeller: 0,
                        sellerApplicationDate: knex.raw("NOW()"),
                        sellerApprovalCode: sellerApprovalCode
                    })
                    .then(() => {
                        return callback();
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            callback => {
                knex
                    .select(
                        "username",
                        "email",
                        "firstname",
                        "lastname",
                        "sellerApprovalDate",
                        "sellerApplicationDate",
                        "user_img_path AS img_path",
                        "user_img_name AS img_name",
                        "user_img_type AS img_type",
                        "user_img_size AS img_size",
                        "uuid"
                    )
                    .from("users")
                    .where("userId", user_id)
                    .orWhere("uuid", user_id)
                    .first()
                    .then(response => {
                        response.sellerApprovalCode = sellerApprovalCode;
                        return callback(null, response);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            }
        ],
        next
    );
};

exports.validatePassword = (uuid, data, next) => {
    knex
        .select("password")
        .from("users")
        .where("userId", uuid)
        .orWhere("uuid", uuid)
        .first()
        .then(response => {
            if (response) {
                bcrypt.compare(data.password, response.password, (err, res) => {
                    if (res) {
                        return next(null, {
                            success: true
                        });
                    } else {
                        return next(null, {
                            success: false
                        });
                    }
                });
            } else {
                return next(null, {
                    success: false
                });
            }
        })
        .catch(err => {
            return next(err, null);
        });
};

exports.getAllUserBanks = (uuid, params, next) => {
    if (params.limit <= 10) params.limit = 10;

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + "," + numPerPage;

    async.waterfall(
        [
            callback => {
                knex("user_bank")
                    .count("* AS numRows")
                    .where("userId", knex.select("userId").from("users").where("uuid", uuid).first())
                    .then(results => {
                        numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, callback) => {
                let strSQL = knex
                    .select()
                    .from("user_bank")
                    .where("userId", knex.select("userId").from("users").where("uuid", uuid).first());
                if (params.limit && params.startWith) {
                    strSQL = strSQL.limit(numPerPage).offset(skip);
                } else if (params.limit) {
                    strSQL = strSQL.limit(numPerPage);
                }
                strSQL
                    .then(results => {
                        _.each(results, row => {
                            row.accountNo = "*" + row.accountNo.slice(-4);
                        });
                        return callback(null, numRows, numPages, results);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (numRows, numPages, banks, callback) => {
                if (params.limit && params.startWith) {
                    if (page < numPages) {
                        callback(null, {
                            result: banks,
                            pagination: {
                                total: numRows,
                                startAt: page + 1,
                                page_size: numPerPage,
                                maxResult: numPages
                            }
                        });
                    } else {
                        callback(null, {
                            result: banks,
                            pagination: {
                                total: numRows,
                                startAt: page,
                                page_size: numPerPage,
                                maxResult: numPages || 0
                            }
                        });
                    }
                } else {
                    callback(null, {
                        result: banks,
                        pagination: {
                            total: numRows,
                            startAt: 0,
                            page_size: 0,
                            maxResult: 0
                        }
                    });
                }
            }
        ],
        next
    );
};

exports.saveUserBank = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                let strSQL = mysql.format(
                    "SELECT userId FROM users WHERE uuid = ? LIMIT 1;",
                    [uuid]
                );
                console.log("strSQL: ", strSQL);
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                if (user) {
                    console.log("user: ", user);
                    knex("user_bank")
                        .insert({
                            name: data.name,
                            accountNo: data.accountNo,
                            userId: user.userId,
                            bankId: data.bankId,
                            uuid: knex.raw("UUID()")
                        })
                        .asCallback(callback);
                } else {
                    return next({
                            message: "No user or token associated"
                        },
                        null
                    );
                }
            }
        ],
        next
    );
};

exports.getUserBank = (uuid, _id, next) => {
    knex
        .select(
            "ub.userBankId as _id",
            "ub.name",
            "ub.accountNo",
            "ub.bankId",
            knex.raw("(SELECT b.name FROM bank b WHERE b.bankId=ub.bankId LIMIT 1) as bankName"),
            "ub.uuid"
        )
        .from("user_bank as ub")
        .where(str => {
            str.where("ub.userBankId", _id).orWhere("ub.uuid", _id);
        })
        .andWhere("userId", uuid)
        .asCallback(next);
};

exports.deleteUserBank = (uuid, _id, next) => {
    knex
        .transaction(trx => {
            return trx
                .delete()
                .from("user_bank")
                .where(str => {
                    str.where("userBankId", _id).orWhere("uuid", _id);
                })
                .andWhere("userId", uuid);
        })
        .asCallback(next);
};

exports.updateUserBank = (uuid, _id, data, next) => {
    knex
        .transaction(trx => {
            return trx
                .update({
                    name: data.name,
                    accountNo: data.accountNo,
                    bankId: data.bankId
                })
                .from("user_bank")
                .where(str => {
                    str.where("userBankId", _id).orWhere("uuid", _id);
                })
                .andWhere("userId", uuid);
        })
        .asCallback(next);
};

exports.createUserSellerShipping = (uuid, data, next) => {
    knex
        .transaction(trx => {
            return trx
                .insert({
                    shippingId: data.shippingId,
                    userId: knex
                        .select("userId")
                        .from("users")
                        .where("uuid", uuid)
                        .first(),
                    uuid: knex.raw("UUID()")
                })
                .into("user_shipping");
        })
        .asCallback(next);
};

exports.getAllUserSellerShipping = (uuid, next) => {
    knex
        .select(
            "shippingId",
            knex.raw("(SELECT name FROM shipping_courier WHERE shippingCourierId= shippingId LIMIT 1) AS courierName")
        )
        .from("user_shipping")
        .where("userId", uuid)
        .asCallback(next);
};

exports.deleteUserSellerShipping = (uuid, _id, next) => {
    knex
        .transaction(trx => {
            return trx
                .delete()
                .from("user_shipping")
                .where("userId", knex.select("userId").from("users").where("uuid", uuid).first())
                .andWhere("shippingId", _id);
        })
        .asCallback(next);
};

exports.saveUserDevice = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                if (user && data) {
                    console.log("user: ", user);
                    knex
                        .transaction(trx => {
                            return trx
                                .insert({
                                        userId: user.userId,
                                        onesignal_player_id: data.player_id,
                                        device_model: data.device_model,
                                        device_version: data.device_version,
                                        device_manufacturer: data.device_manufacturer,
                                        device_uuid: data.device_uuid,
                                        device_serial: data.device_serial,
                                        date_created: knex.raw("NOW()"),
                                        uuid: knex.raw("UUID()")
                                    },
                                    "device_id"
                                )
                                .into("user_devices");
                        })
                        .asCallback(callback);
                } else {
                    callback();
                }
            }
        ],
        next
    );
};

exports.deleteUserDevice = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                if (user && data) {
                    console.log("user: ", user);
                    knex("user_devices")
                        .where("userId", user.userId)
                        .andWhere("device_uuid", data.device_uuid)
                        .andWhere("onesignal_player_id", data.player_id)
                        .delete()
                        .asCallback(next);
                } else {
                    callback();
                }
            }
        ],
        next
    );
};

exports.linkFacebookAccount = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                knex
                    .transaction(trx => {
                        return trx
                            .update({
                                facebookId: data.Id
                            })
                            .from("users")
                            .where("userId", user.userId);
                    })
                    .asCallback(callback);
            }
        ],
        next
    );
};

exports.unlinkFacebookAccount = (uuid, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                console.log("user: ", user);
                knex
                    .transaction(trx => {
                        return trx
                            .update({
                                facebookId: null
                            })
                            .from("users")
                            .where("userId", user.userId);
                    })
                    .asCallback(callback);
            }
        ],
        next
    );
};

exports.linkGoogleAccount = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                console.log("user: ", user);
                knex
                    .transaction(trx => {
                        return trx
                            .update({
                                googleId: data.Id
                            })
                            .from("users")
                            .where("userId", user.userId);
                    })
                    .asCallback(callback);
            }
        ],
        next
    );
};

exports.unlinkGoogleAccount = (uuid, next) => {
    async.waterfall(
        [
            callback => {
                knex
                    .select("userId")
                    .from("users")
                    .where("uuid", uuid)
                    .first()
                    .then(user => {
                        return callback(null, user);
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            },
            (user, callback) => {
                console.log("user: ", user);
                knex
                    .transaction(trx => {
                        return trx
                            .update({
                                googleId: null
                            })
                            .from("users")
                            .where("userId", user.userId);
                    })
                    .asCallback(callback);
            }
        ],
        next
    );
};

exports.loginSocial = (provider, Id, next) => {
    let strSQL = knex.select(
        "username",
        "password",
        "isVerify",
        "email",
        "firstname",
        "lastname",
        "isSeller",
        "sellerApprovalCode",
        "user_img_path AS img_path",
        "user_img_name AS img_name",
        "user_img_type AS img_type",
        "user_img_size AS img_size",
        "uuid"
    ).from("users");

    if (_.toLower(provider) == "facebook") {
        strSQL = strSQL.where("facebookId", Id).first();
    } else if (_.toLower(provider) == "google") {
        strSQL = strSQL.where("googleId", Id).first();
    }
    strSQL.asCallback(next);
};

exports.isSocialAccountExisted = (provider, Id, next) => {
    let strSQL = null;
    if (_.toLower(provider) == "facebook") {
        strSQL = knex("users")
            .where("facebookId", Id)
            .first();
    } else if (_.toLower(provider) == "google") {
        strSQL = knex("users")
            .where("googleId", Id)
            .first();
    }
    strSQL.asCallback(next);
};

exports.socialSignUp = (data, next) => {
    const SendBird = require("sendbird-nodejs");
    let sb = SendBird(config.sendbird_app_token);

    data.uuid = uuidv1();
    data.verificationCode = func.generateString(36);

    const payload = {
        user_id: data.uuid,
        nickname: data.email,
        profile_url: data.email,
        issue_access_token: true // (Optional)
    };

    let strSQL;
    sb.users.create(payload).then(
        response => {
            console.log("SendBird response: ", response);

            let sSQL = {
                firstname: data.firstname,
                lastname: data.lastname,
                username: data.username,
                email: data.email,
                isVerify: data.isVerify || 0,
                verificationCode: data.verificationCode,
                registrationDate: moment(Date.now()).format("YYYY-MM-DD HH:mm:ss"),
                sendBirdToken: response.access_token,
                uuid: data.uuid
            };

            if (_.toLower(data.provider) == "facebook") {
                sSQL.facebookId = data.Id;
                sSQL.user_img_path = data.img_path;
            } else if (_.toLower(data.provider) == "google") {
                sSQL.googleId = data.Id;
                sSQL.user_img_path = data.img_path;
            }

            knex.transaction(trx => {
                return trx.insert(sSQL, "userId")
                    .into("users")
                    .then(user => {
                        user = user[0];
                        return knex.select('username', 'email', 'firstname', 'lastname', 'phone', 'fax', 'verificationCode', 'uuid')
                            .from("users")
                            .where("userId", user)
                            .first();
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            }).asCallback(next);
        },
        error => {
            console.log("SendBird error: ", error);
        }
    );
};

exports.followUser = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex.select('userId', 'username', 'firstname', 'lastname', 'user_img_path', 'uuid').from('users').where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid);
                }).first().then(follower => {
                    return callback(null, follower);
                }).catch(err => {
                    return next(err, null);
                });
            },
            (follower, callback) => {
                knex.select('userId', 'username', 'firstname', 'lastname', 'user_img_path', 'uuid').from('users').where(sql => {
                    sql.where("uuid", data.userId).orWhere("userId", data.userId);
                }).first().then(currentUser => {
                    if (follower.userId === currentUser.userId) {
                        return next(null, {
                            msg: "You are not allow to follow your account",
                            result: null,
                            success: false
                        });
                    } else {
                        return callback(null, follower, currentUser);
                    }
                }).catch(err => {
                    return next(err, null);
                });
            },
            (follower, currentUser, callback) => {
                console.log("currentUser: ", currentUser);
                console.log("follower: ", follower);
                knex.transaction(trx => {
                    return trx.insert({
                            userId: follower.userId,
                            followerUserId: currentUser.userId,
                            followedDate: knex.raw('NOW()'),
                            uuid: knex.raw('UUID')
                        }, "userFollowersId").into("user_followers")
                        .then(response => {
                            return callback(null, follower, currentUser, {
                                msg: "Followed successfully!",
                                result: response,
                                success: true
                            });
                        }).catch(err => {
                            return next(err, null);
                        });
                });

            },
            (follower, currentUser, newsletter, callback) => {
                const linkObj = {
                    action: "follow",
                    followerId: follower.userId,
                    followerUUID: follower.uuid,
                    userId: currentUser.userId,
                    userUUID: currentUser.uuid
                };
                const title = follower.username + " started following you.";
                const content = "<b>" + follower.username + "</b> started following you.";
                knex.transaction(trx => {
                    return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw('NOW()'),
                        link: JSON.stringify(linkObj),
                        notifTypeId: 2
                    }, 'notificationId').into('notifications').then(notifId => {
                        notifId = notifId[0];
                        knex.from("user_notifications").insert(function () {
                            this.from('user_devices as ud')
                                .where('ud.userId', currentUser.userId)
                                .select(knex.raw('?', [notifId]), 0, knex.raw('?', [currentUser.userId]), 'NOW()', 'UUID()')
                        }).then(() => {
                            return callback(null, newsletter, notifId, linkObj);
                        });
                    }).catch(err => {
                        return next(err, null);
                    });
                });
            },
            (newsletter, notifId, linkObj, callback) => {
                knex('notifications').where("notificationId", notifId).first().then(notifications => {
                    knex.select('userId', 'user_player_ids').from('user_notifications').where("notificationId", notifId)
                        .then(notifications => {
                            console.log("user_notifications: ", user_notifications);
                            notifications.user_notifications = user_notifications;
                            notifications.notification_data = linkObj;
                            console.log("notifications: ", notifications);
                            return callback(null, {
                                notifications: notifications,
                                response: newsletter
                            });
                        }).catch(err => {
                            return next(err, null);
                        });
                }).catch(err => {
                    return next(err, null);
                });
            }
        ],
        next
    );
};

exports.unfollowUser = (uuid, data, next) => {
    async.waterfall(
        [
            callback => {
                knex.select('userId').from('users').where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid);
                }).first().then(user => {
                    return callback(null, user);
                }).catch(err => {
                    return next(err, null);
                });

            },
            (user, callback) => {
                knex.select('userId').from('users').where(sql => {
                    sql.where("uuid", data.userId).orWhere("userId", data.userId);
                }).first().then(follower => {
                    return callback(null, user, follower);
                }).catch(err => {
                    return next(err, null);
                });
            },
            (user, follower, callback) => {
                knex.transaction(trx => {
                    return trx.where(sql => {
                        sql.where("userId", user.userId).andWhere("followerUserId", follower.userId)
                    }).from("user_followers").delete().asCallback(callback);
                });
            }
        ],
        next
    );
};

exports.checkIfUserIfFollowed = (uuid, userId, next) => {
    async.waterfall(
        [
            callback => {
                knex.select('userId').from('users').where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid);
                }).first().then(user => {
                    return callback(null, user);
                }).catch(err => {
                    return next(err, null);
                });
            },
            (user, callback) => {
                knex.select('userId').from('users').where(sql => {
                    sql.where("uuid", userId).orWhere("userId", userId);
                }).first().then(follower => {
                    return callback(null, user, follower);
                }).catch(err => {
                    return next(err, null);
                });

            },
            (user, follower, callback) => {
                knex('user_followers').where(sql => {
                    sql.where("userId", user.userId).andWhere("followerUserId", follower.userId);
                }).first().asCallback(callback);
            }
        ],
        next
    );
};

exports.getUserFollowing = (uuid, next) => {
    async.waterfall(
        [
            callback => {
                knex.select('userId').from('users').where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid);
                }).first().then(user => {
                    return callback(null, user);
                }).catch(err => {
                    return next(err, null);
                });
            },
            (user, callback) => {
                knex.select('u.username', 'u.email', 'u.firstname', 'u.lastname', 'u.sellerName', 'u.sellerDescription', 'u.user_img_path', 'u.uuid')
                    .from('user_followers AS uf').innerJoin('users AS u', "u.userId", "uf.followerUserId")
                    .where("uf.userId", user.userId).debug(true).asCallback(callback);
            }
        ],
        next
    );
};

exports.getUserFollowers = (uuid, next) => {
    async.waterfall(
        [
            callback => {
                knex.select('userId').from('users').where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid);
                }).first().then(user => {
                    return callback(null, user);
                }).catch(err => {
                    return next(err, null);
                });
            },
            (user, callback) => {
                knex.select('u.username', 'u.email', 'u.firstname', 'u.lastname', 'u.sellerName', 'u.sellerDescription', 'u.user_img_path', 'u.uuid')
                    .from('user_followers AS uf').innerJoin('users AS u', "u.userId", "uf.userId")
                    .where("uf.followerUserId", user.userId).asCallback(callback);
            }
        ],
        next
    );
};

exports.uploadUserAvatar = (data, next) => {
    knex.transaction(trx => {
        return trx.update({
            user_img_name: data.img_name,
            user_img_path: data.img_path,
            user_img_type: data.img_type,
            user_img_size: data.img_size,
        }).from('users').where(sql => {
            sql.where("userId", data.uuid).orWhere("uuid", data.uuid)
        }).asCallback(next);
    });
};

exports.applyUserAsSellerUploadSelfie = (data, next) => {
    let saveToHistory = (data_to_history, trx) => {
        knex('seller_evaluation_history').transacting(trx).insert({
                seller_evaluationId: data_to_history.seller_evaluationId,
                userId: data_to_history.userId,
                statusClass: data_to_history.statusClass,
                tagName: data_to_history.tagName,
                content: data_to_history.content,
                eval_categoryId: data_to_history.eval_categoryId
            }).then(function (resp) {
                let id = resp[0];
                return {
                    seller_evaluationId: data_to_history.seller_evaluationId,
                    userId: data_to_history.userId,
                    seller_evaluation_history_resp: resp[0]
                }
            })
            .then(trx.commit)
            .catch(trx.rollback);
    };
    knex.select('userId', 'username', 'uuid').from('users').where({
        uuid: data.uuid
    }).first().then(result => {
        let shop_ref_code = func.generateNumberString(10);
        let shop_url_code = func.generateString(42);
        knex.transaction(function (trx) {
                knex('seller_evaluation').transacting(trx).insert({
                        userId: result.userId,
                        seller_selfie_file: data.seller_selfie_file,
                        seller_selfie_filename: data.seller_selfie_filename,
                        seller_government_idnumber: '',
                        seller_government_id: '',
                        seller_government_id_filename: '',
                        seller_proof_address: '',
                        seller_proof_address_filename: '',
                        seller_supporting_docs: '',
                        seller_supporting_docs_filename: '',
                        seller_ref_code: shop_ref_code,
                        seller_url_code: shop_url_code,
                    })
                    .then(function (resp) {
                        let id = resp[0];
                        // This is the fields required in seller_evaluation_history
                        let hist = {
                            seller_evaluationId: id,
                            userId: result.userId,
                            statusClass: data['status'] ? data['status'] : '',
                            tagName: data['tag'] ? data['tag'] : '',
                            content: data['content'] ? data['content'] : '',
                            eval_categoryId: 1,
                        }
                        return saveToHistory(hist, trx);
                    })
            })
            .then(function (resp) {
                return next(null, resp);
            })
            .catch(function (err) {
                console.error(err);
                return next(err, err)
            });
    }).catch(err => {
        return next(err, null);
    });

};

exports.applyUserAsSellerUploadProof = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                seller_proof_address: data.seller_proof_address,
                seller_proof_address_filename: data.seller_proof_address_filename,
            })
            .from('seller_evaluation')
            .where({
                seller_evaluationId: data.seller_evaluationId
            })
    }).debug(true).asCallback(next);
}

exports.applyUserAsSellerUploadGovId = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                seller_government_id: data.seller_government_id,
                seller_government_id_filename: data.seller_government_id_filename,
                seller_government_idnumber: data.seller_government_idnumber
            })
            .from('seller_evaluation')
            .where({
                seller_evaluationId: data.seller_evaluationId
            })
    }).debug(true).asCallback(next);
}

exports.applyUserAsSellerUploadPersonalId = (data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                seller_supporting_docs: data.seller_supporting_docs,
                seller_supporting_docs_filename: data.seller_supporting_docs_filename,
            })
            .from('seller_evaluation')
            .where({
                seller_evaluationId: data.seller_evaluationId
            })
    }).debug(true).asCallback(next);
}

exports.uploadUserBanner = (data, next) => {
    let strSQL = mysql.format(
        "UPDATE users SET user_banner_name=?,user_banner_path=?,user_banner_type=?,user_banner_size=? WHERE (userId=? OR uuid=?);",
        [
            data.img_name,
            data.img_path,
            data.img_type,
            data.img_size,
            data.uuid,
            data.uuid
        ]
    );
    console.log("uploadUserBanner strSQL: ", strSQL);
    db.insertWithId(strSQL, next);
};

exports.deleteUserAvatar = (uuid, next) => {
    require("isomorphic-fetch");
    const Dropbox = require("dropbox").Dropbox;
    const dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    knex
        .select("user_img_path", "user_img_name", "user_img_type", "user_img_size")
        .from("users")
        .where({
            userId: uuid
        })
        .orWhere({
            uuid: uuid
        })
        .first()
        .then(res => {
            if (res) {
                dbx
                    .filesDelete({
                        path: res.user_img_name
                    })
                    .then(resp => {
                        console.log("Delete User Avatar: ", resp);
                        return next();
                    })
                    .catch(error => {
                        console.error("Delete User Avatar error: ", error);
                        return next();
                    });
            } else {
                return next();
            }
        })
        .catch(err => {
            return next(err, null);
        });
};

exports.deleteUserBanner = (uuid, next) => {
    require("isomorphic-fetch");
    const Dropbox = require("dropbox").Dropbox;
    const dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });
    knex
        .select(
            "user_banner_path",
            "user_banner_name",
            "user_banner_type",
            "user_banner_size"
        )
        .from("users")
        .where({
            userId: uuid
        })
        .orWhere({
            uuid: uuid
        })
        .first()
        .then(res => {
            if (res) {
                dbx
                    .filesDelete({
                        path: res.user_banner_name
                    })
                    .then(resp => {
                        console.log("Delete User Banner: ", resp);
                        return next();
                    })
                    .catch(error => {
                        console.error("Delete User Banner error: ", error);
                        return next();
                    });
            } else {
                return next();
            }
        })
        .catch(err => {
            return next(err, null);
        });
};

exports.userRatings = (uuid, params, next) => {
    if (params.limit <= 10) params.limit = 10;
    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 10;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + "," + numPerPage;

    let ratingsArr = []

    async.waterfall([
        (callback) => {
            knex
                .select("userId")
                .from("users")
                .where("uuid", uuid)
                .first()
                .asCallback((err, resp) => {
                    if (resp && resp.userId) {
                        callback(null, resp.userId)
                    } else {
                        next(err, resp)
                    }
                })
        },
        (userId, callback) => {
            let strSQL = knex.count("* AS numRows")
                .from("buyer_rating AS br")
                .where("br.buyer_userId", userId);

            strSQL.asCallback((err, results) => {
                if (err) {
                    return next(err, null);
                } else {
                    numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages, userId);
                }
            });
        },
        (numRows, numPages, userId, callback) => {
            let strSQL = knex.select(
                "*",
                knex.raw("(select u.username from users u where u.userId = br.seller_userId) as seller_name")
            )
            .from("buyer_rating AS br")
            .where("br.buyer_userId", userId);

            if (params.limit && params.startWith) {
                strSQL.limit(limit).offset(params.startWith);
            } else if (params.limit) {
                strSQL.limit(numPerPage);
            } else {
                strSQL.limit(numPerPage);
            }
            strSQL.asCallback((err, buyer_ratings) => {
                console.log('err ', err)
                if (err) {
                    next(err, null)
                } else {
                    callback(null, numRows, numPages, buyer_ratings)
                }
            })
        },
        (numRows, numPages, buyer_ratings, callback) => {
            knex.select("od.*", "p.productSKU", "p.productName", "p.productDesc", "p.productPrice",
                "p.productWeight", "p.productWidth", "p.productCondition", "p.productBrandId",
                "p.productStock", "p.productCategoryID", "p.productSubCategoryId", "p.productLength",
                "p.productHeight",
                knex.raw("(SELECT b.name FROM brand b where b.brandId = p.productBrandId) as brand"),
                knex.raw("(SELECT c.name FROM categories c where c.categoryId = p.productCategoryID) as category_name"),
                knex.raw("(SELECT s.name FROM subcategory s where s.subcategoryId = p.productSubCategoryID) as sub_category_name")
            )
            .from("order_details as od")
            .rightJoin("products as p", "od.productId", "p.productId")
            .asCallback((err, order_details) => {
                if(err) {
                    next(err, null)
                } else {
                    callback(null, numRows, numPages, buyer_ratings, order_details)
                }
            })
        },
        (numRows, numPages, buyer_ratings, order_details, callback) => {
            if(buyer_ratings && buyer_ratings.length > 0){
                _.each(buyer_ratings, (value, index) => {
                    value['order_details'] = _.filter(order_details, (od)=>{
                        return value.orderId == od.orderId
                    })

                    if(index == buyer_ratings.length-1){
                        callback(null, numRows, numPages, buyer_ratings)
                    }
                })
            } else {
                callback(null, numRows, numPages, buyer_ratings)
            }
        },
        (numRows, numPages, buyer_ratings, callback) => {
            callback(null, {
                result: buyer_ratings,
                pagination: {
                    total: numRows,
                    startAt: (page + 1),
                    page_size: numPerPage,
                    maxResult: numPages
                }
            })
        }
    ], next)
};