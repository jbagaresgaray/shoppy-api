'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const constant = require("../../../config/constants");
const async = require('async');
const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkIfProductIsLiked = (user, productId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, response);
                }
            });
        },
        (_user, product, callback) => {
            knex("product_like").where("liked_userId", _user.userId).andWhere("productId", product.productId)
                .first().asCallback(callback);
        }
    ], next);
};


exports.likeProduct = (user, productId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId", "username").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("productId", "productName", "userId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, response);
                }
            });
        },
        (_user, product, callback) => {
            knex.transaction(trx => {
                return trx.insert({
                        liked_userId: _user.userId,
                        productId: product.productId,
                        likedDateTime: knex.raw("NOW()"),
                        uuid: knex.raw("UUID()")
                    }, 'productLikeId').into("product_like")
                    .asCallback((err, response) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            response = response[0];
                            return callback(null, _user, product, response);
                        }
                    });
            });
        },
        (_user, product, productLikeId, callback) => {
            const title = _user.username + ' liked your product: ' + product.productName;
            const content = '<b>' + _user.username + '</b> liked your product: <b>' + product.productName + '</b>';
            const linkObj = {
                action: 'like',
                likeBy: _user.userId,
                productId: product.productId,
                productInfo: product,
                storeUserId: product.userId
            };
            knex.transaction(trx => {
                return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw("NOW()"),
                        link: JSON.stringify(linkObj),
                        notifTypeId: constant.NOTIFICATION_TYPE.ACTIVITY,
                        uuid: knex.raw("UUID()")
                    }, 'notificationId')
                    .into("notifications")
                    .asCallback((err, notifId) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            async.waterfall([
                                (cb) => {
                                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) ' +
                                        'SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                                            notifId, product.userId, product.userId
                                        ]).asCallback((err, resp) => {
                                        if (err) {
                                            return next(err);
                                        } else {
                                            return cb();
                                        }
                                    });
                                },
                                (cb) => {
                                    knex.select("n.*", "un.isRead", "un.userId", "un.user_player_ids")
                                        .from("notifications AS n")
                                        .innerJoin("user_notifications AS un", "n.notificationId", "un.notificationId")
                                        .where("n.notificationId", notifId)
                                        .asCallback((err, resp) => {
                                            if (err) {
                                                return next(err);
                                            } else {
                                                const callbc = {
                                                    notifId: notifId,
                                                    productLikeId: productLikeId,
                                                    notification: resp,
                                                    notification_data: linkObj
                                                };
                                                console.log("callbc: ", callbc)
                                                return cb(null, callbc);
                                            }
                                        });
                                }
                            ], callback);
                        }
                    });
            });
        }
    ], next);
};


exports.unlikeProduct = (user, productId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, response);
                }
            });
        },
        (_user, product, callback) => {
            knex.transaction(trx => {
                return trx.from("product_like")
                    .where("liked_userId", _user.userId).andWhere("productId", product.productId)
                    .delete()
                    .asCallback(callback);
            })
        }
    ], next);
};


exports.getAllUserProductLikes = (user, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("p.*", knex.raw("IF(pr.rating,AVG(pr.rating),0)  AS rating"), knex.raw("COUNT(pl.productId) AS liked"))
                .from("products AS p")
                .leftJoin("product_like AS pl", "p.productId", "pl.productId")
                .leftJoin("product_rating AS pr", "p.productId", "pr.productId")
                .where("pl.liked_userId", _user.userId)
                .groupBy("p.productId")
                .asCallback((err, results) => {
                    if (err) {
                        next(err, null);
                    } else {
                        async.eachSeries(results, (item, cb) => {
                            async.parallel([
                                (callback1) => {
                                    knex("categories").where("categoryId", item.productCategoryID).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                if (response) {
                                                    item.category = response;
                                                } else {
                                                    item.category = {};
                                                }
                                                return callback1();
                                            }
                                        })
                                },
                                (callback1) => {
                                    knex("subcategory").where("subcategoryId", item.productSubCategoryId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                if (response) {
                                                    item.subcategory = response;
                                                } else {
                                                    item.subcategory = {};
                                                }
                                                return callback1();
                                            }
                                        });
                                },
                                (callback1) => {
                                    knex("brand").where("brandId", item.productBrandId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                next(err, null);
                                            } else {
                                                if (response) {
                                                    item.brand = response;
                                                } else {
                                                    item.brand = {};
                                                }
                                                return callback1();
                                            }
                                        })
                                },
                                (callback1) => {
                                    knex("product_variation").where("productId", item.productId)
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                item.variants = response;
                                                return callback1();
                                            }
                                        })
                                },
                                (callback1) => {
                                    knex("product_wholesale").where("productId", item.productId)
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                item.wholesale = response;
                                                return callback1();
                                            }
                                        });
                                },
                                (callback1) => {
                                    knex.select("pse.*", "sc.name")
                                        .from("product_shipping_fee AS pse")
                                        .innerJoin("shipping_courier AS sc", "pse.shippingCourierId", "sc.shippingCourierId")
                                        .where("pse.productId", item.productId)
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                item.shippingFee = response;
                                                return callback1();
                                            }
                                        });
                                },
                                (callback1) => {
                                    knex("product_images").where("productId", item.productId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                if (response) {
                                                    item.image = response;
                                                } else {
                                                    item.image = {};
                                                }
                                                return callback1();
                                            }
                                        });
                                }
                            ], cb);
                        }, () => {
                            callback(null, results);
                        });
                    }
                })
        }
    ], next);
};