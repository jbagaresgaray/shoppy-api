'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const func = require('../../../app/utils/functions');
const constant = require("../../../config/constants");

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
const async = require('async');
const fs = require('fs');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.isOrderCancelledRequested = (uuid, orderId, next) => {
    knex("orders").whereRaw("cancelledRequested IS NOT NULL").andWhereRaw("cancelledRequestDeadline IS NOT NULL").andWhere(sql => {
        sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
    }).asCallback(next);
};

exports.isOrderCancelled = (uuid, orderId, next) => {
    knex("orders").where("isCancelled", 1).andWhereRaw("cancelledDateTime IS NOT NULL").andWhere(sql => {
        sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
    }).asCallback(next);
};

exports.cancelOrderRequest = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where(sql => {
                    sql.where("userId", uuid).andWhere("uuid", uuid);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where("userId", knex.raw("(SELECT sellerId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1))", [orderId, orderId, orderId]))
                .first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        cancelledRequested: data.cancelledRequested,
                        cancelledRequestDeadline: data.cancelledRequestDeadline,
                        cancelledReason: data.cancelledReason,
                        cancelledBy: data.cancelledBy
                    }).from("orders").where(sql => {
                        sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId)
                    }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]))
                    .asCallback((err, response) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            console.log('cancelOrderRequest Response: ', response);
                            return callback(null, buyer, seller);
                        }
                    });
            });
        },
        (buyer, seller, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o").where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE (uuid=? OR userId=?) LIMIT 1)", [
                    seller.userId, seller.userId
                ])).andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        return callback(null, orders, buyer, seller);
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("od.*", "p.productDesc").from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("orderId", orderId).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first().asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (response) {
                                        item.image = response;
                                        return cb();
                                    } else {
                                        item.image = {};
                                        return cb();
                                    }
                                }
                            });
                        }, () => {
                            orders.details = response;
                            return callback(null, orders, buyer, seller);
                        });
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", seller.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        console.log('storesPlayerID: ', storesPlayerID);
                        return callback(null, orders, buyer, seller);
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            const linkObj = {
                action: 'order_cancel',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
                seller: seller
            };

            const title = 'Order Cancellation Request'
            const content = '<b>' + buyer.username + '</b> has requested for cancellation of order <b>' + orders.orderNum + '</b>. Please respond or the order will be automatically cancelled.';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into("notifications");
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                orders: orders,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};

exports.cancelOrderDirectly = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where(sql => {
                    sql.where("userId", uuid).andWhere("uuid", uuid);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where("userId", knex.raw("(SELECT sellerId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1))", [orderId, orderId, orderId]))
                .first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                    isCancelled: 1,
                    cancelledDateTime: data.cancelledDateTime,
                    cancelledReason: data.cancelledReason,
                    cancelledBy: data.cancelledBy
                }).from("orders").where(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
                }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            }).asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('cancelOrderRequest Response: ', response);
                    return callback(null, buyer, seller);
                }
            });
        },
        (buyer, seller, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o").where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE (uuid=? OR userId=?) LIMIT 1)", [seller.userId, seller.userId])).andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        return callback(null, orders, buyer, seller);
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("od.*", "p.productDesc").from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("orderId", orderId).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first().asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (response) {
                                        item.image = response;
                                        return cb();
                                    } else {
                                        item.image = {};
                                        return cb();
                                    }
                                }
                            });
                        }, () => {
                            orders.details = response;
                            return callback(null, orders, buyer, seller);
                        });
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", seller.userId).asCallback((err, devices) => {
                if (err) {
                    return next(err, null);
                } else {
                    _.each(devices, (row) => {
                        storesPlayerID.push(row);
                    });
                    console.log('storesPlayerID: ', storesPlayerID);
                    return callback(null, orders, buyer, seller);
                }
            });
        },
        (orders, buyer, seller, callback) => {
            const linkObj = {
                action: 'order_cancel',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
                seller: seller
            };
            const title = 'Cancellation Request Accepted';
            const content = 'Your cancellation request has been approved. Order <b>' + orders.orderNum + '</b> has been cancelled.';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into('notifications');
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                orders: orders,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};

exports.cancelOrder = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where(sql => {
                    sql.where("userId", uuid).andWhere("uuid", uuid);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (seller, callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where("userId", knex.raw("(SELECT sellerId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1))", [orderId, orderId, orderId]))
                .first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, seller, user);
                    }
                });
        },
        (seller, buyer, callback) => {
            knex.transaction(trx => {
                return trx.update({
                    isCancelled: 1,
                    cancelledDateTime: data.cancelledDateTime
                }).from("orders").where(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
                }).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        console.log('shippedOrder Response: ', response);
                        return callback(null, seller, buyer);
                    }
                });
            });
        },
        (seller, buyer, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o").where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE (uuid=? OR userId=?) LIMIT 1)", [seller.userId, seller.userId])).andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        return callback(null, orders, seller, buyer);
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("od.*", "p.productDesc").from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("orderId", orderId).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first().asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (response) {
                                        item.image = response;
                                        return cb();
                                    } else {
                                        item.image = {};
                                        return cb();
                                    }
                                }
                            });
                        }, () => {
                            orders.details = response;
                            return callback(null, orders, seller, buyer);
                        });
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", buyer.userId).asCallback((err, devices) => {
                if (err) {
                    return next(err, null);
                } else {
                    _.each(devices, (row) => {
                        storesPlayerID.push(row);
                    });
                    console.log('storesPlayerID: ', storesPlayerID);
                    return callback(null, orders, seller, buyer);
                }
            });
        },
        (orders, seller, buyer, callback) => {
            const linkObj = {
                action: 'order_cancel',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
            };
            const title = 'Cancellation Request Accepted';
            const content = 'Your cancellation request has been approved. Order <b>' + orders.orderNum + '</b> has been cancelled.';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into('notifications');
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                orders: orders,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};

exports.rejectCancelOrder = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where(sql => {
                    sql.where("userId", uuid).andWhere("uuid", uuid);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (seller, callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where("userId", knex.raw("(SELECT sellerId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1))", [orderId, orderId, orderId]))
                .first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, seller, user);
                    }
                });
        },
        (seller, buyer, callback) => {
            knex.transaction(trx => {
                return trx.update({
                    isRejectCancelled: 1,
                    rejectCancelled: data.rejectCancelled
                }).from("orders").where(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
                }).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        console.log('shippedOrder Response: ', response);
                        return callback(null, seller, buyer);
                    }
                });
            });
        },
        (seller, buyer, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o").where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE (uuid=? OR userId=?) LIMIT 1)", [seller.userId, seller.userId])).andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        return callback(null, orders, seller, buyer);
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("od.*", "p.productDesc").from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("orderId", orderId).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first().asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (response) {
                                        item.image = response;
                                        return cb();
                                    } else {
                                        item.image = {};
                                        return cb();
                                    }
                                }
                            });
                        }, () => {
                            orders.details = response;
                            return callback(null, orders, seller, buyer);
                        });
                    }
                });
        },
        (orders, seller, buyer, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", buyer.userId).asCallback((err, devices) => {
                if (err) {
                    return next(err, null);
                } else {
                    _.each(devices, (row) => {
                        storesPlayerID.push(row);
                    });
                    console.log('storesPlayerID: ', storesPlayerID);
                    return callback(null, orders, seller, buyer);
                }
            });
        },
        (orders, seller, buyer, callback) => {
            const linkObj = {
                action: 'order_cancel',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
            };
            const title = 'Cancellation Request Rejected'
            const content = 'Your cancellation request has been rejected. Order <b>' + orders.orderNum + '</b> will proceed to shipping.';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into('notifications');
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                orders: orders,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};

exports.withdrawCancelOrder = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where(sql => {
                    sql.where("userId", uuid).andWhere("uuid", uuid);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select("userId", "username", "email", "phone", "user_img_path as img_path", "user_img_name AS img_name", "uuid")
                .from("users").where("userId", knex.raw("(SELECT sellerId FROM orders WHERE (orderNum = ? OR orderID = ? OR uuid= ?) LIMIT 1))", [orderId, orderId, orderId]))
                .first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                    isWithdrawCancelled: 1,
                    WithdrawCancelled: data.WithdrawCancelled
                }).from("orders").where(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId).orWhere("uuid", orderId);
                }).andWhere("userId", knex.raw("(SELECT userId FROM users WHERE uuid=? LIMIT 1)", [uuid]));
            }).asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('cancelOrderRequest Response: ', response);
                    return callback(null, buyer, seller);
                }
            });
        },
        (buyer, seller, callback) => {
            knex.select("o.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT username FROM users WHERE userId = o.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = o.userId LIMIT 1) AS buyerEmail")
                )
                .from("orders AS o").where("o.sellerId", knex.raw("(SELECT userId FROM users WHERE (uuid=? OR userId=?) LIMIT 1)", [seller.userId, seller.userId])).andWhere(sql => {
                    sql.where("orderNum", orderId).orWhere("orderId", orderId);
                }).asCallback((err, orders) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (orders && orders.length > 0) {
                            orders = orders[0];
                        }
                        return callback(null, orders, buyer, seller);
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("od.*", "p.productDesc").from("order_details AS od").innerJoin("products AS p", "od.productId", "p.productId")
                .where("orderId", orderId).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first().asCallback((err, response) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (response) {
                                        item.image = response;
                                        return cb();
                                    } else {
                                        item.image = {};
                                        return cb();
                                    }
                                }
                            });
                        }, () => {
                            orders.details = response;
                            return callback(null, orders, buyer, seller);
                        });
                    }
                });
        },
        (orders, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", seller.userId).asCallback((err, devices) => {
                if (err) {
                    return next(err, null);
                } else {
                    _.each(devices, (row) => {
                        storesPlayerID.push(row);
                    });
                    console.log('storesPlayerID: ', storesPlayerID);
                    return callback(null, orders, buyer, seller);
                }
            });
        },
        (orders, buyer, seller, callback) => {
            const linkObj = {
                action: 'order_cancel',
                orderId: orders.orderId,
                orderNum: orders.orderNum,
                seller: seller
            };

            const title = 'Order Cancellation Withdraw'
            const content = '<b>' + buyer.username + '</b> has withdraw cancellation of order <b>' + orders.orderNum + '</b>. Please respond or the order will be automatically cancelled.';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into('notifications');
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                orders: orders,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};