'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.saveUserLedger = (userId, data, next) => {
    console.log('saveUserLedger userId: ', userId);
    console.log('saveUserLedger data: ', data);

    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', '[user_img_name AS img_name]', 'uuid')
                .from("users")
                .where(sql => {
                    sql.where("userId", userId).orWhere("uuid", userId)
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                })
        },
        (user, callback) => {
            const sSQL = {
                userId: userId,
                TransDate: new Date(),
                TransCode: data.TransCode,
                Referenceno: data.Referenceno,
                Amount: data.Amount,
                Balance: data.Balance,
                Remarks: data.Remarks,
                isPosted: data.isPosted || 0
            };
            knex.transaction(trx => {
                return trx.insert(sSQL, 'ledgerId').into("ledger").asCallback((err, ledgerId) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        ledgerId = ledgerId[0];
                        knex("ledger").where("ledgerId", ledgerId).first().asCallback((err, ledger) => {
                            if (err) {
                                return next(err, null);
                            } else {
                                if (ledger) {
                                    return callback(null, {
                                        ledgerId: ledger.ledgerId,
                                        Referenceno: ledger.Referenceno,
                                        TransCode: ledger.TransCode
                                    });
                                } else {
                                    ledger = null;
                                    return callback(null, ledger);
                                }
                            }
                        })
                    }
                })
            });
        },
    ], next);
};


exports.postUserTransaction = (userId, data, next) => {
    const sSQL = {
        isPosted: data.isPosted,
        PostedDate: new Date()
    };
    knex.transaction(trx => {
        return trx
            .update(sSQL)
            .from("ledger")
            .where("ledgerId", data.ledgerId)
            .andWhere("userId", userId)
            .asCallback(next);
    })
};