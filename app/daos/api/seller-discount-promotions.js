'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

// Get All Discount Promotions
exports.getDiscountPromotions = (uuid, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: uuid
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback) => {
            knex.select("*").from("discount_promotions").where("sellerId", user.userId).asCallback(callback);
        }
    ], next);
};

// Create Discount Promotion
exports.createDiscountPromotion = (uuid, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: uuid
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback) => {
            knex.transaction((trx) => {
                return trx.insert({
                        promo_name: data.promo_name,
                        event_from: data.event_from,
                        event_to: data.event_to,
                        sellerId: user.userId,
                        uuid: knex.raw("UUID()")
                    }, 'dPromoteId')
                    .into('discount_promotions')
                    .asCallback(callback);
            });
        }
    ], next);
};

// Get All Discount Promotion by Id
exports.getDiscountPromotion = (uuid, id, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: uuid
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback) => {
            knex.select("*").from("discount_promotions").where("sellerId", user.userId).andWhere("dPromoteId", id).first().asCallback((err, response) => {
                if (err) {
                    return next(err);
                } else {
                    callback(null, response);
                }
            });
        }
    ], next);
};

// Update Discount Promotion by Id
exports.updateDiscountPromotion = (uuid, id, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: uuid
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback) => {
            knex.transaction((trx) => {
                return trx.update({
                        promo_name: data.promo_name,
                        event_from: data.event_from,
                        event_to: data.event_to
                    })
                    .from("discount_promotions")
                    .where("dPromoteId", id).andWhere("sellerId", user.userId)
                    .asCallback(callback);
            });
        }
    ], next);

};

// Delete Discount Promotion by Id
exports.deleteDiscountPromotion = (uuid, id, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid)
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback) => {
            knex.transaction((trx) => {
                return trx.from("discount_promotions")
                    .where("dPromoteId", id).andWhere("sellerId", user.userId)
                    .delete().asCallback(callback);
            });
        }
    ], next);
};

// Get all discount promotions to a product
exports.getProductDiscountPromotions = (uuid, id, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid)
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback1) => {
            knex.select("p.*", "DPP.*", "DPP.uuid AS discount_uuid")
                .from("discount_promotions AS DP")
                .innerJoin("discount_promotion_products AS DPP", "DP.dPromoteId", "DPP.dPromoteId")
                .innerJoin("products AS p", "p.productId", "DPP.productId")
                .where("DP.dPromoteId", id).andWhere("DP.sellerId", user.userId).groupBy("DPP.productId").asCallback((err, products) => {
                    if (err) {
                        return next(err);
                    }

                    async.eachSeries(products, (item, cb) => {
                        async.waterfall([
                            (callback) => {
                                knex("categories").where("categoryId", item.productCategoryID).first().asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response) {
                                        item.category = response;
                                    } else {
                                        item.category = {};
                                    }
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("subcategory").where("subcategoryId", item.productSubCategoryId).asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response && response.length > 0) {
                                        item.subcategory = response[0];
                                    } else {
                                        item.subcategory = {};
                                    }
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("brand").where("brandId", item.productBrandId).first().asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response) {
                                        item.brand = response;
                                    } else {
                                        item.brand = {};
                                    }
                                    return callback();
                                })
                            },
                            (callback) => {
                                knex("product_variation").where("productId", item.productId).asCallback((err, variantsArr) => {
                                    if (err) {
                                        return next(err, null);
                                    }

                                    if (!_.isEmpty(variantsArr)) {
                                        async.eachSeries(variantsArr, (variant, cbVariants) => {
                                            if (variant) {
                                                knex.select("*").from("discount_promotion_products")
                                                    .where("productVariationId", variant.productVariationId).andWhere("dPromoteId", item.dPromoteId).first()
                                                    .asCallback((err, dpProduct) => {
                                                        if (err) {
                                                            return next(err);
                                                        }
                                                        if (!_.isEmpty(dpProduct)) {
                                                            dpProduct.dp_uuid = dpProduct.uuid;
                                                            if (dpProduct.dp_uuid) {
                                                                variant.selected = true;
                                                            } else {
                                                                variant.selected = false;
                                                            }
                                                            Object.assign(variant, dpProduct);
                                                        } else {
                                                            variant.selected = false;
                                                        }
                                                        console.log("variant: ", variant);
                                                        cbVariants();
                                                    });
                                            } else {
                                                console.log("variant: ", variant);
                                                cbVariants();
                                            }
                                        }, () => {
                                            item.variants = variantsArr;
                                            return callback();
                                        });
                                    } else {
                                        item.variants = [];
                                        return callback();
                                    }
                                });
                            },
                            (callback) => {
                                knex("product_wholesale").where("productId", item.productId).asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    item.wholesale = response;
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("product_images").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response && response.length > 0) {
                                            item.images = response;
                                            item.image = response[0];
                                        } else {
                                            item.images = [];
                                            item.image = {};
                                        }
                                        return callback();
                                    });
                            }
                        ], cb);
                    }, () => {
                        return callback1(null, products);
                    });
                });
        }
    ], next);
};

// Create Product Discount Promotions
exports.createProductDiscountPromotion = (uuid, id, data, next) => {
    knex.transaction((trx) => {
        return trx.insert({
                dPromoteId: id,
                productId: data.productId,
                productVariationId: data.productVariationId,
                discount: data.discount,
                discount_price: data.discount_price,
                purchase_limit: data.purchase_limit,
                uuid: knex.raw("UUID()")
            }, "dPromoteProductId")
            .into("discount_promotion_products").asCallback(next);
    });
};

// Update Product Discount Promotions
exports.updateProductVariantsDiscountPromotion = (uuid, id, productId, variantId, data, next) => {
    knex.transaction((trx) => {
        return trx
            .update({
                discount: data.discount,
                discount_price: data.discount_price,
                purchase_limit: data.purchase_limit
            })
            .from("discount_promotion_products")
            .where("productId", productId)
            .andWhere("productVariationId", variantId)
            .andWhere("dPromoteId", id)
            .asCallback(next);
    });
};

exports.deleteProductVariantsDiscountPromotion = (uuid, id, productId, variantId, next) => {
    knex.transaction((trx) => {
        return trx
            .from("discount_promotion_products")
            .where("productId", productId)
            .andWhere("productVariationId", variantId)
            .andWhere("dPromoteId", id).delete()
            .asCallback(next);
    });
};

exports.deleteProductDiscountPromotion = (uuid, id, productId, next) => {
    knex.transaction((trx) => {
        return trx.from("discount_promotion_products")
            .where("productId", productId)
            .andWhere("dPromoteId", id)
            .delete().asCallback(next);
    })
};