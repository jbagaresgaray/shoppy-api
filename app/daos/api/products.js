'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const func = require('../../../app/utils/functions');

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const uuidv1 = require('uuid/v1');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.getProductById = (uuid, next) => {
    knex('products').where(sql => {
        sql.where("productId", uuid).orWhere("uuid", uuid);
    }).first().asCallback(next);
};

exports.getProductImages = (uuid, next) => {
    knex.select('productId', 'productSKU', 'productName', 'productPrice', 'productDesc', 'uuid').from("products")
        .where(sql => {
            sql.where("productId", uuid).orWhere("uuid", uuid).first().then(product => {
                if (product) {
                    knex("product_images").where("productId", product.productId).first().then(product_images => {
                        if (product_images && product_images.length > 0) {
                            const img = product_images[0];
                            product.image = img;
                            return next(null, product);
                        } else {
                            product.image = {};
                            return next(null, product);
                        }
                    }).catch(err => {
                        return next(err, null);
                    })
                } else {
                    next(null, {});
                }
            }).catch(err => {
                return next(err, null);
            });
        });
};

exports.createProduct = (user, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'firstname', 'lastname', 'uuid').from("users").where("uuid", user.uuid).first()
                .asCallback((err, result) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, result);
                    }
                });
        },
        (_user, callback) => {
            const sSQL = {
                productSKU: data.SKU,
                productName: data.name,
                productDesc: data.desc,
                productPrice: data.price,
                productBrandId: data.brandId,
                productStock: data.stock,
                productCategoryID: data.categoryId,
                productSubCategoryId: data.subcategoryId,
                productWeight: data.weight,
                productCondition: data.condition,
                productLength: data.length,
                productHeight: data.width,
                productWidth: data.height,
                isActive: 1,
                isPreOrder: data.isPreOrder || 0,
                preOrderDays: data.preOrderDays,
                userId: _user.userId,
                uuid: uuidv1()
            };
            knex.transaction(trx => {
                return trx.insert(sSQL, 'productId').into('products').asCallback(callback);
            });
        }
    ], next);
};

exports.updateProduct = (user, product_id, data, next) => {
    console.log('updateProduct user: ', user);
    console.log('updateProduct product_id: ', product_id);
    console.log('updateProduct data: ', data);

    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'firstname', 'lastname', 'uuid').from("users").where("uuid", user.uuid).first()
                .asCallback((err, result) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, result);
                    }
                });
        },
        (_user, callback) => {
            knex.select('productId').from("products").where(sql => {
                sql.where("uuid", product_id).orWhere("productId", product_id);
            }).first().asCallback((err, result) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, result);
                }
            });
        },
        (_user, product, callback) => {
            const sSQL = {
                productSKU: data.SKU,
                productName: data.name,
                productDesc: data.desc,
                productPrice: data.price,
                productBrandId: data.brandId,
                productStock: data.stock,
                productCategoryID: data.categoryId,
                productSubCategoryId: data.subcategoryId,
                productWeight: data.weight,
                productCondition: data.condition,
                productLength: data.length,
                productHeight: data.width,
                productWidth: data.height,
                isActive: 1,
                isPreOrder: data.isPreOrder,
                preOrderDays: data.preOrderDays,
                userId: _user.userId,
                uuid: uuidv1()
            };
            knex.transaction(trx => {
                return trx.update(sSQL).from("products").where("productId", product.productId).asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        knex.select('productId', 'productSKU', 'productName', 'productPrice', 'uuid').from("products").where("productId", product.productId)
                            .asCallback((err, result) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    if (result && result.length > 0) {
                                        result = result[0];
                                        result.image = {};
                                        return callback(null, result);
                                    } else {
                                        return callback(null, result);
                                    }
                                }
                            });
                    }
                });
            });
        }
    ], next);
};

exports.getProductInfo = (product_id, params, next) => {
    console.log({
        params
    })
    async.waterfall([
        (callback) => {
            knex.select('p.*',
                    knex.raw('(SELECT IF(pr.rating,AVG(pr.rating),0) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rating'),
                    knex.raw('(SELECT COUNT(*) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rated'),
                    knex.raw('(SELECT COUNT(pl.productId) FROM product_like pl WHERE pl.productId = p.productId) AS liked'),
                    knex.raw('(SELECT COUNT(o.orderId) FROM order_details o WHERE p.productId = o.productId) AS orderscount')
                )
                .from("products AS p")
                .where(sql => {
                    sql.where("p.productId", product_id).orWhere("p.uuid", product_id);
                }).first().asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (product, callback) => {
            knex("categories").where("categoryId", product.productCategoryID).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response) {
                        product.category = response;
                    } else {
                        product.category = {};
                    }
                    return callback(null, product);
                }
            });
        },
        (product, callback) => {
            knex("subcategory").where("subcategoryId", product.productSubCategoryId).asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response && response.length > 0) {
                        product.subcategory = response[0];
                    } else {
                        product.subcategory = {};
                    }
                    return callback(null, product);
                }
            });
        },
        (product, callback) => {
            knex("brand").where("brandId", product.productBrandId).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    if (response) {
                        product.brand = response;
                    } else {
                        product.brand = {};
                    }
                    return callback(null, product);
                }
            })
        },
        (product, callback) => {
            knex("product_variation").where("productId", product.productId).asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    product.variants = response;
                    return callback(null, product);
                }
            });
        },
        (product, callback) => {
            knex("product_wholesale").where("productId", product.productId).asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    product.wholesale = response;
                    return callback(null, product);
                }
            });
        },
        (product, callback) => {
            knex.select('pse.*', 'sc.name', 'sc.description', "sc.slug", "sc.phone", "sc.web_url").from("product_shipping_fee AS pse")
                .innerJoin("shipping_courier AS sc", "pse.shippingCourierId", "sc.shippingCourierId")
                .where("pse.productId", product.productId)
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        product.shippingFee = response;
                        return callback(null, product);
                    }
                });
        },
        (product, callback) => {
            knex("product_images").where("productId", product.productId)
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (response && response.length > 0) {
                            product.images = response;
                        } else {
                            product.images = [];
                        }
                        return callback(null, product);
                    }

                });
        },
        (product, callback) => {
            knex("user_address").where("userId", product.userId).andWhere("isPickUpAddress", 1).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (response) {
                            product.shipFrom = response;
                        } else {
                            product.shipFrom = {};
                        }
                        return callback(null, product);
                    }
                });
        },
        (product, callback) => {
            knex.select("username", "firstname", "lastname", "sellerName", "uuid").from("users").where("userId", product.userId).first()
                .asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (response) {
                            product.shopInfo = response;
                        } else {
                            product.shopInfo = {};
                        }
                        callback(null, product);
                    }
                });
        },
        (product, callback) => {
            if (!_.isEmpty(product.variants)) {
                const stock = _.sumBy(product.variants, (row) => {
                    return row.stock;
                });
                if (stock < 1) {
                    product.isOutOfStock = true;
                } else {
                    product.isOutOfStock = false;
                }
            } else {
                if (product.productStock < 1) {
                    product.isOutOfStock = true;
                } else {
                    product.isOutOfStock = false;
                }
            }

            callback(null, product);
        },
        (product, callback) => {
            if (product.variants.length > 0) {
                knex.select("dpp.*",
                        "dp.promo_name",
                        "dp.sellerId",
                        "dp.event_from",
                        "dp.event_to")
                    .from("discount_promotion_products as dpp")
                    .leftJoin("discount_promotions as dp", "dpp.dPromoteId", "dp.dPromoteId")
                    .where("dpp.productID", product.productId)
                    .andWhere(function () {
                        this.whereIn("dpp.dPromoteId", [knex.raw(`SELECT dPromoteId FROM discount_promotions WHERE NOW() BETWEEN event_from AND event_to`)])
                    })
                    .asCallback((err, resp) => {
                        if (err) {
                            callback(null, product)
                        } else {
                            _.each(product.variants, (value, index) => {
                                value.discount = _.find(resp, (val) => {
                                    return val.productVariationId == value.productVariationId
                                })

                                if (index == product.variants.length - 1) {
                                    callback(null, product)
                                }
                            })
                        }
                    })
            } else {
                knex.select("dpp.*",
                        "dp.promo_name",
                        "dp.sellerId",
                        "dp.event_from",
                        "dp.event_to")
                    .from("discount_promotion_products as dpp")
                    .leftJoin("discount_promotions as dp", "dpp.dPromoteId", "dp.dPromoteId")
                    .where("dpp.productID", product.productId)
                    .andWhere(function () {
                        this.whereIn("dpp.dPromoteId", [knex.raw(`SELECT dPromoteId FROM discount_promotions WHERE NOW() between event_from and event_to`)])
                    })
                    .first()
                    .asCallback((err, resp) => {
                        if (err) {
                            callback(null, product)
                        } else {
                            product['discount'] = resp
                            callback(null, product)
                        }
                    })
            }
        }
    ], next);
};

exports.getProductVouchers = (uuid, next) => {
    knex
        .select("a.*").from("vouchers as a")
        .innerJoin("voucher_products as b", "a.voucherId", "b.voucherId")
        .leftJoin("products as p", "b.productId", "p.productId")
        .where(sql => {
            sql.where("p.productId", uuid).orWhere("p.uuid", uuid);
        })
        .asCallback(next);
};

exports.getProductDiscount = (uuid, next) => {
    knex
        .select("a.*", "b.*")
        .from("discount_promotions as a")
        .leftJoin("discount_promotion_products as b", "a.dPromoteId", "b.dPromoteId")
        .where("b.uuid", uuid)
        .asCallback(next)
};

exports.getProductOnSameShop = (product_id, next) => {
    async.waterfall([
        (callback) => {
            knex("products")
                .where(sql => {
                    sql.where("productId", product_id).orWhere("uuid", product_id);
                }).first().asCallback((err, response) => {
                    if (err) {
                        next(err, null);
                    } else {
                        if (response && response.length > 0) {
                            response = response[0];
                        }
                        return callback(null, response);
                    }
                });
        },
        (product, callback) => {
            knex.select("p.*",
                    knex.raw('(SELECT IF(pr.rating,AVG(pr.rating),0) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rating'),
                    knex.raw('(SELECT COUNT(*) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rated'),
                    knex.raw('(SELECT COUNT(pl.productId) FROM product_like pl WHERE pl.productId = p.productId) AS liked'))
                .from("products AS p")
                .where("p.userId", product.userId).andWhere("p.productId", "<>", product.productId)
                .groupBy("p.productId").limit(20)
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        async.eachSeries(response, (item, cb) => {
                            knex("product_images").where("productId", item.productId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        if (response) {
                                            item.image = response;
                                        } else {
                                            item.image = {};
                                        }
                                        return cb();
                                    }
                                });
                        }, () => {
                            // product.moreproducts = response;
                            // return callback(null, product);
                            return callback(null, response);
                        });
                    }
                });
        }
    ], next);
};

exports.deleteProduct = (uuid, next) => {
    require("isomorphic-fetch");
    const Dropbox = require("dropbox").Dropbox;
    const dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("productId", uuid).orWhere("uuid", uuid);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex.select('img_path', 'img_name', 'img_type', 'img_size').from("product_images")
                .where("productId", product.productId).asCallback((err, res) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (res && res.length > 0) {
                            async.eachSeries(res, (item, cb) => {
                                dbx.filesDelete({
                                    path: item.img_name
                                }).then((resp) => {
                                    console.log('Delete deleteProductImage: ', resp);
                                    return cb();
                                }).catch((error) => {
                                    console.error('DropBox error 4: ', error);
                                    return cb();
                                });
                            }, () => {
                                return callback();
                            });
                        } else {
                            return callback();
                        }
                    }
                });
        },
        (callback) => {
            knex.transaction(trx => {
                return trx.from("products").where(sql => {
                    sql.where("productId", uuid).orWhere("uuid", uuid);
                }).delete().asCallback(callback);
            });
        }
    ], next);
};

exports.uploadProductImage = (uuid, data, next) => {
    knex.transaction(trx => {
        return trx.insert({
                img_name: data.img_name,
                img_path: data.img_path,
                img_type: data.img_type,
                img_size: data.img_size,
                productId: data.productId,
                uuid: knex.raw("UUID()")
            }, 'productImagesId')
            .into("product_images")
            .asCallback(next);
    })
};

exports.deleteProductImage = (uuid, imageId, next) => {
    require("isomorphic-fetch");
    const Dropbox = require("dropbox").Dropbox;
    const dbx = new Dropbox({
        accessToken: config.DROPBOX_ACCESS
    });

    async.waterfall([
        (callback) => {
            knex.select('img_path', 'img_name', 'img_type', 'img_size').from("product_images")
                .where(sql => {
                    sql.where("productImagesId", imageId).orWhere("uuid", uuid);
                }).first().asCallback((err, res) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (res) {
                            dbx.filesDelete({
                                path: res.img_path
                            }).then((resp) => {
                                console.log('Delete deleteProductImage: ', resp);
                                return callback();
                            }).catch((error) => {
                                console.error('DropBox error 4: ', error);
                                return callback();
                            });
                        } else {
                            return callback();
                        }
                    }
                });
        },
        (callback) => {
            knex.transaction(trx => {
                return trx.from("product_images").where(sql => {
                    sql.where("productId", uuid).andWhere("productImagesId", imageId);
                }).delete().asCallback(callback);
            });
        }
    ], next);
};

exports.addProductShippingFee = (productId, data, next) => {
    if (data.shippingCourierId) {
        data.courierId = data.shippingCourierId;
    }
    if (data.shippingfee) {
        data.fee = data.shippingfee;
    }

    knex.transaction(trx => {
        return trx.insert({
                shippingCourierId: data.courierId,
                productId: productId,
                fee: data.fee,
                isCover: data.isCover
            }, "product_shipping_feeId")
            .into("product_shipping_fee")
            .asCallback(next);
    });
};

exports.deleteProductShipping = (productId, next) => {
    knex.transaction(trx => {
        return trx.from("product_shipping_fee")
            .where("productId", productId)
            .delete()
            .asCallback(next);
    });
};

exports.getProductShipping = (productId, next) => {
    knex.select("psf.product_shipping_feeId AS _id", "sf.shippingCourierId", "sf.name", "sf.description", "sf.slug", "sf.phone",
            "sf.web_url", "psf.productId", "psf.fee", "psf.isCover", "psf.uuid AS psf_uuid")
        .from("product_shipping_fee AS psf")
        .innerJoin("shipping_courier AS sf", "psf.shippingCourierId", "sf.shippingCourierId")
        .where("productId", knex.select("productId").from("products").where("uuid", productId))
        .asCallback(next);
};

exports.getAllProducts = (params, next) => {
    console.log('params: ', params);
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            let strSQL = knex.count("* AS numRows").from("products AS p").leftJoin("product_rating AS pr", "p.productId", "pr.productId");
            if (params.search) {
                strSQL.where(sql => {
                    sql.where("productName", "LIKE", "'%" + params.search + "%'")
                        .orWhere("productDesc", "LIKE", "'%" + params.search + "%'")
                });
            }
            strSQL.asCallback((err, results) => {
                console.log("error: ", err);
                if (err) {
                    return next(err, null);
                } else {
                    numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages);
                }
            });
        },
        (numRows, numPages, callback) => {
            let strSQL = knex.select("p.*",
                knex.raw('(SELECT IF(pr.rating,AVG(pr.rating),0) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rating'),
                knex.raw('(SELECT COUNT(pl.productId) FROM product_like pl WHERE pl.productId = p.productId) AS liked'),
                knex.raw('(SELECT COUNT(*) FROM product_rating AS pr WHERE pr.productId = p.productId) AS rated'),
                knex.raw("(SELECT COUNT(o.orderId) FROM  order_details o WHERE p.productId = o.productId) AS orderscount")
            ).from("products AS p");

            if (params.search) {
                strSQL.where(sql => {
                    sql.where("productName", "LIKE", "'%" + params.search + "%'").orWhere("productDesc", "LIKE", "'%" + params.search + "%'");
                });
                if (params.limit && params.startWith) {
                    strSQL.groupBy("p.productId").limit(limit).offset(params.startWith);
                } else if (params.limit) {
                    strSQL.groupBy("p.productId").limit(numPerPage);
                } else {
                    strSQL.groupBy("p.productId").limit(numPerPage);
                }
            } else {
                if (params.limit && params.startWith) {
                    strSQL.groupBy("p.productId").limit(limit).offset(params.startWith);
                } else if (params.limit) {
                    strSQL.groupBy("p.productId").limit(numPerPage);
                } else {
                    strSQL.groupBy("p.productId").limit(numPerPage);
                }
            }
            strSQL.asCallback((err, results) => {
                if (err) {
                    return next(err, null);
                } else {
                    async.eachSeries(results, (item, cb) => {
                        async.parallel([
                            (callback1) => {
                                knex("categories").where("categoryId", item.productCategoryID).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            if (response) {
                                                item.category = response;
                                            } else {
                                                item.category = {};
                                            }
                                            return callback1();
                                        }
                                    });
                            },
                            (callback1) => {
                                knex("subcategory").where("subcategoryId", item.productSubCategoryId).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            if (response) {
                                                item.subcategory = response;
                                            } else {
                                                item.subcategory = {};
                                            }
                                            return callback1();
                                        }
                                    });
                            },
                            (callback1) => {
                                knex("brand").where("brandId", item.productBrandId).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            if (response) {
                                                item.brand = response;
                                            } else {
                                                item.brand = {};
                                            }
                                            return callback1();
                                        }
                                    })
                            },
                            (callback1) => {
                                knex("product_variation").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            item.variants = response;

                                            if (!_.isEmpty(item.variants)) {
                                                const stock = _.sumBy(item.variants, (row) => {
                                                    return row.stock;
                                                });
                                                if (stock < 1) {
                                                    item.isOutOfStock = true;
                                                } else {
                                                    item.isOutOfStock = false;
                                                }
                                            } else {
                                                if (item.productStock < 1) {
                                                    item.isOutOfStock = true;
                                                } else {
                                                    item.isOutOfStock = false;
                                                }
                                            }

                                            return callback1();
                                        }
                                    });
                            },
                            (callback1) => {
                                knex("product_wholesale").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            item.wholesale = response;
                                            return callback1();
                                        }
                                    });
                            },
                            (callback1) => {
                                knex.select("pse.*", "sc.name")
                                    .from("product_shipping_fee AS pse")
                                    .innerJoin("shipping_courier AS sc", "pse.shippingCourierId", "sc.shippingCourierId")
                                    .where("pse.productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            item.shippingFee = response;
                                            return callback1();
                                        }
                                    });
                            },
                            (callback1) => {
                                knex("product_images").where("productId", item.productId).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        } else {
                                            if (response) {
                                                item.image = response;
                                                return callback1();
                                            } else {
                                                item.image = {};
                                                return callback1();
                                            }
                                        }
                                    });
                            }
                        ], cb);
                    }, () => {
                        return callback(null, numRows, numPages, results);
                    });
                }
            });
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getproductsByCategory = (sellerId, categoryId, params, next) => {
    console.log({
        sellerId
    })
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex
                .select("userId")
                .from("users")
                .where("uuid", sellerId)
                .orWhere("userId", sellerId)
                .first()
                .asCallback((err, resp) => {
                    if (resp && resp.userId) {
                        callback(null, resp.userId)
                    } else {
                        next(err, resp)
                    }
                })
        },
        (userId, callback) => {
            console.log({
                userId
            })
            knex
                .select("categoryId")
                .from("categories")
                .where("uuid", categoryId)
                .orWhere("categoryId", categoryId)
                .first()
                .asCallback((err, resp) => {
                    if (resp && resp.categoryId) {
                        callback(null, resp, userId)
                    } else {
                        next(err, resp)
                    }
                })
        },
        (category, userId, callback) => {
            let strSQL = knex.count("* AS numRows")
                .from("products AS p")
                .where("p.productCategoryId", category.categoryId)
                .andWhere("p.userId", userId);
            if (params.search) {
                strSQL.where(sql => {
                    sql.where("productName", "LIKE", "'%" + params.search + "%'")
                        .orWhere("productDesc", "LIKE", "'%" + params.search + "%'")
                });
            }
            strSQL.asCallback((err, results) => {
                console.log("error: ", err);
                if (err) {
                    return next(err, null);
                } else {
                    numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages, category.categoryId, userId);
                }
            });
        },
        (numRows, numPages, categoryId, userId, callback) => {
            let strSQL = knex.select("p.*")
                .from("products AS p")
                .where("p.productCategoryId", categoryId)
                .andWhere("p.userId", userId);
            if (params.search) {
                console.log('IF params.search : ', params.search)
                strSQL.where(sql => {
                    sql.where("productName", "LIKE", "'%" + params.search + "%'").orWhere("productDesc", "LIKE", "'%" + params.search + "%'");
                });
                if (params.limit && params.startWith) {
                    strSQL.groupBy("p.productId").limit(limit).offset(params.startWith);
                } else if (params.limit) {
                    strSQL.groupBy("p.productId").limit(numPerPage);
                } else {
                    strSQL.groupBy("p.productId").limit(numPerPage);
                }
            } else {
                console.log('ELSE params.search : ', params.search)
                if (params.limit && params.startWith) {
                    console.log("one")
                    console.log({
                        params
                    })
                    console.log({
                        limit
                    })
                    strSQL.groupBy("p.productId").limit(limit).offset(params.startWith);
                } else if (params.limit) {
                    console.log("two ", numPerPage)
                    strSQL.groupBy("p.productId").limit(numPerPage);
                } else {
                    console.log("three")
                    strSQL.groupBy("p.productId").limit(numPerPage);
                }
            }
            strSQL.asCallback((err, products) => {
                console.log('err ', err)
                if (err) {
                    next(err, null)
                } else {
                    callback(null, numRows, numPages, products)
                }
            })
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next)
};