'use strict';

const mysql = require('mysql');
const Database = require('../../../app/utils/database').Database;
const db = new Database();
const func = require('../../../app/utils/functions');
const constant = require("../../../config/constants");
const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
const async = require('async');
const fs = require('fs');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.isOrderReturned = (returnId, orderId, next) => {
    knex("order_return")
        .where("isReturned", 1).andWhereRaw("returnedDateTime IS NOT NULL")
        .andWhere("returnId", returnId).andWhere("orderId", orderId)
        .asCallback(next);
};

exports.isOrderReturnRequested = (orderId, next) => {
    knex("order_return").whereRaw("returnTransCode IS NOT NULL").andWhereRaw("returnedRequested IS NOT NULL")
        .andWhere("orderId", orderId).asCallback(next);
};

exports.isOrderReturnAccepted = (returnId, orderId, next) => {
    knex("order_return").whereRaw("returnedRequested IS NOT NULL").andWhere(sql => {
        sql.where("isReturnRequestAccepted", 1).orWhereRaw("returnRequestedAccepted IS NOT NULL");
    }).andWhere("orderId", orderId).andWhere("returnId", returnId).asCallback(next);
};

exports.isOrderReturnCancelled = (returnId, orderId, next) => {
    knex("order_return").whereRaw("returnedRequested IS NOT NULL").andWhereRaw("cancelReturnRequest IS NOT NULL")
        .andWhere("returnId", returnId).andWhere("orderId", orderId).asCallback(next);
};

exports.returnOrderRequest = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.userId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.sellerId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            const sSQLObj = {
                orderId: orderId || data.orderId,
                userId: buyer.userId,
                sellerId: seller.userId,
                returnTransCode: func.generateNumberString(15),
                returnedRequested: data.returnedRequested,
                returnedReason: data.returnedReason,
                returnEmailAddress: data.returnEmailAddress,
                returnTotal: data.returnTotal
            };
            knex.transaction(trx => {
                return trx.insert(sSQLObj, 'returnId').into("order_return")
                    .asCallback((err, orderReturnId) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            orderReturnId = orderReturnId[0];
                            async.eachSeries(data.returnedItems, (item, cb) => {
                                const strObj = {
                                    orderId: orderId || data.orderId || item.orderId,
                                    returnId: orderReturnId,
                                    orderDetailId: item.detailsId,
                                    productId: item.productId,
                                    detailName: item.detailName,
                                    detailPrice: item.detailPrice,
                                    detailSKU: item.detailSKU,
                                    detailQty: item.detailQty,
                                    variantId: item.variantId,
                                    variantName: item.variantName
                                };
                                knex.insert(strObj).from("order_return_details").asCallback((err, resp) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        return cb(null, resp);
                                    }
                                });
                            }, () => {
                                return callback(null, orderReturnId, buyer, seller);
                            });
                        }
                    });
            });
        },
        (orderReturnId, buyer, seller, callback) => {
            knex.select("o.orderNum", "ot.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = ot.sellerId LIMIT 1) AS sellerPhone"),
                    knex.raw("(SELECT username FROM users WHERE userId = ot.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = ot.userId LIMIT 1) AS buyerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = ot.userId LIMIT 1) AS buyerPhone")
                )
                .from("order_return AS ot").innerJoin("orders AS o", "ot.orderId", "o.orderId")
                .where("ot.orderId", orderId).andWhere("ot.returnId", orderReturnId)
                .asCallback((err, order_return) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, order_return, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", seller.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            const linkObj = {
                action: 'return_refund',
                orderId: returns.orderId,
                returnId: returns.returnId,
                returnRequestId: returns.returnTransCode,
                returnRequestedDeadline: returns.returnRequestedDeadline,
                userId: returns.userId,
                sellerId: returns.sellerId
            };

            const title = 'Order Return Request'
            const content = '<b>' + buyer.username + '</b> has requested to return the order <b>' + returns.orderNum + '</b>. You can contact the buyer, accept the return request or submit a dispute.  Please respond by ' + returns.returnRequestedDeadline + ' or refund will be automatically released to the buyer.';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                });
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };

                            return callback(null, {
                                returns: returns,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};

exports.uploadReturnRequestFiles = (uuid, orderId, data, next) => {
    const returnObj = {
        orderReturnId: data.orderReturnId,
        file_name: data.file_name,
        file_key: data.file_key,
        file_path: data.file_path,
        file_size: data.file_size,
        file_type: data.file_type,
        uuid: uuidv1()
    };
    knex.transaction(trx => {
        return trx.insert(returnObj, "order_return_filesId").into("order_return_files")
            .asCallback(next);
    });
};

exports.acceptReturnOrderRequest = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.buyerId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.sellerId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        isReturnRequestAccepted: 1,
                        returnRequestedAccepted: data.returnRequestedAccepted,
                        returnRequestedDeadline: data.returnRequestedDeadline,
                        isReturnRequestedAcceptedShipping: data.isReturnRequestedAcceptedShipping,
                        isReturnRequestedAcceptedRefund: data.isReturnRequestedAcceptedRefund
                    }).from("order_return")
                    .where("returnId", data.returnId).andWhere("orderId", orderId)
                    .asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            return callback(null, buyer, seller);
                        }
                    });
            });
        },
        (buyer, seller, callback) => {
            knex.select("o.orderNum", "rt.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerPhone"),
                    knex.raw("(SELECT username FROM users WHERE userId = rt.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.userId LIMIT 1) AS buyerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.userId LIMIT 1) AS buyerPhone"),
                    'sc.name AS courier_name', 'sc.description AS courier_desc', 'sc.slug AS courier_slug',
                    'sc.phone  AS courier_phone', 'sc.web_url  AS courier_web_url'
                )
                .from("orders AS o").innerJoin("order_return AS rt", "rt.orderId", "o.orderId")
                .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .whereRaw("rt.returnedRequested IS NOT NULL").andWhere("o.orderId", orderId).andWhere("rt.returnId", data.returnId)
                .asCallback((err, returns) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", buyer.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            const linkObj = {
                action: 'return_refund',
                orderId: returns.orderId,
                returnId: returns.returnId,
                returnRequestId: returns.returnTransCode,
                userId: returns.userId,
                sellerId: returns.sellerId
            };

            const title = 'Return Request is Received'
            let content = '';
            if (data.isReturnRequestedAcceptedRefund == 1) {
                content = 'We have received your request. The seller have to complete the validation process whether to approve return or approve refund or submit a dispute. Please wait within 1-3 days';
            } else {
                content = 'We have received your request. Please attach the return label sent to your email on top of the package and ship to accredited shipping carrier partners. The return will not be processed if the package is not shipped within 3 calendar days after return submission.';
            }
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into("notifications");
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID,
                            };
                            return callback(null, {
                                returns: returns,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            })
        }
    ], next);
};

exports.uploadReturnShippingFiles = (uuid, orderId, data, next) => {
    const returnObj = {
        orderReturnId: data.orderReturnId,
        file_name: data.file_name,
        file_key: data.file_key,
        file_path: data.file_path,
        file_size: data.file_size,
        file_type: data.file_type,
        uuid: uuidv1()
    };
    knex.transaction(trx => {
        return trx.insert(returnObj, 'order_return_filesId').into("order_return_shipping_files").asCallback(next);
    });
};

exports.returnOrderShipmentInfo = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.userId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.sellerId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            let sSQLObj = {
                returnCarrierId: data.returnCarrierId,
                returnTrackingNumber: data.returnTrackingNumber,
                returnRemarks: data.returnRemarks
            };
            knex.transaction(trx => {
                return trx.update(sSQLObj).from("order_return")
                    .where("orderId", orderId).andWhere("returnId", data.returnId);
            }).asCallback((err, resp) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, buyer, seller);
                }
            });
        },
        (buyer, seller, callback) => {
            knex.select("o.orderNum", "rt.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerPhone"),
                    knex.raw("(SELECT username FROM users WHERE userId = rt.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.userId LIMIT 1) AS buyerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.userId LIMIT 1) AS buyerPhone"),
                    'sc.name AS courier_name', 'sc.description AS courier_desc', 'sc.slug AS courier_slug',
                    'sc.phone  AS courier_phone', 'sc.web_url AS courier_web_url'
                )
                .from("order_return AS rt").innerJoin("orders AS o", "rt.orderId", "o.orderId")
                .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("ot.orderId", orderId).andWhere("ot.returnId", data.returnId).andWhereRaw("rt.returnedRequested IS NOT NULL")
                .asCallback((err, returns) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("devices").where("userId", seller.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, returns, buyer, seller);
                    }
                })
        },
        (returns, buyer, seller, callback) => {
            const linkObj = {
                action: 'return_refund',
                orderId: returns.orderId,
                orderNum: returns.orderNum,
                orderId: returns.orderId,
                returnId: returns.returnId,
                returnRequestId: returns.returnTransCode,
                userId: returns.userId,
                sellerId: returns.sellerId,
                returnTrackingNumber: returns.returnTrackingNumber
            };

            const title = 'Order Return Ship-out'
            const content = '<b>' + buyer.username + '</b> has shipped out the return request <b>' + returns.returnTransCode + '</b> for order <b>' + returns.orderNum + '</b>. Kindly check the details on the app';
            knex.transaction(trx => {
                return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw("NOW()"),
                        link: JSON.stringify(linkObj),
                        notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                    }, 'notificationId')
                    .into("notifications");
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                returns: returns,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};

exports.returnOrder = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", data.userId).orWhere("uuid", data.userId);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where(sql => {
                    sql.where("userId", data.sellerId).orWhere("uuid", data.sellerId);
                }).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                    isReturned: 1,
                    returnedDateTime: data.returnedDateTime
                }).from("order_return").where("returnId", data.returnId).andWhere("orderId", orderId);
            }).asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    console.log('order_return response: ', response);
                    return callback(null, buyer, seller);
                }
            });
        },
        (buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", buyer.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, buyer, seller);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.select("o.orderNum", "rt.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerPhone"),
                    knex.raw("(SELECT username FROM users WHERE userId = rt.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.userId LIMIT 1) AS buyerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.userId LIMIT 1) AS buyerPhone"),
                    'sc.name AS courier_name', 'sc.description AS courier_desc', 'sc.slug AS courier_slug',
                    'sc.phone AS courier_phone', 'sc.web_url AS courier_web_url'
                )
                .from("order_return AS rt").innerJoin("orders AS o", "rt.orderId", "o.orderId")
                .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("ot.orderId", orderId).andWhere("ot.returnId", data.returnId).andWhereRaw("rt.returnedRequested IS NOT NULL")
                .asCallback((err, returns) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            const linkObj = {
                action: 'return_refund',
                orderId: returns.orderId,
                orderNum: returns.orderNum,
                returnRefNo: returns.returnTransCode,
                returnId: returns.returnId,
                userId: buyer.userId,
                sellerId: seller.userId
            };

            const title = 'Order Return Approved';
            const content = '<b>' + seller.username + '</b> has approved your return request <b>' + returns.returnTransCode + '</b> We are transferring payment for order <b>' + returns.orderNum + '</b> to you soon.';
            const strSQL = mysql.format('INSERT INTO notifications(title,content,datetime,link,notifTypeId) VALUES (?,?,NOW(),?,?);', [
                title, content, JSON.stringify(linkObj), 5
            ]);

            knex.transaction(trx => {
                return trx.insert({

                }, 'notificationId').into("notifications");
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    notifId = notifId[0];
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, buyer.userId, buyer.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                returns: returns,
                                buyer: buyer,
                                seller: seller,
                                notification: notification
                            });
                        }
                    });
                }
            })
        }
    ], next);
};

exports.returnOrderCancelRequest = (uuid, orderId, data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.buyerId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.sellerId).first().asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            knex.transaction(trx => {
                return trx.update({
                    cancelReturnRequest: data.cancelReturnRequest
                }).from("order_return").where("returnId", data.returnId).andWhere("orderId", orderId);
            }).asCallback((err, resp) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, buyer, seller);
                }
            })
        },
        (buyer, seller, callback) => {
            knex.select("o.orderNum", "rt.*",
                    knex.raw("(SELECT sellerName FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.sellerId LIMIT 1) AS sellerPhone"),
                    knex.raw("(SELECT username FROM users WHERE userId = rt.userId LIMIT 1) AS buyerName"),
                    knex.raw("(SELECT email FROM users WHERE userId = rt.userId LIMIT 1) AS buyerEmail"),
                    knex.raw("(SELECT phone FROM users WHERE userId = rt.userId LIMIT 1) AS buyerPhone"),
                    'sc.name AS courier_name', 'sc.description AS courier_desc', 'sc.slug AS courier_slug',
                    'sc.phone AS courier_phone', 'sc.web_url AS courier_web_url'
                )
                .from("order_return AS rt").innerJoin("orders AS o", "rt.orderId", "o.orderId")
                .leftJoin("shipping_courier AS sc", "o.shippingCourierId", "sc.shippingCourierId")
                .where("ot.orderId", orderId).andWhere("ot.returnId", data.returnId).andWhereRaw("rt.returnedRequested IS NOT NULL")
                .asCallback((err, returns) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            knex.select("onesignal_player_id").from("user_devices").where("userId", seller.userId)
                .asCallback((err, devices) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        _.each(devices, (row) => {
                            storesPlayerID.push(row);
                        });
                        return callback(null, returns, buyer, seller);
                    }
                });
        },
        (returns, buyer, seller, callback) => {
            const linkObj = {
                action: 'return_refund',
                status: 'cancel_order_return_request',
                orderId: returns.orderId,
                returnId: returns.returnId,
                returnRequestId: returns.returnTransCode,
                userId: returns.userId,
                sellerId: returns.sellerId
            };

            const title = 'Return Request Cancelled'
            const content = '<b>' + buyer.username + '</b> has cancelled the order return request for order <b>' + data.orderNum + '</b>';
            knex.transaction(trx => {
                return trx.insert({
                    title: title,
                    content: content,
                    datetime: knex.raw("NOW()"),
                    link: JSON.stringify(linkObj),
                    notifTypeId: constant.NOTIFICATION_TYPE.ORDER_UPDATES
                }, 'notificationId').into("notifications");
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, seller.userId, seller.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID,
                            };
                            return callback(null, {
                                returns: returns,
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            });
        }
    ], next);
};