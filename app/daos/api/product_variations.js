'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const func = require('../../../app/utils/functions');

const async = require('async');
const fs = require('fs');
const path = require('path');
const uuidv1 = require('uuid/v1');
const knex = require("knex")({
	client: "mysql",
	connection: db.configuration
});


exports.createVariations = (uuid, data, next) => {
	async.waterfall([
		(callback) => {
			knex.select("productId").from("products").where(sql => {
				sql.where("uuid", uuid).orWhere("productId", uuid);
			}).first().asCallback((err, response) => {
				if (err) {
					return next(err, null);
				} else {
					return callback(null, response);
				}
			});
		},
		(product, callback) => {
			const sSQL = {
				productId: product.productId,
				name: data.name,
				price: data.price,
				stock: data.stock,
				uuid: uuidv1()
			};
			knex.transaction(trx => {
				return trx.insert(sSQL, 'productVariationId').into("product_variation")
					.asCallback(callback);
			});
		}
	], next);
};

exports.getProductVariations = (user, uuid, next) => {
	async.waterfall([
		(callback) => {
			knex.select("productId").from("products").where(sql => {
				sql.where("uuid", uuid).orWhere("productId", uuid);
			}).first().asCallback((err, response) => {
				if (err) {
					return next(err, null);
				} else {
					return callback(null, response);
				}
			});
		},
		(product, callback) => {
			knex("product_variation").where("productId", product.productId).asCallback(callback);
		}
	], next);
};

exports.deleteAllProductVariations = (user, uuid, next) => {
	async.waterfall([
		(callback) => {
			knex.select("productId").from("products").where(sql => {
				sql.where("uuid", uuid).orWhere("productId", uuid);
			}).first().asCallback((err, response) => {
				if (err) {
					return next(err, null);
				} else {
					return callback(null, response);
				}
			});
		},
		(product, callback) => {
			knex.transaction(trx => {
				return trx.from("product_variation")
					.where("productId", product.productId)
					.delete().asCallback(callback);
			});
		}
	], next);
};

exports.deleteProductVariations = (uuid, id, next) => {
	async.waterfall([
		(callback) => {
			knex.select("productId").from("products").where(sql => {
				sql.where("uuid", uuid).orWhere("productId", uuid);
			}).first().asCallback((err, response) => {
				if (err) {
					return next(err, null);
				} else {
					return callback(null, response);
				}
			});
		},
		(product, callback) => {
			knex.transaction(trx => {
				return trx.from("product_variation")
					.where("productId", product.productId).andWhere(sql => {
						sql.where("productVariationId", id).orWhere("uuid", id);
					})
					.delete().asCallback(callback);
			});
		}
	], next);
};

exports.updateProductVariations = (uuid, id, data, next) => {
	async.waterfall([
		(callback) => {
			knex.select("productId").from("products").where(sql => {
				sql.where("uuid", uuid).orWhere("productId", uuid);
			}).first().asCallback((err, response) => {
				if (err) {
					return next(err, null);
				} else {
					return callback(null, response);
				}
			});
		},
		(product, callback) => {
			knex.transaction(trx => {
				return trx.update({
						name: data.name,
						price: data.price,
						stock: data.stock
					}).from("product_variation")
					.where("productId", product.productId).andWhere(sql => {
						sql.where("productVariationId", id).orWhere("uuid", id);
					}).asCallback(callback);
			});
		}
	], next);
};