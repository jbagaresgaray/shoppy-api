'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const func = require('../../../app/utils/functions');

const async = require('async');
const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkIfProductHasWholesale = (productId, next) => {
    async.waterfall([
        (_user, callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            })
        },
        (product, callback) => {
            knex("product_wholesale").where("productId", product.productId).first().asCallback(callback);
        }
    ], next);
};

exports.createProductWholesale = (productId, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex.transaction(trx => {
                return trx.insert({
                        productId: product.productId,
                        minAmt: data.minAmt,
                        maxAmt: data.maxAmt,
                        price: data.price
                    }, 'productWholesaleId').into("product_wholesale")
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.deleteAllProductWholeSale = (productId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex.transaction(trx => {
                return trx.from("product_wholesale")
                    .where("productId", product.productId)
                    .delete()
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.deleteProductWholeSale = (productId, _id, next) => {
    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex.transaction(trx => {
                return trx.from("product_wholesale")
                    .where("productId", product.productId).andWhere("productWholesaleId", _id)
                    .delete()
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.getProductWholeSale = (productId, _id, next) => {
    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex("product_wholesale").where(sql => {
                sql.where("productWholesaleId", _id).orWhere("productId", product.productId);
            }).first().asCallback(callback);
        }
    ], next);
};

exports.updateProductWholeSale = (productId, _id, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        minAmt: data.minAmt,
                        maxAmt: data.maxAmt,
                        price: data.price
                    }).from("product_wholesale")
                    .where("productId", product.productId).andWhere("productWholesaleId", _id)
                    .asCallback(callback);
            });
        }
    ], next);
};

exports.getAllProductWholeSale = (productId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, response);
                }
            });
        },
        (product, callback) => {
            knex("product_wholesale").where("productId", product.productId).asCallback(callback);
        }
    ], next);
};