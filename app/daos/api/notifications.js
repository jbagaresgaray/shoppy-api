'use strict';

let Database = require('../../../app/utils/database').Database;
let db = new Database();

let env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.getUpdatesNotifications = (user, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'user_img_path', 'uuid')
                .from("users")
                .where(sql => {
                    sql.where("uuid", user.uuid).orWhere("userId", user.uuid);
                }).first().asCallback((err, users) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, users);
                    }
                });
        },
        (users, callback) => {
            knex.select("n.*", "un.isRead", "un.userId", "un.user_player_ids")
                .from("notifications AS n").innerJoin("user_notifications AS un", "n.notificationId", "un.notificationId")
                .where("un.userId", users.userId).groupBy("n.notificationId")
                .asCallback((err, notifications) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (notifications && notifications.length > 0) {
                            _.each(notifications, (row) => {
                                row.userinfo = users;
                            });
                            knex("notificationtype").asCallback((err, types) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    _.each(notifications, (item, cb) => {
                                        item.notiftype = _.find(types, {
                                            'notifTypeId': item.notifTypeId
                                        }) || {};
                                    });
                                    return callback(null, notifications);
                                }
                            });
                        } else {
                            return callback(null, []);
                        }
                    }
                });
        }
    ], next);
};

exports.getNotificationDetails = (user, notifId, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'user_img_path', 'uuid')
                .from("users")
                .where(sql => {
                    sql.where("userId", user.uuid).orWhere("uuid", user.uuid);
                }).first().asCallback((err, users) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (users && users.length > 0) {
                            users = users[0];
                        }
                        callback(null, users);
                    }
                });
        },
        (users, callback) => {
            knex.select("n.*", "un.isRead", "un.userId", "un.user_player_ids")
                .from("notifications AS n").innerJoin("user_notifications AS un", "n.notificationId", "un.notificationId")
                .where("un.userId", users.userId).andWhere("n.notificationId", notifId).first()
                .asCallback((err, notifications) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        if (notifications && notifications.length > 0) {
                            _.each(notifications, (row) => {
                                row.userinfo = users;
                            });

                            knex("notificationtype").asCallback((err, types) => {
                                if (err) {
                                    return next(err, null);
                                } else {
                                    _.each(notifications, (item, cb) => {
                                        item.notiftype = _.find(types, {
                                            'notifTypeId': item.notifTypeId
                                        }) || {};
                                    });
                                    return callback(null, notifications);
                                }
                            });
                        } else {
                            callback(null, []);
                        }
                    }
                });
        }
    ], next);
};

exports.generateNotification = (data, next) => {
    let storesPlayerID = [];
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.userId).first()
                .asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, user);
                    }
                });
        },
        (buyer, callback) => {
            knex.select('userId', 'username', 'email', 'phone', 'user_img_path as img_path', 'user_img_name AS img_name', 'uuid')
                .from("users").where("userId", data.sellerId).first()
                .asCallback((err, user) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, buyer, user);
                    }
                });
        },
        (buyer, seller, callback) => {
            let sSQL = knex.select("onesignal_player_id").from("user_devices");
            if (data.action == 'seller') {
                sSQL.where("userId", seller.userId);
            } else if (data.action == 'buyer') {
                sSQL.where("userId", buyer.userId);
            }
            sSQL.asCallback((err, devices) => {
                if (err) {
                    return next(err, null);
                } else {
                    _.each(devices, (row) => {
                        storesPlayerID.push(row);
                    });
                    return callback(null, buyer, seller);
                }
            });
        },
        (buyer, seller, callback) => {
            const linkObj = data.params;
            const title = data.title
            const content = data.content;
            knex.transaction(trx => {
                return trx.insert({
                        title: title,
                        content: content,
                        datetime: knex.raw("NOW()"),
                        link: JSON.stringify(linkObj),
                        notifTypeId: data.notifType
                    }, 'notificationId')
                    .into("notifications");
            }).asCallback((err, notifId) => {
                if (err) {
                    return next(err, null);
                } else {
                    knex.raw('INSERT INTO user_notifications(notificationId,isRead,userId,user_player_ids,date_created,uuid) SELECT ?,0,?,ud.onesignal_player_id,NOW(),UUID() FROM user_devices ud WHERE ud.userId=?;', [
                        notifId, buyer.userId, buyer.userId
                    ]).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            const notification = {
                                notifId: notifId,
                                title: title,
                                content: content,
                                notification_data: linkObj,
                                devices: storesPlayerID
                            };
                            return callback(null, {
                                seller: seller,
                                buyer: buyer,
                                notification: notification
                            });
                        }
                    });
                }
            })
        }
    ], next);
};

exports.updateNotification = (user, notifId, next) => {
    async.waterfall([
        (callback) => {
            knex
                .select("userId", "username", "firstname", "lastname", "user_img_path", "uuid")
                .from("users").where({
                    userId: user.uuid
                }).orWhere("uuid", "=", user.uuid)
                .first().then(results => {
                    return callback(null, results);
                }).catch((error) => {
                    return callback(error, null);
                });
        },
        (users, callback) => {
            knex.transaction((trx) => {
                return trx
                    .update({
                        isRead: 1
                    })
                    .from('user_notifications')
                    .where({
                        notificationId: notifId,
                        userId: users.userId
                    })
            }).asCallback(callback);
        }
    ], next);
};

exports.deleteNotification = (user, notifId, next) => {
    async.waterfall([
        (callback) => {
            console.log("deleteNotification 1:");
            knex
                .select("userId", "username", "firstname", "lastname", "user_img_path", "uuid")
                .from("users").where({
                    userId: user.uuid
                }).orWhere("uuid", "=", user.uuid)
                .first().then(results => {
                    return callback(null, results);
                }).catch((error) => {
                    return callback(error, null);
                });
        },
        (users, callback) => {
            console.log("deleteNotification 2:");
            knex.transaction(trx => {
                return trx("user_notifications")
                    .where({
                        notificationId: notifId,
                        userId: users.userId
                    })
                    .del().asCallback(callback);
            })
        }
    ], next)
};