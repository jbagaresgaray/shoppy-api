'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


// ============================== VOUCHERS =============================== //
// ============================== VOUCHERS =============================== //
// ============================== VOUCHERS =============================== //

exports.checkVoucherName = (sellerId, name, next) => {
    knex("vouchers").where(sql => {
        sql.where("sellerId", sellerId).andWhere("voucherName", "LIKE", '%"' + name + '"%')
    }).asCallback(next);
};

exports.checkVoucherCode = (sellerId, code, next) => {
    knex("vouchers").where(sql => {
        sql.where("sellerId", sellerId).andWhere("voucherCode", "LIKE", '%"' + code + '"%')
    }).asCallback(next);
};

exports.getSellerVouchers = (sellerId, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where(sql => {
                    sql.where("uuid", sellerId).orWhere("userId", sellerId)
                })
                .first()
                .asCallback((error, result) => {
                    if(error){
                        return next(error, null);
                    } else {
                        return callback(null, result);
                    }
                });
        },
        (user, callback) => {
            knex
                .select(
                    "v.*",
                    knex.raw("(select count(*) from user_vouchers where voucherId = v.voucherId and isUsed = 1 ) as claimed"),
                    knex.raw("(select count(*) from user_vouchers where voucherId = v.voucherId and isUsed != 1 ) as unclaimed"),
                )
                .from("vouchers as v")
                .where("v.sellerId", user.userId)
                .asCallback(callback)
        }
    ], next);
};


exports.createSellerVoucher = (sellerId, data, next) => {
    knex.transaction((trx) => {
        knex("vouchers")
            .insert({
                voucherName: data.voucherName,
                voucherCode: data.voucherCode,
                voucherType: data.voucherType,
                voucherAmt: data.voucherAmt,
                minimumOrderAmt: data.minimumOrderAmt,
                voucherFrom: data.voucherFrom,
                voucherTo: data.voucherTo,
                voucherQuantity: data.voucherQuantity,
                maxDiscountPrice: data.maxDiscountPrice,
                isNoLimit: data.isNoLimit,
                isApplytoWholeShop: data.isApplytoWholeShop,
                isDisplayToApp: data.isDisplayToApp,
                sellerId: sellerId,
                uuid: knex.raw("UUID()")
            }).asCallback(next);
    });
};

exports.updateSellerVoucher = (sellerId, voucherId, data, next) => {
    knex.transaction((trx) => {
        return trx.update({
                voucherName: data.voucherName,
                voucherCode: data.voucherCode,
                voucherType: data.voucherType,
                voucherAmt: data.voucherAmt,
                minimumOrderAmt: data.minimumOrderAmt,
                voucherFrom: data.voucherFrom,
                voucherTo: data.voucherTo,
                voucherQuantity: data.voucherQuantity,
                maxDiscountPrice: data.maxDiscountPrice,
                isNoLimit: data.isNoLimit,
                isApplytoWholeShop: data.isApplytoWholeShop,
                isDisplayToApp: data.isDisplayToApp
            })
            .from("vouchers")
            .where("voucherId", voucherId)
            .andWhere("sellerId", sellerId)
            .asCallback(next);
    });
};

exports.deleteSellerVoucher = (sellerId, voucherId, next) => {
    knex.transaction((trx) => {
        return trx.from("vouchers")
            .where("voucherId", voucherId)
            .andWhere("sellerId", sellerId)
            .delete()
            .asCallback(next)
    });
};

// ======================== PRODUCT VOUCHERS ============================ //
// ======================== PRODUCT VOUCHERS ============================ //
// ======================== PRODUCT VOUCHERS ============================ //

exports.getSellerProductVouchers = (sellerId, voucherId, next) => {
    async.waterfall([
        (cb) => {
            knex("vouchers")
                .where("sellerId", sellerId)
                .andWhere("voucherId", voucherId)
                .first()
                .asCallback((err, resp) => {
                    if (err) {
                        return next(err, null)
                    } else {
                        return cb(null, resp)
                    }
                });
        },
        (voucher, cb) => {
            knex.select('p.*').from("products AS p").innerJoin("voucher_products AS stp", "p.productId", "stp.productId")
                .where("stp.voucherId", voucher.voucherId).asCallback((err, products) => {
                    if (err) {
                        return next(err, null);
                    }
                    async.eachSeries(products, (product, callbackEach) => {
                        async.waterfall([
                            (callback) => {
                                knex("categories").where("categoryId", product.productCategoryID).first().asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response) {
                                        product.category = response;
                                    } else {
                                        product.category = {};
                                    }
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("subcategory").where("subcategoryId", product.productSubCategoryId).asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response && response.length > 0) {
                                        product.subcategory = response[0];
                                    } else {
                                        product.subcategory = {};
                                    }
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("brand").where("brandId", product.productBrandId).first().asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response) {
                                        product.brand = response;
                                    } else {
                                        product.brand = {};
                                    }
                                    return callback();
                                })
                            },
                            (callback) => {
                                knex("product_variation").where("productId", product.productId).asCallback((err, variantsArr) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (!_.isEmpty(variantsArr)) {
                                        async.eachSeries(variantsArr, (variant, cbVariants) => {
                                            knex.select("*").from("discount_promotion_products")
                                                .where("productVariationId", variant.productVariationId).first().asCallback((err, dpProduct) => {
                                                    if (err) {
                                                        return next(err);
                                                    }
                                                    if (!_.isEmpty(dpProduct)) {
                                                        Object.assign(variant, dpProduct);
                                                    }
                                                    cbVariants();
                                                });
                                        }, () => {
                                            product.variants = variantsArr;
                                        });
                                    } else {
                                        product.variants = [];
                                    }
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("product_wholesale").where("productId", product.productId).asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    product.wholesale = response;
                                    return callback();
                                });
                            },
                            (callback) => {
                                knex("product_images").where("productId", product.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response && response.length > 0) {
                                            product.images = response;
                                            product.image = response[0];
                                        } else {
                                            product.images = [];
                                            product.image = {};
                                        }
                                        return callback();
                                    });
                            }
                        ], () => {
                            return callbackEach();
                        });
                    }, () => {
                        voucher.products = products;
                        return cb(null, voucher);
                    });
                });
        }
    ], next)
};

exports.createSellerProductVoucher = (sellerId, voucherId, data, next) => {
    knex.transaction((trx) => {
        return trx
            .insert({
                voucherId: voucherId,
                productId: data.productId,
                uuid: knex.raw("UUID()")
            }).into("voucher_products").asCallback(next);
    });
};

exports.deleteSellerProductVoucher = (sellerId, voucherId, productId, next) => {
    knex.transaction((trx) => {
        return trx
            .from("voucher_products")
            .where("voucherId", voucherId).andWhere("productId", productId)
            .delete().asCallback(next);
    });
};