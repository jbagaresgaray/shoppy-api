'use strict';

const mysql = require('mysql');
const Database = require('../../../app/utils/database').Database;
const db = new Database();
const func = require('../../../app/utils/functions');
const constants = require('../../../config/constants');

const env = process.env.NODE_ENV || 'development';
const config = require('../../../config/environment/' + env);

const async = require('async');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const uuidv1 = require('uuid/v1');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.getSellerInfo = (uuid, next) => {
    // knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
    //     .from('users')
    //     .where({
    //         isSeller: 1,
    //         userId: uuid,
    //     })
    //     .orWhere("uuid", uuid)
    //     .asCallback(next);

    knex.select(
            'u.userId',
            'u.username',
            'u.firstname',
            'u.lastname',
            'u.email',
            'u.phone',
            'u.fax',
            'u.sellerDescription',
            'u.sellerName',
            'u.sellerIsVacationMode',
            'u.sellerApprovalDate',
            'u.sellerApplicationDate',
            'u.uuid',
            'os.*')
        .from('users as u')
        .leftJoin("official_shop as os", "u.userId", "os.userId")
        .where("u.isSeller", 1)
        .andWhere("u.userId", uuid)
        .orWhere("u.uuid", uuid)
        .asCallback(next)
};

exports.getAllSellers = (params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            const strSQL = knex("users").count("* as numRows")
            if (params.search) {
                strSQL.where("isSeller", 1).andWhere(sql => {
                    sql.where("username", "LIKE", '%"' + params.search + '"%').orWhere("sellerName", "LIKE", '%"' + params.search + '"%')
                });
            }
            strSQL.then(results => {
                    numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages);
                })
                .catch(err => {
                    return next(err, null);
                });
        },
        (numRows, numPages, callback) => {
            const strSQL = knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'user_img_path', 'sellerDescription',
                    'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users');
            if (params.search) {
                strSQL.where("isSeller", 1).andWhere(sql => {
                    sql.where("username", "LIKE", "%'" + params.search + "'%").orWhere("sellerName", "LIKE", "%'" + params.search + "'%");
                });
                if (params.limit && params.startWith) {
                    strSQL.limit(limit);
                } else if (params.limit) {
                    strSQL.limit(numPerPage);
                } else {
                    strSQL.limit(numPerPage);
                }
            } else {
                if (params.limit && params.startWith) {
                    strSQL.limit(limit);
                } else if (params.limit) {
                    strSQL.limit(numPerPage);
                } else {
                    strSQL.limit(numPerPage);
                }
            }
            strSQL.then(results => {
                return callback(null, numRows, numPages, results);
            }).catch(err => {
                return next(err, null);
            });
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
}

exports.getUserShopProfile = (uuid, next) => {
    /* async.waterfall([
        (callback) => {
            knex.select('u.userId', 'u.username', 'u.firstname', 'u.lastname', 'u.email', 'u.phone', 'u.fax', 'u.sellerDescription', 'u.sellerName',
                    knex.raw('IF(sellerIsVacationMode,1,0) AS sellerIsVacationMode'), 'u.sellerApprovalDate', 'u.sellerApplicationDate', 'u.uuid',
                    knex.raw('COUNT(p.productId) AS productCount'), 'ud.detailAddress', 'ud.suburb', 'ud.city', 'ud.state', 'ud.zipcode', 'ud.country',
                    'u.user_img_path as img_path', 'u.user_img_name AS img_name', 'u.user_banner_path AS banner_path', 'u.user_banner_name AS banner_name',
                    'os.shop_application_date', 'os.shop_appoved_date', 'os.shop_approved_by')
                .from('users AS u')
                .leftJoin('user_address as ud', 'u.userId', 'ud.userId')
                .leftJoin('products AS p', 'u.userId', 'p.userId')
                .leftJoin('official_shop AS os', 'u.userId', 'os.userId')
                .where((sql) => {
                    sql.where({
                        'u.uuid': uuid
                    }).orWhere({
                        'u.userId': uuid
                    })
                }).andWhere({
                    'ud.isDefaultAddress': 1
                })
                .groupBy('u.userId')
                .first().then(response => {
                    return callback(null, response);
                }).catch(err => {
                    return next(err, null);
                });
        }
    ], next); */

    async.waterfall([
        (cb) => {
            knex.select('u.userId', 'u.username', 'u.firstname', 'u.lastname', 'u.email', 'u.phone', 'u.fax', 'u.sellerDescription', 'u.sellerName',
            knex.raw('IF(sellerIsVacationMode,1,0) AS sellerIsVacationMode'), 'u.sellerApprovalDate', 'u.sellerApplicationDate', 'u.uuid',
            knex.raw('(SELECT COUNT(productId) FROM products WHERE userId = u.userId) AS productCount'),
            'ud.detailAddress', 'ud.suburb', 'ud.city', 'ud.state', 'ud.zipcode', 'ud.country',
            'u.user_img_path as img_path', 'u.user_img_name AS img_name', 'u.user_banner_path AS banner_path', 'u.user_banner_name AS banner_name',
            'os.shop_application_date', 'os.shop_appoved_date', 'os.shop_approved_by')
            .from('users AS u')
            .leftJoin('user_address as ud', 'u.userId', 'ud.userId')
            .leftJoin('official_shop AS os', 'u.userId', 'os.userId')
            .where((sql) => {
                sql.where({
                    'u.uuid': uuid
                }).orWhere({
                    'u.userId': uuid
                })
            }).andWhere({
                'ud.isDefaultAddress': 1
            })
            .groupBy('u.userId')
            .first().asCallback((err, resp) => {
                if(err) {
                    next(err, null)
                } else {
                    cb(null, resp)
                }
            });
        },
        (profile, cb) => {
            if(profile && profile.userId) {
                knex.select("*").from("preferred_seller").where("preferred_sellerId", profile.userId).first()
                    .asCallback((err, resp) => {
                        if(err) {
                            next(err, null)
                        } else {
                            profile.preferred = resp
                            cb(null, profile)
                        }
                    })
            } else {
                next({
                    msg: 'profile not found'
                }, null)
            }
        }
    ], next)
};

exports.updateShopInformation = (uuid, data, next) => {
    const store = {
        sellerName: data.sellerName,
        sellerDescription: data.sellerDescription,
        sellerIsVacationMode: data.sellerIsVacationMode || 0
    };
    knex.transaction(function (trx) {
        return trx
            .update(store)
            .from('users')
            .where({
                userId: uuid,
            }).orWhere({
                uuid: uuid
            })
            .asCallback(next);
    });
};

exports.getSellerIncome = (uuid, next) => {
    console.log('getSellerIncome 3');

    async.waterfall([
        (callback) => {
            knex.select('o.*', knex.raw('(SELECT sellerName FROM users WHERE userId=o.sellerId LIMIT 1) AS sellerName'))
                .from('orders AS o')
                .where('o.sellerId', knex.select('userId').from('users').where((sql) => {
                    sql.where({
                            userId: uuid
                        })
                        .orWhere({
                            uuid: uuid
                        })
                }).first())
                .then(orders => {
                    return callback(null, orders);
                }).catch(err => {
                    return next(err, null);
                });
        },
        (orders, callback) => {
            async.eachSeries(orders, (order, orderCallback) => {
                async.parallel([
                    (callback1) => {
                        knex.select('od.*', 'p.productDesc')
                            .from('order_details AS od')
                            .innerJoin('products AS p', 'od.productId', 'p.productId')
                            .where({
                                orderId: order.orderId
                            })
                            .then(response => {
                                async.eachSeries(response, (product, callback2) => {
                                    knex('product_images').where('productId', '=', product.productId).first()
                                        .then(response => {
                                            if (response) {
                                                product.image = response;
                                                return callback2();
                                            } else {
                                                product.image = {};
                                                return callback2();
                                            }
                                        }).catch(err => {
                                            return next(err, null);
                                        });
                                }, () => {
                                    order.details = response;
                                    callback1(null, order);
                                });
                            })
                            .catch(err => {
                                return next(err, null);
                            });
                    },
                    (callback1) => {
                        knex.select('userId', 'username', 'email', 'firstname', 'lastname', 'user_img_path AS img_path', 'user_img_name AS img_name', 'user_img_type AS img_type', 'uuid')
                            .from('users')
                            .where({
                                userId: order.userId
                            })
                            .first()
                            .then(result => {
                                order.user_info = result;
                                return callback1(null, order);
                            }).catch(err => {
                                return next(err, null);
                            });
                    }
                ], orderCallback);
            }, () => {
                callback(null, orders);
            });
        }
    ], next);
};

exports.getSellerCustomers = (uuid, next) => {
    console.log('getSellerCustomers 3');
    knex('orders AS o')
        .distinct('u.uuid', 'u.username', 'u.firstname', 'u.lastname', 'u.email', 'u.user_img_path')
        .select()
        .innerJoin('users AS u', 'o.userId', 'u.userId')
        .where('o.sellerID', knex.select('userId').from('users').where('uuid', '=', uuid).first())
        .asCallback(next);
};

exports.getSellerRatings = (uuid, next) => {
    knex.select('pl.*', 'u.username AS user_username', 'u.email AS user_email', 'u.user_img_path', 'u.user_img_name')
        .from('products AS p')
        .innerJoin('product_rating AS pl', 'p.productId', 'pl.productId')
        .leftJoin('users AS u', 'pl.userId', 'u.userId')
        .where({
            'pl.productId': product.productId
        })
        .asCallback(callback);
};

exports.getSellerProducts = (user, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'firstname', 'lastname', 'uuid')
                .from('users')
                .where({
                    userId: user.uuid
                }).orWhere({
                    uuid: user.uuid
                })
                .first()
                .then(result => {
                    return callback(null, result);
                }).catch(err => {
                    return next(err, null);
                });
        },
        (_user, callback) => {
            knex.select('p.*', knex.raw('IF(pr.rating,AVG(pr.rating),0) AS rating'), knex.raw('COUNT(pl.productId) AS liked'))
                .from('products AS p')
                .leftJoin('product_rating AS pr', 'p.productId', 'pr.productId')
                .leftJoin('product_like AS pl', 'p.productId', 'pl.productId')
                .where({
                    'p.userId': _user.userId
                })
                .groupBy('p.productId')
                .then(response => {
                    async.eachSeries(response, (item, cb) => {
                        async.parallel([
                            (callback1) => {
                                knex("categories").where("categoryId", item.productCategoryID).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response) {
                                            item.category = response;
                                        } else {
                                            item.category = {};
                                        }
                                        return callback1();
                                    });
                            },
                            (callback1) => {
                                knex("subcategory").where("subcategoryId", item.productSubCategoryId).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response) {
                                            item.subcategory = response;
                                        } else {
                                            item.subcategory = {};
                                        }
                                        return callback1();
                                    });
                            },
                            (callback1) => {
                                knex("brand").where("brandId", item.productBrandId).first()
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response) {
                                            item.brand = response;
                                        } else {
                                            item.brand = {};
                                        }
                                        return callback1();
                                    })
                            },
                            (callback1) => {
                                knex("product_variation").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        item.variants = response;
                                        return callback1();
                                    });
                            },
                            (callback1) => {
                                knex("product_wholesale").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        item.wholesale = response;
                                        return callback1();
                                    });
                            },
                            (callback1) => {
                                knex("product_images").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response) {
                                            item.image = response[0];
                                            item.images = response;
                                            return callback1();
                                        } else {
                                            item.image = {};
                                            image.images = [];
                                            return callback1();
                                        }
                                    });
                            }
                        ], cb);
                    }, () => {
                        callback(null, response);
                    });
                }).catch(err => {
                    return next(err, null);
                });
        }
    ], next);
};

exports.applySellerAsOfficialShop = (shopId, user, next) => {
    const shop_url_code = func.generateString(42);
    const shop_ref_code = func.generateNumberString(10);
    const duration = 5; //In Days
    const shop_url_token = jwt.sign({
        shopId: shopId,
        shop_url_code: shop_url_code
    }, config.token_secret_mobile, {
        expiresIn: Math.floor(Date.now() / 1000) + (duration * 24 * 60 * 60 * 1000),
        jwtid: uuidv1()
    });

    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'sellerName', 'sellerDescription', 'uuid').from('users').where({
                uuid: user.uuid
            }).first().then(result => {
                return callback(null, result);
            }).catch(err => {
                return next(err, null);
            });
        },
        (_user, callback) => {

            knex.transaction((trx) => {
                return trx
                    .insert({
                        userId: _user.userId,
                        shop_ref_code: shop_ref_code,
                        shop_application_date: knex.raw('NOW()'),
                        shop_url_code: shop_url_code,
                        shop_url_token: shop_url_token,
                        shop_isapproved: 0
                    }, 'official_shop_id')
                    .into('official_shop')
                    .then((official_shop_id) => {
                        knex.select('shop_ref_code', 'shop_application_date', 'shop_url_code', 'shop_url_token', 'shop_name', 'shop_application_date')
                            .from('official_shop')
                            .where({
                                official_shop_id: official_shop_id
                            }).first()
                            .then(result => {
                                let shop_name = '';
                                if (_.isEmpty(result.shop_name)) {
                                    shop_name = '<span class="text-danger">NO NAME</span>';
                                } else {
                                    shop_name = result.shop_name;
                                }

                                const content = '<b>SHOP NAME:</b> ' + shop_name + '<br> <b>SELLER NAME:</b> ' + _user.sellerName + '<br> <b>APPLICATION DATE:</b> ' + new Date(result.shop_application_date);
                                let strSQL1 = mysql.format('INSERT INTO official_shop_evaluation(official_shop_id,date,statusClass,tagName,content,eval_categoryID) VALUES (?,NOW(),?,?,?,?) ', [
                                    official_shop_id, constants.EVAL_STATUS.info, null, content, constants.EVAL_CATEGORY.applicants
                                ]);
                                db.actionQuery(strSQL1, (err, response) => {
                                    console.log('official_shop_evaluation err: ', err);
                                    console.log('official_shop_evaluation response: ', response);
                                });

                                return callback(null, {
                                    insertedId: official_shop_id,
                                    shopInfo: _user,
                                    official_shop: result
                                });
                            })
                            .catch(err => {
                                return next(err, null);
                            });
                    })
                    .catch(err => {
                        return next(err, null);
                    });
            });
        }
    ], next);
};

exports.officialShopApplicationStatus = (shopId, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: shopId
                }).first()
                .then((result) => {
                    console.log('user result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (user, callback) => {
            console.log('user: ', user);
            knex.select("os.*", "u.username", "u.sellerName", "u.sellerDescription")
                .table("official_shop as os")
                .innerJoin("users as u", "u.userId", "=", "os.userId")
                .where("u.userId", user.userId)
                .first()
                .then((result) => {
                    console.log('official_shop result: ', result);
                    callback(null, result);
                })
                .catch((error) => {
                    return next(error, null);
                });
        },
        (shop, callback) => {
            knex.select('*').from('official_shop_evaluation').where({
                    official_shop_id: shop.official_shop_id
                }).orderBy('date', 'desc')
                .then((evaluation) => {
                    if (!_.isEmpty(evaluation)) {
                        shop.evaluation = evaluation[0];
                    } else {
                        shop.evaluation = {};
                    }
                    shop.activity = evaluation;
                    callback(null, shop);
                }).catch((error) => {
                    return next(error, null);
                });
        },
    ], next);
};

exports.getShopRatings = (uuid, params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'email', 'sellerName', 'sellerDescription', "user_img_path", 'uuid')
                .from("users")
                .first().where(sql => {
                    sql.where("uuid", uuid).orWhere("userId", uuid)
                }).asCallback((err, user) => {
                    if (err) {
                        return next(err);
                    }
                    return callback(null, user);
                });
        },
        (user, callback) => {
            knex.count("* AS numRows")
                .from("product_rating AS pr")
                .innerJoin("products AS p", "p.productId", "pr.productId")
                .whereIn("pr.productId", knex.raw("(select p.productId from products p where p.userId = ?)", [user.userId]))
                .asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages, user);
                    }
                });
        },
        (numRows, numPages, user, callback) => {
            console.log('user.userId : ', user.userId)
            knex.select("pr.productRatingId", "pr.rating", "pr.comment",
                    "pr.ratedOn", "pr.productId as pr_poductId", "pr.variantId",
                    "pr.orderId", "pr.userId as pr_userId",
                    "p.userId as p_userId", "p.productSKU", "p.productName", "p.productDesc",
                    "p.productPrice", "p.productWeight", "p.productCondition",
                    "p.productBrandId", "p.productStock", "p.productCategoryID", "p.productSubCategoryId",
                    "p.productLength", "p.productHeight", "p.productWidth", "p.userId as p_userId",
                    "p.isPreOrder", "p.isActive", "p.productId as p_productId")
                .from("product_rating AS pr")
                .innerJoin("products AS p", "p.productId", "pr.productId")
                .whereIn("pr.productId", knex.raw("(select p.productId from products p where p.userId = ?)", [user.userId]))
                .asCallback((err, products) => {
                    if (err) {
                        console.log({err})
                        next(err, null)
                    } else {
                        async.eachSeries(products, (item, cb) => {
                            async.parallel([
                                (callback1) => {
                                    knex.select("username", "firstname", "lastname", "user_img_path", "uuid").from("users").where("userId", item.pr_userId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                item.buyer = response;
                                                return callback1();
                                            }
                                        });
                                },
                                (callback1) => {
                                    knex.select("username", "firstname", "lastname", "user_img_path", "uuid").from("users").where("userId", item.p_userId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            } else {
                                                item.productSeller = response;
                                                return callback1();
                                            }
                                        });
                                },
                                (callback1) => {
                                    knex("product_variation").where("productVariationId", item.variantId).first()
                                        .asCallback((err, response) => {
                                            if (err) {
                                                console.log({err})
                                                return next(err, null);
                                            } else {
                                                item.variants = response;
                                                return callback1();
                                            }
                                        });
                                },
                                (callback1) => {
                                    knex("product_images").where("productId", item.p_productId)
                                        .asCallback((err, response) => {
                                            if (err) {
                                                console.log({err})
                                                return next(err, null);
                                            } else {
                                                if (response) {
                                                    item.image = response[0];
                                                    item.images = response;
                                                    return callback1();
                                                } else {
                                                    item.image = {};
                                                    item.images = [];
                                                    return callback1();
                                                }
                                            }
                                        });
                                }
                            ], cb);
                        }, () => {
                            return callback(null, numRows, numPages, products);
                        });
                    }
                });
        },
        (numRows, numPages, ratings, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: ratings,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: ratings,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: ratings,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.rateShop = (uuid, data, next) => {
    async.waterfall([
        (cb) => {
            knex
                .select("userId")
                .from("users")
                .where("uuid", uuid)
                .first()
                .asCallback((err, resp) => {
                    if (err) {
                        next(err, null)
                    } else {
                        cb(null, resp.userId)
                    }
                })
        },
        (userId, cb) => {
            knex.transaction(trx => {
                return trx
                    .insert({
                            rating: data.rating,
                            comment: data.comment,
                            orderId: data.orderId,
                            userId: userId,
                            productId: data.productId,
                            ratedOn: knex.fn.now(),
                            uuid: knex.raw("UUID()")
                        },
                        "productRatingId"
                    )
                    .into("product_rating")
                    .asCallback(next);
            });
        }
    ])
};

exports.getShopSummaryRatings = (uuid, next) => {
    async.waterfall([
        (callback) => {
            knex
                .select("userId")
                .from("users")
                .where("uuid", uuid)
                .orWhere("userId", uuid)
                .first()
                .asCallback((err, resp) => {
                    if (resp && resp.userId) {
                        callback(null, resp.userId, resp.isSeller)
                    } else {
                        next(err, resp)
                    }
                })
        },
        (userId, isSeller, callback) => {
            knex.select(
                    "pr.*")
                .from("product_rating as pr")
                .whereIn("pr.productId", knex.select("productId").from("products").where("userId", userId))
                .asCallback((err, ratings) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, ratings, isSeller);
                    }
                })
        },
        (ratings, isSeller, callback) => {
            (async () => {

                let buyer_rating = 0;
                let buyer_rating_total = 0;

                let seller_rating = _.sumBy(ratings, 'rating') / ratings.length;
                let seller_rating_total = _.sumBy(ratings, 'rating');

                let total_avg_rating = () => {
                    if (seller_rating > 0 && buyer_rating > 0) {
                        return (seller_rating + buyer_rating) / 2
                    } else if (seller_rating > 0 && buyer_rating == 0) {
                        return seller_rating
                    } else if (seller_rating == 0 && buyer_rating > 0) {
                        return buyer_rating
                    } else if (seller_rating == 0 && buyer_rating == 0) {
                        return 0
                    } else {
                        return 0
                    }
                }

                let respObj = {
                    "total_avg_rating": await total_avg_rating(),
                    "buyer_rating_summary": {
                        "rating": buyer_rating,
                        "rating_total": buyer_rating_total
                    },
                    "seller_rating_summary": {
                        "rating": seller_rating,
                        "rating_total": seller_rating_total
                    }
                };
                callback(null, respObj)
            })()
        }
    ], next)
};

exports.getShopCategories = (user, next) => {
    async.waterfall([
        (callback) => {
            knex
                .select("userId")
                .from("users")
                .where("uuid", user.uuid)
                .orWhere("userId", user.uuid)
                .first()
                .asCallback((err, resp) => {
                    if (resp && resp.userId) {
                        callback(null, resp.userId, resp.isSeller)
                    } else {
                        next(err, resp)
                    }
                })
        },
        (userId, isSeller, callback) => {
            knex
                .select("p.*", "c.name as category_name", "c.img_path as category_img_path")
                .from("products as p")
                .rightJoin("categories as c", "p.productCategoryID", "c.categoryId")
                .where("p.userId", userId)
                .asCallback((err, products) => {
                    if (err) {
                        next(err, null)
                    } else {
                        callback(null, userId, products)
                    }
                })
        },
        (userId, products, callback) => {
            (async () => {
                let grouped = _.groupBy(products, 'category_name')
                callback(null, await grouped)
            })()
        }
    ], next)
};

exports.getShopCategoriesByOtherSeller = (uuid, next) => {
    async.waterfall([
        (callback) => {
            knex
                .select("userId")
                .from("users")
                .where("uuid", uuid)
                .orWhere("userId", uuid)
                .first()
                .asCallback((err, resp) => {
                    if (resp && resp.userId) {
                        callback(null, resp.userId, resp.isSeller)
                    } else {
                        next(err, resp)
                    }
                })
        },
        (userId, isSeller, callback) => {
            knex
                .select("p.*", "c.name as category_name", "c.img_path as category_img_path")
                .from("products as p")
                .rightJoin("categories as c", "p.productCategoryID", "c.categoryId")
                .where("p.userId", userId)
                .asCallback((err, products) => {
                    if (err) {
                        next(err, null)
                    } else {
                        callback(null, userId, products)
                    }
                })
        },
        (userId, products, callback) => {
            (async () => {
                let grouped = _.groupBy(products, 'category_name')
                callback(null, await grouped)
            })()
        }
    ], next)
};