'use strict';

let mysql = require('mysql');
let Database = require('../../../app/utils/database').Database;
let db = new Database();
let func = require('../../../app/utils/functions');

let env = process.env.NODE_ENV || 'development';
let config = require('../../../config/environment/' + env);

const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
let async = require('async');
let fs = require('fs');
let _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});

exports.orderShippingInformation = (uuid, data, next) => {};

exports.isOrderArrangeShipment = (orderId, data, next) => {
    // let strSQL = mysql.format('SELECT * FROM order_tracking WHERE orderId=? AND slug=? LIMIT 1;', [orderId, data.slug]);
    // db.query(strSQL, next);

    knex.select("*")
        .from("order_tracking")
        .where("orderId", orderId)
        .andWhere("slug", data.slug)
        .limit(1)
        .asCallback(next)
};

exports.arrangeShipment = (orderId, data, next) => {
    // let sqlObj = {
    //     orderId: orderId,
    //     shippingCourierId: data.shippingCourierId,
    //     slug: data.slug,
    //     customer_name: data.customer_name,
    //     pickupNote: data.pickupNote,
    //     shipment_package_count: data.shipment_package_count,
    //     shipment_pickup_date: data.shipment_pickup_date,
    //     shipment_weight: data.shipment_weight,
    //     shipment_weight_unit: data.shipment_weight_unit,
    //     pickUpUserAddressId: data.pickUpUserAddressId,
    //     pickUpLocation: data.pickUpLocation,
    //     pickUpCity: data.pickUpCity,
    //     pickUpState: data.pickUpState,
    //     pickUpZip: data.pickUpZip,
    //     pickUpCountry: data.pickUpCountry
    // };
    // let strSQL = mysql.format('INSERT INTO order_tracking SET ? ', sqlObj);
    // console.log('strSQL: ', strSQL);
    // db.insertWithId(strSQL, (err, order_trackingId) => {
    //     if (err) {
    //         return next(err, null);
    //     } else {
    //         console.log('arrangeShipment: ', order_trackingId);
    //         let sqlObj1 = {
    //             pk_order_trackingId: order_trackingId,
    //             slug: data.slug,
    //             tag: 'Pending',
    //             subtag: 'Pending_001',
    //             message: 'Order has been confirmed and is pending pickup.',
    //             checkpoint_time: data.checkpoint.checkpoint_time,
    //             coordinates_lng: data.checkpoint.coordinates_lng,
    //             coordinates_lat: data.checkpoint.coordinates_lat,
    //             location: data.checkpoint.location,
    //             city: data.checkpoint.city,
    //             state: data.checkpoint.state,
    //             country_name: data.checkpoint.country_name,
    //             country_iso3: data.checkpoint.country_iso3,
    //             zip: data.checkpoint.zip,
    //             created_at: new Date()
    //         };
    //         let strSQL1 = mysql.format('INSERT INTO order_tracking_checkpoints SET ? ', sqlObj1);
    //         console.log('strSQL1: ', strSQL1);
    //         db.insertWithId(strSQL1, (err, response) => {
    //             if (err) {
    //                 return next(err, null);
    //             }
    //             console.log('order_tracking_: ', response);
    //         });
    //         next(null, order_trackingId);
    //     }
    // });


    let saveToOrderTrackingCheckpoints = (order_trackingId, _data, trx) => {
        knex("order_tracking_checkpoints")
            .transacting(trx)
            .insert(_data)
            .then((resp) => {
                return order_trackingId
            })
            .then(trx.commit)
            .catch(trx.rollback)
    };

    let sqlObj = {
        orderId: orderId,
        shippingCourierId: data.shippingCourierId,
        slug: data.slug,
        customer_name: data.customer_name,
        pickupNote: data.pickupNote,
        shipment_package_count: data.shipment_package_count,
        shipment_pickup_date: data.shipment_pickup_date,
        shipment_weight: data.shipment_weight,
        shipment_weight_unit: data.shipment_weight_unit,
        pickUpUserAddressId: data.pickUpUserAddressId,
        pickUpLocation: data.pickUpLocation,
        pickUpCity: data.pickUpCity,
        pickUpState: data.pickUpState,
        pickUpZip: data.pickUpZip,
        pickUpCountry: data.pickUpCountry
    };

    knex.transaction((trx) => {
            knex("order_tracking")
                .transacting(trx)
                .insert(sqlObj)
                .then((resp) => {
                    let order_trackingId = resp && resp[0] ? resp[0] : resp
                    try {
                        console.log(`order_trackingId : ${order_trackingId}`)
                        let sqlObj1 = {
                            pk_order_trackingId: order_trackingId,
                            slug: data.slug,
                            tag: 'Pending',
                            subtag: 'Pending_001',
                            message: 'Order has been confirmed and is pending pickup.',
                            checkpoint_time: data.checkpoint.checkpoint_time,
                            coordinates_lng: data.checkpoint.coordinates_lng,
                            coordinates_lat: data.checkpoint.coordinates_lat,
                            location: data.checkpoint.location,
                            city: data.checkpoint.city,
                            state: data.checkpoint.state,
                            country_name: data.checkpoint.country_name,
                            country_iso3: data.checkpoint.country_iso3,
                            zip: data.checkpoint.zip,
                            created_at: new Date()
                        };
                        return saveToOrderTrackingCheckpoints(order_trackingId, sqlObj1, trx)
                    } catch (e) {
                        console.log('err here :', e)
                        knex("order_tracking")
                            .where("order_trackingId", order_trackingId)
                            .delete()
                            .asCallback((e, r) => {
                                console.log('e ', e)
                                console.log('r ', r)
                            })
                        let err_obj = {
                            msg: (e).toString(),
                            err_msg: (e).toString()
                        }
                        next(err_obj, null)
                    }
                })
        })
        .then((order_trackingId) => {
            return next(null, order_trackingId)
        })
        .catch((err) => {
            return next(err, null);
        })
};

exports.getOrderTrackings = (orderId, slug, next) => {
    // let strSQL = mysql.format('SELECT ot.* FROM order_tracking ot WHERE orderId=? AND slug=? LIMIT 1;', [orderId, slug]);
    // console.log('strSQL: ', strSQL);
    // db.query(strSQL, (err, order_tracking) => {
    //     if (err) {
    //         return next(err, null);
    //     } else {
    //         if (order_tracking && order_tracking.length > 0) {
    //             order_tracking = order_tracking[0];
    //         }

    //         let strSQL2 = mysql.format('SELECT * FROM order_tracking_checkpoints WHERE pk_order_trackingId =?;', [order_tracking.order_trackingId]);
    //         console.log('strSQL2: ', strSQL2);
    //         db.query(strSQL2, (err, checkpoints) => {
    //             if (err) {
    //                 return next(err, null);
    //             } else {
    //                 _.each(checkpoints, (row) => {
    //                     delete row.pk_order_trackingId;
    //                 });
    //                 order_tracking.checkpoints = checkpoints;
    //                 next(null, order_tracking);
    //             }
    //         });
    //     }
    // });

    knex.select("ot.*")
        .from("order_tracking as ot")
        .where("orderId", orderId)
        .andWhere("slug", slug)
        .limit(1)
        .asCallback((err, order_tracking) => {
            if (err) {
                return next(err, null);
            } else {
                if (order_tracking && order_tracking.length > 0) {
                    order_tracking = order_tracking[0];
                }

                knex.select("*")
                    .from("order_tracking_checkpoints")
                    .where("pk_order_trackingId", order_tracking.order_trackingId)
                    .asCallback((err, checkpoints) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            _.each(checkpoints, (row) => {
                                delete row.pk_order_trackingId;
                            });
                            order_tracking.checkpoints = checkpoints;
                            next(null, order_tracking);
                        }
                    })
            }
        })
};

exports.getOrderTrackingLastCheckPoint = (orderId, slug, next) => {
    // let strSQL = mysql.format('SELECT order_trackingId as id,tracking_number,slug FROM order_tracking ot WHERE orderId=? AND slug=? LIMIT 1;', [orderId, slug]);
    // console.log('strSQL: ', strSQL);
    // db.query(strSQL, (err, order_tracking) => {
    //     if (err) {
    //         return next(err, null);
    //     } else {
    //         if (order_tracking && order_tracking.length > 0) {
    //             order_tracking = order_tracking[0];
    //         }
    //         let strSQL2 = mysql.format('SELECT * FROM order_tracking_checkpoints WHERE pk_order_trackingId =? ORDER BY checkpoint_time DESC LIMIT 1;', [order_tracking.id]);
    //         console.log('strSQL2: ', strSQL2);
    //         db.query(strSQL2, (err, checkpoints) => {
    //             if (err) {
    //                 return next(err, null);
    //             } else {
    //                 _.each(checkpoints, (row) => {
    //                     delete row.pk_order_trackingId;
    //                 });
    //                 if (checkpoints && checkpoints.length > 0) {
    //                     checkpoints = checkpoints[0];
    //                 }
    //                 order_tracking.checkpoint = checkpoints;
    //                 next(null, order_tracking);
    //             }
    //         });
    //     }
    // });

    knex.select("order_trackingId as id", "tracking_number", "slug")
        .from("order_tracking as ot")
        .where("orderId", orderId)
        .andWhere("slug", slug)
        .asCallback((err, order_tracking) => {
            if (err) {
                return next(err, null);
            } else {
                if (order_tracking && order_tracking.length > 0) {
                    order_tracking = order_tracking[0];
                }
                knex.select("*")
                    .from("order_tracking_checkpoints")
                    .where("pk_order_trackingId", order_tracking.id)
                    .orderBy("checkpoint_time", "desc")
                    .limit(1)
                    .asCallback((err, checkpoints) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            _.each(checkpoints, (row) => {
                                delete row.pk_order_trackingId;
                            });
                            if (checkpoints && checkpoints.length > 0) {
                                checkpoints = checkpoints[0];
                            }

                            order_tracking.checkpoint = checkpoints;
                            next(null, order_tracking);
                        }
                    })
            }
        })
};

exports.updateStocks = (orderId, next) => {
    knex.select("*").from("order_details").where("orderId", orderId)
        .asCallback((e, r)=>{
            _.forEach(r, (value) => {
                if(value.variantId){
                    console.log('has variant');
                    async.waterfall([
                        (_cb) => {
                            knex.raw(`SELECT 
                                            IF(stock, stock - ?, 0) as newStock 
                                                FROM product_variation 
                                                    WHERE productId = ? 
                                                    and productVariationId = ? limit 1`, [value.detailQty, value.productId, value.variantId])
                                .asCallback((e,r)=>{
                                    if(e) {
                                        _cb(null, false)
                                    } else {
                                        _cb(null, r && r[0] && r[0][0] ? r[0][0].newStock : r.newStock)
                                    }
                                })
                        },
                        (newStock, _cb) => {
                            if(newStock) {
                                knex("product_variation")
                                    .where("productId", value.productId)
                                    .andWhere("productVariationId", value.variantId)
                                    .update({
                                        stock: newStock
                                    })
                                    .asCallback((ee, rr)=>{
                                        if(ee){
                                            console.log('ee ', ee)
                                        } else {
                                            console.log('rr ', rr)
                                        }
                                    })
                            } else {
                                console.log('it returned an empty stock value')
                            }
                        }
                    ])
                } else {
                    console.log('it has no variant')
                    async.waterfall([
                        (_cb) => {
                            knex.raw(`SELECT IF(productStock, productStock - ?, 0) as newStock FROM products WHERE productId = ? limit 1`, [value.detailQty, value.productId])
                                .asCallback((e,r)=>{
                                    console.log({e,r})
                                    if(e) {
                                        _cb(null, false)
                                    } else {
                                        _cb(null, r && r[0] && r[0][0] ? r[0][0].newStock : r.newStock)
                                    }
                                })
                        },
                        (newStock, _cb) => {
                            if(newStock) {
                                knex("products")
                                    .where("productId", value.productId)
                                    .update({
                                        productStock: newStock
                                    })
                                    .asCallback((ee, rr)=>{
                                        console.log({ee, rr})
                                        if(ee){
                                            console.log('ee ', ee)
                                        } else {
                                            console.log('rr ', rr)
                                        }
                                    })
                            } else {
                                console.log('it returned an empty stock value')
                            }
                        }
                    ])
                }
            });
            //  this is subject for improvement.
            return next(null, true)
        })
};