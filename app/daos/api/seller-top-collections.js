'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();
const async = require('async');
const _ = require('lodash');

const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.createTopPicks = (sellerId, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: sellerId
                }).first().asCallback((error, result) => {
                    if (error) {
                        return next(error, null);
                    }
                    console.log('user result: ', result);
                    callback(null, result);
                });
        },
        (user, callback) => {
            knex.transaction((trx) => {
                return trx
                    .insert({
                        collectionName: data.collectionName,
                        sellerId: user.userId,
                        isActive: 0,
                        uuid: knex.raw("UUID()")
                    }, "collectionId").into("seller_top_picks").asCallback(callback);
            });
        }
    ], next);
};

exports.getTopPicks = (sellerId, params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            const strSQL = knex("seller_top_picks").count("* as numRows").where("sellerId", sellerId);
            if (params.search) {
                strSQL.andWhere("collectionName", "LIKE", '%"' + params.search + '"%');
            }
            strSQL.then(results => {
                    numRows = results && results[0] && results[0].numRows ? results[0].numRows : 0;
                    numPages = Math.ceil(numRows / numPerPage);
                    return callback(null, numRows, numPages);
                })
                .catch(err => {
                    return next(err, null);
                });
        },
        (numRows, numPages, callback) => {
            const strSQL = knex("seller_top_picks");
            if (params.search) {
                strSQL.andWhere("collectionName", "LIKE", '%"' + params.search + '"%');
                if (params.limit && params.startWith) {
                    strSQL.limit(limit);
                } else if (params.limit) {
                    strSQL.limit(numPerPage);
                } else {
                    strSQL.limit(numPerPage);
                }
            } else {
                if (params.limit && params.startWith) {
                    strSQL.limit(limit);
                } else if (params.limit) {
                    strSQL.limit(numPerPage);
                } else {
                    strSQL.limit(numPerPage);
                }
            }
            strSQL.then(results => {
                async.eachSeries(results, (item, cb) => {
                    knex.select('p.*').from("products AS p").innerJoin("seller_top_picks_products AS stp", "p.productId", "stp.productId")
                        .where("stp.collectionId", item.collectionId).asCallback((err, products) => {
                            if (err) {
                                return next(err, null);
                            }
                            async.eachSeries(products, (product, callbackEach) => {
                                async.waterfall([
                                    (callback) => {
                                        knex("categories").where("categoryId", product.productCategoryID).first().asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            }
                                            if (response) {
                                                product.category = response;
                                            } else {
                                                product.category = {};
                                            }
                                            return callback();
                                        });
                                    },
                                    (callback) => {
                                        knex("subcategory").where("subcategoryId", product.productSubCategoryId).asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            }
                                            if (response && response.length > 0) {
                                                product.subcategory = response[0];
                                            } else {
                                                product.subcategory = {};
                                            }
                                            return callback();
                                        });
                                    },
                                    (callback) => {
                                        knex("brand").where("brandId", product.productBrandId).first().asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            }
                                            if (response) {
                                                product.brand = response;
                                            } else {
                                                product.brand = {};
                                            }
                                            return callback();
                                        })
                                    },
                                    (callback) => {
                                        knex("product_variation").where("productId", product.productId).asCallback((err, variantsArr) => {
                                            if (err) {
                                                return next(err, null);
                                            }
                                            if (!_.isEmpty(variantsArr)) {
                                                async.eachSeries(variantsArr, (variant, cbVariants) => {
                                                    knex.select("*").from("discount_promotion_products")
                                                        .where("productVariationId", variant.productVariationId).first().asCallback((err, dpProduct) => {
                                                            if (err) {
                                                                return next(err);
                                                            }
                                                            if (!_.isEmpty(dpProduct)) {
                                                                Object.assign(variant, dpProduct);
                                                            }
                                                            cbVariants();
                                                        });
                                                }, () => {
                                                    product.variants = variantsArr;
                                                });
                                            } else {
                                                product.variants = [];
                                            }
                                            return callback();
                                        });
                                    },
                                    (callback) => {
                                        knex("product_wholesale").where("productId", product.productId).asCallback((err, response) => {
                                            if (err) {
                                                return next(err, null);
                                            }
                                            product.wholesale = response;
                                            return callback();
                                        });
                                    },
                                    (callback) => {
                                        knex("product_images").where("productId", product.productId)
                                            .asCallback((err, response) => {
                                                if (err) {
                                                    return next(err, null);
                                                }
                                                if (response && response.length > 0) {
                                                    product.images = response;
                                                    product.image = response[0];
                                                } else {
                                                    product.images = [];
                                                    product.image = {};
                                                }
                                                return callback();
                                            });
                                    }
                                ], () => {
                                    item.products = products;
                                    return callbackEach();
                                });
                            }, cb);
                        });
                }, () => {
                    return callback(null, numRows, numPages, results);
                });
            }).catch(err => {
                return next(err, null);
            });
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.getTopPick = (sellerId, collectionId, next) => {
    knex.select("*")
        .from("seller_top_picks")
        .where(sql => {
            sql.where("collectionId", collectionId).andWhere("sellerId", knex.select("userId").from("users").where("uuid", sellerId).first())
        })
        .first()
        .asCallback(next)
};

exports.updateTopPick = (id, sellerId, data, next) => {
    knex.transaction((trx) => {
        knex("seller_top_picks")
            .update({
                collectionName: data.collectionName,
            })
            .where("collectionId", id)
            .andWhere("sellerId", sellerId)
            .asCallback(next);
    });
};

exports.deleteTopPick = (sellerId, id, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: sellerId
                }).first().asCallback((error, result) => {
                    if (error) {
                        return next(error, null);
                    }
                    console.log('user result: ', result);
                    callback(null, result);
                });
        },
        (user, callback) => {
            knex.transaction((trx) => {
                return trx.from("seller_top_picks")
                    .where("collectionId", id)
                    .andWhere("sellerId", user.userId)
                    .delete().asCallback(callback);
            });
        }
    ], next);
};

exports.activateTopPickCollection = (sellerId, collectionId, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where(sql => {
                    sql.where("uuid", sellerId).orWhere("userId", sellerId)
                }).first().asCallback((error, result) => {
                    if (error) {
                        return next(error, null);
                    }
                    callback(null, result);
                });
        },
        (user, callback) => {
            knex.update({
                    isActive: 1
                }).from("seller_top_picks")
                .where("collectionId", collectionId).andWhere("sellerId", user.userId)
                .then(() => {
                    knex.update({
                            isActive: 0
                        }).from("seller_top_picks")
                        .where("collectionId", "<>", collectionId)
                        .andWhere("sellerId", user.userId).asCallback(callback);
                });
        }
    ], next);
};

exports.deActivateTopPickCollection = (sellerId, collectionId, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where(sql => {
                    sql.where("uuid", sellerId).orWhere("userId", sellerId)
                }).first().asCallback((error, result) => {
                    if (error) {
                        return next(error, null);
                    }
                    callback(null, result);
                });
        },
        (user, callback) => {
            knex.update({
                    isActive: 0
                }).from("seller_top_picks")
                .where("collectionId", collectionId).andWhere("sellerId", user.userId)
                .asCallback(callback);
        }
    ], next);
};

exports.getActiveTopPickCollection = (sellerId, next) => {
    console.log("getActiveTopPickCollection 3: ", sellerId);
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where(sql => {
                    sql.where("uuid", sellerId).orWhere("userId", sellerId)
                }).first().asCallback((error, result) => {
                    if (error) {
                        return next(error, null);
                    }
                    callback(null, result);
                });
        },
        (user, callback) => {
            knex.select("*").from("seller_top_picks")
                .where(sql => {
                    sql.where("sellerId", user.userId).andWhere("isActive", 1)
                }).first().asCallback((err, collection) => {
                    if (err) {
                        return next(err, null);
                    }

                    if (collection) {
                        knex.select('p.*').from("products AS p").innerJoin("seller_top_picks_products AS stp", "p.productId", "stp.productId")
                            .where("stp.collectionId", collection.collectionId).asCallback((err, products) => {
                                if (err) {
                                    return next(err, null);
                                }
                                console.log("products: ", products);
                                if (products) {
                                    async.eachSeries(products, (product, callbackEach) => {
                                        async.waterfall([
                                            (callback1) => {
                                                knex("categories").where("categoryId", product.productCategoryID).first().asCallback((err, response) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    }
                                                    if (response) {
                                                        product.category = response;
                                                    } else {
                                                        product.category = {};
                                                    }
                                                    return callback1();
                                                });
                                            },
                                            (callback1) => {
                                                knex("subcategory").where("subcategoryId", product.productSubCategoryId).asCallback((err, response) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    }
                                                    if (response && response.length > 0) {
                                                        product.subcategory = response[0];
                                                    } else {
                                                        product.subcategory = {};
                                                    }
                                                    return callback1();
                                                });
                                            },
                                            (callback1) => {
                                                knex("brand").where("brandId", product.productBrandId).first().asCallback((err, response) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    }
                                                    if (response) {
                                                        product.brand = response;
                                                    } else {
                                                        product.brand = {};
                                                    }
                                                    return callback1();
                                                })
                                            },
                                            (callback1) => {
                                                knex("product_variation").where("productId", product.productId).asCallback((err, variantsArr) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    }
                                                    if (!_.isEmpty(variantsArr)) {
                                                        async.eachSeries(variantsArr, (variant, cbVariants) => {
                                                            knex.select("*").from("discount_promotion_products")
                                                                .where("productVariationId", variant.productVariationId).first().asCallback((err, dpProduct) => {
                                                                    if (err) {
                                                                        return next(err);
                                                                    }
                                                                    if (!_.isEmpty(dpProduct)) {
                                                                        Object.assign(variant, dpProduct);
                                                                    }
                                                                    cbVariants();
                                                                });
                                                        }, () => {
                                                            product.variants = variantsArr;
                                                        });
                                                    } else {
                                                        product.variants = [];
                                                    }
                                                    return callback1();
                                                });
                                            },
                                            (callback1) => {
                                                knex("product_wholesale").where("productId", product.productId).asCallback((err, response) => {
                                                    if (err) {
                                                        return next(err, null);
                                                    }
                                                    product.wholesale = response;
                                                    return callback1();
                                                });
                                            },
                                            (callback1) => {
                                                knex("product_images").where("productId", product.productId)
                                                    .asCallback((err, response) => {
                                                        if (err) {
                                                            return next(err, null);
                                                        }
                                                        if (response && response.length > 0) {
                                                            product.images = response;
                                                            product.image = response[0];
                                                        } else {
                                                            product.images = [];
                                                            product.image = {};
                                                        }
                                                        return callback1();
                                                    });
                                            }
                                        ], () => {
                                            collection.products = products;
                                            return callbackEach();
                                        });
                                    }, () => {
                                        return callback(null, collection);
                                    });
                                } else {
                                    collection.products = [];
                                    return callback(null, collection);
                                }
                            });
                    } else {
                        return callback(null, {});
                    }
                });
        }
    ], next);
};

// ======================== SELLER TOP PICKS PRODUCTS ============================ //
// ======================== SELLER TOP PICKS PRODUCTS ============================ //
// ======================== SELLER TOP PICKS PRODUCTS ============================ //

exports.createTopPickProduct = (sellerId, collectionId, data, next) => {
    knex.transaction((trx) => {
        return trx
            .insert({
                collectionId: collectionId,
                productId: data.productId,
                uuid: knex.raw("UUID()")
            }, "topPickProductId")
            .into("seller_top_picks_products")
            .asCallback(next);
    });
};

exports.getTopPickProducts = (sellerId, collectionId, next) => {
    async.waterfall([
        (callback) => {
            knex.select('userId', 'username', 'firstname', 'lastname', 'email', 'phone', 'fax', 'sellerDescription', 'sellerName', 'sellerIsVacationMode', 'sellerApprovalDate', 'sellerApplicationDate', 'uuid')
                .from('users')
                .where({
                    uuid: sellerId
                }).first().asCallback((error, result) => {
                    if (error) {
                        return next(error, null);
                    }
                    console.log('user result: ', result);
                    callback(null, result);
                });
        },
        (user, callback) => {
            knex.select("stp.topPickProductId", "p.*", "stp.uuid AS stp_product_uuid")
                .from("seller_top_picks_products AS stp")
                .innerJoin("seller_top_picks AS sp", "sp.collectionId", "stp.collectionId")
                .innerJoin("products AS p", "p.productId", "stp.productId")
                .where("stp.collectionId", collectionId).andWhere("sp.sellerId", user.userId)
                .asCallback((err, products) => {
                    if (err) {
                        return next(err);

                    }
                    async.eachSeries(products, (item, cb) => {
                        async.parallel([
                            (callback1) => {
                                knex("categories").where("categoryId", item.productCategoryID).first().asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response) {
                                        item.category = response;
                                    } else {
                                        item.category = {};
                                    }
                                    return callback1();
                                });
                            },
                            (callback1) => {
                                knex("subcategory").where("subcategoryId", item.productSubCategoryId).asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response && response.length > 0) {
                                        item.subcategory = response[0];
                                    } else {
                                        item.subcategory = {};
                                    }
                                    return callback1();
                                });
                            },
                            (callback1) => {
                                knex("brand").where("brandId", item.productBrandId).first().asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (response) {
                                        item.brand = response;
                                    } else {
                                        item.brand = {};
                                    }
                                    return callback1();
                                })
                            },
                            (callback1) => {
                                knex("product_variation").where("productId", item.productId).asCallback((err, variantsArr) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    if (!_.isEmpty(variantsArr)) {
                                        async.eachSeries(variantsArr, (variant, cbVariants) => {
                                            knex.select("*").from("discount_promotion_products")
                                                .where("productVariationId", variant.productVariationId).first().asCallback((err, dpProduct) => {
                                                    if (err) {
                                                        return next(err);
                                                    }
                                                    if (!_.isEmpty(dpProduct)) {
                                                        Object.assign(variant, dpProduct);
                                                    }
                                                    cbVariants();
                                                });
                                        }, () => {
                                            item.variants = variantsArr;
                                        });
                                    } else {
                                        item.variants = [];
                                    }
                                    return callback1();
                                });
                            },
                            (callback1) => {
                                knex("product_wholesale").where("productId", item.productId).asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    }
                                    item.wholesale = response;
                                    return callback1();
                                });
                            },
                            (callback1) => {
                                knex("product_images").where("productId", item.productId)
                                    .asCallback((err, response) => {
                                        if (err) {
                                            return next(err, null);
                                        }
                                        if (response && response.length > 0) {
                                            item.images = response;
                                            item.image = response[0];
                                        } else {
                                            item.images = [];
                                            item.image = {};
                                        }
                                        return callback1();
                                    });
                            }
                        ], cb);
                    }, () => {
                        return callback(null, products);
                    });
                });
        }
    ], next);
};

exports.getTopPickProduct = (sellerId, collectionId, productId, next) => {
    knex.select("*")
        .from("seller_top_picks_products")
        .where("collectionId", collectionId)
        .andWhere("productId", productId)
        .first()
        .asCallback(next)
};

exports.deleteTopPickProduct = (sellerId, collectionId, productId, next) => {
    knex.transaction((trx) => {
            knex("seller_top_picks_products")
                .where("collectionId", collectionId)
                .andWhere("productId", productId)
                .delete()
                .then((resp) => {
                    return resp;
                })
                .then(trx.commit)
                .catch(trx.rollback)
        })
        .then((resp) => {
            next(null, resp)
        })
        .catch((err) => {
            next(err, err)
        })
    knex.transaction(trx => {
        return trx("discount_promotion_products")
            .where("dPromoteProductId", promoId)
            .andWhere("dPromoteId", id)
            .delete()
            .asCallback(next)
    })
    // knex.transaction((trx) => {
    //         knex("discount_promotion_products")
    //             .where("dPromoteProductId", promoId)
    //             .andWhere("dPromoteId", id)
    //             .delete()
    //             .then((resp) => {
    //                 return resp;
    //             })
    //             .then(trx.commit)
    //             .catch(trx.rollback)
    //     })
    //     .then((resp) => {
    //         next(null, resp)
    //     })
    //     .catch((err) => {
    //         next(err, err);
    //     })
};
