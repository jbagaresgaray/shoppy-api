'use strict';

const Database = require('../../../app/utils/database').Database;
const db = new Database();

const func = require('../../../app/utils/functions');
const _ = require("lodash");
const async = require('async');
const knex = require("knex")({
    client: "mysql",
    connection: db.configuration
});


exports.checkIfProductIsRated = (user, productId, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, response);
                }
            });
        },
        (_user, product, callback) => {
            knex("product_rating").where("userId", _user.userId).andWhere("productId", product.productId).first().asCallback(callback);
        }
    ], next);
};


exports.rateProduct = (user, productId, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, response);
                }
            });
        },
        (_user, product, callback) => {
            knex.transaction(trx => {
                return trx.insert({
                        rating: data.rating,
                        comment: data.comment,
                        userId: _user.userId,
                        productId: product.productId,
                        ratedOn: knex.raw("NOW()"),
                        uuid: knex.raw('UUID()')
                    }, 'productRatingId').into("product_rating")
                    .asCallback(callback);
            });
        }
    ], next);
};


exports.updateRating = (user, productId, data, next) => {
    async.waterfall([
        (callback) => {
            knex.select("userId").from("users").where("uuid", user.uuid).first()
                .asCallback((err, response) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        return callback(null, response);
                    }
                });
        },
        (_user, callback) => {
            knex.select("productId").from("products").where(sql => {
                sql.where("uuid", productId).orWhere("productId", productId);
            }).first().asCallback((err, response) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, _user, response);
                }
            });
        },
        (_user, product, callback) => {
            knex.transaction(trx => {
                return trx.update({
                        rating: data.rating,
                        comment: data.comment,
                        ratedOn: knex.raw("NOW()")
                    }).from("product_rating")
                    .where("userId", _user.userId).andWhere("productId", product.productId)
                    .asCallback(callback);
            });
        }
    ], next);
};


exports.getAllProductRatings = (productId, params, next) => {
    if (!_.isEmpty(params)) {
        if (params.limit <= 20) {
            params.limit = 20;
        }
    } else {
        params.limit = 20;
        params.startWith = 0;
    }

    let numRows;
    let queryPagination;
    let numPerPage = parseInt(params.limit, 10) || 20;
    let page = parseInt(params.startWith, 10) || 0;
    let numPages;
    let skip = page * numPerPage;
    let limit = skip + ',' + numPerPage;

    async.waterfall([
        (callback) => {
            knex.count("* AS numRows")
                .from("product_rating AS pr")
                .innerJoin("products AS p", "p.productId", "pr.productId")
                .where(sql => {
                    sql.where("p.uuid", productId).orWhere("p.productId", productId)
                })
                .asCallback((err, results) => {
                    if (err) {
                        return next(err, null);
                    } else {
                        numRows = (results && results[0] && results[0].numRows) ? results[0].numRows : 0;
                        numPages = Math.ceil(numRows / numPerPage);
                        return callback(null, numRows, numPages);
                    }
                });
        },
        (numRows, numPages, callback) => {
            let strSQL = knex.select("pr.productRatingId", "pr.rating", "pr.comment",
                    "pr.ratedOn", "pr.productId as pr_poductId", "pr.variantId",
                    "pr.orderId", "pr.userId as pr_userId",
                    "p.userId as p_userId", "p.productSKU", "p.productName", "p.productDesc",
                    "p.productPrice", "p.productWeight", "p.productCondition",
                    "p.productBrandId", "p.productStock", "p.productCategoryID", "p.productSubCategoryId",
                    "p.productLength", "p.productHeight", "p.productWidth", "p.userId as p_userId",
                    "p.isPreOrder", "p.isActive", "p.productId as p_productId")
                .from("product_rating AS pr")
                .innerJoin("products AS p", "p.productId", "pr.productId")
                .where(sql => {
                    sql.where("p.uuid", productId).orWhere("p.productId", productId)
                });

            if (params.limit && params.startWith) {
                strSQL.limit(limit).offset(params.startWith);
            } else if (params.limit) {
                strSQL.limit(numPerPage);
            } else {
                strSQL.limit(numPerPage);
            }

            strSQL.asCallback((err, products) => {
                if (err) {
                    next(err, null)
                }

                async.eachSeries(products, (item, cb) => {
                    async.parallel([
                        (callback1) => {
                            knex.select("username", "firstname", "lastname", "user_img_path", "uuid").from("users").where("userId", item.pr_userId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        item.buyer = response;
                                        return callback1();
                                    }
                                });
                        },
                        (callback1) => {
                            knex.select("username", "firstname", "lastname", "user_img_path", "uuid").from("users").where("userId", item.p_userId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        item.productSeller = response;
                                        return callback1();
                                    }
                                });
                        },
                        (callback1) => {
                            knex("product_variation").where("productVariationId", item.variantId).first()
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        item.variants = response;
                                        return callback1();
                                    }
                                });
                        },
                        (callback1) => {
                            knex("product_images").where("productId", item.p_productId)
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        if (response) {
                                            item.image = response[0];
                                            item.images = response;
                                            return callback1();
                                        } else {
                                            item.image = {};
                                            item.images = [];
                                            return callback1();
                                        }
                                    }
                                });
                        },
                        (callback1) => {
                            knex.select("*").from("product_rating_attachments").where("productRatingId", item.productRatingId)
                                .asCallback((err, response) => {
                                    if (err) {
                                        return next(err, null);
                                    } else {
                                        item.rating_attachments = response;
                                        return callback1();
                                    }
                                });
                        },
                    ], cb);
                }, () => {
                    return callback(null, numRows, numPages, products);
                });
            });
        },
        (numRows, numPages, products, callback) => {
            if (params.limit && params.startWith) {
                if (page < numPages) {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: (page + 1),
                            page_size: numPerPage,
                            maxResult: numPages
                        }
                    });
                } else {
                    callback(null, {
                        result: products,
                        pagination: {
                            total: numRows,
                            startAt: page,
                            page_size: numPerPage,
                            maxResult: numPages || 0
                        }
                    });
                }
            } else {
                callback(null, {
                    result: products,
                    pagination: {
                        total: numRows,
                        startAt: 0,
                        page_size: 0,
                        maxResult: 0
                    }
                });
            }
        }
    ], next);
};

exports.uploadRatingAttachments = (productRatingId, data, next) => {
    async.waterfall([
        (callback) => {
            knex
            .select("productRatingId")
            .from("product_rating")
            .where("uuid", productRatingId)
            .orWhere("productRatingId", productRatingId)
            .first()
            .asCallback((err, resp) => {
                if (err) {
                    return next(err, null);
                } else {
                    return callback(null, resp.productRatingId);
                }
            })
        },
        (ratingId, callback) => {
            knex.transaction(trx => {
                return trx("product_rating_attachments")
                    .insert({
                        img_name: data.img_name,
                        img_path: data.img_path,
                        img_type: data.img_type,
                        img_size: data.img_size,
                        productRatingId: ratingId,
                        uuid: knex.raw("UUID()")
                    }).asCallback((err, resp) => {
                        if (err) {
                            return next(err, null);
                        } else {
                            return callback(null, resp && resp[0] ? resp[0] : resp);
                        }
                    });
            });
        }
    ], next)
    
};