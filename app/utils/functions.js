'use strict';

const env = process.env.NODE_ENV || 'development';
let config = require('../../config/environment/' + env);

const fs = require('fs');
const ejs = require('ejs');
const OneSignal = require('onesignal-node');
const multer = require('multer');
const mkdirp = require('mkdirp');
const uuidv1 = require('uuid/v1');

let existsSync = (filePath) => {
    if (!filePath) {
        return false;
    }
    try {
        return fs.statSync(filePath).isFile();
    } catch (e) {
        return false;
    }
};

let generateString = (length) => {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

let generateNumberString = (length) => {
    let text = '';
    let possible = '0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

let generateOrderNumber = (length) => {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

let sendMail = (data, next) => {
    let request = require('request');
    let helper = require('sendgrid').mail;

    let objectData = {};

    if (data.attachment) {
        let file = request(data.attachment);
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: file
        };

    } else {
        objectData = {
            from: data.from,
            to: data.email,
            subject: data.subject,
            html: data.message,
            attachment: null
        };
    }

    let from_email = new helper.Email(objectData.from);
    let to_email = new helper.Email(objectData.to);
    let subject = objectData.subject;
    let content = new helper.Content('text/html', objectData.html);
    let mail = new helper.Mail(from_email, subject, to_email, content);

    if (objectData.attachment) {
        let attachment = new helper.Attachment();
        let sfile = fs.readFileSync(objectData.attachment);
        let base64File = new Buffer(sfile).toString('base64');
        attachment.setContent(base64File);
        attachment.setType('application/text');
        attachment.setFilename(objectData.filename);
        attachment.setDisposition('attachment');
        mail.addAttachment(attachment);
    }

    let sg = require('sendgrid')(config.sendgrid_key);
    let requests = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(requests, (error, response) => {
        if (error) {
            next(error, null);
        }
        if (response.statusCode === 200 || response.statusCode === 202) {
            next(null, {
                result: response.body,
                msg: 'Email Successfully Sent',
                success: true
            });
        } else {
            next({
                result: error,
                msg: error.message,
                success: false
            }, null);
        }
    });
};

let sendMailTemplate = (ejstemplate, data, from, email, subject) => {
    ejs.renderFile(ejstemplate, data, (err, html) => {
        if (err) {
            console.log(err); // Handle error
        }

        let mailData = {
            from: from,
            email: email,
            subject: subject,
            message: html,
            attachment: null
        };

        this.sendMail(mailData, (err, mailresponse) => {
            if (err) {
                console.log('err: ', err.body);
            }
            console.log('mailresponse: ', mailresponse);
        });
    });
};

let readJsonFileSync = (filepath, encoding) => {

    if (typeof(encoding) == 'undefined') {
        encoding = 'utf8';
    }
    let file = fs.readFileSync(filepath, encoding);
    return JSON.parse(file);
};

let sendNotifications = (title, content, dataParams, devices, next) => {
    var myClient = new OneSignal.Client({
        userAuthKey: config.ONESIGNAL_USER_KEY,
        app: {
            appAuthKey: config.ONESIGNAL_API_KEY,
            appId: config.ONESIGNAL_APP_ID
        }
    });

    var firstNotification = new OneSignal.Notification({
        contents: {
            en: content
        }
    });
    firstNotification.setTargetDevices(devices);
    firstNotification.setExcludedSegments(['Inactive Users']);
    firstNotification.setParameter('headings', {
        "en": title
    });
    firstNotification.setParameter('data', dataParams);
    myClient.sendNotification(firstNotification, (err, httpResponse, data) => {
        if (err) {
            console.log('Something went wrong...');
            next({
                msg: 'OneSignal error',
                result: err,
                success: false
            }, null);
        } else {
            console.log('sendNotification: ', data, httpResponse.statusCode);
            next(null, {
                msg: 'Notification successfully sent!',
                result: data,
                success: true
            });
        }
    });
};

let sendGLobalNotifications = (title, content, dataParams, next) => {
    var myClient = new OneSignal.Client({
        userAuthKey: config.ONESIGNAL_USER_KEY,
        app: {
            appAuthKey: config.ONESIGNAL_API_KEY,
            appId: config.ONESIGNAL_APP_ID
        }
    });

    var firstNotification = new OneSignal.Notification({
        contents: {
            en: content
        }
    });
    firstNotification.setIncludedSegments(['All']);
    firstNotification.setExcludedSegments(['Inactive Users']);
    firstNotification.setParameter('headings', {
        "en": title
    });
    firstNotification.setParameter('data', dataParams);
    myClient.sendNotification(firstNotification, (err, httpResponse, data) => {
        if (err) {
            console.log('Something went wrong...');
            next({
                msg: 'OneSignal error',
                result: err,
                success: false
            }, null);
        } else {
            console.log('sendNotification: ', data, httpResponse.statusCode);
            next(null, {
                msg: 'Notification successfully sent!',
                result: data,
                success: true
            });
        }
    });
};

let uploadFileLimits = ()=>{
    return {
        fileSize: 1024 * 1024 * 10 // 10MB
    };
}

let uploadDirectory = (directory, req, res) => {
    return multer.diskStorage({
        destination: (req, file, cb) => {
            mkdirp(directory, (err) => {
                cb(err, directory);
            });
        },
        filename: (req, file, cb) => {
            cb(null, uuidv1() + '_' + file.originalname);
        }
    });
};


exports.existsSync = existsSync;
exports.generateString = generateString;
exports.generateNumberString = generateNumberString;
exports.sendMail = sendMail;
exports.readJsonFileSync = readJsonFileSync;
exports.generateOrderNumber = generateOrderNumber;
exports.sendNotifications = sendNotifications;
exports.sendGLobalNotifications = sendGLobalNotifications;
exports.sendMailTemplate = sendMailTemplate;
exports.uploadFileLimits = uploadFileLimits;
exports.uploadDirectory = uploadDirectory;