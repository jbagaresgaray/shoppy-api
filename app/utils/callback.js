'use strict';

exports.setupResponseCallback = function(res) {

    return function(error, returnValue) {
        if (error) {
            return res.status(500).json(error);
        }

        res.status(200).json(returnValue);
    };
};
