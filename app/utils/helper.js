'use strict';

const CronJob = require('cron').CronJob;
const path = require('path');
const fs = require('fs-extra');



module.exports = function(database, server, config, log) {
    /* database.connect(function onConnect(err, isConnected) {
        if (!isConnected) {
            log.error('ENVIRONMENT: ' + config.env + ' Error Connecting To MYSQL database');
        } else {
            var port = process.env.PORT || config.port || 5000;
            server.listen(port, function connection(err) {
                if (err instanceof Error) {
                    log.error('ENVIRONMENT: ' + config.env + ' Unable to start Server', app.get('port'));
                } else {
                    log.info('ENVIRONMENT: ' + config.env + ' Server started at PORT: ' + config.port + ' Using API VERSION: ' + config.api_version);

                    var job = new CronJob({
                        cronTime: '00 30 11 * * 1-7',
                        onTick: function() {
                            console.log('Cron Job started');
                            fs.emptyDir('./public/uploads', err => {
                                if (err) return console.error(err)

                                console.log('public uploads deleted success!')
                            });
                        },
                        start: false,
                        timeZone: 'Asia/Manila'
                    });
                    job.start();
                    console.log('job1 status', job.running);
                }
            });
        }
    }); */

    var port = process.env.PORT || config.port || 5000;
    server.listen(port, function connection(err) {
        if (err instanceof Error) {
            log.error('ENVIRONMENT: ' + config.env + ' Unable to start Server', app.get('port'));
        } else {
            log.info('ENVIRONMENT: ' + config.env + ' Server started at PORT: ' + config.port + ' Using API VERSION: ' + config.api_version);


            var job = new CronJob({
                cronTime: '00 30 11 * * 1-7',
                onTick: function() {
                    console.log('Cron Job started');
                    fs.emptyDir('./public/uploads', err => {
                        if (err) return console.error(err)

                        console.log('public uploads deleted success!')
                    });
                },
                start: false,
                timeZone: 'Asia/Manila'
            });
            job.start();
            console.log('job1 status', job.running);
        }
    });
};