'use strict';

let env = process.env.NODE_ENV;
let config = require('../../config/environment/' + env);

let jwt = require('jsonwebtoken');
let moment = require('moment');

let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, Authorization, X-Requested-With, Content-Type, Accept, Cache-Control,AccessKey,AccessCode,Auth_Token');
    // next();
    if (req.method === 'OPTIONS') {
        res.statusCode = 204;
        return res.end();
    } else {
        return next();
    }
};

var authorizeMobileBasicAuth = function(req, res, next) {
    if (!req.header('authorization')) {
        return res.status(401).json({
            response: {
                msg: 'Access to this service or resource is forbidden with the given authorization. Authorization header is missing!',
                result: null,
                success: false
            },
            statusCode: 403
        });
    }

    var payload = null;
    try {
        var token = req.header('Authorization').split(' ')[1];
        payload = jwt.decode(token, config.token_secret_mobile);
        if (payload && payload.exp <= moment().unix()) {
            return res.status(401).send({
                msg: 'Token has expired',
                result: null,
                success: false
            });
        }
        delete payload.iat;
        delete payload.exp;
        delete payload.jit;
        delete payload.aud;
        req.user = {
            msg: 'Login successfully',
            success: true,
            result: {
                user: payload,
                token: token
            }
        };
        next();
    } catch (err) {
        console.log('authorizeMobileBasicAuth err: ', err);
        return res.status(403).send({
            msg: err.message,
            result: err,
            success: false
        });
    }
};

var authorizeAPIAuth = function(req, res, next) {
    if (!req.header('authorization')) {
        return res.status(401).json({
            response: {
                msg: 'Access to this service or resource is forbidden with the given authorization. Authorization header is missing!',
                result: null,
                success: false
            },
            statusCode: 403
        });
    }

    var payload = null;
    try {
        var token = req.header('Authorization').split(' ')[1];
        payload = jwt.decode(token, config.token_secret);
        if (payload && payload.exp <= moment().unix()) {
            return res.status(401).send({
                msg: 'Token has expired',
                result: null,
                success: false
            });
        }
        delete payload.iat;
        delete payload.exp;
        delete payload.jit;
        delete payload.aud;
        req.user = {
            msg: 'Login successfully',
            success: true,
            result: {
                user: payload,
                token: token
            }
        };
        next();
    } catch (err) {
        console.log('authorizeMobileBasicAuth err: ', err);
        return res.status(403).send({
            msg: err.message,
            result: err,
            success: false
        });
    }
};

var authorizeSellerApplicationAuth = function(req, res, next) {
    if (!req.header('auth_token')) {
        return res.status(401).json({
            response: {
                msg: 'Access to this service or resource is forbidden with the given authorization. Authorization header is missing!',
                result: null,
                success: false
            },
            statusCode: 403
        });
    }else{
        next();
    }
};


exports.allowCrossDomain = allowCrossDomain;
exports.authorizeMobileBasicAuth = authorizeMobileBasicAuth;
exports.authorizeAPIAuth = authorizeAPIAuth;
exports.authorizeSellerApplicationAuth = authorizeSellerApplicationAuth;