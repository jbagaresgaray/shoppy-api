'use strict';

exports.validateBuyerRating = (req, res, next) => {
    req.checkBody('rating', 'Please provide Rating').notEmpty();
    req.checkBody('orderId', 'Please provide Order Id').notEmpty();
    req.checkBody('seller_userId', 'Please provide Seller User Id').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateShopRating = (req, res, next) => {
    req.checkBody('rating', 'Please provide Rating').notEmpty();
    req.checkBody('orderId', 'Please provide Order Id').notEmpty();
    req.checkBody('userId', 'Please provide User Id').notEmpty();
    req.checkBody('productId', 'Please provide Product Id').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};