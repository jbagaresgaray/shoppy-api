'use strict';

exports.validateContents = (req, res, next) => {
    req.checkBody('title', 'Please provide Content title').notEmpty();
    req.checkBody('summary', 'Please provide Content summary').notEmpty();
    req.checkBody('content', 'Please provide Content details').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};


exports.validateNewsletter = (req, res, next) => {
    req.checkBody('title', 'Please provide content title').notEmpty();
    req.checkBody('summary', 'Please provide content summary').notEmpty();
    req.checkBody('content', 'Please provide content').notEmpty();
    req.checkBody('tags', 'Please provide content tags').notEmpty();
    req.checkBody('type', 'Please provide content type').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};