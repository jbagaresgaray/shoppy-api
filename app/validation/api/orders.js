'use strict';

exports.validateOrderParams = (req, res, next) => {
    req.checkQuery('action', 'Please provide action').notEmpty();
    req.checkQuery('type', 'Please provide type').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};