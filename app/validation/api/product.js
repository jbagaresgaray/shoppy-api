'use strict';

exports.validateProduct = (req, res, next) => {
    // req.checkBody('SKU', 'Please provide Product SKU or Barcode').notEmpty();
    req.checkBody('name', 'Please provide Product Name').notEmpty();
    req.checkBody('desc', 'Please provide Product Description').notEmpty();
    req.checkBody('price', 'Please provide Product Price').notEmpty();
    req.checkBody('brandId', 'Please provide Product Brand').notEmpty();
    req.checkBody('stock', 'Please provide Product Initial Stock').notEmpty();
    req.checkBody('categoryId', 'Please provide Product Category').notEmpty();
    req.checkBody('weight', 'Please provide Product Weight').notEmpty();

    if(req.body.isPreOrder)
{       req.checkBody('preOrderDays', 'Please provide Pre Order days').notEmpty();
        req.checkBody('preOrderDays', 'Pre Order days must be in a minimum of 7 days').isInt({ min: 7 });
    }

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateProductVariation = (req, res, next) => {
    req.checkBody('name', 'Please provide Product Name').notEmpty();
    req.checkBody('price', 'Please provide Product Price').notEmpty();
    req.checkBody('stock', 'Please provide Product Initial Stock').notEmpty();
    req.checkParams('uuid', 'Please provide Product Id or Product UUID').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};


exports.validateProductWholesale = (req, res, next) => {
    req.checkBody('price', 'Please provide Product Price').notEmpty();
    req.checkBody('minAmt', 'Please provide Minimum Quantity').notEmpty();
    req.checkBody('maxAmt', 'Please provide Maximum Quantity').notEmpty();
    req.checkParams('uuid', 'Please provide Product Id or Product UUID').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};