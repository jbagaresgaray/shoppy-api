'use strict';

exports.validateShare = (req, res, next) => {
    req.checkBody('from', 'Please provide Email From').notEmpty();
    req.checkBody('subject', 'Please provide Email Subject').notEmpty();
    req.checkBody('email', 'Please provide Email Address').notEmpty();
    req.checkBody('email', 'Email Address is not a valid email format').isEmail();
    req.checkBody('message', 'Please provide Email Content').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};


exports.validateSend = (req, res, next) => {
    req.checkBody('from', 'Please provide Email From').notEmpty();
    req.checkBody('subject', 'Please provide Email Subject').notEmpty();
    req.checkBody('message', 'Please provide Email Content').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};