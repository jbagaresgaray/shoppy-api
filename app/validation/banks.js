'use strict';

exports.validateBanks = (req, res, next) => {
    req.checkBody('name', 'Please provide Bank name').notEmpty();
    req.checkBody('address', 'Please provide Bank address').notEmpty();
    req.checkBody('city', 'Please provide your Bank city address').notEmpty();
    req.checkBody('suburb', 'Please provide your Bank suburb address').notEmpty();
    req.checkBody('state', 'Please provide your Bank state').notEmpty();
    req.checkBody('zipcode', 'Please provide your Bank zip code').notEmpty();
    req.checkBody('country', 'Please provide your Bank country location').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};