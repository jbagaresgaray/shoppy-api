'use strict';

exports.validatePayment = (req, res, next) => {
    req.checkBody('name', 'Please provide Payment Provider name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};