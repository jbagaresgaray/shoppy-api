'use strict';

exports.validateFormInptType = (req, res, next) => {
    req.checkBody('name', 'Please provide Form Input Type Name').notEmpty();
    req.checkBody('label', 'Please provide Form Input Type Label').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};