'use strict';

exports.validateCategory = (req, res, next) => {
    req.checkBody('name', 'Please provide Category name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};


exports.validateBrand = (req, res, next) => {
    req.checkBody('name', 'Please provide Brand name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};