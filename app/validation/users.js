'use strict';
let _ = require('lodash');

exports.validateUser = (req, res, next) => {
    req.checkBody('lastname', 'Please provide Lastname').notEmpty();
    req.checkBody('firstname', 'Please provide Firstname').notEmpty();
    req.checkBody('username', 'Please provide Username').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('email', 'Please provide Email Address').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();

    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateUserProfile = (req, res, next) => {
    req.checkBody('lastname', 'Please provide Lastname').notEmpty();
    req.checkBody('firstname', 'Please provide Firstname').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateUserAccount = (req, res, next) => {
    req.checkBody('username', 'Please provide Username').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateUserEmail = (req, res, next) => {
    req.checkBody('email', 'Please provide Email Address').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateBasicAuth = (req, res, next) => {
    var auth = req.headers.authorization;

    var uname = {
        param: 'username',
        msg: 'Please provide your Username'
    };

    var pword = {
        param: 'password',
        msg: 'Please provide your Password'
    };

    if (!auth) {
        res.status(401).send({
            result: [uname, pword],
            msg: '',
            success: false
        });
    } else if (auth) {
        var tmp = auth.split(' ');
        var buf = new Buffer(tmp[1], 'base64');
        var plain_auth = buf.toString();
        var creds = plain_auth.split(':');
        var username = creds[0];
        var password = creds[1];

        if (_.isEmpty(username) && _.isEmpty(password)) {
            res.status(401).send({
                result: [uname, pword],
                msg: '',
                success: false
            });
        } else if (_.isEmpty(username) && !_.isEmpty(password)) {
            res.status(401).send({
                result: [uname],
                msg: '',
                success: false
            });
        } else if (!_.isEmpty(username) && _.isEmpty(password)) {
            res.status(401).send({
                result: [pword],
                msg: '',
                success: false
            });
        } else if (username === 'undefined' && password === 'undefined') {
            res.status(401).send({
                result: [uname, pword],
                msg: '',
                success: false
            });
        } else if (!_.isEmpty(username) && password === 'undefined') {
            res.status(401).send({
                result: [pword],
                msg: '',
                success: false
            });
        } else if (username === 'undefined' && !_.isEmpty(password)) {
            res.status(401).send({
                result: [uname],
                msg: '',
                success: false
            });
        } else {
            next();
        }
    }
};

exports.validateForgot = (req, res, next) => {
    req.checkBody('email', 'Please provide your Account Username').notEmpty();
    req.checkBody('email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateReset = (req, res, next) => {
    req.checkBody('uuid', 'Please provide UUID').notEmpty();
    req.checkBody('code', 'Please provide Verification').notEmpty();
    req.checkBody('password', 'Please provide Password').notEmpty();
    req.checkBody('password', 'Password should be 8-20 characters long.').len(8, 20);
    req.checkBody('confirmpassword', 'Please provide Confirm Password').notEmpty();
    req.assert('confirmpassword', 'Password and Confirm Password do not match').equals(req.body.password);

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};