'use strict';

exports.validateAttribute = (req, res, next) => {
    req.checkBody('attribute_id', 'Please provide Attribute Id').notEmpty();
    req.checkBody('category_id', 'Please provide Category Id').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateAttributeCategory = (req, res, next) => {
    req.checkBody('attributes[*].*').trim().isLength({ min: 1 })

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};