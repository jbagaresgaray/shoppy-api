'use strict';

exports.validateDiscountPromotion = (req, res, next) => {
    req.checkBody('promo_name', 'Please provide Promotion Name').notEmpty();
    req.checkBody('event_from', 'Please provide Promotion event start date').notEmpty();
    req.checkBody('event_to', 'Please provide Promotion event end date').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateProductDiscountPromotion = (req, res, next) => {
    req.checkParams('uuid', 'Please provide Shop UUID').notEmpty();
    req.checkParams('id', 'Please provide Promotion Id').notEmpty();

    req.checkBody('productId', 'Please provide Product Id').notEmpty();
    req.checkBody('discount', 'Please provide Discount Value').notEmpty();
    req.checkBody('discount_price', 'Please provide Discount Price').notEmpty();
    req.checkBody('purchase_limit', 'Please provide Purchase Limit').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};

exports.validateProductVariantsDiscountPromotion = (req, res, next) => {
    req.checkParams('uuid', 'Please provide Shop UUID').notEmpty();
    req.checkParams('id', 'Please provide Promotion Id').notEmpty();

    req.checkBody('productId', 'Please provide Product Id').notEmpty();
    req.checkBody('productVariationId', 'Please provide Product Variant Id').notEmpty();
    req.checkBody('discount', 'Please provide Discount Value').notEmpty();
    req.checkBody('discount_price', 'Please provide Discount Price').notEmpty();
    req.checkBody('purchase_limit', 'Please provide Purchase Limit').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};