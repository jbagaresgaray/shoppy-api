'use strict';

exports.validateAttribute = (req, res, next) => {
    req.checkBody('name', 'Please provide Attribute name').notEmpty();
    req.checkBody('label', 'Please provide Attribute label').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};