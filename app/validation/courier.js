'use strict';

exports.validateCourier = (req, res, next) => {
    req.checkBody('name', 'Please provide Shipping Courier name').notEmpty();
    req.checkBody('description', 'Please provide Shipping Courier Description').notEmpty();
    req.checkBody('slug', 'Please provide Shipping Courier Slug').notEmpty();
    req.checkBody('phone', 'Please provide Shipping Courier phone number').notEmpty();


    var errors = req.validationErrors();
    if (errors) {
        res.status(400).send({
            result: errors,
            msg: '',
            success: false
        });
    } else {
        next();
    }
};